export const calcPopupWindowPosition = (width, height) => {
  let left = screen.width / 2 - width / 2
  const top = screen.height / 2 - height / 2
  left += window.screenX
  return { left, top }
}

export const getPopupWindowPropsString = (width = 1500, height = 800) => {
  const { left, top } = calcPopupWindowPosition(width, height)
  return `scrollbars=1,width=${width},height=${height},left=${left},top=${top}`
}

export const customerServiceWindow = (url, width = 700, height = 540) => {
  const windowPosition = getPopupWindowPropsString( width,  height)
  return window.open(url, "customerServiceWindow" , windowPosition)
}
