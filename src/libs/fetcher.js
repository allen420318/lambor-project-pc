import { store } from "@/store/index.js"

const generateConfig = (payload, httpVerb = "POST") => {
  let result = {}

  const defaultConfig = {
    method: httpVerb,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    signal: store.state.fetchController.signal
  }

  if (payload && httpVerb !== "GET") {
    result = Object.assign(defaultConfig, { body: JSON.stringify(payload) })
  } else {
    result = defaultConfig
  }

  return result
}

export const fetcher = async (payload, httpVerb = "POST") => {
  const endpoint = payload.endpoint
  const config = generateConfig(payload, httpVerb)

  const response = await fetch(endpoint, config)

  if (response.ok === false) {
    const httpStatus = parseInt(response.status)
    throw new Error(`${httpStatus} - http status code`)
  } else {
    return response.json()
  }
}
