export const isTriggerDebugLogger = () => {
  const debug = localStorage.getItem("#debug")
  return debug ? true : false
}

export const logger = (key, data) => {
  sessionStorage.setItem(key, JSON.stringify(data))
}

export const apiLogger = (apiData, isSuccess = true) => {
  const payloadkey = isSuccess
    ? `# ST_${apiData.st}_AT_${apiData.at} payload`
    : `! ST_${apiData.st}_AT_${apiData.at} payload ${new Date().getTime()}`

  const resultkey = isSuccess
    ? `# ST_${apiData.st}_AT_${apiData.at} result`
    : `! ST_${apiData.st}_AT_${apiData.at} result ${new Date().getTime()}`

  logger(resultkey, apiData.result)
  logger(payloadkey, apiData.payload)
}

export const signalRLogger = data => {
  const sn = new Date().getTime()
  const key = `@ ${sn} singalR`
  logger(key, data)
}
