export const encryptBankAccountNumber = (data) => {
  const length = data.length
  const star = "*"

  if (length >= 4 && length < 10) {
    switch (length) {
      case 4:
        return data.slice(0, 1) + star.repeat(2) + data.slice(-1)
      case 5:
        return data.slice(0, 1) + star.repeat(3) + data.slice(-1)
      case 6:
        return data.slice(0, 2) + star.repeat(2) + data.slice(-2)
      case 7:
        return data.slice(0, 2) + star.repeat(3) + data.slice(-2)
      case 8:
        return data.slice(0, 2) + star.repeat(4) + data.slice(-2)
      default:
        return data
    }
  } else {
    return data
  }
}

export const formatNumber = (num, { localeCode = "en-US", fractionDigits = 2, isFloor = true } = {}) => {
  if (isFloor === true) {
    const chopFractionDigits = fractionDigits + 1

    const value = new Intl.NumberFormat(localeCode, {
      minimumFractionDigits: chopFractionDigits,
      maximumFractionDigits: chopFractionDigits
    }).format(num)

    const diff = fractionDigits === 0 ? 2 : 1

    return value.substring(0, value.length - diff)
  } else {
    return new Intl.NumberFormat(localeCode, {
      minimumFractionDigits: fractionDigits,
      maximumFractionDigits: fractionDigits
    }).format(num)
  }
}

export const convertPercentToNumber = (value, precision = 4) => {
  return Number((value * 100).toPrecision(precision))
}

export const convertNumberToPercent = (value, precision = 4) => {
  return Number((value / 100).toPrecision(precision))
}
