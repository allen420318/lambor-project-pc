import HttpService from 'scripts/common/HttpService';
import BLDef from 'scripts/common/BLDef';

export default {
    // 盈利榜
    async HomePageLoadRankAndOnlineNumPage (DataObj) {
        const retData = await HttpService.PostDynamic(BLDef.NewIndexService.ServiceType, BLDef.NewIndexService.ActType.HomePage_LoadRankAndOnlineNum, DataObj);
        return retData;
    },

};
