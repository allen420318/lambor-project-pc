import HttpService from 'scripts/common/HttpService';
import BLDef from 'scripts/common/BLDef';

export default {

    // 獲取新聞清單
    async GetNewsList(DataObj) {
        const retData = await HttpService.PostDynamic(BLDef.InformationService.ServiceType, BLDef.InformationService.ActType.NewsCenter_Query, DataObj);    
        return retData;
    }, // end GetNewsList

    // 取得新聞詳細
    async GetNewsDetail(DataObj) {
        const retData = await HttpService.PostDynamic(BLDef.InformationService.ServiceType, BLDef.InformationService.ActType.NewsCenter_LoadDetailPage, DataObj);    
        return retData;
    }, // end GetNewsDetail

    // 取得優惠活動清單
    async GetActivityList(DataObj) {
        const retData = await HttpService.PostDynamic(BLDef.InformationService.ServiceType, BLDef.InformationService.ActType.BounsActivity_Query, DataObj);    
        return retData;
    }, // end GetActivityList

    // 取得優惠活動詳細
    async GetActivityDetail(DataObj) {
        const retData = await HttpService.PostDynamic(BLDef.InformationService.ServiceType, BLDef.InformationService.ActType.BounsActivity_LoadDetailPage, DataObj);    
        return retData;
    }, // end GetActivityDetail

    // 載入連繫我們畫面(已登入)
    async ContactUs_LoadMainPage_LoginUser(DataObj) {
        const retData = await HttpService.PostAes(BLDef.InformationService.ServiceType, BLDef.InformationService.ActType.ContactUs_LoadMainPage_LoginUser, DataObj);    
        return retData;
    }, // end ContactUs_LoadMainPage_LoginUser

    // 載入連繫我們畫面(未登入)
    async ContactUs_LoadMainPage_Guest(DataObj) {
        const retData = await HttpService.PostDynamic(BLDef.InformationService.ServiceType, BLDef.InformationService.ActType.ContactUs_LoadMainPage_Guest, DataObj);    
        return retData;
    }, // end ContactUs_LoadMainPage_Guest

    // 送出連繫我們訊息(已登入)
    async ContactUs_Add_LoginUser(DataObj) {
        const retData = await HttpService.PostAes(BLDef.InformationService.ServiceType, BLDef.InformationService.ActType.ContactUs_Add_LoginUser, DataObj);    
        return retData;
    }, // end ContactUs_Add_LoginUser

    // 送出連繫我們訊息(未登入)
    async ContactUs_Add_Guest(DataObj) {
        const retData = await HttpService.PostDynamic(BLDef.InformationService.ServiceType, BLDef.InformationService.ActType.ContactUs_Add_Guest, DataObj);    
        return retData;
    }, // end ContactUs_Add_Guest
};