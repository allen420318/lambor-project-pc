/* eslint-disable */
export default {
    //不同分辨率下的flash的适配
    FlashAdaptation(id){
        var VideoWidth = $(id).width(); //获取video的宽
        var VideoMarginLeft = 0; //初始化video的位移
        var ScreenWidthShow = document.body.clientWidth; //除去滚动条的屏宽
        //如果分辨率大于等于1320，获取到分辨率计算flash高度
        if (ScreenWidthShow>=1320) {
            VideoMarginLeft = -(VideoWidth - ScreenWidthShow) / 2;
            $(id).css({
                "marginLeft": VideoMarginLeft
            });
        } else{		//否则固定flash的宽度，根据比例计算高度
            VideoMarginLeft = -(VideoWidth - ScreenWidthShow) / 2;
            $(id).css({
                "marginLeft": VideoMarginLeft
            });
        }
    },
    //分辨率跟窗口改变的监听
    FlashInit(id) {
        const self = this;
        self.FlashAdaptation(id);
        window.addEventListener("onorientationchange" in window ? "orientationchange" : "resize", function () {
            setTimeout(self.FlashAdaptation(id), 50);
        })
    },

    //进场取舍播放
    FlashEnter(id1,id2){
        const self = this;
        $(id2).hide();
        setTimeout(function(){
            $(id1).hide();
            $(id2).show();
            self.FlashAdaptation(id2);
            self.FlashInit(id2);
        }, 1000);
    }
};