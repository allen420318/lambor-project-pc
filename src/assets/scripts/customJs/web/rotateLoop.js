/*eslint-disable*/

export default {
    rotateLoop: function () {
        var i = 0,
            timer = null;
        var that = $('.rotateLoop');
        timer = setInterval(function () {
            i++;
            that.css('transform', 'rotate(' + i + 'deg)');
            if ($('.rotateLoop').css('display') == "none") {
                clearInterval(timer);
                i = 0;
            }
        }, 0);
    }
};
