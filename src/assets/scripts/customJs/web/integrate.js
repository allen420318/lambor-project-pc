/* eslint-disable */
//ready

 export default {
   // 游戏列表与投注教程的切换
   ChangeListCourse: function (selector) {
       $(selector).click(function(){
         if ($(".introduce-nav").is(":hidden")) {
           $(".introduce-nav").show();
           $(".betting-game-block").hide();
           $(this).children("img").attr("src","/static/images/web/lotteryBetting/lottery_come.png?VersionCode");
         } else{
           $(".introduce-nav").hide();
           $(".betting-game-block").show();
           $(this).children("img").attr("src","/static/images/web/lotteryBetting/betting-btn.png?VersionCode");
         }
       });
   },
   // 电子游戏 游戏图标的切换
   /*ChangeListLogo: function () {
     $(".scrollList li").click(function(){
       var logoUrl = $(this).children().children("img").attr("src");
       $(".logo-show").children("img").attr("src",logoUrl)
     });
   },*/
   // 资金管理的厂商logo切换
   HoverImg: function (index) {
     $('.fund-transform-select div.div' + index).mouseenter(function () {
       if ($(this).hasClass('click-flase')) {
         return;
       } else {
         $(this).children('img').attr('src', '/static/images/web/frame/vendorLogo/bglight/v-logo' + index + '.png?VersionCode');
       }
     });
     $('.fund-transform-select div.div' + index).mouseleave(function () {
       // $(this).children('img').attr('src', '/static/images/web/frame/vendorLogo/bgdeep/v-logo' + index + '.png?VersionCode');
       if ($(this).hasClass('hasblock')) {
         // $(this).children('img').attr('src', '/static/images/web/frame/vendorLogo/bglight/v-logo' + index + '.png?VersionCode');
         return;
       } else {
          $(this).children('img').attr('src', '/static/images/web/frame/vendorLogo/bgdeep/v-logo' + index + '.png?VersionCode');
       }
     });
   }

}

jQuery(function () {
    //选项卡
    $.extend({
        selectCard: function () {
            var card = $(".search-select a");
            var num = 1;

            card.on('click', function () {
                num = $(this).attr("data-num");
                $(this).addClass("hasSelect").siblings("a").removeClass("hasSelect");
                $(".item" + num + "").show().siblings(".item").hide();
            });
        },
    });

    $.selectCard();
});

//ready======================================== end

//新开窗口并保持居中
function openWin(win_url, win_w, win_h) {
    var url = win_url;                             //转向页面的地址;
    var name = '';                            //页面名称，可为空;
    var iWidth = win_w;                          //弹出窗口的宽度;
    var iHeight = win_h;                         //弹出窗口的高度;
    //获得窗口的垂直位置
    var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
    //获得窗口的水平位置
    var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
    window.open(url, name, 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',status=no,toolbar=no,menubar=no,location=no,resizable=no,scrollbars=0,titlebar=no');
}
