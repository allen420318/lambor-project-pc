/* eslint-disable */
// 显示更多
export function showMoreNChildren($children, n) {
  //显示某jquery元素下的前n个隐藏的子元素
  var $hiddenChildren = $children.filter(":hidden");
  var cnt = $hiddenChildren.length;

  for (var i = 0; i < n && i < cnt ; i++) {
    $hiddenChildren.eq(i).show();
  }
  return cnt - n;//返回还剩余的隐藏子元素的数量
}

export function showMore(selector) {
  // 对页中现有的class=showMorehandle的元素，在之后添加显示更多条，并绑定点击行为
  var pagesize = $(selector).attr("showSize");
  var $children = $(selector).children();
  if ($children.length > pagesize) {
    for (var i = pagesize; i < $children.length; i++) {
      $children.eq(i).hide();
    }

    $(".load-more").click(function(){
      if (showMoreNChildren($children, pagesize) <= 0) {
        //如果目标元素已经没有隐藏的子元素了，就隐藏“显示更多”
        $(".load-more").hide();
      };
    })
  }else {
     $(".load-more").hide();
  }
}

export function showMore1(selector) {
  //对页中现有的class=showMorehandle的元素，在之后添加显示更多条，并绑定点击行为
  var pagesize = $(selector).attr("showSize");
  var $children = $(selector).children().not(".load-more");
  if ($children.length > pagesize) {
    for (var i = pagesize; i < $children.length; i++) {
      $children.eq(i).not(".load-more").hide();
    }
    // $(selector).append("<div class=\"video_list video_list_add\"><img src=\"/static/images/web/video/live-show-more.png\" /></div>");
    $(".load-more").click(function(){
      if (showMoreNChildren($children, pagesize) <= 0) {
        //如果目标元素已经没有隐藏的子元素了，就隐藏“显示更多”
        $(this).hide();
      };
    })
  }else {
    $(".load-more").hide();
  }
}

//ready
/*jQuery(function () {
  $.extend({
    showMoreNChildren: function ($children, n) {
      console.log("0002");
      //显示某jquery元素下的前n个隐藏的子元素
      var $hiddenChildren = $children.filter(":hidden");
      var cnt = $hiddenChildren.length;

      for (var i = 0; i < n && i < cnt ; i++) {
        $hiddenChildren.eq(i).show();
      }
      return cnt - n;//返回还剩余的隐藏子元素的数量
    },
    showMore1: function (selector) {
      if (selector == undefined) { selector = ".showMoreNChildren" }
      //对页中现有的class=showMorehandle的元素，在之后添加显示更多条，并绑定点击行为
      $(selector).each(function () {
        var pagesize = $(this).attr("pagesize") || 8;
        var $children = $(this).children();
        if ($children.length > pagesize) {
          for (var i = pagesize; i < $children.length; i++) {
            $children.eq(i).hide();
          }
          $(this).append("<div class=\"video_list video_list_add\"><img src=\"/static/images/web/video/live-show-more.png\" /></div>");
          $(".video_list_add").click(function(){
            if (showMoreNChildren($children, pagesize) <= 0) {
              //如果目标元素已经没有隐藏的子元素了，就隐藏“显示更多”
              $(this).hide();
            };
          })
        }
      });
    },
    showMore2: function (selector) {
      if (selector == undefined) { selector = ".showMoreNChildren" }
      //对页中现有的class=showMorehandle的元素，在之后添加显示更多条，并绑定点击行为
      $(selector).each(function () {
        var pagesize = $(this).attr("pagesize") || 12;
        var $children = $(this).children();
        if ($children.length > pagesize) {
          for (var i = pagesize; i < $children.length; i++) {
            $children.eq(i).hide();
          }

          $("<div class=\"load-more\"><span>加载更多</span></div>").insertAfter($(this)).click(function(){
            if (showMoreNChildren($children, pagesize) <= 0) {
              //如果目标元素已经没有隐藏的子元素了，就隐藏“显示更多”
              $(this).hide();
            };
          })
        }
      });
    },
  });
  $.showMore1(".video_box");
});*/
