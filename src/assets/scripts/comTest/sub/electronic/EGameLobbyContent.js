import BLDef from 'scripts/common/BLDef';
import CommUtility from 'scripts/common/CommUtility';
import EventBus from 'scripts/common/EventBus';
import URLService from 'scripts/common/URLService';

export const EGameLobbyContent = {
    props: {
        LoadMainPageDataModel: {},
        getGameNum: null,
    },

    data() {
        return {
            DataObj: {
                GameAPIVendor: '',
                GameCode: '',
                GameCatlog: BLDef.GameCatlogType.EGames,
                PlayType: BLDef.IdentityType.FORMAL,
                PlatformType: BLDef.PlatformType.Web_PC
            },
            imgString: '',
            defaultImg: 'this.src="' + this.$ResourceCDN + '/EditionImg/Lambor1.0/images/web/game/electronic/Temp.png?VersionCode"',

            // 2019.01.12 判断所展示游戏是否为维护中游戏 SeyoChen
            showGameList: [],
            showGame: false,
            GameAPITypeFilter: 5
        };
    },
    created() {
        console.log(this.LoadMainPageDataModel)
        //      this.$nextTick(function () {
        //          this.getImg(1);
        //      });
    },
    methods: {
        // 玩電子遊戲
        PlayElectronicGame: function (Item) {
            console.log(Item)

            this.DataObj.GameAPIVendor = Item.GameApiNo;
            this.DataObj.GameCode = Item.GameCode;
            this.DataObj.GameCatlog = BLDef.GameCatlogType.ElectronicGame;
            console.log(this.DataObj)
            // url 要修改成遊戲的URL
            localStorage.setItem('PlayGamePostData', URLService.GetUrlParameterFromObj(this.DataObj));

            // EventBus.$emit('showTransferMessage', this.DataObj);

            if (Item.GameApiNo === 31) {
                CommUtility.OpenPlayGameWindow('webPlayPngGame', 'PlayGamePopUpWindow');
            } else {
                CommUtility.OpenPlayGameWindow('webPlayGame', 'PlayGamePopUpWindow');
            }
        }, // end PlayElectronicGame

        // 更改圖片路徑(根據不同遊戲)

        // 2019.01.12 判断所展示游戏是否为维护中游戏 SeyoChen
        JudgeMainTain: function (num) {
            this.showGameList = JSON.parse(localStorage.getItem('GameList'));
            for (let i = 0; i < this.showGameList.length; i++) {
                // this.showGame = true;
                // 2为 欧博游戏 2019.01.12 筛选游戏 并 判定状态 SeyoChen
                if (this.showGameList[i].GameTypeNo == '4' && this.showGameList[i].GameApiNo == num) {
                    switch (this.showGameList[i].GameTypeState) {
                        case 703:
                            this.showGame = true;
                            break;
                        default:
                            this.showGame = false;
                            break;
                    }
                    return;
                }
            }
        }
        // 2019.01.12 判断所展示游戏是否为维护中游戏 SeyoChen end
    },

    watch: {
        // LoadMainPageDataModel: function () {
        //     this.getImg();
        // },
        getGameNum: function (numb) {
            this.JudgeMainTain(numb);
            // this.PlayElectronicGame(numb);
        },
    }
};
