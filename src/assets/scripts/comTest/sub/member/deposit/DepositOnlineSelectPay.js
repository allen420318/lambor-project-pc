
   import Vue from 'vue';
   import EventBus from 'scripts/common/EventBus';
   import Regex from 'scripts/common/CommDef';
   import URLService from 'scripts/common/URLService';
   import PersonalCenterService from 'scripts/businessLogic/personalCenterService';
   import CommUtility from 'scripts/common/CommUtility';

    export const initOsp = {
        props: {
            CurSelectedActivity: {
                BonusSerialNo: undefined,   // 红利对应的资料序号
                ActivityType: undefined,    // 红利活动方式 ==》 -1：未定义; 1：存款(红利设定); 2：红利活动
                ActType: undefined,     // 红利方式细项分类 ==》 1: 首存 2: 续存
            }
        },
        data() {
            return {
                Submitted: false,
                Reg: Regex.Regex,
                CurrentPageData: {},
                InitialFormData: undefined,
                PostDepositData: {
                    PType: undefined,
                    BankCode: undefined,
                    Amount: '',
                    BonusSerialNo: this.CurSelectedActivity.BonusSerialNo,
                    BonusWay: this.CurSelectedActivity.ActivityType
                },
                CommUtility: CommUtility,

                ShowWeChat: false, // 微信支付需要显示提示
                ShowWeChatTip: false,
                ShowZFB: false
            };
        },
        created: function () {
            const self = this;
            // 清空表单
            EventBus.$on('resetDepositForm', () => {
                self.ResetDeposit();
            });
            this.InitialFormData = Vue.util.extend({}, this.PostDepositData);

            // console.log('-------++++进入在线支付时选择的活动+++-------');
            // console.log(this.CurSelectedActivity);
            this.GetPageData();
        },
        mounted: function () {
            CommUtility.BindEnterTrigger('OnlineDepositAddForm', this.CheckFormData);   // 绑定键盘Enter
        },
        methods: {
            // 取得頁面資料
            GetPageData: async function () {
                const dataObj = {
                    ActType: this.CurSelectedActivity.ActType,
                    BonusSerialNo: this.CurSelectedActivity.BonusSerialNo,
                    ActivityType: this.CurSelectedActivity.ActivityType
                };
                const data = await PersonalCenterService.OnlinePaymentDeposit_LoadMainPage(dataObj);
                if (data.Ret == 0 ) {
                    this.CurrentPageData = data.Data;
                    // EventBus.$emit('showNotifyMessageAlert', this.CurrentPageData);
                    this.$emit('CurrentPageData', this.CurrentPageData);    // 将此后台返回的活动详情传到父级 deposit
                }
            },
            // 檢查存款資料並開新視窗
            CheckFormData: function () {
                // 微信充值检查充值金额 SeyoChen
                if ( this.PostDepositData.PType == 2 && this.PostDepositData.Amount > 0 && this.PostDepositData.Amount < 100 && this.PostDepositData.Amount % 10 != 0) {
                    this.ShowWeChatTip = true;
                    return;
                } else if (this.PostDepositData.PType == 2 && this.PostDepositData.Amount >= 100 && this.PostDepositData.Amount % 100 != 0) {
                    this.ShowWeChatTip = true;
                    return;
                } else if ( this.PostDepositData.PType == 3 && this.PostDepositData.Amount < 200) {
                    this.ShowWeChatTip = true;
                    return;
                } else if (this.PostDepositData.PType == 3 && this.PostDepositData.Amount > 5000) {
                    this.ShowWeChatTip = true;
                    return;
                } else {
                    this.ShowWeChatTip = false;
                }

                // 微信充值检查充值金额 SeyoChen end
                this.Submitted = true;
                // 2018.10.11 SeyoChen
                if (this.CurrentPageData.ActivityDetail.Status.Value != '702') {
                    const notifyData = {
                        NotifyMessage: '该活动已失效'
                    };
                    EventBus.$emit('showNotifyMessage', notifyData);
                    return;
                }
                // 2018.10.11 SeyoChen end

                // 输入为空的时候返回 2018.11.22 SeyoChen
                if (this.PostDepositData.Amount === '') {
                    const notifyData = {
                        NotifyMessage: '请输入金额'
                    };
                    EventBus.$emit('showNotifyMessage', notifyData);
                    return;
                }
                // 输入为空的时候返回 2018.11.22 SeyoChen end

                this.Submitted = true;
                Object.keys(this.fields).forEach(key => {
                    this.fields[key].touched = true;
                });
                this.$validator.validateAll();

                const result = this.errors.any();

                if (result === true || this.PostDepositData.PType === undefined) {
                    return;
                }
                // 检查最低入金是否符合要求 2019.03.28
                if (this.PostDepositData.Amount < this.CurrentPageData.ActivityDetail.MinDepositAmt) {
                    EventBus.$emit('showDepositAmtLimitTip', this.ContinueSubmit);
                    return;
                }
                // 检查最低入金是否符合要求 2019.03.28 end

                this.ContinueSubmit();

                // const inputObj = this.PostDepositData;

                // const paymentData = URLService.GetUrlParameterFromObj(inputObj);
                // const HashKey = new Date().getTime();
                // localStorage.setItem(HashKey.toString(), paymentData);
                // console.log("11")
                // console.log(HashKey.toString())
                // console.log(paymentData)
                // console.log("11")
                // CommUtility.OpenPlayGameWindow('webDepositPayment', 'DepositPaymentPopUpWindow', HashKey.toString());
                // this.GetOnlineDeposit(this.PostDepositData);
            },
            ContinueSubmit: function () {
                const inputObj = this.PostDepositData;

                const paymentData = URLService.GetUrlParameterFromObj(inputObj);
                const HashKey = new Date().getTime();
                localStorage.setItem(HashKey.toString(), paymentData);
                CommUtility.OpenPlayGameWindow('webDepositPayment', 'DepositPaymentPopUpWindow', HashKey.toString());
            },
            GetOnlineDeposit: async function (PostDepositData) {
                this.PostDepositData.Device = '0';
                this.PostDepositData.TagUrl = document.location.origin;
                const rspData = await PersonalCenterService.OnlineDeposit_Add(PostDepositData);
                if (rspData.Ret == 0) {
                    const notifyData = {
                        NotifyMessage: '测试 PHP '
                    };
                    EventBus.$emit('showNotifyMessage', notifyData);
                }
            },

            // 選擇支付方式
            SelectPayType: function (type) {
                this.PostDepositData.PType = type;
                // 2019.01.29 SeyoChen 2: 微信
                if (type == 2) {
                    this.ShowWeChat = true;
                    this.ShowZFB = false;
                } else if (type == 3) {
                    this.ShowZFB = true;
                    this.ShowWeChat = false;
                } else {
                    this.ShowZFB = false;
                    this.ShowWeChat = false;
                }
                // if (type == 1) {
                   //  CommUtility.WebShowUniqueForm('bankListForm');
                // }
            },
            // 選擇銀行
            SelectBank: function (bankCode) {
                this.PostDepositData.BankCode = bankCode;
                CommUtility.WebCloseUniqueForm('bankListForm');
            },
            // 返回存款初始
            ResetDeposit: function () {
                this.Submitted = false;
                this.PostDepositData = Vue.util.extend({}, this.InitialFormData);
                this.$validator.reset();
                // 2018.11.13 SeyoChen 取消返回上一层
                this.$parent.CurSelectedActivity.BonusSerialNo = undefined;     // 作用是返回上一层
                this.$parent.IsShow = false;    // 作用是返回上一层
                // 2018.11.13 SeyoChen end
            }
        },
        beforeDestroy() {
            EventBus.$off('resetDepositForm');
        },
    };
