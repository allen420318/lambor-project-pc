
    import EventBus from 'scripts/common/EventBus';
    import PersonalCenterService from 'scripts/businessLogic/personalCenterService';
    import MemberDepositActivityList from '@/components/sub/member/deposit/memberDepositActivityList';

    export const initDeSa = {
        data() {
            return {
                OnlinePaymentAcctCnt: undefined,    // 在线支付号标
                CompAcctCnt: undefined, // 公司入款号标
                DepositType: 1,  // 选择的存款类型，默认在线支付
                TargetAcctCount: undefined, // 切换选项卡后所在的存款类型
                TargetActivityList: [], // 活动列表
                OnlineActivityList: [], // 在线支付活动列表
                RemittanceActivityList: [], // 公司入款活动列表
                TitleFonts: undefined,  // 选项卡标题（在线支付/公司入款)
                TitleFonts1: '在线支付',
                TitleFonts2: '公司入款',
            };
        },
        props: {
            HaveNoSelectedBonus: undefined, // 是否显示不参与活动按钮 2019.03.28
            RecordShow: undefined
        },
        created: function () {
            this.GetDepositData();
        },
        methods: {
            // 向父组件传递参数--通知获取存款记录 2019.04.18 SeyoChen
            GetDepositRecord: function () {
                this.DepositType = 0;
                this.$parent.GetDepositRecord();
            },
            // 切换存款方式
            SwitchDepositType: function (type) {
                this.$parent.CloseDepositRecord();
                this.DepositType = type;
                this.TargetAcctCount = this.DepositType == 1 ? this.OnlinePaymentAcctCnt : this.CompAcctCnt;
                this.TargetActivityList = this.DepositType == 1 ? this.OnlineActivityList : this.RemittanceActivityList;
                
                // 从优惠活动进入存款 判断是否需要清空重置 2019.03.26 SeyoChen
                if (this.$route.params.ActivityData != undefined) {
                    this.TitleFonts = this.DepositType == 1 ? '在线支付' : '公司入款';
                    this.ShowTitleFonts();
                    return;
                }
                // 从优惠活动进入存款 判断是否需要清空重置 2019.03.26 SeyoChen end
                
                EventBus.$emit('resetDepositForm'); // 清空输入框

                this.TitleFonts = this.DepositType == 1 ? '在线支付' : '公司入款';
                this.ShowTitleFonts();
                // console.log(this.TargetAcctCount);
            },

            // 进入不同存款方式显示不同的选项卡标题
            ShowTitleFonts: function () {
                EventBus.$emit('ShowTitleFonts', this.TitleFonts);
            },

            // 取得最佳存款优惠
            GetDepositData: async function () {
                const data = await PersonalCenterService.Deposit_LoadMainPage();
                // console.log(data.Data);
                if (data.Ret !== 0) {
                    return;
                }

                this.OnlinePaymentAcctCnt = data.Data.OnlinePaymentAcctCnt;
                this.CompAcctCnt = data.Data.CompAcctCnt;

                this.OnlineActivityList = data.Data.OnlineDepositActivityList;
                this.RemittanceActivityList = data.Data.RemittanceDepositActivityList;

                if (this.OnlinePaymentAcctCnt != 0) {
                    this.SwitchDepositType(1); // 初始设置切换选项卡后所在的存款类型为在线支付
                } else if (this.CompAcctCnt != 0) {
                    this.SwitchDepositType(2); // 初始设置切换选项卡后所在的存款类型为公司入款
                }

                // this.TargetAcctCount = this.OnlinePaymentAcctCnt;   // 初始设置切换选项卡后所在的存款类型为在线支付
                // this.TargetActivityList = this.OnlineActivityList;  // 初始获得在线支付的优惠活动
            },
        },
        components: {
            MemberDepositActivityList
        }
    };