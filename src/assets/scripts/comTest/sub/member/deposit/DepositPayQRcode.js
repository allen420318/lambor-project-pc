
    export const initQRcd = {
        props: {
            RemittanceBankInfo: {}
        },
        data() {
            return {
                RemittancePayNum: 0 // 判断二维码数量
            };
        },
        methods: {
            // 判断支付二维码数量 进行设置 0：隐藏当前页面 1：二维码居中 2：显示同排
            JudgeQRcodeNum: function () {
                for (let i = 0; i < this.RemittanceBankInfo.length; i++) {
                    if (this.RemittanceBankInfo[i].PaymentWayType == 2) {
                        this.RemittancePayNum++;
                    }
                }
                if (this.RemittancePayNum == 1) {
                    $('.qrcode_msg').addClass('qrcode_only_msg');
                }
            } 
        },
        watch: {
            RemittanceBankInfo: function () {
                this.JudgeQRcodeNum();
            }
        }
    };