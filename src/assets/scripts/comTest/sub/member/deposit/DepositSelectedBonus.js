import EventBus from 'scripts/common/EventBus';
import CommUtility from 'scripts/common/CommUtility';
import MemberDepositActivityDetail from '@/components/sub/member/deposit/memberDepositActivityDetail';
import MemberDepositAmountLimit from '@/components/sub/member/deposit/memberDepositAmountLimit';

export const initDeSb = {
    props: {
        BonusWayType: undefined,
        ActivityType: undefined,
        ActivityName: undefined,
        ActivityDetail: {
            type: Object,
            default: function () {
                return {};
            }
        }
    },
    data () {
        return {
            CommUtility: CommUtility,
            TitleFonts: '在线支付', // 选项卡标题
            AlertActivityDetail: {}
        };
    },
    created: async function () {
        // 2018.09.27 SeyoChen
        const self = this;
        setTimeout(function () {
            if (self.$route.params.ChargeType == 2) {
                self.TitleFonts = '公司入款';
            } else {
                self.TitleFonts = '在线支付';
            }
        }, 500);
        // 2018.09.27 SeyoChen end

        // 进入不同存款方式显示不同的选项卡标题
        EventBus.$on('ShowTitleFonts', (fonts) => {
            self.TitleFonts = fonts;
            // console.log(self.TitleFonts);
        });
    },
    methods: {
        // 显示活动详情
        ShowActivityDetail: function () {
            // 2018.09.26 SeyoChen
            // if (this.$route.params.ActivityData != undefined) {
            //     this.BonusWayType = this.$route.params.ChargeType;
            // }
            // 2018.09.26 SeyoChen end
            EventBus.$emit('showActivityDetail', this.BonusWayType, this.ActivityDetail, this.ActivityType); // 将父级deposit传过来的活动详情传给子级 memberDepositActivityDetail
            // EventBus.$emit('showActivityDetail');
        },
        // 返回重选
        ReselectActivity: function () {
            this.$parent.CurSelectedActivity.BonusSerialNo = undefined;
            this.$parent.IsShow = false;
            // EventBus.$emit('resetSelectedDepositActivity');
        }
    },
    watch: {
        // 监听父级存款类型改变来改变选项卡标题 2019.06.18 ququ
        BonusWayType () {
            console.log('------------------BonusWayType');
            if (this.BonusWayType == 2) {
                this.TitleFonts = '公司入款';
            } else {
                this.TitleFonts = '在线支付';
            }
        }
    },
    components: {
        MemberDepositActivityDetail,
        MemberDepositAmountLimit
    },
    beforeDestroy () {
        EventBus.$off('ShowTitleFonts');
    }
};
