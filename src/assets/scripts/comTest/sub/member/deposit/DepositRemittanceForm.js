import Vue from 'vue';
import moment from 'moment';
import EventBus from 'scripts/common/EventBus';
import Regex from 'scripts/common/CommDef';
import Datepicker from '@/components/main/common/datePicker';
import CommUtility from 'scripts/common/CommUtility';
import BLDef from 'scripts/common/BLDef';
import CommonService from 'scripts/businessLogic/commonService';
import PersonalCenterService from 'scripts/businessLogic/personalCenterService';

export const initDeRf = {
    props: {
        CurrentPageData: {},
        CurSelectedActivity: {
            BonusSerialNo: undefined, // 红利对应的资料序号
            ActivityType: undefined, // 红利活动方式 ==》 -1：未定义; 1：存款(红利设定); 2：红利活动
            ActType: undefined, // 红利方式细项分类 ==》 1: 首存 2: 续存
        }
    },
    data () {
        return {
            Reg: Regex.Regex,
            InitialFormData: undefined,
            PostDepositData: {
                // OrderNo: '',    // 订单编号***
                // DepositAmt: '',     // 金额
                // DepositWithdrawSerialNo: '',    // 对应的公司账户序号
                // DepositDateTime: undefined, // 汇款时间(存入时间)***
                // DepositDateTick: undefined, // 汇款时间(存入时间)
                // DepositorName: '',  // 汇款人姓名 (存款人姓名)
                // RemittanceDepositType: undefined,   // 存款方式 (对应共用的CODE表)
                // BankProvince: undefined,    // 银行支行省
                // BankCity: undefined,    // 银行支行市、
                // BankDistrict: undefined,    // 银行支行区
                // BankBranchName: '',     // 银行支行名称
                // BonusWay: '',    // 红利方式
                // BonusSerialNo: '',  // 红利对应的资料序号
                //
                // DepositDate: undefined,
                // Hours: 0,
                // Minutes: 0,
                // BankProvinceModel: {},
                // BankCityModel: {},
                // BankDistrictModel: {},
                // CurSelectedActivity: {},     // 当前选中的活动

                DepositAmt: '', // 金额
                DepositorName: '', // 汇款人姓名 (存款人姓名)
                RemittanceDepositType: undefined, // 存款方式 (对应共用的CODE表)
                BankProvince: undefined, // 银行支行省
                BankCity: undefined, // 银行支行市、
                BankDistrict: undefined, // 银行支行区
                BankBranchName: '', // 银行支行名称
                BonusWay: this.CurSelectedActivity.ActivityType, // 红利方式
                BonusSerialNo: this.CurSelectedActivity.BonusSerialNo, // 红利对应的资料序号
                DepositDate: undefined,
                Hours: 0,
                Minutes: 0,
                BankProvinceModel: {},
                BankCityModel: {},
                BankDistrictModel: {},
                Remark: ''
            },
            CalendarDisabled: {
                from: new Date(moment().add(1, 'days').format('YYYY/MM/DD')), // Disable all dates after specific date
            },
            Submitted: false,
            ProcessingFlag: false,
            DropDownInvalid: false,
            DefaultTownList: [],
            DefaultDistList: []
        };
    },
    created: function () {
        const self = this;

        // 2018.09.26 SeyoChen
        // if (this.$route.params.ActivityData != undefined) {
        //     self.PostDepositData.BonusSerialNo = this.$route.params.ActivityData.ID;
        //     self.PostDepositData.BonusWay = 2;
        // }
        // 2018.09.26 SeyoChen end

        // 清空表单
        EventBus.$on('resetDepositForm', () => {
            self.ResetDeposit();
        });
        this.BindingEvent();
        this.InitialFormData = Vue.util.extend({}, this.PostDepositData);
    },
    mounted: function () {
        CommUtility.BindEnterTrigger('CompanyDepositAddForm', this.CheckFormData);
    },
    methods: {
        BindingEvent: function () {
            document.addEventListener('click', this.HideDropDown);
        },
        // 顯示紀錄類型選單
        ShowDropDown: function (targetClass) {
            $('.down_box').slideUp(100);
            if (this.DropDowning !== targetClass) {
                $('.' + targetClass).slideDown(300);
                this.DropDowning = targetClass;
            } else {
                $('.' + targetClass).slideUp(100);
                this.DropDowning = '';
            }
        },
        // 隱藏紀錄類型選單
        HideDropDown: function (event) {
            const target = $(event.target);
            if (target.closest('.d_course').length === 0) {
                $('.down_box').slideUp(100);
                this.DropDowning = '';
            }
        },
        // 載入頁面資料
        SetPageData: async function () {
            this.DefaultTownList = Vue.util.extend([], this.CurrentPageData.TownSelectList);
            this.DefaultDistList = Vue.util.extend([], this.CurrentPageData.DistSelectList);
            this.PostDepositData.BankProvinceModel = this.CurrentPageData.ProvinceSelectList[0];
            this.PostDepositData.BankCityModel = this.CurrentPageData.TownSelectList[0];
            this.PostDepositData.BankDistrictModel = this.CurrentPageData.DistSelectList[0];
        },
        // 選擇省分
        ChangeProvince: async function (selectProvince) {
            const inputObj = {
                SType: BLDef.SelectTypesDef.CITIES,
                InputValues: [selectProvince.Value]
            };
            const data = await CommonService.GetCityTypes(inputObj);
            if (data.Ret === 0) {
                this.PostDepositData.BankProvince = selectProvince.Value;
                this.PostDepositData.BankProvinceModel = selectProvince;
                this.CurrentPageData.TownSelectList = data.Data.SelectList;
                this.PostDepositData.BankCityModel = data.Data.SelectList[0];
                this.CurrentPageData.DistSelectList = this.DefaultDistList;
                this.PostDepositData.BankDistrictModel = this.DefaultDistList[0];
            }
        },
        // 選擇城市
        ChangeTown: async function (selectTown) {
            const inputObj = {
                SType: BLDef.SelectTypesDef.DISTS,
                InputValues: [selectTown.Value]
            };
            const data = await CommonService.GetDistTypes(inputObj);
            if (data.Ret === 0) {
                this.PostDepositData.BankCity = selectTown.Value;
                this.PostDepositData.BankCityModel = selectTown;
                this.CurrentPageData.DistSelectList = data.Data.SelectList;
                this.PostDepositData.BankDistrictModel = data.Data.SelectList[0];
                if (this.CurrentPageData.DistSelectList.length === 1) {
                    this.DropDownInvalid = false;
                }
            }
        },
        // 選擇區域
        ChangeDist: function (selectDist) {
            this.PostDepositData.BankDistrict = selectDist.Value;
            this.PostDepositData.BankDistrictModel = selectDist;
            if (selectDist.Value !== '-1') {
                this.DropDownInvalid = false;
            }
        },
        // 切換轉帳存款方式
        SelectRemittanceType: function (item) {
            this.PostDepositData.RemittanceDepositType = item.SettingCode;
            // ATM/銀行臨櫃 重置省市區選單
            if (item.SettingCode == 1 || item.SettingCode == 2 || item.SettingCode == 3) {
                this.DropDownInvalid = false;
                this.CurrentPageData.TownSelectList = Vue.util.extend([], this.DefaultTownList);
                this.CurrentPageData.DistSelectList = Vue.util.extend([], this.DefaultDistList);
                this.PostDepositData.BankProvinceModel = this.CurrentPageData.ProvinceSelectList[0];
                this.PostDepositData.BankCityModel = this.CurrentPageData.TownSelectList[0];
                this.PostDepositData.BankDistrictModel = this.CurrentPageData.DistSelectList[0];
                this.PostDepositData.BankBranchName = undefined;
                if (this.fields.BankBranchName !== undefined) {
                    // this.fields.BankBranchName.touched = false;
                    this.$validator.reset();
                }
            }
        },
        // 檢查下拉選單合法性
        CheckDropDownInvalid: function () {
            // 非需檢查的存款選項
            if (this.PostDepositData.RemittanceDepositType === 1 || this.PostDepositData.RemittanceDepositType === 2 || this.PostDepositData.RemittanceDepositType === 3) {
                if (!this.PostDepositData.BankProvinceModel || !this.PostDepositData.BankCityModel) {
                    return true;
                }
                if (this.PostDepositData.BankProvinceModel.Value == -1 || this.PostDepositData.BankCityModel.Value == -1) {
                    return true;
                }
                if (this.CurrentPageData.DistSelectList.length > 1 && (!this.PostDepositData.BankDistrictModel || this.PostDepositData.BankDistrictModel.Value == -1)) {
                    return true;
                }
            }
            return false;
        },
        // 點擊取消
        ResetDeposit: function () {
            this.Submitted = false;
            this.PostDepositData = Vue.util.extend({}, this.InitialFormData);
            this.$validator.reset();
            this.$parent.$parent.CurSelectedActivity.BonusSerialNo = undefined; // 作用是返回上一层
            this.$parent.$parent.IsShow = false; // 作用是返回上一层
        },
        // 清空提示语 2019.02.18 SeyoChen
        ResetTip: function () {
            this.Submitted = false;
            this.PostDepositData = Vue.util.extend({}, this.InitialFormData);
            this.$validator.reset();
        },
        // 清空提示语 2019.02.18 SeyoChen end
        // 檢查發送資料
        CheckFormData: async function () {
            // 2018.10.11 SeyoChen
            if (this.CurrentPageData.ActivityDetail.Status.Value != '702') {
                const notifyData = {
                    NotifyMessage: '该活动已失效'
                };
                EventBus.$emit('showNotifyMessage', notifyData);
                return;
            }
            // 2018.10.11 SeyoChen end
            this.Submitted = true;
            this.DropDownInvalid = this.CheckDropDownInvalid();
            Object.keys(this.fields).forEach(key => {
                this.fields[key].touched = true;
            });
            const result = await this.$validator.validateAll();
            if (result === false || this.PostDepositData.DepositDate === undefined || this.PostDepositData.RemittanceDepositType === undefined || this.DropDownInvalid === true) {
                return;
            }

            // 检查最低入金是否符合要求 2019.03.28
            if (this.PostDepositData.DepositAmt < this.CurrentPageData.ActivityDetail.MinDepositAmt) {
                EventBus.$emit('showDepositAmtLimitTip', this.SubmitDepositData);
                return;
            }
            // 检查最低入金是否符合要求 2019.03.28 end

            this.SubmitDepositData();
        },
        // 發送存款資料
        SubmitDepositData: async function () {
            if (this.ProcessingFlag === true) {
                return;
            }

            this.ProcessingFlag = true;
            const fullHour = this.PostDepositData.Hours > 9 ? this.PostDepositData.Hours : '0' + this.PostDepositData.Hours;
            const fullMinute = this.PostDepositData.Minutes > 9 ? this.PostDepositData.Minutes : '0' + this.PostDepositData.Minutes;
            const fullDate = moment(this.PostDepositData.DepositDate).format('YYYY/MM/DD') + ' ' + fullHour + ':' + fullMinute + ':00';
            const inputObj = this.PostDepositData;

            // console.log('----------------公司入款提交的inputObj---------------');
            // console.log(inputObj);  // 发送给后台的资料
            // console.log('----------------公司入款提交的inputObj---------------');

            inputObj.DepositDateTick = CommUtility.LocalTimeToUtcTicks(fullDate, false, true);
            inputObj.BankInfo = this.CurrentPageData.BankInfo;
            EventBus.$emit('GlobalLoadingTrigger', true);
            const data = await PersonalCenterService.CompanyRemittance_Add(inputObj);
            EventBus.$emit('GlobalLoadingTrigger', false);

            this.ProcessingFlag = false;

            const notifyData = {
                NotifyMessage: data.Message
            };
            if (data.Ret == 0) {
                notifyData.NotifySubMessage = '您好，您的申请已经提交，我们会尽快处理，请耐心等待';
                notifyData.CloseFunction = this.ResetDeposit;
                EventBus.$emit('remittanceDepositSuccess');
            }

            EventBus.$emit('showNotifyMessage', notifyData);
        },
    },
    components: {
        Datepicker
    },
    watch: {
        CurrentPageData: function () {
            this.SetPageData();
        }
    },
    beforeDestroy () {
        EventBus.$off('resetDepositForm');
    }
};
