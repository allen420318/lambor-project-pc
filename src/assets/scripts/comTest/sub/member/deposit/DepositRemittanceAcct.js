
    export const initDeRa = {
        props: {
            RemittanceBankInfo: {}
        },
        data() {
            return {
                RemittancePayData: ''
            };
        },
        created: function () {
            this.GetPayData();
        },
        methods: {
            // 获取公司入款类型为 1 ，非扫码支付
            GetPayData: function () {
                for (let i = 0; i < this.RemittanceBankInfo.length; i++) {
                    if (this.RemittanceBankInfo[i].PaymentWayType == 1) {
                        this.RemittancePayData = this.RemittanceBankInfo[i];
                        return;
                    }
                }
            }
        },
        watch: {
            RemittanceBankInfo: function () {
                this.GetPayData();
            }
        }
    };
