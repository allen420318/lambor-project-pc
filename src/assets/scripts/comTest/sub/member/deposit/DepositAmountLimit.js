
    import CommUtility from 'scripts/common/CommUtility';
    import EventBus from 'scripts/common/EventBus';

    export const initDeAli = {
        data() {
            return {
                CommUtility: CommUtility,
                ConfirmFunction: undefined
            };
        },
        created: function () {
            const self = this;
            EventBus.$on('showDepositAmtLimitTip', (func) => {
                self.ConfirmFunction = func;
                CommUtility.WebShowUniqueForm('depositAmtLessActivityMinAmtErrorForm');
            });
        },
        methods: {
            ExecuteConfirmFunct: function () {
                CommUtility.WebCloseUniqueForm('depositAmtLessActivityMinAmtErrorForm');
                this.ConfirmFunction();
            }
        },
        beforeDestroy() {
            EventBus.$off('showDepositAmtLimitTip');
        }
    };