
   	import EventBus from 'scripts/common/EventBus';
    import CommUtility from 'scripts/common/CommUtility';

    export const initDeAd = {
        created: function () {
            const self = this;
            // 2018.09.26 SeyoChen
            if (this.$route.params.ActivityData != undefined) {
                if (this.$route.params.ChargeType == 2) {
                    this.TitleFonts = '公司入款';
                }
                this.BonusWayType = this.$route.params.ChargeType;
                EventBus.$on('ActivityData', (flag) => {
                    self.ActivityDetail = flag;
                });
            }
            // 2018.09.26 SeyoChen end

            EventBus.$on('showActivityDetail', (notifyBonusWayType, notifyActivityDetail, notifyActivityType) => {      // 接收父级 memberDepositSelectedBonus 传过来的活动详情
                self.BonusWayType = notifyBonusWayType;
                self.ActivityDetail = notifyActivityDetail.ActivityDetail;
                self.ActivityType = notifyActivityType;
                CommUtility.WebShowUniqueForm('activityDetailForm');
            });
        },
        data() {
            return {
                CommUtility: CommUtility,
                BonusWayType: undefined,
                ActivityDetail: {},
                ActivityType: undefined
            };
        },
        methods: {
            CloseDetail: function () {
                this.BonusWayType = undefined;
                this.ActivityDetail = {};
                CommUtility.WebCloseUniqueForm('activityDetailForm');
            }
        },
        beforeDestroy() {
            EventBus.$off('showActivityDetail');
        }
    };