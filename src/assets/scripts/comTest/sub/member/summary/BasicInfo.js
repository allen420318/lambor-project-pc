
    import EventBus from 'scripts/common/EventBus';
    import download from 'downloadjs';    
    import Clipboard from 'scripts/customJs/web/clipboard.min';   

    export const initBaIf = {
        data() {
            return {
                gradeDetailFlag: true,
                classificationFlag: false
            };
        },
        created: function () {
            this.InitClipboard();
        },
        methods: {
            InitClipboard: function () {
                new Clipboard('.a_copy');
            },
            ShowNotify: function () {
                const notifyData = {
                    NotifyMessage: '复制成功'
                };
                EventBus.$emit('showNotifyMessage', notifyData);
            },
            QRCodeDownload: function () {
                download(this.MemberData.PromotionLinkImg, 'QrCodeImg.png', 'image/png');
            },
            gradeDetail: function () {
                this.gradeDetailFlag = false;
                this.classificationFlag = true;
                EventBus.$emit('gradeDetailw', this.gradeDetailFlag);
            },
            classificationBack: function () {
                this.gradeDetailFlag = true;
                this.classificationFlag = false;
                EventBus.$emit('gradeDetailw', this.gradeDetailFlag);
            },
            Percentage: function (number1, number2) {
                const realVal = parseFloat((parseInt(number1) / (parseInt(number1) + parseInt(number2))) * 100.00).toFixed(2); 
                return (realVal + '%'); // 小数点后两位百分比
            }
        },
        props: {
            MemberData: {  
                type: Object,                      
                default: function () {
                    return {};             
                }
            },
            MemberUpgrade: {  
                type: Object,                      
                default: function () {
                    return {};             
                }
            },
            memberLvLimitListData: {  
                type: Array,                      
                default: function () {
                    return [];             
                }
            },
        }
    };
