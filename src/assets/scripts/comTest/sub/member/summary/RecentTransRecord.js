import Vue from 'vue';
import BLDef from 'scripts/common/BLDef';
import PersonalCenterService from 'scripts/businessLogic/personalCenterService';
import MemberRecordTable from '@/components/sub/member/transactionRecord/memberRecordTable';

export const initReTr = {
    data() {
        return {
            RecordList: []
        };
    },
    created: function () {
        this.UpdateRecord();
    },
    methods: {
        UpdateRecord: async function () {
            const inputObj = {
                WGUID: Vue.prototype.$WGUID,
                Type: '-1',
                PagingInfo: {
                    PageNo: 1,
                    PageSize: 8
                }
            };
            const data = await PersonalCenterService.CashFlowLog_Query(inputObj);
            if (data.Ret === BLDef.ErrorRetType.SUCCESS) {
                this.RecordList = data.Data.LogList;
            }
        }
    },
    components: {
        MemberRecordTable
    }

};
