import Vue from 'vue';
import moment from 'moment';
import Regex from 'scripts/common/CommDef';
import EventBus from 'scripts/common/EventBus';
import PersonalCenterService from 'scripts/businessLogic/personalCenterService';
import CommonService from 'scripts/businessLogic/commonService';
import Datepicker from '@/components/main/common/datePicker';
import MemberPhoneEdit from '@/components/sub/member/profile/memberPhoneEdit';
import EmailVerify from '@/components/sub/member/profile/emailVerify';
import EmailVerifyFail from '@/components/sub/member/profile/emailVerifyFail';
import BLDef from 'scripts/common/BLDef';
import CommUtility from 'scripts/common/CommUtility';

export const initPfEt = {
    data () {
        return {
            ProcessingFlag: false,
            DisplayPhoneNo: undefined,
            SexTypeSelectList: [],
            LoadMainPageDataModel: {
                Member: {},
                OrgMember: {}
            },
            Reg: Regex.Regex,
            CalendarDisabled: {
                from: new Date(moment().format('YYYY/MM/DD'))
            },

            LoadEmailVerifyData: {

            },

            // 信箱欄位內容
            Email: undefined,
            // 信箱驗證旗標
            EmailVerifyFlag: undefined,
            // 信箱欄位驗證旗標
            EmailValidFlag: undefined,
            // 信箱已被使用旗標
            EmailUsedFlag: undefined,
            // 信箱額度旗標
            IsEmailSendable: undefined,
            // 倒數時間
            CountTime: undefined,
        };
    },
    created: function () {
        this.GetMemberInfo();
        this.GetSexSelectList();
        EventBus.$on('SignalR_VerifyEmail_Success', () => {
            EmailVerify.methods.CloseWindow();
            this.EmailVerifyFlag = true;
        });
    },
    mounted: function () {
        CommUtility.BindEnterTrigger('UpdPersonalInfoForm', this.UpdPersionalInfo);
    },
    methods: {
        // 載入畫面
        GetMemberInfo: async function () {
            const data = await CommonService.Comm_GetMemberInfo();
            if (data.Ret === 0) {
                if (data.Data.Member.Birth === null) {
                    data.Data.Member.Birth = undefined;
                }
                this.LoadMainPageDataModel = data.Data;
                this.Email = data.Data.Member.Email;
                this.LoadMainPageDataModel.Member.NewPhoneNo = Vue.util.extend(data.Data.Member.PhoneNo);
                // 修改前的會員資料
                this.LoadMainPageDataModel.OrgMember = Vue.util.extend({}, data.Data.Member);
                this.DisplayPhoneNo = this.LoadMainPageDataModel.Member.NewPhoneNo;

                // 取得信箱是否已被驗證
                this.EmailVerifyFlag = this.LoadMainPageDataModel.Member.IsEmailVerify;
            } // end if

            const retData = await PersonalCenterService.MemberInfo_GetValidEmailInfo();
            if (retData.Ret === 0) {
                this.IsEmailSendable = retData.Data.IsEmailSendable;
                this.LoadEmailVerifyData = retData.Data;
            } // end if
        },
        // 取得性別選單
        GetSexSelectList: async function () {
            const data = await CommonService.SexTypeList_Get();
            if (data.Ret === 0) {
                this.SexTypeSelectList = data.Data.SexTypeSelectList;
            }
        },
        TriggerEditPhone: function () {
            EventBus.$emit('phoneEdit');
        },
        UpdPersionalInfo: async function () {
            // 清掉信箱已被使用的錯誤訊息
            this.EmailUsedFlag = false;

            Object.keys(this.fields).forEach(key => {
                this.fields[key].touched = true;
            });

            const result = await this.$validator.validateAll();
            if (result === false) {
                return;
            }

            // 處理中則直接返回
            if (this.ProcessingFlag === true) {
                return;
            }

            this.ProcessingFlag = true;
            if (this.LoadMainPageDataModel.Member.Birth !== undefined) {
                this.LoadMainPageDataModel.Member.Birth = moment(this.LoadMainPageDataModel.Member.Birth).format('YYYY/MM/DD');
            }
            const inputObj = {
                Detail: this.LoadMainPageDataModel.Member,
                OrgDetail: this.LoadMainPageDataModel.OrgMember
            };

            EventBus.$emit('GlobalLoadingTrigger', true);
            const data = await PersonalCenterService.MemberInfo_Mod(inputObj);
            EventBus.$emit('GlobalLoadingTrigger', false);

            const notifyData = {
                NotifyMessage: data.Message
            };

            if (data.Ret != 0) {
                this.LoadMainPageDataModel.Member = Vue.util.extend({}, this.LoadMainPageDataModel.OrgMember);
                this.LoadMainPageDataModel.Member.NewPhoneNo = this.LoadMainPageDataModel.Member.PhoneNo;
                EventBus.$emit('showNotifyMessage', notifyData);
            } else {
                notifyData.CloseFunction = this.GetMemberInfo;
                EventBus.$emit('Profile_Update_Success');
                EventBus.$emit('showNotifyMessage', notifyData);
                EventBus.$emit('fillTipUpdate');
                this.$validator.reset();
            }

            this.ProcessingFlag = false;
        },

        // 信箱驗證
        EmailVerifyInfo: async function () {
            if (this.EmailUsedFlag == true) {
                return;
            } // end if

            // 欄位驗證
            if (this.$validator.errors.has('MemberEmail') || this.LoadMainPageDataModel.Member.Email == '') {
                this.EmailValidFlag = true;
                return;
            } // end if
            this.EmailValidFlag = false;

            // 判斷額度是否已大於上限
            if (this.IsEmailSendable == false) {
                //                  EmailVerifyFail.methods.OpenWindow();
                EventBus.$emit('phoneEmail');
                return;
            } // end if

            // 是否已經倒數完
            if (this.CountTime > 0) {
                EventBus.$emit('verifyEmailWindowTrigger');
                return;
            } // end if

            // 處理中則直接返回
            if (this.ProcessingFlag === true) {
                return;
            } // end if
            this.ProcessingFlag = true;

            // 資料物件
            const dataObj = {
                Email: this.LoadMainPageDataModel.Member.Email,
                Domain: document.location.protocol + '//' + location.host,
                OrgEmail: this.LoadMainPageDataModel.OrgMember.Email,
            };

            EventBus.$emit('GlobalLoadingTrigger', true);
            const retData = await PersonalCenterService.EmailVerify_SendValidEmail(dataObj);
            EventBus.$emit('GlobalLoadingTrigger', false);

            if (retData.Ret == 0) {
                // 成功
                this.IsEmailSendable = retData.Data.IsEmailSendable;
                this.LoadMainPageDataModel.OrgMember.Email = this.LoadMainPageDataModel.Member.Email;
                EventBus.$emit('verifyEmailWindowTrigger');
            } else if (retData.Ret == BLDef.ErrorRetType.COMM_VERIFICATION_EMAIL_EXIST) {
                // 此信箱已被使用
                this.EmailUsedFlag = true;
            } else if (retData.Ret == BLDef.ErrorRetType.COMM_SEND_VERIFICATION_EMAIL_COUNT_LIMIT) {
                // 寄信額度超過
                this.EmailVerifyLimitUp();
            } else {
                const data = await CommonService.Comm_GetMemberInfo();
                if (data.Ret === 0) {
                    this.LoadMainPageDataModel = data.Data;
                    // 修改前的會員資料
                    this.LoadMainPageDataModel.OrgMember = Vue.util.extend({}, data.Data.Member);
                } // end if
                // 操作失敗
                const notifyData = {
                    NotifyMessage: retData.Message,
                };
                EventBus.$emit('showNotifyMessage', notifyData);
            } // end if
            this.ProcessingFlag = false;
        }, // end EmailVerify

        // 信箱額度已滿彈窗
        EmailVerifyLimitUp: function () {
            //              EmailVerifyFail.methods.OpenWindow();
            EventBus.$emit('phoneEmail');
        }, // end EmailVerifyLimitUp

    },
    components: {
        Datepicker,
        MemberPhoneEdit,
        EmailVerify,
        EmailVerifyFail
    },
    beforeDestroy () {
        EventBus.$off('SignalR_VerifyEmail_Success');
    },

    watch: {
        Email: function () {
            this.EmailUsedFlag = false;
            this.LoadMainPageDataModel.Member.Email = this.Email;
        },
    }
};
