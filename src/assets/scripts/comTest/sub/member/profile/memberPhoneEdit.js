import Vue from 'vue';
import Regex from 'scripts/common/CommDef';
import CommUtility from 'scripts/common/CommUtility';
import EventBus from 'scripts/common/EventBus';

export const initMePe = {
    data () {
        return {
            Reg: Regex.Regex,
            OrgPhoneNo: undefined,
            NewPhoneNo: undefined
        };
    },
    props: {
        DisplayPhoneNo: undefined
    },
    mounted: function () {
        CommUtility.BindEnterTrigger('phoneNumberForm', this.SubmitPhoneEdit);
        EventBus.$on('Profile_Update_Success', () => {
            this.ResetForm();
        });
        EventBus.$on('phoneEdit', () => {
            this.ShowEditPhone();
        });
    },
    methods: {
        // 開啟彈窗

        ShowEditPhone: function () {
            CommUtility.WebShowUniqueForm('phoneNumberForm');
        }, // end ShowEditPhone

        // 關閉彈窗
        HideEditPhone: function () {
            this.ResetForm(); // 2018.12.04 关闭弹窗时清空内容
            this.$validator.reset();
            CommUtility.WebCloseUniqueForm('phoneNumberForm');
        }, // end HideEditPhone

        // 暫存要修改後的電話
        SubmitPhoneEdit: async function () {
            Object.keys(this.fields).forEach(key => {
                this.fields[key].touched = true;
            });

            const result = await this.$validator.validateAll();
            if (result === false) {
                return;
            }

            // 2018.11.22 SeyoChen 输入手机是否与原手机相同
            if (this.OrgPhoneNo !== this.DisplayPhoneNo &&
                this.DisplayPhoneNo != '') {
                return;
            }
            // 2018.11.22 SeyoChen end

            this.$parent.LoadMainPageDataModel.Member.PhoneNo = Vue.util.extend(this.OrgPhoneNo);
            this.$parent.LoadMainPageDataModel.Member.NewPhoneNo = Vue.util.extend(this.NewPhoneNo);
            this.ResetForm(); // 2018.12.04 关闭弹窗时清空内容
            this.$validator.reset();
            CommUtility.WebCloseUniqueForm('phoneNumberForm');
        }, // end SubmitPhoneEdit

        // 重置欄位
        ResetForm: function () {
            this.OrgPhoneNo = undefined;
            this.NewPhoneNo = undefined;
        }, // end ResetForm
    },
    beforeDestroy () {
        EventBus.$off('Profile_Update_Success');
    },
};
