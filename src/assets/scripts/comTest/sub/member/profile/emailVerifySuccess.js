
    import CommUtility from 'scripts/common/CommUtility';
	import EventBus from 'scripts/common/EventBus';
	
	export const initEmVf = {
	
	    mounted: function () {
			const self = this;
			EventBus.$on('SignalR_VerifyEmail_Notify', () => {
				self.OpenWindow();          
			});  
		},
	
		methods: {
			// 開啟視窗
			OpenWindow: function () {
				CommUtility.WebShowUniqueForm('emailVerifySuccessForm');
			}, // end OpenWindow
	
			// 關閉視窗
			CloseWindow: function () {            
				CommUtility.WebCloseUniqueForm('emailVerifySuccessForm');            
			}, // end CloseWindow
		},   
		
		beforeDestroy() {
			EventBus.$off('SignalR_VerifyEmail_Notify');
		},
	};
