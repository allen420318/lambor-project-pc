	import EventBus from 'scripts/common/EventBus';
	import CommUtility from 'scripts/common/CommUtility';

	export const initEmVi = {
	    mounted: function () {
	        EventBus.$on('phoneEmail', () => {
	            this.OpenWindow();
	        });
	    },
	    methods: {
	        // 弹出客服窗口
	        OpenNewBox: function (serveLink) {
	            window.open(serveLink, '客服', 'scrollbars=1,width=365,height=565,left=10,top=150');
	            this.CloseWindow();
	        },
	        // 開啟視窗
	        OpenWindow: function () {
	            CommUtility.WebShowUniqueForm('emailVerifyFailForm');
	        }, // end OpenWindow

	        // 關閉視窗
	        CloseWindow: function () {
	            CommUtility.WebCloseUniqueForm('emailVerifyFailForm');
	        }, // end CloseWindow

	        // 客服彈窗
	        CustomerWindow: function () {
	            this.CloseWindow();
	            // CommUtility.WebShowUniqueForm('CustomerServiceWindow');
	        }, // end  CustomerWindow
	    },
	};
