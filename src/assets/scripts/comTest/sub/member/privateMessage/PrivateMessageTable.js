	import EventBus from 'scripts/common/EventBus';
    import PersonalCenterService from 'scripts/businessLogic/personalCenterService'; 
    import Vue from 'vue';

    export const initPrMt = {
        props: {
            ReadStatusList: {
                type: Array,
                default: function () {
                    return [];
                }
            }
        },
        data() {
            return {
                FilterStatus: -1,
                MessageList: [],                
                DisplayList: [],
                CurrentPage: undefined,
                CurrentReadingIndex: undefined,
                CurrentReadingItem: undefined
            };
        },
        created: function () {
            const self = this;
            // 監聽更新列表 (2017/12/21 修改過)
            EventBus.$on('PrivateMessage_UpdateList', data => {
                self.FilterStatus = -1;
                self.CurrentPage = 1;
                self.MessageList = data;
                this.DisplayList = [];
                // *************************************************************************
                self.DisplayList = Vue.util.extend({}, self.MessageList.slice(0, 10));          
                EventBus.$emit('Paginate_ResetPageNo');            
            });
            
            // 監聽更新頁數 (2017/12/21新增)
            EventBus.$on('PrivateMessage_UpdatePageNo', data => {                
                const pageNo = data;
                self.CurrentPage = data;
                self.DisplayList = [];
                const tempList = this.UpdateDisplayList();
                self.DisplayList = Vue.util.extend({}, tempList.slice(0 + (pageNo - 1) * 10, 10 + (pageNo - 1) * 10));                          
            });
            // 刪除特定項目
            EventBus.$on('PrivateMessage_DeleteItem', () => {    
                // 刪除總列表項目
                const ListIndex = self.MessageList.indexOf(self.CurrentReadingItem);
                self.MessageList.splice(ListIndex, 1);
                
                // 刪除顯示列表項目
                self.DisplayList = $.map(self.DisplayList, function (value) {
                    return [value];
                });            
                self.DisplayList.splice(self.CurrentReadingIndex, 1);

                // 刪除項目為本頁最後一筆, 則跳回前一頁
                if (self.DisplayList.length == 0 && self.CurrentPage > 1) {
                    const tempList = self.UpdateDisplayList();
                    EventBus.$emit('Paginate_ResetPageNo', self.CurrentPage - 1);       
                    self.$parent.PageCount = Math.ceil(tempList.length / self.$parent.PageSize);     
                    self.DisplayList = Vue.util.extend({}, tempList.slice(0, 10));   
                }
            });
        },
        methods: {
            ShowDetail: async function (MessageData, index) {       
                this.CurrentReadingIndex = index;                       
                this.CurrentReadingItem = MessageData;
                if (MessageData.ReadStatus.Value == 2) {                    
                    // update read status
                    const inputObj = {
                        PrivateMessageID: MessageData.ID
                    };
                    await PersonalCenterService.PrivateMessage_LoadDetailPage(inputObj);
                    MessageData.ReadStatus.Value = 1;                    
                    if (this.FilterStatus == 2) {
                        this.DisplayList = $.map(this.DisplayList, function (value) {
                            return [value];
                        });
                        this.DisplayList.splice(index, 1);
                    }
                }

                EventBus.$emit('PrivateMessage_UpdateDetail', MessageData);
                this.$parent.SwitchMode(2);
            },
            UpdateDisplayList: function () {
                const tempList = [];
                for ( let i = 0; i < this.MessageList.length; i++) {
                    if (this.MessageList[i].ReadStatus.Value == this.FilterStatus || this.FilterStatus == -1) {
                        tempList.push(this.MessageList[i]);
                    } // end if
                }
                return tempList;
            }
        },
        // WATCH 部分於 2017/12/21新增
        watch: {
            FilterStatus: function () {                
                self.CurrentPage = 1;
                const tempList = this.UpdateDisplayList();
                // ***********************************************************************************************
                // 更新頁數
                EventBus.$emit('Paginate_ResetPageNo');       
                this.$parent.PageCount = Math.ceil(tempList.length / this.$parent.PageSize);     
                this.DisplayList = Vue.util.extend({}, tempList.slice(0, 10));                  
            },
        },
        beforeDestroy() {
            EventBus.$off('PrivateMessage_UpdateList');
            EventBus.$off('PrivateMessage_UpdatePageNo');
            EventBus.$off('PrivateMessage_DeleteItem');
        }
    };