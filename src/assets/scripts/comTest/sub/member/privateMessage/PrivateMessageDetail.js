	import EventBus from 'scripts/common/EventBus';
    import CommUtility from 'scripts/common/CommUtility';
    import PersonalCenterService from 'scripts/businessLogic/personalCenterService'; 

    export const initPrMd = {
        data() {
            return {
                CommUtility: CommUtility,
                MessageData: {
                    Type: {}
                }
            };
        },
        created: function () {
            const self = this;
            EventBus.$on('PrivateMessage_UpdateDetail', data => {
                self.MessageData = data;
            });            
        },
        methods: {
            ReturnList: function () {
                this.MessageData = { Type: {} };
                this.$parent.SwitchMode(1);
            },
            TriggerDeleteWindow: function () {
                CommUtility.WebShowUniqueForm('DeleteMessageForm');
            },
            ConfirmDelete: async function () {
                CommUtility.WebCloseUniqueForm('DeleteMessageForm');
                // delete this message
                const inputObj = {
                    PrivateMessageID: this.MessageData.ID
                };

                const data = await PersonalCenterService.PrivateMessage_Del(inputObj);

                if (data.Ret == 0) {     
                    // 通知列表重新取得資料       
                    EventBus.$emit('PrivateMessage_DeleteItem');        
                    this.ReturnList();
                } else {
                    const notifyData = {
                        NotifyMessage: data.Message
                    };
                    EventBus.$emit('showNotifyMessage', notifyData);
                }
            }
        },
        beforeDestroy() {
            EventBus.$off('PrivateMessage_UpdateDetail');
        }
    };