
    import Vue from 'vue';
    import EventBus from 'scripts/common/EventBus';
    import Regex from 'scripts/common/CommDef';
    import BLDef from 'scripts/common/BLDef';
    import CommUtility from 'scripts/common/CommUtility';
    import PersonalCenterService from 'scripts/businessLogic/personalCenterService';
    import CommonService from 'scripts/businessLogic/commonService';

	export const initNet = {
		data() {
            return {
                ProcessingFlag: false,
                DropDowning: '',
                Reg: Regex.Regex,
                BankData: {
                    CardName: '',
                    BankName: '',
                    BranchName: '',
                    BankAcct: '',
                    Province: {},
                    Town: {},
                    Dist: {},
                },
                OrgBankData: {},
                ProvincesList: [],
                TownsList: [],
                DistsList: [],
                DefaultDistsItem: {},
                DefaultDistsList: [],
                DropDownInvalid: false
            };
        },
        created: function () {
            this.BindingEvent();
            this.LoadPage();
        },
        mounted: function () {
            CommUtility.BindEnterTrigger('bankInfoForm', this.UpdateBankData);
        },
        methods: {
            BindingEvent: function () {
                document.addEventListener('click', this.HideDropDown);
            },
            // 顯示紀錄類型選單
            ShowDropDown: function (targetClass) {
                $('.down_box').slideUp(100);
                if (this.DropDowning !== targetClass) {
                    $('.' + targetClass).slideDown(300);
                    this.DropDowning = targetClass;
                } else {
                    $('.' + targetClass).slideUp(100);
                    this.DropDowning = '';
                }
            },
            // 隱藏紀錄類型選單
            HideDropDown: function (event) {
                const target = $(event.target);
                if (target.closest('.d_course').length === 0) {
                    $('.down_box').slideUp(100);
                    this.DropDowning = '';
                }
            },
            // 載入畫面
            LoadPage: async function () {
                const data = await PersonalCenterService.BankInfo_LoadMainPage();
                if (data.Data.BankAcct.Province.Name == '') {
                    data.Data.BankAcct.Province = Vue.util.extend({}, data.Data.ProvinceSelectList[0]);
                }
                if (data.Data.BankAcct.Town.Name == '') {
                    data.Data.BankAcct.Town = Vue.util.extend({}, data.Data.TownSelectList[0]);
                }
                if (data.Data.BankAcct.Dist.Name == '') {
                    data.Data.BankAcct.Dist = Vue.util.extend({}, data.Data.DistSelectList[0]);
                }
                this.BankData = Vue.util.extend({}, data.Data.BankAcct);
                this.OrgBankData = Vue.util.extend({}, data.Data.BankAcct);
                this.ProvincesList = data.Data.ProvinceSelectList;
                this.TownsList = Vue.util.extend([], data.Data.TownSelectList);
                this.DistsList = Vue.util.extend([], data.Data.DistSelectList);
                this.DefaultDistsItem = Vue.util.extend({}, data.Data.DistSelectList[0]);
                this.DefaultDistsList = [this.DefaultDistsItem];
            },
            // 選擇省分
            ChangeProvince: async function (selectProvince) {
                const inputObj = {
                    SType: BLDef.SelectTypesDef.CITIES,
                    InputValues: [selectProvince.Value]
                };
                const data = await CommonService.GetCityTypes(inputObj);
                if (data.Ret === 0 ) {
                    this.BankData.Province = selectProvince;
                    this.TownsList = data.Data.SelectList;
                    this.BankData.Town = data.Data.SelectList[0];
                    this.DistsList = this.DefaultDistsList;
                    this.BankData.Dist = this.DefaultDistsItem;
                }
            },
            // 選擇城市
            ChangeTown: async function (selectTown) {
                const inputObj = {
                    SType: BLDef.SelectTypesDef.DISTS,
                    InputValues: [selectTown.Value]
                };
                const data = await CommonService.GetDistTypes(inputObj);
                if (data.Ret === 0 ) {
                    this.BankData.Town = selectTown;
                    this.DistsList = data.Data.SelectList;
                    this.BankData.Dist = data.Data.SelectList[0];
                    if (this.DistsList.length === 1) {
                        this.DropDownInvalid = false;
                    }
                }
            },
            // 選擇區域
            ChangeDist: function (selectDist) {
                this.BankData.Dist = selectDist;
                if (selectDist.Value !== '-1') {
                    this.DropDownInvalid = false;
                }
            },
            // 檢查下拉選單合法性
            CheckDropDownInvalid: function () {
                if (!this.BankData.Province || !this.BankData.Town) {
                    return true;
                }
                if (this.BankData.Province.Value == -1 || this.BankData.Town.Value == -1) {
                    return true;
                }
                if (this.DistsList.length > 1 && (!this.BankData.Dist || this.BankData.Dist.Value == -1)) {
                    return true;
                }
                return false;
            },
            // 更新銀行資料
            UpdateBankData: async function () {
                this.DropDownInvalid = this.CheckDropDownInvalid();
                Object.keys(this.fields).forEach(key => {
                    this.fields[key].touched = true;
                });
                const result = await this.$validator.validateAll();
                if (result === false || this.DropDownInvalid === true) {
                    return;
                }

                if (this.ProcessingFlag === true) {
                    return;
                }

                this.ProcessingFlag = true;

                const inputObj = { OrgBankAcct: this.OrgBankData, BankAcct: this.BankData };

                EventBus.$emit('GlobalLoadingTrigger', true);
                const data = await PersonalCenterService.BankInfo_Mod(inputObj);
                EventBus.$emit('GlobalLoadingTrigger', false);

                this.ProcessingFlag = false;
                if (data.Ret === 0) {
                    this.OrgBankAcct = Vue.util.extend({}, this.BankData);
                    EventBus.$emit('fillTipUpdate');
                }

                const notifyData = {
                    NotifyMessage: data.Message
                };

                EventBus.$emit('showNotifyMessage', notifyData);
            }
        }
    };