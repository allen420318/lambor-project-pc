// import Vue from 'vue';
import EventBus from 'scripts/common/EventBus';
// import BLDef from 'scripts/common/BLDef';
import Regex from 'scripts/common/CommDef';
import CommUtility from 'scripts/common/CommUtility';
import PersonalCenterService from 'scripts/businessLogic/personalCenterService';
// import CommonService from 'scripts/businessLogic/commonService';

export const bankCurrency = {
    data() {
        return {
            Reg: Regex.Regex,
            CurrencyData: {
                ReceiptAddress: ''
            },

            Address: ''
        };
    },
    created: function () {
        this.LoadPage();
    },
    mounted: function () {
        CommUtility.BindEnterTrigger('currencyForm', this.UpdateBankData);
    },
    methods: {
        // 載入畫面
        LoadPage: async function () {
            const retData = await PersonalCenterService.MemberCurrencyMerchant_LoadMainPage();
            if (retData.Ret === 0) {
                this.Address = retData.Data.VirtualAcct.ReceiptAddress;
                this.CurrencyData.ReceiptAddress = retData.Data.VirtualAcct.ReceiptAddress;
            }
        },
        // 修改會員銀行帳戶資料
        UpdateCurrencyData: async function () {
            const dataObj = {
                OrgVirtualAcct: { MerchantName: 'Sunny Pay', MerchantWeb: 'https://wallet.sunnypay.io', ReceiptAddress: this.CurrencyData.ReceiptAddress },
                VirtualAcct: { MerchantName: 'Sunny Pay', MerchantWeb: 'https://wallet.sunnypay.io', ReceiptAddress: this.Address },
            };

            // 收款地址不能为空 2019.04.22 SeyoChen
            if (this.Address === '') {
                const notifyData = {
                    NotifyMessage: '收款地址不能为空'
                };
                EventBus.$emit('showNotifyMessage', notifyData);
                return;
            }
            
            EventBus.$emit('GlobalLoadingTrigger', true);
            const data = await PersonalCenterService.MemberCurrencyMerchant_Mod(dataObj);
            EventBus.$emit('GlobalLoadingTrigger', false);

            // 2019.04.17 SeyoChen
            const notifyData = {
                NotifyMessage: data.Message
            };
            EventBus.$emit('showNotifyMessage', notifyData);
            return;
        }
    }
};