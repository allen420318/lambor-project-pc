import BLDef from 'scripts/common/BLDef';
import MemberBettingRecordDetail from '@/components/sub/member/transactionRecord/memberBettingRecordDetail';
import PersonalCenterService from 'scripts/businessLogic/personalCenterService';
import EventBus from 'scripts/common/EventBus';

export const initBeRa = {
    props: {
        RecordList: {
            type: Array,
            default: function () {
                return [];
            }
        },
        RecordTime: {
            default: function () {
                return 'YYYY-mm-dd HH:mm:ss';
            }
        },
        QueryType: {
            default: function () {
                return '';
            }
        }
    },

    data () {
        return {
            BettingRecord: undefined,
            ValueList: [],
            BettingMoreDetials: []
        };
    },

    methods: {
        // 时间格式化函数，此处仅针对yyyy-MM-dd hh:mm:ss 的格式进行格式化
        dateFormat: function (time) {
            var date = new Date(time);
            var year = date.getFullYear();
            /* 在日期格式中，月份是从0开始的，因此要加0
             * 使用三元表达式在小于10的前面加0，以达到格式统一  如 09:11:05
             * */
            var month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
            var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
            var hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
            var minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
            var seconds = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
            // 拼接
            // return year+"-"+month+"-"+day+" "+hours+":"+minutes+":"+seconds;
            return year + '-' + month + '-' + day;
        },

        // 判別數字正負並改變顏色
        SetAmtStyle: function (Amount) {
            if (Amount < 0) {
                return {
                    color: 'rgb(255, 0, 0)'
                };
            }
            return {};
        }, // end SetAmtStyle

        // 顯示詳細
        ShowDetail: async function (log) {
            const self = this;
            const dataObj = {
                ID: log.ID,
                BetDateTime: log.BetDateTime,
                BetStartDateTime: self.dateFormat(self.RecordTime.StartTime) + ' 00:00:00',
                BetEndDateTime: self.dateFormat(self.RecordTime.EndTime) + ' 23:59:59'
            };
            console.log(dataObj)
            EventBus.$emit('GlobalLoadingTrigger', true);
            const retData = await PersonalCenterService.BetLog_LoadDetailPage(dataObj);
            EventBus.$emit('GlobalLoadingTrigger', false);
            console.log(retData)
            if (retData.Ret === BLDef.ErrorRetType.SUCCESS) {
                self.BettingRecord = retData.Data.Detail;

                self.BettingMoreDetials = retData.Data.MoreDetials;
                const list = retData.Data.MoreDetials;
                let isValueList;
                console.log(list);
                for (let i = 0; i < list.length; i++) {
                    if (list[i].Name == '投注详细') {
                        list[i].Value = JSON.parse(list[i].Value);
                        for (let j = 0; j < list[i].Value.length; j++) {
                            isValueList = false;
                            for (let z = 0; z < self.ValueList.length; z++) {
                                if (list[i].Value[j].Key == self.ValueList[z]) {
                                    isValueList = true;
                                }
                            }
                            if (!isValueList) {
                                self.ValueList.push(list[i].Value[j].Key);
                            }
                        }
                    }
                }

                // MemberBettingRecordDetail.methods.OpenWindow();
                EventBus.$emit('recordDetail');
            } else {
                const notifyData = {
                    NotifyMessage: retData.Message
                };
                EventBus.$emit('showNotifyMessage', notifyData);
            } // end if
        }, // end ShowDetail
    },

    components: {
        MemberBettingRecordDetail,
    },
    mounted () {
        // 滚动条
        $('#record1-scroll').perfectScrollbar();
    }
};
