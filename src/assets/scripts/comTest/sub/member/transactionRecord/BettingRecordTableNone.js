import BLDef from 'scripts/common/BLDef';    
import MemberBettingRecordDetail from '@/components/sub/member/transactionRecord/memberBettingRecordDetail';
import PersonalCenterService from 'scripts/businessLogic/personalCenterService';
import EventBus from 'scripts/common/EventBus';

export const initReTn = {
    props: {
        RecordList: {
            type: Array,
            default: function () {
                return [];
            }
        }
    },

    data() {
        return {
            BettingRecord: undefined,
        };
    },

    methods: {

        // 判別數字正負並改變顏色
        SetAmtStyle: function (Amount) {
            if (Amount < 0) {
                return { color: 'rgb(255, 0, 0)' };
            }
            return {};
        }, // end SetAmtStyle 

        // 顯示詳細
        ShowDetail: async function ( log) {
            const dataObj = {
                ID: log.ID,
                BetDateTime: log.BetDateTime,
                NoSettled: true
            }; 
            const retData = await PersonalCenterService.BetLog_LoadDetailPage(dataObj);
            if ( retData.Ret === BLDef.ErrorRetType.SUCCESS) {
                this.BettingRecord = retData.Data.Detail;
                // MemberBettingRecordDetail.methods.OpenWindow();
                EventBus.$emit('recordDetail');
            } else {
                const notifyData = {
                    NotifyMessage: retData.Message
                };
                EventBus.$emit('showNotifyMessage', notifyData);
            } // end if
        }, // end ShowDetail 
    },

    components: {
        MemberBettingRecordDetail,
    },
    mounted() {
		// 滚动条
		$('#record1-scroll').perfectScrollbar();
	}
};
