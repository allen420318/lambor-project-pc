
    import CommUtility from 'scripts/common/CommUtility';
    import PersonalCenterService from 'scripts/businessLogic/personalCenterService';

    export const initRdRe = {
        data() {
            return {
                TransType: {
                    TransType_Deposit: 1,
                    TransType_EWallet: 2,
                    TransType_BonusActivity: 3,
                    TransType_RakeSettled: 4,
                    TransType_Withdraw: 5
                },
                ColumnNameList: []
            };
        },
        props: {
            RakeSettleDetail: {
                type: Object,
                default: function () {
                    return {};
                }
            },
            RecordCondition: {
                type: Object,
                default: function () {
                    return {};
                }
            },
        },
        methods: {
            ExportDetail: async function () {
                const DLDate = new Date();
                const TimeZone = DLDate.getTimezoneOffset() / (-60);
                const inputObj = this.RecordCondition;
                inputObj.TimeDiff = TimeZone;
                await PersonalCenterService.CashFlowLog_ExportDetail('返水发放', inputObj);
            },
            CloseDetail: function () {
                CommUtility.WebCloseUniqueForm('RecordDetailRakeForm');
            }
        },
        watch: {
            RakeSettleDetail: function () {      
                this.ColumnNameList = this.RakeSettleDetail.RakeList[0].DetailList;
                CommUtility.WebShowUniqueForm('RecordDetailRakeForm');
            }
        }
    };
