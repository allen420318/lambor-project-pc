import CommUtility from 'scripts/common/CommUtility';
import EventBus from 'scripts/common/EventBus';
export const initBeRd = {

    props: {
        // 投注記錄詳細資料
        BettingRecord: {
            type: Object,
            default: function () {
                return {};
            },           
        },
        BettingMoreDetials: {
            default: function () {
                return [];
            },
        },
        ValueList: {
            default: function () {
                return [];
            },
        },
        QueryType: {
            default: function () {
                return '';
            }
        },
        BettingType: undefined     
    },

    data() {
        return {
            BetType: undefined,
            GameResult: undefined,
        };               
    },
    mounted: function () {
        EventBus.$on('recordDetail', () => {                  
            this.OpenWindow();
        }); 
    },
    methods: {
        // 判別數字正負並改變顏色
        SetAmtStyle: function (Amount) {
            if (Amount < 0) {
                return { color: 'rgb(255, 0, 0)' };
            }
            return {};
        }, // end SetAmtStyle

        // 顯示投注類型和開牌結果
        ShowBettingAndResult: function (bettingType) {
            switch (bettingType) {
                case this.TransType.TransType_Deposit:
                case this.TransType.TransType_RakeSettled:
                case this.TransType.TransType_Withdraw:
                    return true;
                default:
                    return false;
            }
        }, // end ShowBettingAndResult

        // 開啟視窗
        OpenWindow: function () {
            CommUtility.WebShowUniqueForm('BettingRecordDetailForm');
        }, // end OpenWindow
        
        // 關閉彈窗
        CloseWindow: function () {            
            CommUtility.WebCloseUniqueForm('BettingRecordDetailForm');
        }, // end CloseWindow          
    },

    watch: {
        // 開啟彈窗
        BettingRecord: function () {      
            this.BetType = this.BettingRecord.BetType;
            this.GameResult = this.BettingRecord.GameResult;
        }, // end BettingRecord
    } 
};