
    import CommUtility from 'scripts/common/CommUtility';

    export const initRdNl = {
        data() {
            return {
                TransType: {
                    TransType_Deposit: 1,
                    TransType_EWallet: 2,
                    TransType_BonusActivity: 3,
                    TransType_RakeSettled: 4,
                    TransType_Withdraw: 5,
                    TransType_DepositReturn: 6,
                    TransType_AdjustBalance: 7,
                    TransType_Authorize:8
                },
            };
        },
        props: {
            RecordType: undefined,
            RakeSettleDetail: {
                type: Object,
                default: function () {
                    return {};
                }
            },
            OtherRecordDetail: {
                type: Object,
                default: function () {
                    return {};
                }
            }
        },
        methods: {
            ShowFee: function (inputTransType) {
                switch (inputTransType) {                    
                    case this.TransType.TransType_RakeSettled:
                    case this.TransType.TransType_Withdraw:
                        return true;
                    default:
                        return false;
                }
            },
            CloseDetail: function () {
                CommUtility.WebCloseUniqueForm('RecordDetailNormalForm');
            }
        },
        watch: {
            OtherRecordDetail: function () {      
                CommUtility.WebShowUniqueForm('RecordDetailNormalForm');
            }
        }
    };
