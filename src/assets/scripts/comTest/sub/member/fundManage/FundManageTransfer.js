import Vue from 'vue';
import EventBus from 'scripts/common/EventBus';
import Regex from 'scripts/common/CommDef';
import CommUtility from 'scripts/common/CommUtility';
import PersonalCenterService from 'scripts/businessLogic/personalCenterService';

export const initFmTr = {
    props: {
        GameVendorList: {
            type: Array,
            default: function () {
                return [];
            }
        }
    },
    data() {
        return {
            Reg: Regex.Regex,
            Submitted: false,
            ProcessingFlag: false,
            InitialFormData: undefined,
            CurrentGameVendorList: [],
            FromAPIVendorIDIndex: undefined,
            ToAPIVendorIDIndex: undefined,
            TransferData: {
                FromAPIVendorID: undefined,
                ToAPIVendorID: undefined,
                Amount: ''
            }
        };
    },
    created: function () {
        this.InitialFormData = Vue.util.extend({}, this.TransferData);
    },
    mounted: function () {
        CommUtility.BindEnterTrigger('transform', this.PostTransfer);
    },
    methods: {
        // 選取轉出帳戶
        SelectFromAPIVendorID: function (index, vendorID, IsMaintaining) {
            if (IsMaintaining === '702') {
                this.FromAPIVendorIDIndex = index;
                this.TransferData.FromAPIVendorID = vendorID;
                if (this.TransferData.ToAPIVendorID == vendorID) {
                    this.TransferData.ToAPIVendorID = undefined;
                }
            }
        },
        // 選取轉入帳戶
        SelectToAPIVendorID: function (index, vendorID, IsMaintaining) {
            if (IsMaintaining === '702') {
                this.ToAPIVendorIDIndex = index;
                this.TransferData.ToAPIVendorID = vendorID;
                if (this.TransferData.FromAPIVendorID == vendorID) {
                    this.TransferData.FromAPIVendorID = undefined;
                }
            }
        },
        // 進行轉帳
        PostTransfer: async function () {
            Object.keys(this.fields).forEach(key => {
                this.fields[key].touched = true;
            });
            this.Submitted = true;
            const result = await this.$validator.validateAll();
            if (result === false || this.TransferData.FromAPIVendorID === undefined || this.TransferData.ToAPIVendorID === undefined) {
                return;
            }
            if (this.ProcessingFlag === true) {
                return;
            }

            this.ProcessingFlag = true;

            const inputObj = this.TransferData;

            EventBus.$emit('GlobalLoadingTrigger', true);
            const data = await PersonalCenterService.EWalletTransfer_Transfer(inputObj);
            EventBus.$emit('GlobalLoadingTrigger', false);

            this.ProcessingFlag = false;

            const notifyData = {
                NotifyMessage: data.Message
            };

            if (data.Ret == 0) {
                this.ResetForm();
                notifyData.CloseFunction = this.UpdateBalance;
            }

            // 彈窗訊息
            EventBus.$emit('showNotifyMessage', notifyData);
        },
        // 重置選項與金額
        ResetForm: function () {
            this.Submitted = false;
            this.TransferData = Vue.util.extend({}, this.InitialFormData);
            this.$validator.reset();
        },
        // 更新查詢餘額
        UpdateBalance: function () {
            EventBus.$emit('memberHeaderUpdateBalance');
        }
    },
    watch: {
        GameVendorList: function () {
            this.CurrentGameVendorList = this.GameVendorList;
            this.ResetForm();
        },
    }
};