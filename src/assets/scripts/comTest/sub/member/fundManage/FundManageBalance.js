import EventBus from 'scripts/common/EventBus';
import CommUtility from 'scripts/common/CommUtility';
import PersonalCenterService from 'scripts/businessLogic/personalCenterService';

export const initFmba = {
    props: {
        GameVendorList: {
            type: Array,
            default: function () {
                return [];
            }
        }
    },
    data () {
        return {
            tmpSummaryList: [],
            CurrentGameVendorList: [],
            SummaryAmount: 0,
            SummaryFlag: false,
            AvailableVendorCount: 0,
            CompleteVendorCount: 0,
            GameVendorListObj: [],
        };
    },
    methods: {
        // 取得遊戲商餘額
        BalanceInit: function (vendorIndex) {
            this.CurrentGameVendorList[vendorIndex].BalanceCompleted = false;
            if (this.CurrentGameVendorList[vendorIndex].IsMaintaining !== '702') {
                this.tmpSummaryList[vendorIndex].Amount = this.$t('message.top_under_maintenance');
                this.CurrentGameVendorList[vendorIndex].VAmount = this.$t('message.top_under_maintenance');
                this.CurrentGameVendorList[vendorIndex].BalanceCompleted = true;
            } else {
                this.AvailableVendorCount += 1;
                this.GetBalance(vendorIndex, this.CurrentGameVendorList[vendorIndex].GameAPIVendor);
            }
        },
        GetBalance: async function (vendorIndex, vendorID) {
            const inputObj = {
                APIVendorID: vendorID
            };
            console.log(inputObj);
            const data = await PersonalCenterService.EWalletTransfer_GetBalance(inputObj);
            this.CompleteVendorCount += 1;
            console.log(data);
            if (data.Ret == 0) {
                this.SummaryFlag = true;
                this.tmpSummaryList[vendorIndex].Amount = data.Data.GameApiBalance.Amount;
                this.CurrentGameVendorList[vendorIndex].VAmount = data.Data.GameApiBalance.VAmount;
            } else {
                this.tmpSummaryList[vendorIndex].Amount = this.$t('message.top_under_maintenance');
                this.CurrentGameVendorList[vendorIndex].VAmount = this.$t('message.top_under_maintenance');
            }
            this.CurrentGameVendorList[vendorIndex].BalanceCompleted = true;

            if (this.CompleteVendorCount === this.AvailableVendorCount) {
                if (this.SummaryFlag === false) {
                    this.SummaryAmount = this.$t('message.top_under_maintenance');
                } else {
                    let tmpAmount = 0;
                    for (let i = 0; i < this.tmpSummaryList.length; i++) {
                        if ((!isNaN(parseFloat(this.tmpSummaryList[i].Amount)) && isFinite(this.tmpSummaryList[i].Amount))) {
                            tmpAmount = CommUtility.NumberAdd(tmpAmount, this.tmpSummaryList[i].Amount);
                        }
                    }
                    this.SummaryAmount = tmpAmount;
                }
                EventBus.$emit('fundGetBalanceComplete', this.SummaryAmount);
            }
        },
        SetGameVendorList: function () {
            const that = this;
            setTimeout(function () {
                that.GameVendorListObj = that.GameVendorList;
            }, 200);
        },
    },
    watch: {
        GameVendorList: function () {
            this.SetGameVendorList();
        },
        GameVendorListObj: function () {
            this.SummaryAmount = 0;
            this.SummaryFlag = false;
            this.CurrentGameVendorList = this.GameVendorListObj;
            for (let i = 0; i < this.CurrentGameVendorList.length; i++) {
                this.tmpSummaryList[i] = {};
                this.BalanceInit(i);
            }
        }
    },
    created: function () {
        this.SetGameVendorList();
    },
    mounted () {
        // 滚动条
        $('#scroll_fundBox').perfectScrollbar();
    }
};
