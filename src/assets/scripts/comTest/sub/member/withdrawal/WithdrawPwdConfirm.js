import Vue from 'vue';
import CommUtility from 'scripts/common/CommUtility';
import BLDef from 'scripts/common/BLDef';
import Regex from 'scripts/common/CommDef';
import EventBus from 'scripts/common/EventBus';
import PersonalCenterService from 'scripts/businessLogic/personalCenterService';

export const initWpCf = {
    props: {
        ApplyAmount: undefined,
        PostWithdrawData: undefined
    },
    data() {
        return {
            CommUtility: CommUtility,
            Reg: Regex.Regex,
            ProcessingFlag: false,
            InitialFormData: undefined,
            WithdrawData: {
                WithdrawPwd: '',
                WithdrawMode: undefined,
            },
            CurrentWithdrawPwdErrCnt: undefined
        };
    },
    created: function () {
        this.InitialFormData = Vue.util.extend({}, this.WithdrawData);
        const self = this;
        EventBus.$on('withdrawalPwdWindowTrigger', function (PwdErrCnt) {
            if (self.CurrentWithdrawPwdErrCnt === undefined) {
                self.CurrentWithdrawPwdErrCnt = PwdErrCnt;
            }
            self.TriggerPwdWindow();
        });
    },
    mounted: function () {
        CommUtility.BindEnterTrigger('withdrawPwdForm', this.SubmitWithdraw);
    },
    methods: {
        // 關閉視窗
        CloseWindow: function () {
            CommUtility.WebCloseUniqueForm('pwdErrorForm');
            CommUtility.WebCloseUniqueForm('withdrawPwdForm');
        }, // end CloseWindow
        // 弹出客服窗口
        OpenNewBox: function (serveLink) {
            window.open(serveLink, '客服', 'scrollbars=1,width=365,height=565,left=10,top=150');
            this.CloseWindow();
        },

        // 取款密碼彈窗
        TriggerPwdWindow: function () {
            if (this.CurrentWithdrawPwdErrCnt >= 3) {
                CommUtility.WebShowUniqueForm('pwdErrorForm');
            } else {
                this.ResetPwdForm();
                CommUtility.WebShowUniqueForm('withdrawPwdForm');
            }
        },
        // 發送取款申請
        SubmitWithdraw: async function () {
            Object.keys(this.fields).forEach(key => {
                this.fields[key].touched = true;
            });
            const result = await this.$validator.validateAll();
            if (result === false) {
                return;
            }

            if (this.ProcessingFlag === true) {
                return;
            }

            this.ProcessingFlag = true;
            this.WithdrawData.Amt = this.ApplyAmount;
            this.WithdrawData.WithdrawMode = parseInt(this.PostWithdrawData);

            EventBus.$emit('GlobalLoadingTrigger', true);
            const data = await PersonalCenterService.WithdrawApply_Add(this.WithdrawData);
            EventBus.$emit('GlobalLoadingTrigger', false);

            this.ProcessingFlag = false;

            const notifyData = {
                NotifyMessage: data.Message
            };

            if (data.Ret !== 0) {
                // 取款密碼錯誤
                if (data.Ret == BLDef.ErrorRetType.COMM_PWD_CONFIRM_FAIL) {
                    this.CurrentWithdrawPwdErrCnt = data.Data.PwdErrCnt;
                    if (data.Data.PwdErrCnt >= 3) {
                        this.ResetPwdForm();
                        CommUtility.WebCloseUniqueForm('withdrawPwdForm');
                        CommUtility.WebShowUniqueForm('pwdErrorForm');
                    }
                } else { // 其他錯誤
                    this.ResetPwdForm();
                    CommUtility.WebCloseUniqueForm('withdrawPwdForm');
                    EventBus.$emit('showNotifyMessage', notifyData);
                }
            } else {
                // 成功
                this.ResetPwdForm();
                CommUtility.WebCloseUniqueForm('withdrawPwdForm');
                notifyData.NotifySubMessage = '您好，您的申请已经提交，我们会尽快处理，请耐心等待！';
                notifyData.CloseFunction = this.WithdrawalReset;
                EventBus.$emit('showNotifyMessage', notifyData);
                // EventBus.$emit('memberHeaderUpdateBalance');
            }
        },
        // 重置取款密碼表單
        ResetPwdForm: function () {
            this.WithdrawData = Vue.util.extend({}, this.InitialFormData);
            this.$validator.reset();
        },
        // 取款功能初始
        WithdrawalReset: function () {
            this.$parent.$parent.$parent.SwitchMode(1);
        }
    },
    beforeDestroy() {
        EventBus.$off('withdrawalPwdWindowTrigger');
    }
};
