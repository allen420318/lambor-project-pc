
    import Regex from 'scripts/common/CommDef';
    import EventBus from 'scripts/common/EventBus';
    import CommUtility from 'scripts/common/CommUtility';
    import PersonalCenterService from 'scripts/businessLogic/personalCenterService';
    import MemberWithdrawPwdConfirm from '@/components/sub/member/withdrawal/memberWithdrawPwdConfirm';

    export const initWdAf = {
        data() {
            return {
                WithdrawData: {},
                Reg: Regex.Regex,
                ApplyAmount: undefined,
                RealMinLimit: undefined,
                RealVMinLimit: undefined,
                RealMaxLimit: undefined,
                RealVMaxLimit: undefined,
                PostWithdrawData: undefined                
            };
        },
        created: function () {
            this.GetWithdrawData();
        },
        mounted: function () {
            CommUtility.BindEnterTrigger('PersonalWithdrawForm', this.WithdrawNextStep);
        },
        methods: {
            // 關閉視窗
            CloseWindow: function () {
                CommUtility.WebCloseUniqueForm('widthdrawData');
            }, // end CloseWindow  

            // 選擇支付方式
            SelectPayType: function (type, key) {
                // 2019.04.18 SeyoChen 1. 银行入款 2: 虚拟货币
                this.PostWithdrawData = type;
                
                // 判断是否完善资料，未完善弹出消息窗口
                switch (type) {
                    case '1':
                        if (key == 'false') {
                            CommUtility.WebShowUniqueForm('widthdrawData');
                        }
                        break;
                    case '3':
                        if (key == 'false') {
                            CommUtility.WebShowUniqueForm('widthdrawData');
                        }
                        break;
                    default:
                        break;
                }
            },

            TransWidthdrawType: function (type) {
                if (type === '3') {
                    this.$router.push({ name: 'BankData', params: { Type: type } });
                } else {
                    this.$router.push({ path: 'BankData' } );
                }
            },
            GetWithdrawData: async function () {
                const data = await PersonalCenterService.WithdrawApply_GetWithdrawLimitInfo();
                if (data.Ret === 0) {
                    this.WithdrawData = data.Data;      
                    this.RealMinLimit = this.WithdrawData.MinLimitAmt;
                    this.RealVMinLimit = this.WithdrawData.VMinLimitAmt; 

                    if (this.WithdrawData.CurrBalance < this.WithdrawData.MaxLimitAmt && this.WithdrawData.CurrBalance > this.WithdrawData.MinLimitAmt) {
                        this.RealMaxLimit = this.WithdrawData.CurrBalance;
                        this.RealVMaxLimit = this.WithdrawData.VCurrBalance;
                    } else {
                        this.RealMaxLimit = this.WithdrawData.MaxLimitAmt;
                        this.RealVMaxLimit = this.WithdrawData.VMaxLimitAmt;
                    }

                    // 默认选择 已完善资料的取款方式
                    for (let i = 0; i < data.Data.AcctList.length; i++) {
                        if (data.Data.AcctList[i].Key == 'true') {
                            this.PostWithdrawData = data.Data.AcctList[i].Value;
                            return;
                        }
                    }
                }
            },
            WithdrawNextStep: async function () {
                // 判断是否已选择取款方式
                if (this.PostWithdrawData == undefined) {
                    const notifyData = {
                        NotifyMessage: '请选择取款方式',
                    };
                    EventBus.$emit('showNotifyMessage', notifyData);
                    return;
                }
                
                Object.keys(this.fields).forEach(key => {
                    this.fields[key].touched = true;
                });
                const result = await this.$validator.validateAll();
                if (result === false) {
                    return;
                }
                EventBus.$emit('withdrawalPwdWindowTrigger', this.WithdrawData.WithdrawPwdErrCnt);
            }
        },
        components: {
            MemberWithdrawPwdConfirm
        }
    };
