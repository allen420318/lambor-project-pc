
    import MemberRakeData from '@/components/sub/member/withdrawal/memberRakeData';
    import MemberWithdrawAddForm from '@/components/sub/member/withdrawal/memberWithdrawAddForm';

    export const initWdAd = {
        components: {
            MemberRakeData,
            MemberWithdrawAddForm
        }
    };
