import Vue from 'vue';
import BLDef from 'scripts/common/BLDef';
import PersonalCenterService from 'scripts/businessLogic/personalCenterService';
import MemberDepositHistory from '@/components/sub/member/withdrawal/memberDepositHistory';
import EventBus from 'scripts/common/EventBus';

export const initRkDa = {
    data() {
        return {
            RakeData: {},
            WithdrawPwdErrCnt: undefined,
            DepositHistoryData: {}
        };
    },
    created: function () {
        this.LoadRakeData();
    },
    methods: {
        // 載入洗碼量資料
        LoadRakeData: async function () {
            const inputObj = {
                WGUID: Vue.prototype.$WGUID,
                PagingInfo: {
                    PageNo: -1,
                    PageSize: -1
                }
            };
            const data = await PersonalCenterService.WithdrawApply_GetRakeAmtInfo(inputObj);
            if (data.Ret === 0) {
                this.RakeData = data.Data;
                this.DepositHistoryData = this.RakeData;
            }
        },
        // 顯示參與活動列表
        ShowDepositHistory: function () {
            EventBus.$emit('depositHist');
        }
    },
    components: {
        MemberDepositHistory
    }
};
