import BLDef from 'scripts/common/BLDef';
import CommUtility from 'scripts/common/CommUtility';
import URLService from 'scripts/common/URLService';

export const html5CoTn = {
    props: {
        PTHtml5GameList: {},
    },
    data () {
        return {
            defaultImg: 'this.src="' + this.$ResourceCDN + '/EditionImg/Lambor1.0/images/web/game/html5/Temp.png?VersionCode"'
        };
    },

    methods: {
        // 玩遊戲
        PlayPTHtml5Game: function (item) {
            const dataObj = {
                GameAPIVendor: BLDef.GameApiType.PT,
                GameCode: item.GameCode,
                GameCatlog: BLDef.GameCatlogType.ElectronicGameHtml5,
                PlayType: BLDef.IdentityType.FORMAL,
                Platform: BLDef.PlatformType.Web_PC
            };
            localStorage.setItem('PlayGamePostData', URLService.GetUrlParameterFromObj(dataObj));
            CommUtility.OpenPlayGameWindow('webPlayGame', 'PlayGamePopUpWindow');
        }, // end PlayPTHtml5Game
    },
};
