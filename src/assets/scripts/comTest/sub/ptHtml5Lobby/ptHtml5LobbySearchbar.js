import Vue from 'vue';
import GameService from 'scripts/businessLogic/gameService';
import BLDef from 'scripts/common/BLDef';
import CommUtility from 'scripts/common/CommUtility';
import EventBus from 'scripts/common/EventBus';

export const html5SaBr = {
    props: {
        GetCurrentPageData: '',
    },
    data() {
        return {
            // 載入頁面資料
            LoadMainPageDataModel: {
                FilterSList: []
            },


            // 遊戲類型過濾
            GameFilter: {
                Value: undefined
            },


            // 分頁資料
            PageInfo: {
                PageNo: 1,
                PageSize: 18,
                TotalCount: 0,
            },

        };
    },

    created: async function () {
        await this.LoadMainPage();
    },
    mounted: function () {
        CommUtility.BindEnterTrigger('H5SearchForm', this.GetPTHtml5GameList);
    },
    methods: {

        // 根據選項來過濾遊戲清單
        GetPTHtml5GameListByGameType: async function (Filter) {
            this.GameFilter = Vue.util.extend({}, Filter);
            this.Keyword = '';
            this.$validator.reset();
            // 設定在第一頁
            this.PageInfo.PageNo = 1;
            this.$parent.SearchPage = 1;
            EventBus.$emit('Paginate_ResetPageNo');
            const dataObj = {
                GameAPIVendor: BLDef.GameApiType.PT,
                Filter: this.GameFilter,
                PagingInfo: this.PageInfo,
            };
            EventBus.$emit('GlobalLoadingTrigger', true);
            const retData = await GameService.GameMobileBet_Query(dataObj);
            EventBus.$emit('GlobalLoadingTrigger', false);
            // 成功
            if (retData.Ret == 0) {
                this.$parent.PTHtml5GameList = retData.Data.GameList;

                // 更新Paging
                if (retData.Data.PagingInfo) {
                    this.PageInfo.PageCount = Math.ceil(retData.Data.PagingInfo.TotalCount / retData.Data.PagingInfo.PageSize);
                    this.$parent.PageInfo.PageCount = this.PageInfo.PageCount;
                } else {
                    this.PageInfo.PageCount = 0;
                } // end if
            }
        }, // end GetPTHtml5GameListByGameType

        // 載入頁面
        LoadMainPage: async function () {
            const dataObj = {};
            const retData = await GameService.LoadMobileBetLobbyPage(dataObj);
            // 失敗
            if (retData.Ret != 0) {
                return;
            } // end if

            // 成功
            this.LoadMainPageDataModel = retData.Data;
            this.$parent.LoadMainPageDataModel = retData.Data;

            // 撈取遊戲類別
            this.GameFilter = Vue.util.extend({}, this.LoadMainPageDataModel.FilterSList[0]);

            this.PageInfo.PageNo = 1;
            this.$parent.SearchPage = 1;

            const queryDataObj = {
                GameAPIVendor: BLDef.GameApiType.PT,
                Keyword: this.Keyword,
                Filter: this.GameFilter,
                PagingInfo: this.PageInfo,
            };

            EventBus.$emit('GlobalLoadingTrigger', true);
            const data = await GameService.GameMobileBet_Query(queryDataObj);
            EventBus.$emit('GlobalLoadingTrigger', false);

            // 成功
            if (data.Ret == 0) {
                this.$parent.PTHtml5GameList = data.Data.GameList;

                // 更新Paging
                if (data.Data.PagingInfo) {
                    this.PageInfo.PageCount = Math.ceil(data.Data.PagingInfo.TotalCount / data.Data.PagingInfo.PageSize);
                    this.$parent.PageInfo.PageCount = this.PageInfo.PageCount;
                } else {
                    this.PageInfo.PageCount = 0;
                } // end if
            }
        }, // end LoadMainPage


        // 根據選項來過濾遊戲清單
        GetPTHtml5GameListByPage: async function (page) {
            this.Keyword = '';
            this.$validator.reset();
            // 設定在第一頁
            this.PageInfo.PageNo = page;
            this.$parent.SearchPage = page;
            const dataObj = {
                GameAPIVendor: BLDef.GameApiType.PT,
                Filter: this.GameFilter,
                PagingInfo: this.PageInfo,
            };
            EventBus.$emit('GlobalLoadingTrigger', true);
            const retData = await GameService.GameMobileBet_Query(dataObj);
            EventBus.$emit('GlobalLoadingTrigger', false);
            // 成功
            if (retData.Ret == 0) {
                this.$parent.PTHtml5GameList = retData.Data.GameList;

                // 更新Paging
                if (retData.Data.PagingInfo) {
                    this.PageInfo.PageCount = Math.ceil(retData.Data.PagingInfo.TotalCount / retData.Data.PagingInfo.PageSize);
                    this.$parent.PageInfo.PageCount = this.PageInfo.PageCount;
                } else {
                    this.PageInfo.PageCount = 0;
                } // end if
            }
        }, // end GetPTHtml5GameListByPage
    },

    watch: {
        GetCurrentPageData: function () {
            if (this.Keyword == undefined || this.Keyword == '') {
                this.GetPTHtml5GameListByPage(this.GetCurrentPageData);
            } else {
                this.GetPTHtml5GameList(this.GetCurrentPageData);
            } // end if
        },
    }

};