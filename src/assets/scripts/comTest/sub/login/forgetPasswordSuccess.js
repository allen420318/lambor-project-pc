import CommUtility from 'scripts/common/CommUtility';
import EventBus from 'scripts/common/EventBus'
export const forgetPsSs = {
    mounted: function () {
        EventBus.$on('ForgetPasswordSuccessAlert', () => {
            this.OpenWindow();
        });
    },
    methods: {
        // 弹出新窗口
        OpenNewBox: function (aa) {
            return window.open(aa, 'newindow', 'height=565,width=357,top=0,left=0,toolbar=no,menubar=no,scrollbars=no,resizable=no,location=no,status=no');
        },
        // 開啟視窗
        //      OpenWindow: function () {
        //          CommUtility.WebShowUniqueForm('informationForm');
        //      }, // end OpenWindow

        // 關閉視窗
        //      CloseWindow: function () {
        //          CommUtility.WebCloseUniqueForm('informationForm');
        //      }, // end CloseWindow

        // 客服彈窗
        //      CustomerWindow: function () {
        //          this.CloseWindow();
        //          CommUtility.WebShowUniqueForm('CustomerServiceWindow');
        //      }, // end  CustomerWindow

        // 開啟視窗
        OpenWindow: function () {
            CommUtility.WebShowUniqueForm('informationForm');
        }, // end OpenWindow

        // 關閉視窗
        CloseWindow: function () {
            CommUtility.WebCloseUniqueForm('informationForm');
        }, // end CloseWindow

        // 客服彈窗
        CustomerWindow: function () {
            this.CloseWindow();
            // CommUtility.WebShowUniqueForm('CustomerServiceWindow');
        }, // end  CustomerWindow

    },
};
