import electronicContent from '@/components/general/electronic/electronicContent';

export const electronic = {
    name: 'electronic',
    components: {
        electronicContent
    }
};
