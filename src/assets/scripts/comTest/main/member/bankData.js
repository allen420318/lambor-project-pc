    import MemberBankInfo from '@/components/sub/member/bankInfo/memberBankInfo';
    import MemberCurrency from '@/components/sub/member/bankInfo/memberCurrency';
    

    export const bankInfo = {
        data() {
            return {
                ModifyType: 0
            };
        },
        components: {
            MemberBankInfo,
            MemberCurrency
        },
        mounted: function () {
            // 判断取款需要完善虚拟账号信息 SeyoChen 2019.04.18
            if (this.$route.params.Type !== undefined) {
                if (this.$route.params.Type === '3') {
                    this.SelectDataType(1);
                }
            }
        },
        methods: {
            SelectDataType: function (num) {
                this.ModifyType = num;
            }
        }
    };
