import URLService from 'scripts/common/URLService';
import EventBus from 'scripts/common/EventBus';
import CommUtility from 'scripts/common/CommUtility';
import PersonalCenterService from 'scripts/businessLogic/personalCenterService';
import MemberPaymentAliPay from '@/components/sub/memberNew/depositPayment/memberPaymentAliPay';
import MemberPaymentWebBank from '@/components/sub/memberNew/depositPayment/memberPaymentWebBank';
import MemberPaymentWeChat from '@/components/sub/memberNew/depositPayment/memberPaymentWeChat';
import memberPaymentQrcode from '@/components/sub/memberNew/depositPayment/memberPaymentQrcode';

export const initDsPy = {
    data () {
        return {
            PaymentType: undefined,
            PaymentData: {},
            ServerRspData: {},
            Vhtml: undefined,

            IsBankPay: false, // 2019.02.20
            TipMessage: undefined,
        };
    },
    mounted: function () {
        EventBus.$emit('GlobalLoadingTrigger', true);
        const self = this;
        // 监听存款页面传来消息
        // window.addEventListener('message', function (e) {
        //     if (e.origin === document.location.origin) {
        //         if (e.data && e.data.Deposit) {
        //             self.GetParams(e.data.DepositObj);
        //         } else if (!e.data) {
        //             $('#DepositLimitBox').css('display', 'block');
        //             EventBus.$emit('GlobalLoadingTrigger', false);
        //             self.TipMessage = self.$t('message.system_is_busy');
        //             CommUtility.WebShowAlertboxForm('DepositLimitBox');
        //         }
        //     } else {
        //         $('#DepositLimitBox').css('display', 'block');
        //         EventBus.$emit('GlobalLoadingTrigger', false);
        //         self.TipMessage = self.$t('message.system_is_busy');
        //         CommUtility.WebShowAlertboxForm('DepositLimitBox');
        //     }
        // });
        // this.GetParams();

        // 循环获取缓存
        self.GetPaymentData = localStorage.getItem('IdentificationNO');
        let timer = setInterval(() => {
            if (self.GetPaymentData !== null) {
                this.GetParams(self.GetPaymentData);
                clearInterval(timer);
            } else {
                self.GetPaymentData = localStorage.getItem('IdentificationNO');
            }
        }, 200);
    },
    methods: {
        // 關閉視窗
        CloseCurrentWindow: function () {
            window.close();
        },

        // 验证是否次数已经达到上限了
        // 校验存款次数
        async validateDepositNumber () {
            let params = {DepositOpertionType:1}
            const retData = await PersonalCenterService.DepositNumber(params);
            if (retData.Ret) {
            // 校验未通过弹出提示信息
                this.TipMessage = this.$t('message.deposit_limitNumber');
                CommUtility.WebShowAlertboxForm('DepositLimitBox');
                return false
            }
            return true
        },

        // 检验次数书否上限
        CheckNumLimit: async function () {
            console.log('11111111');
            let validateDepositNumber = await this.validateDepositNumber();
            console.log('000000000000000000');
            console.log(validateDepositNumber);
            // if (!validateDepositNumber) return;
            let ActivityItem = JSON.parse(localStorage.getItem('ActivityDatas'));
            const DepositObj = {
                BonusWay: ActivityItem.ActivityType,
                BonusSerialNo: ActivityItem.BonusSerialNo
            };
            const data = await PersonalCenterService.DepositActivityRelationQuery(
                DepositObj
            );

            if (data.Ret == 0) {
                if (data.Data.IsLimit) {
                    // CommUtility.WebShowAlertboxForm('depositToplimitForm');
                    this.TipMessage = this.$t('message.member_deposit_activity_limit');
                    CommUtility.WebShowAlertboxForm('DepositLimitBox');
                } else {
                    this.GetParams();
                }
            }
        },

        // 弹出客服窗口
        OpenNewBox: function (serveLink) {
            window.open(serveLink, '客服', 'scrollbars=1,width=365,height=565,left=10,top=150');
            this.CloseWindow();
        },

        // 獲取URL參數
        GetParams: async function (DepositObj) {
            if (this.$route.params.Data != undefined) {
                const HashKey = URLService.GetObjFromUrlParameter(this.$route.params.Data);
                const data = DepositObj;
                
                if (data) {
                    this.PaymentData = URLService.GetObjFromUrlParameter(data);
                    localStorage.removeItem(HashKey);
                    EventBus.$emit('GlobalLoadingTrigger', true);
                    // this.PaymentData.Device = '0';
                    // this.PaymentData.TagUrl = document.location.origin;
                    // const rspData = await PersonalCenterService.OnlineDeposit_Add(this.PaymentData);
                    const rspData = this.PaymentData;
                    EventBus.$emit('GlobalLoadingTrigger', false);
                    // rspData.Data.PType = this.PaymentData.PType;
                    console.log('000000000000000000');
                    console.log(rspData);
                    if (rspData.Ret == 0) {
                        if (rspData.Data.BankType == 1) {
                            // 二维码链接进入页面
                            this.PaymentType = 4;
                            this.ServerRspData = rspData.Data;
                            this.ServerRspData.Qrcode = rspData.Data.TargetUrl;
                        } else if (rspData.Data.BankType == 2) {
                            $('body').html(rspData.Data.TargetUrl);
                            $('form').attr('name', 'Lambor');
                            // document.Lambor.submit(); //关闭自动提交表单
                            // this.IsBankPay = true;
                            // const newwindow = window.open('', '_blank', '');
                            // newwindow.document.write('<iframe src=\'' + rspData.Data.TargetUrl + '\' id=\'Iframe_Game\' srcdoc= \'' + rspData.Data.TargetUrl + '\' frameborder=\'0\' border=\'0\' cellspacing=\'0\' style=\'width:100%;height:896px\'></iframe>');
                        } else if (rspData.Data.BankType == 3) {
                            window.location.href = rspData.Data.TargetUrl;
                        } else {
                            if (rspData.Data.TargetUrl !== '') {
                                this.Vhtml = rspData.Data.TargetUrl;
                            } else {
                                const notifyData = {
                                    NotifyMessage: this.$t('message.member_payment_error'),
                                    CloseFunction: this.CloseWindow
                                };
                                EventBus.$emit('showNotifyMessage', notifyData);
                            }
                        }
                    } else {
                        if (rspData.Message === '') {
                            rspData.Message = this.$t('message.member_payment_error');
                        }
                        const notifyData = {
                            NotifyMessage: rspData.Message,
                            CloseFunction: this.CloseWindow
                        };
                        EventBus.$emit('showNotifyMessage', notifyData);
                    }
                } else {
                    this.PaymentData = URLService.GetObjFromUrlParameter(data);
                    this.Vhtml = this.PaymentData;
                    // this.CloseWindow();
                }
            } else {
                this.CloseWindow();
            }
        },
        CloseWindow: function () {
            window.close();
        }
    },
    components: {
        MemberPaymentAliPay,
        MemberPaymentWebBank,
        MemberPaymentWeChat,
        memberPaymentQrcode
    }
};
