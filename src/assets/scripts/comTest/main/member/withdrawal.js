
    import MemberWithdrawApplyList from '@/components/sub/member/withdrawal/memberWithdrawApplyList';
    import MemberWithdrawAdd from '@/components/sub/member/withdrawal/memberWithdrawAdd';
    import PersonalCenterService from 'scripts/businessLogic/personalCenterService';
	export const initWdWl = {
		data() {
            return {
                DisplayMode: 1
            };
        },
        methods: {
            // 切換取款顯示功能
            SwitchMode: function (Mode) {
                this.DisplayMode = Mode;
                this.CWalletForWithdraw(); //点击取款，执行金额取回操作 2019.04.09
            },
            //点击取款，执行金额取回操作
            CWalletForWithdraw: async function(){
                const inputObj = {};
                await PersonalCenterService.MemberCashFlowInfo_CWalletForWithdraw(inputObj);
            }
        },
        components: {
            MemberWithdrawApplyList,
            MemberWithdrawAdd
        }
	}