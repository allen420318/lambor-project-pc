<template>
	<!--微信-->
	<div class="pay_con">
		<div class="pay_box container">
			<div class="nav_logo">
				<router-link to="/"><img :src="$ResourceCDN+'/EditionImg/Lambor33.0/images/web/frame/logo.png'" /></router-link>
			</div>
			<div class="pay_block1">
				<div class="clearfix">
					<div class="order_msg fl">
						<p class="p_1">兰博在线支付</p>
						<p class="p_2">支付方式：微信</p>
						<p class="p_2">订单号：454545</p>
					</div>
					<div class="pay_sum fr"><em>1000.00</em>元</div>
				</div>
			</div>
			<div class="pay_block2 mr_t20">
				<ul class="pay_style">
					<li><img :src="$ResourceCDN+'/EditionImg/Lambor33.0/images/web/member/pay_code1.png'">
						<p class="p_1">扫一扫二维码即可完成支付</p>
					</li>
					<li><img :src="$ResourceCDN+'/EditionImg/Lambor33.0/images/web/member/pay_method1.png'"></li>
				</ul>
			</div>
		</div>
	</div>
</template>