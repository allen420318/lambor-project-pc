


    import { popup } from '@/assets/scripts/customJs/popup';
	import EventBus from 'scripts/common/EventBus';
    import GameService from 'scripts/businessLogic/gameService';
    import MemberFundManageBalance from '@/components/sub/member/fundManage/memberFundManageBalance';
    import MemberFundManageTransfer from '@/components/sub/member/fundManage/memberFundManageTransfer';

    export const initManage = {
        data() {
            return {
                GameVendorList: []
            };
        },
        mounted: function () {
            this.GetVendorList();
            const self = this;
            EventBus.$on('updateFundBalance', () => {
                self.GetVendorList();
            });
        },
        methods: {
            GetVendorList: async function () {
                const data = await GameService.GetGameMaintainStatus();

                if (data.Ret == 0) {
                    const tmpGameVendorList = data.Data.GameAPIVendorMaintainStatusList;
                    for (let i = 0; i < tmpGameVendorList.length; i++) {
                        tmpGameVendorList[i].VAmount = undefined; // 余额默认为undefined
                        tmpGameVendorList[i].BalanceCompleted = false; // 加载中icon显示
                    }
                    this.GameVendorList = tmpGameVendorList;
                }
            }
        },
        components: {
            MemberFundManageBalance,
            MemberFundManageTransfer
        },
        beforeDestroy() {
            EventBus.$off('updateFundBalance');
        }
    };

