<template>
	<!--会员等级-->
	<div class="level-content">
		<h2 class="p_title clearfix"><span>个人中心 / <i>个人资料</i></span><router-link to="/Member" class="return perbtn fr"><img :src="$ResourceCDN+'/EditionImg/Lambor33.0/images/web/member/return.png'"/>返回</router-link></h2>
		<div class="public-inside">
			<h3 class="p-i-title"><span>个人信息</span></h3>
			<div class="msg_cont_block mr_t20 clearfix">
				<ul class="msg_cont fl">
					<li class="first_li clearfix">
						<div class="fl">
							账号：<em>teenageer236</em>
						</div>
						<div class="fl" style="margin-left: 350px;">
							注册时间：<em>2018/03/10</em>
						</div>
					</li>
					<li class="level_li clearfix">
						<span class="fl">会员等级：</span>
						<div class="level_rt fl">
							<em>普通会员</em>
							<b>（距下一等级还需111洗码量或100个有效推广会员）</b>
						</div>
					</li>
					<li class="clearfix">
						<div class="fl">
							当前有效推广人数：<em>566565</em>
						</div>
						<div class="fl" style="margin-left: 308px;">
							当前累积洗码量：<em>3243423</em>
						</div>
					</li>
				</ul>
			</div>
			<div class="section-level mr_t30 clearfix">
				<i class="level-coordinate"></i>
				<!--<div class="level-line-bg">  </div>-->
				<div class="level-line"></div>
				<div class="level-line-light"></div>
				<dl class="level-tab clearfix">
					<dd>
						<p>普通会员</p><b>0</b></dd>
					<dd>
						<i></i>
						<p>青铜会员</p><b>600</b></dd>
					<dd>
						<i></i>
						<p>白银会员</p><b>1800</b></dd>
					<dd>
						<i></i>
						<p>黄金会员</p><b>3600</b></dd>
					<dd>
						<i></i>
						<p>白金会员</p><b>6000</b></dd>
					<dd>
						<i></i>
						<p>钻石会员</p><b>10800</b></dd>
					<dd>
						<i></i>
						<p>富豪</p><b>32400</b></dd>
					<dd>
						<i></i>
						<p>大富豪</p><b>46800</b></dd>
					<dd style="margin-right: 0;">
						<p>富可敌国</p><b>100000</b></dd>
				</dl>
			</div>
			<div class="member-surface mr_t20">
				<h3 class="p-i-title"><span>等级特权</span></h3>
				<p>兰博国际根据洗码量或有效的推广会员人数，将会员共分为以下9种不同的级别。不同级别的会员享受各种优惠活动的权限和获得优惠红利的比率不同；会员级别越高，所能享受到的优惠幅度越大，具体如下：</p>
				<table cellpadding="0" cellspacing="0" class="member-tabs mr_t10">
					<thead>
						<tr>
							<th rowspan="2">会员等级</th>
							<th colspan="2">在线支付</th>
							<th colspan="2">公司入款</th>
						</tr>
						<tr>
							<th>首存红利</th>
							<th>续存红利</th>
							<th>首存红利</th>
							<th>续存红利</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>普通会员</td>
							<td>3%</td>
							<td>3%</td>
							<td>3%</td>
							<td>3%</td>
						</tr>
						<tr>
							<td>白银会员</td>
							<td>4%</td>
							<td>4%</td>
							<td>4%</td>
							<td>4%</td>
						</tr>
						<tr>
							<td>黄金会员</td>
							<td>5%</td>
							<td>5%</td>
							<td>5%</td>
							<td>5%</td>
						</tr>
						<tr>
							<td>白金会员</td>
							<td>6%</td>
							<td>6%</td>
							<td>6%</td>
							<td>6%</td>
						</tr>
						<tr>
							<td>钻石会员</td>
							<td>7%</td>
							<td>7%</td>
							<td>7%</td>
							<td>7%</td>
						</tr>
						<tr>
							<td>富翁</td>
							<td>8%</td>
							<td>8%</td>
							<td>8%</td>
							<td>8%</td>
						</tr>
						<tr>
							<td>大富翁</td>
							<td>9%</td>
							<td>9%</td>
							<td>9%</td>
							<td>9%</td>
						</tr>
						<tr>
							<td>富可敌国</td>
							<td>10%</td>
							<td>10%</td>
							<td>10%</td>
							<td>10%</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</template>