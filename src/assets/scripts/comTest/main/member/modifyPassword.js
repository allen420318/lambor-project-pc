
    import ModifyPassword from '@/components/sub/member/modifyPassword/memberModifyPassword';
	export const initPsDd = {
		data() {
            return {
                ModifyType: 2,
                BasicData: {}
            };
        },
        methods: {
            SwitchMode: function (mode) {
                this.ModifyType = mode;
            }
        },
        components: {
            ModifyPassword        
        }
	}
