import moment from 'moment';
import BLDef from 'scripts/common/BLDef';
import CommUtility from 'scripts/common/CommUtility';
import PersonalCenterService from 'scripts/businessLogic/personalCenterService';
import Datepicker from '@/components/main/common/datePicker';
import Paginate from '@/components/sub/paginate';
import MemberRecordTable from '@/components/sub/member/transactionRecord/memberRecordTable';
import MemberRecordBettingTable from '@/components/sub/member/transactionRecord/memberBettingRecordTable';
import MemberRecordBettingTableNone from '@/components/sub/member/transactionRecord/memberBettingRecordTableNone';
import EventBus from 'scripts/common/EventBus';

export const initTiRd = {
    data () {
        return {
            DropDowning: false,
            // 資金往來查詢下拉式選單
            TransTypeSList: [],

            // 投注記錄查詢下拉式選單
            BettingTypeList: [],

            // 设定初始值(当投注记录为空时无法切换--因此设定初始值) 2018.12.26 SeyoChen
            RetsetData: [{
                Key: '',
                Name: this.$t('message.member_classification_yet')
            }],

            Submitted: false,
            TimeSequenceInvalid: false,

            dateTimeInterval: false,
            DisplayQueryObj: {
                StartTime: new Date(),
                EndTime: new Date(),
                Type: {},
            },

            // 搜尋類型
            QueryType: 'CashFlow',
            // 搜尋條件
            QueryConditions: {
                Type: -1,
                PagingInfo: {
                    PageSize: 10
                },
                NoSettled: false
            },
            CalendarDisabled: {
                from: new Date(moment().utcOffset(480).add(1, 'days').format('YYYY/MM/DD')), // Disable all dates after specific date
            },
            RecordList: [],
            PageCount: 0
        };
    },
    created: function () {
        this.BindingEvent();
        this.LoadPage();
    },

    mounted: function () {
        CommUtility.BindEnterTrigger('recordQueryForm', this.UpdateQueryCondition);
    },

    methods: {
        /* 时间格式化函数，此处仅针对yyyy-MM-dd hh:mm:ss 的格式进行格式化 */
        dateFormat: function (time) {
            const date = new Date(time);
            const year = date.getFullYear();
            /* 在日期格式中，月份是从0开始的，因此要加0
             * 使用三元表达式在小于10的前面加0，以达到格式统一  如 09:11:05
             * */
            const month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
            const day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
            // const hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours();
            // const minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
            // const seconds = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
            /* 拼接 */
            /* return year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds; */
            return year + '-' + month + '-' + day;
        },
        BindingEvent: function () {
            document.addEventListener('click', this.HideDropDown);
        },
        // 顯示紀錄類型選單
        ShowDropDown: function () {
            if (this.DropDowning === false) {
                $('.down_box').slideDown(300);
                this.DropDowning = true;
            } else {
                $('.down_box').slideUp(300);
                this.DropDowning = false;
            }
        },
        // 隱藏紀錄類型選單
        HideDropDown: function (event) {
            const target = $(event.target);
            if (target.closest('.d_course').length === 0) {
                $('.down_box').slideUp(300);
                this.DropDowning = false;
            }
        },

        // 切換紀錄查詢種類
        ChangeQueryType: function (queryType) {
            this.QueryType = queryType;
            this.DisplayQueryObj.StartTime = new Date();
            this.DisplayQueryObj.EndTime = new Date();
            if (this.QueryType === 'CashFlow') {
                this.DisplayQueryObj.Type = this.TransTypeSList[1];
            } else if (this.QueryType === 'BettingRecord') {
                this.DisplayQueryObj.Type = this.BettingTypeList[1];
            } else if (this.QueryType === 'BettingRecordNone') {
                this.DisplayQueryObj.Type = this.BettingTypeList[1];
            } // end if
        }, // end ChangeQueryType

        // 選取紀錄類型
        SelectType: function (Type) {
            this.DisplayQueryObj.Type = Type;
        },

        // 載入記錄查詢頁面
        LoadPage: async function () {
            const data = await PersonalCenterService.CashFlowLog_LoadMainPag();
            if (data.Ret === BLDef.ErrorRetType.SUCCESS) {
                this.TransTypeSList = data.Data.TransTypeSList;
            } // end if

            const retData = await PersonalCenterService.BetLog_LoadMainPage();
            if (retData.Ret === BLDef.ErrorRetType.SUCCESS) {
                this.BettingTypeList = retData.Data.GameApiTypeList;
            } else {
                // 2018.12.26 SeyoChen 投注记录查询bug
                this.BettingTypeList = this.RetsetData;
            } // end if
            // 類型給預設
            this.DisplayQueryObj.Type = data.Data.TransTypeSList[1];
            this.DisplayQueryObj.StartTime = new Date();
            this.DisplayQueryObj.EndTime = new Date();
            this.QueryConditions.Type = -1;
            this.GetCurrentPageData(1);
        },
        // 點擊搜索，更新搜尋條件
        UpdateQueryCondition: function () {
            this.Submitted = true;
            const anyFieldInvalid = this.CheckFied();
            if (anyFieldInvalid === true) {
                return;
            } // end if
            this.QueryConditions.PagingInfo.PageNo = 1;
            this.QueryConditions.Type = this.DisplayQueryObj.Type.Value;
            this.QueryConditions.StartTick = CommUtility.LocalTimeToUtcTicks(this.dateFormat(this.DisplayQueryObj.StartTime) + ' 00:00:00');
            this.QueryConditions.EndTick = CommUtility.LocalTimeToUtcTicks(this.dateFormat(this.DisplayQueryObj.EndTime) + ' 23:59:59', true);

            console.log('3333333333333333333');
            console.log(this.QueryConditions);
            EventBus.$emit('Paginate_ResetPageNo');
            this.GetCurrentPageData(1);
        },
        // 驗證搜尋欄位正確性
        CheckFied: function () {
            let anyInvalid = false;
            this.TimeSequenceInvalid = false;
            this.dateTimeInterval = false;

            // 開始時間未選
            if (this.DisplayQueryObj.StartTime === undefined) {
                anyInvalid = true;
            }
            // 結束時間未選
            if (this.DisplayQueryObj.EndTime === undefined) {
                anyInvalid = true;
            }
            // 開始日期大於結束日期
            if (this.DisplayQueryObj.StartTime !== undefined && this.DisplayQueryObj.EndTime !== undefined) {
                const st = new Date(this.DisplayQueryObj.StartTime).getTime();
                const dt = new Date(this.DisplayQueryObj.EndTime).getTime();
                const day = dt - st;
                if (Math.floor(day / (24 * 3600 * 1000)) > 31) {
                    this.dateTimeInterval = true;
                    anyInvalid = true;
                }

                if (new Date(this.DisplayQueryObj.StartTime) > new Date(this.DisplayQueryObj.EndTime)) {
                    this.TimeSequenceInvalid = true;
                    anyInvalid = true;
                }
            }

            // 類型未選
            if (this.DisplayQueryObj.Type.Value === '-2') {
                anyInvalid = true;
            }
            return anyInvalid;
        },

        // 查詢紀錄
        GetCurrentPageData: async function (pageNo) {
            this.QueryConditions.PagingInfo.PageNo = pageNo;
            this.QueryConditions.StartTick = CommUtility.LocalTimeToUtcTicks(this.dateFormat(this.DisplayQueryObj.StartTime) + ' 00:00:00');
            this.QueryConditions.EndTick = CommUtility.LocalTimeToUtcTicks(this.dateFormat(this.DisplayQueryObj.EndTime) + ' 23:59:59', true);
            let data;
            EventBus.$emit('GlobalLoadingTrigger', true);
            if (this.QueryType === 'CashFlow') {
                data = await PersonalCenterService.CashFlowLog_Query(this.QueryConditions);
            } else if (this.QueryType === 'BettingRecord') {
                this.QueryConditions.NoSettled = false;
                data = await PersonalCenterService.BetLog_Query(this.QueryConditions);
            } else if (this.QueryType === 'BettingRecordNone') {
                this.QueryConditions.NoSettled = true;
                data = await PersonalCenterService.BetLog_Query(this.QueryConditions);
            } // end if
            EventBus.$emit('GlobalLoadingTrigger', false);
            if (data.Ret === BLDef.ErrorRetType.SUCCESS) {
                this.RecordList = data.Data.LogList;
                this.PageCount = Math.ceil(data.Data.PagingInfo.TotalCount / this.QueryConditions.PagingInfo.PageSize);
            } else {
                const notifyData = {
                    NotifyMessage: data.Message
                };
                EventBus.$emit('showNotifyMessage', notifyData);
            } // end if
        }, // end GetCurrentPageData
    },
    components: {
        MemberRecordTable,
        MemberRecordBettingTable,
        MemberRecordBettingTableNone,
        Datepicker,
        Paginate,
    },

    watch: {
        QueryType: function () {
            // 搜尋條件和顯示重置
            this.DisplayQueryObj.StartTime = new Date();
            this.DisplayQueryObj.EndTime = new Date();
            this.QueryConditions.Type = -1;
            this.QueryConditions.StartTick = CommUtility.LocalTimeToUtcTicks(this.DisplayQueryObj.StartTime);
            this.QueryConditions.EndTick = CommUtility.LocalTimeToUtcTicks(this.DisplayQueryObj.EndTime, true);
            this.GetCurrentPageData(1);
            EventBus.$emit('Paginate_ResetPageNo');
            this.Submitted = false;
        },
    }
};
