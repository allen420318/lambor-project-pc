import CommUtility from 'scripts/common/CommUtility';

export const ContactUsSuccess = {        
    methods: {
        // 開起此彈窗
        OpenWindow: function () {
            CommUtility.WebShowUniqueForm('contactUsSuccessForm');
        }, // end OpenWindow

        // 關閉此彈窗
        CloseWindow: function () {            
            CommUtility.WebCloseUniqueForm('contactUsSuccessForm');
            this.$router.push( { name: 'Index' });
        }, // end CloseWindow      
       
    }
};