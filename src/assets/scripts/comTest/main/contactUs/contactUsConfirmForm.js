import BLDef from 'scripts/common/BLDef';     
import CommUtility from 'scripts/common/CommUtility';
import informationService from 'scripts/businessLogic/informationService';
import EventBus from 'scripts/common/EventBus';
import ContactUsSuccess from '@/components/sub/contactUs/contactUsSuccess';

export const ContactUsConfirmForm = {    
    props: {
        PostDataModel: undefined,
        LoginStatus: undefined,
    },

    methods: {
        // 開起此彈窗
        OpenWindow: function () {
            CommUtility.WebShowUniqueForm('contactUsConfirmForm');
        }, // end OpenWindow

        // 關閉此彈窗
        CloseWindow: function () {            
            CommUtility.WebCloseUniqueForm('contactUsConfirmForm');
        }, // end CloseWindow

        // 確定送出
        Confirm: async function () {
            EventBus.$emit('GlobalLoadingTrigger', true);
            let retData;
            if ( this.LoginStatus) {
                retData = await informationService.ContactUs_Add_LoginUser( this.PostDataModel);
            } else {
                retData = await informationService.ContactUs_Add_Guest( this.PostDataModel);
            }                
            EventBus.$emit('GlobalLoadingTrigger', false);
            

            if ( retData.Ret != BLDef.ErrorRetType.SUCCESS) {
                const notifyData = {
                    NotifyMessage: retData.Message,     
                    CloseFunction: this.CloseWindow,                 
                };
                this.CloseWindow();
                EventBus.$emit('showNotifyMessage', notifyData);
                this.$parent.GetAuthCode();
                return;
            } // end if

            this.CloseWindow();
            ContactUsSuccess.mixins[0].methods.OpenWindow();
        }, // end Confirm       
    },

    components: {
        ContactUsSuccess,
    },
};