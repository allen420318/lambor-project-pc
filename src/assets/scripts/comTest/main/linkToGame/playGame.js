import BLDef from 'scripts/common/BLDef';
import URLService from 'scripts/common/URLService';
import CommonService from 'scripts/businessLogic/commonService';
import GameService from 'scripts/businessLogic/gameService';
import EventBus from 'scripts/common/EventBus';
import CommUtility from 'scripts/common/CommUtility';
import Vue from 'vue';

export const playGame = {
    data() {
        return {
            NotifyObj: {
                NotifyMessage: this.$t('message.transfer_transferFail'),
                NotifySubMessage: undefined,
                CloseFunction: Function
            },
            GamePlayUrl: undefined,
            PlayGameData: {},
            TransferNum: 0,
            TransferShow: false,
            NotifyMessage: this.$t('message.transfer_transferFail'),
            GameList: [],
            callBackHtml: ''
        };
    },
    mounted: function () {
        this.LodingFun(40, 200); // 动画初始阶段 20200109 SC
        // EventBus.$emit('GlobalLoadingTrigger', true);
        this.PageInit();
        this.GetParams();
    },
    methods: {
        // 初始化
        PageInit: function () {
            window.history.forward(1);
            $(window).resize(function () {
                $('#Iframe_Game').css({
                    height: window.innerHeight + 'px'
                });
            });
        },
        // 獲取URL參數
        GetParams: async function () {
            if (localStorage.getItem('PlayGamePostData') != undefined) {
                this.PlayGameData = URLService.GetObjFromUrlParameter(
                    localStorage.getItem('PlayGamePostData')
                );
                localStorage.removeItem('PlayGamePostData');
                if (this.PlayGameData.PlayType != BLDef.IdentityType.TRIAL) {
                    // 檢查權限:凍結不可進行遊戲
                    const permissionData = await this.CheckAcctStatus();
                    if (permissionData.Status != BLDef.SysAccountStatus.LOGINED_ENABLED) {
                        const notifyData = {
                            NotifyMessage: BLDef.SysAccountStatus.NOT_LOGIN == permissionData.Status ||
                                BLDef.SysAccountStatus.NOT_LOGIN_DISABLED ==
                                permissionData.Status ||
                                BLDef.SysAccountStatus.NOT_LOGIN_DELETED ==
                                permissionData.Status ?
                                this.$t('message.top_login_account') : this.$t('message.top_account_freezing'),
                            CloseFunction: this.CloseCurrentWindow
                        };
                        EventBus.$emit('showNotifyMessage', notifyData);
                        return;
                    }

                    this.LodingFun(100); // 动画第一阶段 20200109 SC
                    this.PlayGame();
                } else {
                    this.CheckAuthCode(this.PlayGameData.VCodeObj);
                }
            } else {
                this.CloseCurrentWindow();
            }
        },
        // 檢查登入狀態
        CheckAcctStatus: async function () {
            const retData = await CommonService.Comm_CheckPermission();
            return retData;
        },
        // 檢查驗證碼
        CheckAuthCode: async function (VCodeObj) {
            const inputObj = VCodeObj;
            const retData = await CommonService.VerifyCode_Verify(inputObj);
            if (retData.Ret != 0) {
                const notifyData = {
                    NotifyMessage: retData.Message,
                    CloseFunction: this.CloseCurrentWindow
                };
                EventBus.$emit('showNotifyMessage', notifyData);
            } else {
                this.PlayGame();
            }
        },
        // 進行遊戲
        PlayGame: async function () {
            CommUtility.WebCloseUniqueForm('MessageTransferWindow'); // 关闭转账失败弹窗
            const token =
                this.PlayGameData.PlayType == BLDef.IdentityType.TRIAL ?
                this.PlayGameData.CToken :
                undefined;
            const BaseUrl = window.location.href
                .split('/')
                .slice(0, 3)
                .join('/');
            const inputObj = {
                WGUID: Vue.prototype.$WGUID,
                GameAPIVendor: this.PlayGameData.GameAPIVendor,
                GameCatlog: this.PlayGameData.GameCatlog,
                GameCode: this.PlayGameData.GameCode,
                PlatformType: BLDef.PlatformType.Web_PC,
                PlayType: this.PlayGameData.PlayType,
                Url: BaseUrl
            };

            this.LodingFun(225, 200); // 动画第二阶段 20200109 SC
            // EventBus.$emit('GlobalLoadingTrigger', true);
            const gameData = await GameService.Game_Player(inputObj, token);
            console.log(gameData)
            console.log('!!!!!!!!!!!!!!!!!!!!!')
            // 请求频繁 后端返回限制进入游戏 2019.04.02 SeyoChen
            if (gameData.Ret == 22) {
                const notifyData = {
                    // NotifyMessage: gameData.Message,
                    NotifyMessage: this.$t('message.play_frequently'),
                    CloseFunction: this.CloseCurrentWindow
                };
                EventBus.$emit('showNotifyMessage', notifyData);
                return;
            }
            this.LodingFun(300, 300); // 动画第三阶段 20200109 SC
            // 请求频繁 后端返回限制进入游戏 2019.04.02 SeyoChen end
            if (gameData.Ret != 0) {
                EventBus.$emit('GlobalLoadingTrigger', false);
                const notifyData = {
                    // NotifyMessage: gameData.Message,
                    NotifyMessage: this.$t('message.live_line_upkeep'),
                    CloseFunction: this.CloseCurrentWindow
                };
                EventBus.$emit('showNotifyMessage', notifyData);
                return;
            }
            this.GameList = gameData;
            this.LodingFun(425, 200); // 动画第三阶段 20200109 SC
            // 进入转账是否转账成功 true 成功  false 失败 20191010 SeyoChen
            if (gameData.Data.IsGameTransation === false) {
                EventBus.$emit('GlobalLoadingTrigger', false);
                CommUtility.WebShowUniqueForm('MessageTransferWindow');
                localStorage.setItem('TransferNum', this.TransferNum);
                this.TransferShow = true;
                this.TransferNum++;
                if (this.TransferNum > 2) {
                    this.NotifyMessage = this.$t('message.transfer_transferFailDesc1');
                } else if (this.TransferNum > 1 && this.TransferNum <= 2) {
                    this.NotifyMessage = this.$t('message.transfer_transferFailDesc2');
                }
            } else {
                // localStorage.removeItem('GameApiNoNumber');
                let GameApiNoNumber = localStorage.getItem('gameListGameApiNo')
                console.log(GameApiNoNumber)
                // if (GameApiNoNumber == 115 || GameApiNoNumber == 95) {
                //     console.log('12')
                //     let domainName = document.location.host;
                //     localStorage.removeItem('callbackHTML');
                //     localStorage.setItem('callbackHTML', gameData.Data.GamePlayUrl);
                //     var subUrl = gameData.Data.GamePlayUrl.indexOf('You either');
                //     var subUrl2 = gameData.Data.GamePlayUrl.indexOf('</div>');
                //     this.callBackHtml = gameData.Data.GamePlayUrl.substring(0, subUrl) + gameData.Data.GamePlayUrl.substring(subUrl2)
                //     var httpUrl = gameData.Data.GamePlayUrl.substring(0, 4)
                //     if (httpUrl == 'http') {
                //         console.log('qw')
                //         this.LodingFun(450, 10);
                //         this.IntoGame();
                //     } else {
                //         console.log('we')
                //         let leftScriptStr = gameData.Data.GamePlayUrl.indexOf('<script')
                //         let rightScriptStr = gameData.Data.GamePlayUrl.indexOf('script>')
                //         let scriptStr = gameData.Data.GamePlayUrl.substring(leftScriptStr, rightScriptStr + 7)
                //         let leftScriptStr1 = scriptStr.indexOf('"')
                //         let leftScriptStr2 = scriptStr.indexOf(' type')
                //         scriptStr = scriptStr.substring(leftScriptStr1 + 1, leftScriptStr2 - 1)
                //         let script = document.createElement('script')
                //         script.type = 'text/javascript'
                //         script.src = scriptStr
                //         var da = document.getElementsByTagName('head')[0].appendChild(script)
                //     }
                // } else {
                console.log('13')
                this.LodingFun(450, 10); // 动画最终阶段 20200109 SC
                this.IntoGame();
                // }
            }
            // 进入转账是否转账成功 true 成功  false 失败 20191010 SeyoChen end
        },
        IntoGame: function () {
            CommUtility.WebCloseUniqueForm('MessageTransferWindow'); // 关闭转账失败弹窗
            this.GamePlayUrl = this.GameList.Data.GamePlayUrl;
            location.href = this.GamePlayUrl;
        },
        // 關閉視窗
        CloseCurrentWindow: function () {
            window.close();
        },
        // 弹出客服新窗口
        OpenNewBox: function () {
            return window.open(
                this.$ServeLink,
                'newindow',
                'height=565,width=357,top=0,left=0,toolbar=no,menubar=no,scrollbars=no,resizable=no,location=no,status=no'
            );
        },
        // 动画加载
        LodingFun: function (num, time) {
            $('.loading_top').animate({
                'width': num + 'px'
            }, time);
        }
    }
};
