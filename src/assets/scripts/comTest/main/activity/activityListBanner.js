// import 'scripts/customJs/web/jquery.SuperSlide.2.1.2';
// import 'scripts/customJs/web/slider';
import EventBus from 'scripts/common/EventBus';
import InformationService from 'scripts/businessLogic/informationService';
import CommonService from 'scripts/businessLogic/commonService';
import BLDef from 'scripts/common/BLDef';
import CommUtility from 'scripts/common/CommUtility';
import Vue from 'vue';

export const ActivityListBanner = {
    props: {
        GetCurrentPageData: '',
    },

    data() {
        return {
            // 載入畫面資料
            LoadMainPageDataModel: {
                BannerActivityList: [],
                ActivityList: [],
            },

            // 分頁資料
            PageInfo: {
                PageNo: 1,
                PageSize: 9,
                PageCount: 0,
                TotalCount: 0,
            },

            DefaultActivityList: [],
        };
    },

    created: function () {
        this.GetPromotion(1);
    },

    methods: {

        // 取得畫面資料
        GetPromotion: async function (pageNo) {
            this.PageInfo.PageNo = pageNo;
            const DataObj = {
                WGUID: Vue.prototype.$WGUID,
                PagingInfo: this.PageInfo
            };
            const retData = await InformationService.GetActivityList(DataObj);

            if (retData.Ret != 0) {
                const notifyData = {
                    NotifyMessage: retData.Message
                };
                EventBus.$emit('showNotifyMessage', notifyData);
                return;
            } // end if

            this.LoadMainPageDataModel.ActivityList = retData.Data.ActivityList;
            this.LoadMainPageDataModel.BannerActivityList = retData.Data.ActivityList.slice(0, 3);

            const self = this;
            // bind輪播事件
            setTimeout(function () {
                self.Slider();
            }, 500);

            // 2018.09.20 SeyoChen
            const retDataLv = await CommonService.Comm_CheckPermission();
            if (retDataLv.Status != 0) {
                const dataObj = {
                    WGUID: '',
                    LoginID: '',
                    PagingInfo: this.PageInfo
                };
                dataObj.WGUID = Vue.prototype.$WGUID;
                dataObj.LoginID = localStorage.getItem('Acct');
                const ActivityData = await CommonService.Comm_GetBonusActivity_MemberLv(dataObj);

                this.LoadMainPageDataModel.BannerActivityList = ActivityData.Data.ActivityList.slice(0, 3); // 优惠活动页面 banner 跟随等级更新 2019.04.12 SeyoChen
                this.$parent.ActivityList = ActivityData.Data.ActivityList;
                let pageCount = Math.ceil(ActivityData.Data.PagingInfo.TotalCount / ActivityData.Data.PagingInfo.PageSize);
                this.$parent.PageInfo.PageCount = pageCount === 1 ? 0 : pageCount;
            } else {
                this.$parent.ActivityList = this.LoadMainPageDataModel.ActivityList;
                let pageCount = Math.ceil(retData.Data.PagingInfo.TotalCount / retData.Data.PagingInfo.PageSize);
                this.$parent.PageInfo.PageCount = pageCount === 1 ? 0 : pageCount;
            }

            // this.$parent.ActivityList = this.LoadMainPageDataModel.ActivityList;
            // let pageCount = Math.ceil(retData.Data.PagingInfo.TotalCount / retData.Data.PagingInfo.PageSize);
            // this.$parent.PageInfo.PageCount = pageCount === 1 ? 0 : pageCount;
            $('.activity-expect').remove();

            // 更新Paging

            console.log(this.$parent.PageInfo.PageCount)
            console.log(retData.Data.PagingInfo.TotalCount)
            console.log(retData.Data.PagingInfo.PageSize)
            console.log(Math.ceil(retData.Data.PagingInfo.TotalCount / retData.Data.PagingInfo.PageSize))
            // this.PageInfo.PageCount = Math.ceil(retData.Data.PagingInfo.TotalCount / retData.Data.PagingInfo.PageSize);
            // if (this.PageInfo.PageCount == 0) {
            //     this.$parent.PageInfo.PageCount = 1; // 若無紅利活動也設定為1，使分頁套件可以顯示
            // } else {
            //     this.$parent.PageInfo.PageCount = this.PageInfo.PageCount;
            // }
        }, // end GetPromotion
        // 2018.11.15 SeyoChen 动态识别添加游戏模块
        AjectAddBox: function (num) {
            const boxSize = parseInt(3); // 页面中一行的box个数
            const boxHtml =
                `<li class="activity-li activity-li-PopUp activity-expect">
        <div class="activity-expect-tips"><div><p>${this.$t('message.live_expect')}</p><span>IN MAINTENANCE</span></div></div></li>`;
            const box = $('.activity-lists');
            if (num % boxSize === 0 && num !== 0) {} else {
                for (let i = 0; i < Math.abs(boxSize - (num % boxSize)); i++) {
                    box.append(boxHtml);
                }
            }
        },
        // 優惠活動輪播事件
        Slider: function () {
          this.$nextTick(() => {
            var swiper = new Swiper('.swiper-container', {
                mousewheelControl: true,
                direction: 'horizontal',
                mousewheelForceToAxis: true,
                pagination: '.swiper-pagination',
                paginationClickable :true,
                autoplay: 5000 // 可选选项，自动滑动
            });
            $('.banner_prev').click(function(){
              swiper.slidePrev();
            })
            $('.banner_next').click(function(){
              swiper.slideNext();
            })
        })
        }, // end Silder
        //  跳至詳細
        UrlRedirectDetail: function (target) {
            this.$router.push({
                name: 'ActivityListDetails',
                params: {
                    ID: target.ID
                }
            });
        }, // end UrlRedirectDetail
    },

    watch: {
        GetCurrentPageData: function () {
            this.GetPromotion(this.GetCurrentPageData);
        },
    },
};
