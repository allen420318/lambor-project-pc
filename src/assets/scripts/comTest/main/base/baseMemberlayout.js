// import 'scripts/customJs/web/showdate';
import FrameFooter from '@/components/public/frameFooter'
import FrameHeader from '@/components/public/frameHeader'
import FrameNav from '@/components/public/mainNavigator'
import floatService from '@/components/public/floatService'
import MemberFillTip from '@/components/public/memberFillTip';
import BLDef from 'scripts/common/BLDef';
import CommonService from 'scripts/businessLogic/commonService';
import EventBus from 'scripts/common/EventBus';
import rotateLoopFun from 'scripts/customJs/web/rotateLoop';
import PersonalCenterService from 'scripts/businessLogic/personalCenterService';
export const baseMbLy = {
    components: {
        FrameFooter,
        FrameHeader,
        FrameNav,
        floatService,
        MemberFillTip
    },
    data() {
        return {
            IsLogin: undefined,
            IsGetBalanceIng: false,
            VBalance: '',
            MemberInfo: {},
        };
    },
    mounted() {
        const self = this;
        EventBus.$on('fundGetBalanceComplete', (SummaryAmount) => {
            self.VBalance = SummaryAmount;
            self.IsGetBalanceIng = false;
        });
        EventBus.$on('memberHeaderUpdateBalance', () => {
            self.GetBalance();
        });
        // 每10分钟刷新一次钱包 2019.03.28 SeyoChen
        EventBus.$on('GBKBalance', (Balance) => {
            self.VBalance = Balance;
        });
    },
    created: function () {
        this.CheckIsLogin();
        // this.$nextTick(function () {
        //   function winScroll() {
        //     const topLeft = document.documentElement.scrollLeft;
        //     $('.header').css('left', -topLeft + 'px');
        //   }
        //   window.onscroll = winScroll;
        // });
    },
    methods: {
        // 檢查登入狀態
        CheckIsLogin: async function () {
            const data = await CommonService.Comm_CheckPermission();
            switch (data.Status) {
                case BLDef.SysAccountStatus.NOT_LOGIN:
                    this.IsLogin = false;
                    break;
                case BLDef.SysAccountStatus.LOGINED_ENABLED:
                case BLDef.SysAccountStatus.LOGINED_FROZEN:
                    this.IsLogin = true;
                    break;
                default:
                    break;
            }
        },
        // 檢查帳號狀態
        CheckLoginStatusBeforeRoute: async function (routeName) {
            const notifyData = {};
            const data = await CommonService.Comm_CheckPermission();
            switch (data.Status) {
                case BLDef.SysAccountStatus.LOGINED_ENABLED:
                    this.$router.push({
                        name: routeName
                    });
                    break;
                case BLDef.SysAccountStatus.LOGINED_FROZEN:
                    notifyData.NotifyMessage = this.$t('message.top_account_freezing');
                    EventBus.$emit('showNotifyMessage', notifyData);
                    break;
                default:
                    break;
            }
        },
        GetBalance: async function () {
            this.IsGetBalanceIng = true;
            rotateLoopFun.rotateLoop(); // 刷新按钮的旋转
            if (this.$route.name === 'FundManage') {
                // 更新資金管理餘額
                EventBus.$emit('updateFundBalance');
            } else {
                const inputObj = {};
                const retData = await PersonalCenterService.MemberCashFlowInfo_GetCWalletMoney(inputObj); // 新版获取账户金额 2019.03.28

                if (retData.Ret == 0) {
                    this.VBalance = retData.Data.VAmount
                } else {
                    this.VBalance = this.$t('message.top_under_maintenance')
                }
                this.IsGetBalanceIng = false;
            }
        },
        GetMemberInfo: async function () {
            // 取得會員資訊
            const data = await CommonService.Comm_GetMemberInfo();
            if (data.Ret == 0) {
                this.MemberInfo = data.Data.Member;
                rotateLoopFun.rotateLoop(); // 刷新按钮的旋转
            }
        },
    },
    watch: {
        IsLogin: function () {
            if (this.IsLogin === true) {
                this.GetMemberInfo();
                this.GetBalance();
            }
        },
    },
    beforeDestroy() {
        EventBus.$off('fundGetBalanceComplete');
        EventBus.$off('memberHeaderUpdateBalance');
    }
}
