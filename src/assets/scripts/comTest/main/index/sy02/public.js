publicFun = function() {
	

	function st2(top) {
		var scroll = top; //需要滚动的高度

		/*判断谷歌*/
		var userAgent = navigator.userAgent.toLowerCase(),
			s, o = {};
		var browser = {
			version: (userAgent.match(/(?:firefox|opera|safari|chrome|msie)[\/: ]([\d.]+)/))[1],
			safari: /version.+safari/.test(userAgent),
			chrome: /chrome/.test(userAgent),
			firefox: /firefox/.test(userAgent),
			ie: /msie/.test(userAgent),
			opera: /opera/.test(userAgent)
		}
		/* 获得浏览器的名称及版本信息 */
		if(browser.chrome) {
			$(document.body).animate({
				scrollTop: scroll
			}, 300);
		} else {
			$(document.documentElement).animate({
				scrollTop: scroll
			}, 300);
		}
	}

	//文字特效
	function words() {
		var log = function(msg) {
			return function() {
				if(console) console.log(msg);
			}
		}

		$('code').each(function() {
			var $this = $(this);
			$this.text($this.html());
		})

		var animateClasses = 'flash bounce shake tada swing wobble pulse flip flipInX flipOutX flipInY flipOutY fadeIn fadeInUp fadeInDown fadeInLeft fadeInRight fadeInUpBig fadeInDownBig fadeInLeftBig fadeInRightBig fadeOut fadeOutUp fadeOutDown fadeOutLeft fadeOutRight fadeOutUpBig fadeOutDownBig fadeOutLeftBig fadeOutRightBig bounceIn bounceInDown bounceInUp bounceInLeft bounceInRight bounceOut bounceOutDown bounceOutUp bounceOutLeft bounceOutRight rotateIn rotateInDownLeft rotateInDownRight rotateInUpLeft rotateInUpRight rotateOut rotateOutDownLeft rotateOutDownRight rotateOutUpLeft rotateOutUpRight hinge rollIn rollOut';

		var $form = $('.playground form'),
			$viewport = $('.playground .viewport');

		var getFormData = function() {
			var data = {
				loop: true,
				in: {
					callback: log('in callback called.')
				},
				out: {
					callback: log('out callback called.')
				}
			};

			$form.find('[data-key="effect"]').each(function() {
				var $this = $(this),
					key = $this.data('key'),
					type = $this.data('type');

				data[type][key] = $this.val();
			});

			$form.find('[data-key="type"]').each(function() {
				var $this = $(this),
					key = $this.data('key'),
					type = $this.data('type'),
					val = $this.val();

				data[type].shuffle = (val === 'shuffle');
				data[type].reverse = (val === 'reverse');
				data[type].sync = (val === 'sync');
			});

			return data;
		};

		$.each(animateClasses.split(' '), function(i, value) {
			var type = '[data-type]',
				option = '<option value="' + value + '">' + value + '</option>';

			if(/Out/.test(value) || value === 'hinge') {
				type = '[data-type="out"]';
			} else if(/In/.test(value)) {
				type = '[data-type="in"]';
			}

			if(type) {
				$form.find('[data-key="effect"]' + type).append(option);
			}
		});

		$form.find('[data-key="effect"][data-type="in"]').val('fadeInLeftBig');
		$form.find('[data-key="effect"][data-type="out"]').val('hinge');

		$('.jumbotron h1')
			.fitText(0.5)
			.textillate({ in: {
					effect: 'flipInY'
				}
			});

		$('.jumbotron p')
			.fitText(3.2, {
				maxFontSize: 18
			})
			.textillate({
				initialDelay: 1000,
				in: {
					delay: 3,
					shuffle: true
				}
			});

		setTimeout(function() {
			$('.fade').addClass('in');
		}, 250);

		setTimeout(function() {
			$('h1.glow').removeClass('in');
		}, 2000);

		var $tlt = $viewport.find('.tlt')
			.on('start.tlt', log('start.tlt triggered.'))
			.on('inAnimationBegin.tlt', log('inAnimationBegin.tlt triggered.'))
			.on('inAnimationEnd.tlt', log('inAnimationEnd.tlt triggered.'))
			.on('outAnimationBegin.tlt', log('outAnimationBegin.tlt triggered.'))
			.on('outAnimationEnd.tlt', log('outAnimationEnd.tlt triggered.'))
			.on('end.tlt', log('end.tlt'));

		$form.on('change', function() {
			var obj = getFormData();
			$tlt.textillate(obj);
		}).trigger('change');

	};

	function draw20(id) {
		var canvas = document.getElementById("myCanvas");
		if(canvas == null)
			return false;
		var context = canvas.getContext("2d");
		context.clearRect(0, 0, 684, 684);
		var interal = setInterval(function() {
			move(context);
		}, 0.1);

		var x = 332; //矩形开始坐标
		var y = 0; //矩形结束坐标
		var x1 = 0;
		var y1 = 87;

		//三角 1
		var x2 = 332,
			y2 = 0;
		var x3 = 0;
		var x4 = 664,
			y4 = 562;
		//2
		var x5 = 332,
			y5 = 664;
		var x6 = 664,
			y6 = 120;
		var x7 = 0,
			y7 = 120;

		//弧形 left
		var x8 = 0,
			y8 = 87;

		//弧形 right
		var vx = 664,
			vy = 87,
			vz = 87;
		var vx1 = 660,
			vy1 = 170;
		var vx2 = 656,
			vy2 = 240;

		function move(context) {
			context.strokeStyle = "rgba(255,255,255,0.5)";
			context.fillStyle = "rgba(255,255,255,0.5)";
			context.save();
			context.lineWidth = 1;
			context.globalCompositeOperation = 'destination-atop';

			context.beginPath();

			context.moveTo(332, 0);
			context.lineTo(x, y);

			context.moveTo(0, 87);
			context.lineTo(x1, y1);

			//绘制两个颠倒交错的三角形
			//1
			context.moveTo(332, 0);
			context.lineTo(x2, y2);
			context.moveTo(0, 562);
			context.lineTo(x3, 562);
			context.moveTo(664, 562);
			context.lineTo(x4, y4);
			//2
			context.moveTo(332, 664);
			context.lineTo(x5, y5);
			context.moveTo(664, 120);
			context.lineTo(x6, y6);
			context.moveTo(2, 120);
			context.lineTo(x7, y7);

			context.closePath();
			context.stroke();
			context.restore();

			//弧形 left
			context.beginPath();
			context.bezierCurveTo(0, 87, 0, 120, 4, 170);
			context.stroke();
			context.bezierCurveTo(4, 170, 90, 190, 8, 240);
			context.stroke();
			context.bezierCurveTo(8, 240, 20, 500, 332, 664);
			context.stroke();
			//弧形 right
			context.beginPath();
			//      	context.bezierCurveTo(664,87,664,vz,vx,vy);
			context.bezierCurveTo(664, 87, 664, 120, 660, 170);
			context.stroke();
			context.bezierCurveTo(660, 170, 578, 190, 656, 240);
			context.stroke();
			context.bezierCurveTo(656, 240, 644, 500, 332, 664);
			context.stroke();

			if(x < 664 && y1 == 0) {
				x = x + 4;
			}
			if(y < 87 && y1 == 0) {
				y++;
			}
			if(x1 < 332) {
				x1 = x1 + 4;
			}
			if(y1 > 0) {
				y1--;
			}

			//绘制两个颠倒交错的三角形 1
			if(x2 > 0) {
				x2 = x2 - 3;
				if(x2 < 0) {
					x2 = 0;
				}
			}
			if(y2 < 562) {
				y2 = y2 + 5;
				if(y2 > 562) {
					y2 = 562;
				}
			}
			if(x3 < 664 && y2 >= 562) {
				x3 = x3 + 5;
				if(x3 > 664) {
					x3 = 664;
				}
			}
			if(x4 > 332 && x3 >= 664) {
				x4 = x4 - 3;
				if(x4 < 332) {
					x4 = 332;
				}
			}
			if(y4 > 0 && x3 >= 664) {
				y4 = y4 - 5;
				if(y4 < 0) {
					y4 = 0;
				}
			}
			//2
			if(x5 < 662) {
				x5 = x5 + 3;
				if(x5 > 662) {
					x5 = 662;
				}
			}
			if(y5 > 120) {
				y5 = y5 - 5;
				if(y5 < 120) {
					y5 = 120;
				}
			}
			if(x6 > 2 && y5 <= 120) {
				x6 = x6 - 5;
				if(x6 < 2) {
					x6 = 2;
				}
			}
			if(x7 < 332 && x6 <= 2) {
				x7 = x7 + 3;
				if(x7 > 332) {
					x7 = 332;
				}
			}
			if(y7 < 664 && x6 <= 2) {
				y7 = y7 + 5;
				if(y7 > 664) {
					y7 = 664;
				}
			}

			if(y7 == 664) {
				//      		$(".canvas_box .light_bg").css({"display":"block"});
				$(".light_bg_box").css({
					"display": "block"
				});
			}

		}
	}

	//计算输入的标签距离顶部的距离
	function labelTop(name) {
		var top = $("." + name + "").offset().top;
		return top;
	}
}