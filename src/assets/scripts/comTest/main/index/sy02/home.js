// 首页
homeFun = function () {
    // 线的弹动
    $('.move_circle').jParticle({
        background: '#ffffff',
        color: '#afb0b5'
    });
    var bannerSlider = new Slider($('#banner_tabs'), {
        time: 6000,
        delay: 400,
        event: 'hover',
        auto: true,
        mode: 'fade',
        controller: $('#bannerCtrl'),
        activeControllerCls: 'active'
    });
    $('#banner_tabs .flex-prev').click(function () {
        bannerSlider.prev()
    });
    $('#banner_tabs .flex-next').click(function () {
        bannerSlider.next()
    });
    /* 判断IE */
    var ua = navigator.userAgent.toLowerCase(),
        rMsie = /(msie\s|trident.*rv:)([\w.]+)/,
        isIE = rMsie.exec(ua) != null;

    if (isIE) {
        $('.i_card').children('div').removeClass('feather_box').addClass('feather_ie');
    }

    var num = 1; // 记录从上往下滚动执行事件
    var num1 = 1; // 轮播切换的动画
    var num2 = 1;

    $('.i_sport_man').css({
        'right': '-300px',
        'opacity': '0'
    }).animate({
        'right': '-230px',
        'opacity': '1'
    }, 500).animate({
        'right': '-240px'
    }, 100);

    // $(window).scroll(windowScroll);

    // function windowScroll () {
    //     var top = $(window).scrollTop();
    //     var h = $(window).height();
    //     var height = $('.banner').height();
    //     var labelTop = $('.nav_block').offset().top;
    //     //				alert(h);

    //     if (top > 100) {
    //         $('.service-box').addClass('service-box2');
    //     } else {
    //         $('.service-box').removeClass('service-box2');
    //     }

    //     // 切换屏的动效
    //     if (top >= labelTop - h && num2 == 1) {
    //         var hasSelect = $('.nav_block .hasSelect').attr('data-num');
    //         $('.nav_block .hasSelect').removeClass('hasSelect');
    //         changeBox(hasSelect);
    //         num2 = 2;
    //     } else if (top < labelTop - h && num2 == 2) {
    //         changeBox2(num);
    //         num2 = 1;
    //     }

    //     // 活动宣传栏 动画效果
    //     if (top > 500 && num == 1) {
    //         $('.at_triangle').addClass('at_come');
    //         $('.at_circle').addClass('at_circle1');
    //         $('.at_feather .feather_img').addClass('feather_img1');

    //         // 三角形掉落
    //         $('.at_come').css({
    //             'top': '-160px'
    //         });
    //         at('at_triangle1', 54, 500, 250);
    //         at('at_triangle2', 334, 500, 200);
    //         at('at_triangle3', 94, 500, 100);
    //         at('at_triangle4', 0, 500, 750);
    //         at('at_triangle5', 30, 500, 400);
    //         at('at_triangle6', 3, 500, 750);
    //         at('at_triangle7', 48, 250, 500);

    //         setTimeout(function () {
    //             $('.feather_triangle').addClass('feather');
    //         }, 2300);

    //         $('.at_course').css({
    //             'opacity': '0',
    //             'left': '540px'
    //         }).animate({
    //             'opacity': '1',
    //             'left': '560px'
    //         }, 1000);
    //         num = 2;
    //     } else if (top < 500) {
    //         num = 1;
    //         $('.at_circle').removeClass('at_circle1');
    //         $('.at_feather .feather_img').removeClass('feather_img1');
    //         $('.feather_triangle').removeClass('feather');
    //     }

    //     if (top > 950) {
    //         $('.nav_block_1').addClass('at_same');
    //         num1 = 2;
    //     } else if (top < 950) {
    //         $('.nav_block_1').removeClass('at_same');
    //         num1 = 1;
    //     }
    // }
    setTimeout(function () {
        $('.slides_list1').addClass('slider_at');
    }, 1000);

    function at (name, top, time, delay) {
        $('.' + name + '').delay(delay).animate({
            'top': '' + top + ''
        }, time);
    }
    // JavaScript Document

    // 首页板块切换
    $(function () {
        $('.four_nav_list ul li').mouseenter(function () {
            var num = $(this).attr('data-num');
            //		$(".nav_block_same").removeClass("at_same");
            changeBox(num);
            $(this).addClass('borser_show').siblings().removeClass('borser_show');
        });
    });

    function changeBox (num) {
        if (num == 1 && !$('.nav_block_1').hasClass('hasSelect')) {
            $('.four_nav_img_bg_1').animate({
                opacity: '1'
            }, 500).siblings('.block_bg').animate({
                opacity: '0'
            }, 500);

            $('.nav_block_1').stop();
            $('.nav_block_1').animate({
                marginTop: '0'
            }, 500);
            $('.nav_block_1').addClass('at_same');

            // 当前动效(sport)
            $('.i_sport_man').css({
                'right': '-300px',
                'opacity': '0'
            }).delay(100).animate({
                'right': '-230px',
                'opacity': '1'
            }, 500).animate({
                'right': '-240px'
            }, 300);
            // 版面动效(video)
            $('.i_video_girl').css({
                'bottom': '0px',
                'opacity': '1'
            }).animate({
                'bottom': '-50px',
                'opacity': '0'
            }, 500);
            // 重置被选择的屏块
            $('.nav_block_1').addClass('hasSelect').siblings('.hasSelect').removeClass('hasSelect').removeClass('at_same');
        } else if (num == 2 && !$('.nav_block_2').hasClass('hasSelect')) {
            $('.four_nav_img_bg_2').animate({
                opacity: '1'
            }, 500).siblings('.block_bg').animate({
                opacity: '0'
            }, 500);

            $('.nav_block_1').stop();
            $('.nav_block_1').animate({
                marginTop: '-1000px'
            }, 500);
            $('.nav_block_2').addClass('at_same');

            // 上一版回去动画(elegame)
            $('.i_ele_man3').css({
                'left': '80px',
                'opacity': '1'
            }).animate({
                'left': '170px',
                'opacity': '0'
            }, 200);
            $('.i_ele_man1').css({
                'opacity': '1'
            }).animate({
                'opacity': '0'
            }, 200);
            $('.i_ele_man2').css({
                'left': '660px',
                'opacity': '1'
            }).animate({
                'left': '570px',
                'opacity': '0'
            }, 200);
            // 动效(sport)
            $('.i_sport_man').css({
                'right': '-240px',
                'opacity': '1'
            }).animate({
                'right': '-400px',
                'opacity': '0'
            }, 300);
            // 当前版面动效(video)
            $('.i_video_girl').css({
                'bottom': '-100px',
                'opacity': '0'
            }).animate({
                'bottom': '0',
                'opacity': '1'
            }, 1000);

            // 重置被选择的屏块
            $('.nav_block_2').addClass('hasSelect').siblings('.hasSelect').removeClass('hasSelect').removeClass('at_same');
        } else if (num == 3 && !$('.nav_block_3').hasClass('hasSelect')) {
            $('.four_nav_img_bg_3').animate({
                opacity: '1'
            }, 500).siblings('.block_bg').animate({
                opacity: '0'
            }, 500);

            $('.nav_block_1').stop();
            $('.nav_block_1').animate({
                marginTop: '-2000px'
            }, 500);
            $('.nav_block_3').addClass('at_same');

            // 人物进入
            $('.i_ele_man3').css({
                'left': '220px',
                'opacity': '0'
            }).delay(500).animate({
                'left': '80px',
                'opacity': '1'
            }, 500);
            //			$(".i_ele_man1").css({"left":"374px","opacity":"0"}).delay(800).animate({"left":"374px","opacity":"1"},500);
            $('.i_ele_man2').css({
                'left': '530px',
                'opacity': '0'
            }).delay(500).animate({
                'left': '660px',
                'opacity': '1'
            }, 500);
            // 版面动效(video)
            $('.i_video_girl').css({
                'bottom': '0px',
                'opacity': '1'
            }).animate({
                'bottom': '-50px',
                'opacity': '0'
            }, 500);

            // 重置被选择的屏块
            $('.nav_block_3').addClass('hasSelect').siblings('.hasSelect').removeClass('hasSelect').removeClass('at_same');
        } else if (num == 4 && !$('.nav_block_4').hasClass('hasSelect')) {
            $('.four_nav_img_bg_4').animate({
                opacity: '1'
            }, 500).siblings('.block_bg').animate({
                opacity: '0'
            }, 500);

            $('.nav_block_1').stop();
            $('.nav_block_1').animate({
                marginTop: '-3000px'
            }, 500);
            $('.nav_block_4').addClass('at_same');

            // 上一版回去动画(elegame)
            $('.i_ele_man3').css({
                'left': '80px',
                'opacity': '1'
            }).animate({
                'left': '170px',
                'opacity': '0'
            }, 200);
            $('.i_ele_man1').css({
                'opacity': '1'
            }).animate({
                'opacity': '0'
            }, 200);
            $('.i_ele_man2').css({
                'left': '660px',
                'opacity': '1'
            }).animate({
                'left': '570px',
                'opacity': '0'
            }, 200);

            // 光线效果
            $('.i_lottery_title img').css({
                'opacity': '0'
            });
            $('.i_lottery_title .light1').css({
                'left': '420px',
                'opacity': '1'
            }).delay(1000).animate({
                'left': '-245px'
            }, 3000);
            $('.i_lottery_title .light2').css({
                'top': '-55px',
                'opacity': '1'
            }).delay(2500).animate({
                'top': '110px'
            }, 2000);
            $('.i_lottery_title .light3').css({
                'left': '-245px',
                'opacity': '1'
            }).delay(3500).animate({
                'left': '900px'
            }, 3000);
            $('.i_lottery_title .light4').css({
                'top': '55px',
                'opacity': '1'
            }).delay(5000).animate({
                'top': '-55px'
            }, 2000);

            $('.i_t2').css({
                'opacity': '0'
            }).delay('500').animate({
                'opacity': '1'
            }, 500);

            // 重置被选择的屏块
            $('.nav_block_4').addClass('hasSelect').siblings('.hasSelect').removeClass('hasSelect').removeClass('at_same');
        } else if (num == 5 && !$('.nav_block_5').hasClass('hasSelect')) {
            $('.four_nav_img_bg_5').animate({
                opacity: '1'
            }, 500).siblings('.block_bg').animate({
                opacity: '0'
            }, 500);

            $('.nav_block_1').stop();
            $('.nav_block_1').animate({
                marginTop: '-4000px'
            }, 500);
            $('.nav_block_5').addClass('at_same');

            // 上一版回去动画(elegame)
            $('.i_ele_man3').css({
                'left': '80px',
                'opacity': '1'
            }).animate({
                'left': '170px',
                'opacity': '0'
            }, 200);
            $('.i_ele_man1').css({
                'opacity': '1'
            }).animate({
                'opacity': '0'
            }, 200);
            $('.i_ele_man2').css({
                'left': '660px',
                'opacity': '1'
            }).animate({
                'left': '570px',
                'opacity': '0'
            }, 200);

            // 光线效果
            $('.i_lottery_title img').css({
                'opacity': '0'
            });
            $('.i_lottery_title .light1').css({
                'left': '420px',
                'opacity': '1'
            }).delay(1000).animate({
                'left': '-245px'
            }, 3000);
            $('.i_lottery_title .light2').css({
                'top': '-55px',
                'opacity': '1'
            }).delay(2500).animate({
                'top': '110px'
            }, 2000);
            $('.i_lottery_title .light3').css({
                'left': '-245px',
                'opacity': '1'
            }).delay(3500).animate({
                'left': '900px'
            }, 3000);
            $('.i_lottery_title .light4').css({
                'top': '55px',
                'opacity': '1'
            }).delay(5000).animate({
                'top': '-55px'
            }, 2000);

            $('.i_t2').css({
                'opacity': '0'
            }).delay('500').animate({
                'opacity': '1'
            }, 500);

            // 重置被选择的屏块
            $('.nav_block_5').addClass('hasSelect').siblings('.hasSelect').removeClass('hasSelect').removeClass('at_same');
        }
    }

    // 重置动效
    function changeBox2 (num) {
        // 重置被选择的屏块
        $('.at_same').removeClass('at_same');
        if (num == 1) {
            // 动效(sport)
            $('.i_sport_man').css({
                'right': '-240px',
                'opacity': '1'
            }).animate({
                'right': '-400px',
                'opacity': '0'
            }, 300);
            return;
        }
        if (num == 2) {
            // 版面动效(video)
            $('.i_video_girl').css({
                'bottom': '0px',
                'opacity': '1'
            }).animate({
                'bottom': '-50px',
                'opacity': '0'
            }, 500);
            return;
        }
        if (num == 3) {
            // 上一版回去动画(elegame)
            $('.i_ele_man3').css({
                'left': '80px',
                'opacity': '1'
            }).animate({
                'left': '170px',
                'opacity': '0'
            }, 200);
            $('.i_ele_man1').css({
                'opacity': '1'
            }).animate({
                'opacity': '0'
            }, 200);
            $('.i_ele_man2').css({
                'left': '660px',
                'opacity': '1'
            }).animate({
                'left': '570px',
                'opacity': '0'
            }, 200);
        }
    }
}
