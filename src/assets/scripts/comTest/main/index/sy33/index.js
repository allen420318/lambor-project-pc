import BLDef from 'scripts/common/BLDef';
import CommonService from 'scripts/businessLogic/commonService';
import EventBus from 'scripts/common/EventBus';
export const initIndex33 = {
    data () {
        return {
            IsLogin: undefined,
            MemberInfo: {},
        };
    },
    created: function () {
        this.CheckIsLogin();
        this.$nextTick(() => {
            var swiper = new Swiper('.swiper-container1', {
                mousewheelControl: true,
                direction: 'horizontal',
                mousewheelForceToAxis: true,
                pagination: '.swiper-pagination',
                autoplay: 5000, // 可选选项，自动滑动
                on: {
                    init: function () {
                        swiperAnimateCache(this); // 隐藏动画元素
                        swiperAnimate(this); // 初始化完成开始动画
                    },
                    slideChangeTransitionEnd: function () {
                        swiperAnimate(this); // 每个slide切换结束时也运行当前slide动画
                        // this.slides.eq(this.activeIndex).find('.ani').removeClass('ani'); 动画只展现一次，去除ani类名
                    }
                }
            });
        })
    },
    mounted () {
        // function ScrollFind (box) {
        //     var btnLeft = $(box).find('.btn-left');
        //     var btnRight = $(box).find('.btn-right');
        //     var i = 0;
        //     var list = $(box).find('ul li');
        //     var len = list.length;
        //     animate(0);
        //     var scr = setInterval(function () {
        //         btnRight.click();
        //     }, 3000);
        //     $(box).mouseenter(function () {
        //         clearInterval(scr);
        //     });
        //     $(box).mouseleave(function () {
        //         scr = setInterval(function () {
        //             btnRight.click();
        //         }, 3000);
        //     });
        //     btnLeft.click(function () {
        //         if (i <= 0) {
        //             i = len;
        //         }
        //         if (i > 0) {
        //             i--;
        //             animate(i)
        //         }
        //     });
        //     btnRight.click(function () {
        //         if (i >= len - 1) {
        //             i = -1;
        //         }
        //         if (i < len - 1) {
        //             i++;
        //             animate(i)
        //         }
        //     });

        //     function animate (i) {
        //         list.eq(i).stop().fadeIn().siblings().stop().fadeOut();
        //     }
        // }
        // ScrollFind('.index-scroll-link');
    },
    methods: {

        GetMemberInfo: async function () {
            // 取得會員資訊
            const data = await CommonService.Comm_GetMemberInfo();
            if (data.Ret == 0) {
                this.MemberInfo = data.Data.Member;
            }
        },
        // 檢查登入狀態
        CheckIsLogin: async function () {
            const data = await CommonService.Comm_CheckPermission();
            switch (data.Status) {
                case BLDef.SysAccountStatus.NOT_LOGIN:
                    this.IsLogin = false;
                    break;
                case BLDef.SysAccountStatus.LOGINED_ENABLED:
                case BLDef.SysAccountStatus.LOGINED_FROZEN:
                    this.IsLogin = true;
                    break;
                default:
                    break;
            }
        },
        TriggerMemberUser: function (data) {
            console.log(data);
            this.$store.dispatch('setSwitchNameText', data);
            EventBus.$emit('personalInfo', data);
        },
    },
}
