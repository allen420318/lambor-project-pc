import 'scripts/comTest/main/index/sy34/script.js';
import 'scripts/comTest/main/index/sy34/load.js';
import 'bootstrap';
import BLDef from 'scripts/common/BLDef';
import Vue from 'vue';
import CommonService from 'scripts/businessLogic/commonService';
export const initIndex34 = {
    name: 'HelloWorld',
    components: {},
    data() {
        return {
            loginFlag: true,
            indexActivityObjList: [],
            LoadCompleted: false,
            DefaultActivityList: [],
            ActivityList: [],
            SlideFlag: undefined,
            currentSlideIndex: 0
        };
    },
    created: function () {
        this.SetDefaultActivityList();
        this.load();
    },
    methods: {
        load: function () {
            this.$nextTick(function () {
                memberLoad();
            })
        },
        // 設定預設優惠
        SetDefaultActivityList: function () {
            this.DefaultActivityList = [];
            // 優惠活動-首存
            this.DefaultActivityList.push({
                ID: '-1',
                Name: '首存返利',
                StartTick: new Date().getTime(),
                EndTick: new Date().getTime(),
                ActLogo: this.$ResourceCDN + '/EditionImg/LamborFormal/images/web34/activity/activity_img1.jpg?VersionCode'
            });
            // 優惠活動-續存
            this.DefaultActivityList.push({
                ID: '-2',
                Name: '续存返利',
                StartTick: new Date().getTime(),
                EndTick: new Date().getTime(),
                ActLogo: this.$ResourceCDN + '/EditionImg/LamborFormal/images/web34/activity/activity_img2.jpg?VersionCode'
            });

            this.GetEasyActivity();
        },
        // 取得輪播的優惠活動
        GetEasyActivity: async function () {
            const inputObj = {
                WGUID: Vue.prototype.$WGUID
            };
            const data = await CommonService.Comm_GetBonusActivity(inputObj);
            if (data.Ret == 0) {
                this.indexActivityObjList = data.Data.ActivityList;
                this.DealUpdatedData();
            }
        },
        // 設定輪播活動內容邏輯
        DealUpdatedData: function () {
            if (this.indexActivityObjList.length === 0) {
                this.ActivityList = this.DefaultActivityList;
            } else if (this.indexActivityObjList.length === 1) {
                this.ActivityList[0] = this.DefaultActivityList[0];
                this.ActivityList[1] = this.DefaultActivityList[1];
                this.ActivityList[2] = this.indexActivityObjList[0];
            } else if (this.indexActivityObjList.length === 2) {
                this.ActivityList[0] = this.DefaultActivityList[0];
                this.ActivityList[1] = this.indexActivityObjList[0];
                this.ActivityList[2] = this.indexActivityObjList[1];
            } else {
                this.ActivityList = this.indexActivityObjList;
            }
            this.LoadCompleted = true;
            setTimeout(function () {
                this.SildePromotionList();
            }.bind(this), 100);
        },
        // 啟用活動輪播
        SildePromotionList: function () {
            this.AutoPlay();
            $('#myCarousel').on('slide.bs.carousel', this.SlideEvent);
        },
        // 指定輪播到特定index
        SildeTo: function (item) {
            $('#myCarousel').carousel(item);
            $('#myCarousel').carousel('pause');
        },
        // 輪播事件偵測
        SlideEvent: function (event) {
            this.currentSlideIndex = $(event.relatedTarget).index();
        },
        // 進行自動播放
        AutoPlay: function () {
            $('#myCarousel').carousel('cycle');
        }
    },

};
