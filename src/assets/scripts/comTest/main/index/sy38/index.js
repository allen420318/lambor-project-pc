import { swiper, swiperSlide } from 'vue-awesome-swiper'

export const initIndex38 = {
	data(){
        return {
            swiperOption: {
                loop : true,
                autoplay:{
                    delay: 4000,
                    stopOnLastSlide: false,
                    disableOnInteraction: false,
                },
                pagination: {
                    el: '.swiper-pagination',
                    clickable :true,
                    renderBullet: function (index, className) {
                        return '<span class="' + className + '"><span>' + (index + 1) + '</span></span>';
                    },
                },
            },  
        }
    },
    components: {
    	swiper,
        swiperSlide
    }
};