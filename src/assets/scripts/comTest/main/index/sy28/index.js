export const initIndex28 = {
	mounted () {
		$('.index-btn-right').click(function(){
			$('.index-link ul').stop().animate({
				'left' : '-333px'
			},500,function(){
				$('.index-link ul').css('left','0');
				$('.index-link li:first').stop().insertAfter($('.index-link li:last'));
				});
		});
		$('.index-btn-left').click(function(){
			$('.index-link li:last').stop().insertBefore($('.index-link li:first'));
			$('.index-link ul').css('left','-333px');
			$('.index-link ul').stop().animate({
				'left' : '0'
			},500);
		});
	}
};