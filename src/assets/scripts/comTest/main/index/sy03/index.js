import '@/assets/scripts/customJs/web/jquery.SuperSlide.2.1.3'
import '@/assets/scripts/customJs/web/jquery.mousewheel.min'
import '@/assets/scripts/customJs/web/sectionscroll'
import '@/assets/scripts/customJs/web/bodyMove'

export const initIndex3 = {
    created: function () {
        this.$nextTick(() => {
            var swiper = new Swiper('.swiper-container', {
                mousewheelControl: true,
                direction: 'vertical',
                mousewheelForceToAxis: true,
                pagination: '.swiper-pagination',
                on: {
                    init: function () {
                        swiperAnimateCache(this); // 隐藏动画元素
                        swiperAnimate(this); // 初始化完成开始动画
                    },
                    slideChangeTransitionEnd: function () {
                        swiperAnimate(this); // 每个slide切换结束时也运行当前slide动画
                        // this.slides.eq(this.activeIndex).find('.ani').removeClass('ani'); 动画只展现一次，去除ani类名
                    }
                }
            });
        })
    },
    mounted () {
        // 3版导航隐藏
        $('.main-navList').stop();
        $('.main-navList').animate({
            'left': '815px'
        }, 500);
        // 首页 banner滚轮效果
        function code (scrollTop, h, l) {
            var num = parseInt(scrollTop / h);
            $('.item' + num + '').addClass('hasSelect').siblings('a').removeClass('hasSelect');

            // 背景缩放效果
            if (num == 0) {
                indexBg(1); // 背景动画
                title(1); // 标题的动画
            }
            if (num == 1) {
                indexBg(2); // 背景动画
                title(2); // 标题的动画
            }
            if (num == 2) {
                indexBg(3); // 背景动画
                title(3); // 标题的动画
            }
            if (num == 3) {
                indexBg(4); // 背景动画
                title(4); // 标题的动画
            }
            if (num == 4) {
                indexBg(5); // 背景动画
                title(5); // 标题的动画
            }

            // 判断箭头上下的方向
            if (num == (l - 1)) {
                $('.i_arrow').addClass('i_arrow2');
            } else {
                $('.i_arrow').removeClass('i_arrow2');
            }
        }
        // 首页标题出现函数
        function title (num) {
            $('.i_title_box img').stop();
            if (num == 1) {
                $('.i_play').show();
            } else {
                $('.i_play').hide();
            }
            if (num == 4) {
                $('.section4 .i_title_box img').css({
                    'bottom': '90px',
                    'opacity': '0'
                }).animate({
                    'bottom': '190px',
                    'opacity': '1'
                }, 1500);
            } else {
                $('.section' + num + ' .i_title_box img').css({
                    'margin-top': '100px',
                    'opacity': '0'
                }).animate({
                    'margin-top': '0',
                    'opacity': '1'
                }, 1500);
            }
            $('.section' + num + '').siblings('.section').find('.i_title_box').children('img').css({
                'opacity': '0',
                'margin-top': '100px'
            });
        }
        // 首页背景动画
        function indexBg (num) {
            $('.section' + num + '').addClass('section_at');
            $('.section' + num + '').siblings('.section_at').removeClass('section_at');
        }

        // 页面滚动函数
        function bodyMove (top) {
            /* 判断谷歌 */
            var userAgent = navigator.userAgent.toLowerCase(),
                s, o = {};
            var browser = {
                version: (userAgent.match(/(?:firefox|opera|safari|chrome|msie)[\/: ]([\d.]+)/))[1],
                safari: /version.+safari/.test(userAgent),
                chrome: /chrome/.test(userAgent),
                firefox: /firefox/.test(userAgent),
                ie: /msie/.test(userAgent),
                opera: /opera/.test(userAgent)
            }
            /* 获得浏览器的名称及版本信息 */
            if (browser.chrome) {
                $(document.body).animate({
                    'scrollTop': '' + top + ''
                }, 500);
            } else {
                $(document.documentElement).animate({
                    'scrollTop': '' + top + ''
                }, 500);
            }
        }

        // 页面初始加载
        var h = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
        var l = $('#arrow_up').children('a').length;
        $('#arrow_up').fadeIn(100);
        $('.index_main').height(h);

        // 首页起始标题动画
        $('.section .i_title_box img').css({
            'opacity': '0'
        });
        $('.section1 .i_title_box img').css({
            'margin-top': '100px',
            'opacity': '0'
        }).animate({
            'margin-top': '-50px',
            'opacity': '0.5'
        }, 1500);

        $('.i_play').mouseenter(function () {
            $(this).animate({
                'left': '500px'
            }, 500);
            $('.section1 .i_title_box img').animate({
                'opacity': '1'
            }, 500);
        }).mouseleave(function () {
            $(this).animate({
                'left': '510px'
            }, 500);
            $('.section1 .i_title_box img').animate({
                'opacity': '0.5'
            }, 500);
        });

        $(window).scroll(function () {
            var scrollTop = $(window).scrollTop();
            var Yoffset = window.pageYOffset || document.documentElement.scrollTop;

            if (scrollTop % h == 0) {
                code(scrollTop, h, l);
            }
        });

        $('#arrow_up a').bind('click', function (e) {
            var num = $(this).attr('data-num');
            $(this).addClass('hasSelect').siblings('a').removeClass('hasSelect');

            e.preventDefault();

            bodyMove(num * h); // 滚动函数（传入需要滚动的距离）
        });

        // 箭头
        $('#i_arrow img').click(function () {
            var scrollTop = $(window).scrollTop();
            var h = $(window).innerHeight();
            var length = $('.index_main section').length;
            var num = (scrollTop / h) + 1;

            if (num == length) {
                num = num - 2;
            }

            bodyMove(num * h); // 滚动函数（传入需要滚动的距离）
        });

        // 点击播放视频
        $('.i_play').click(function () {
            // 第一种加载方式
            $('#section1').append("<video autoplay='autoplay' controls='controls' width='100%' height='100%'><source src='/static/flash/000964_VTXHD.mp4' type='video/mp4'/></video>")

            $(this).animate({
                'opacity': '0'
            }, 500);
            $('#section1').removeClass('section_at');
            $('#i_arrow').animate({
                'opacity': '0'
            });

            setTimeout(function () {
                $('.i_play').animate({
                    'opacity': '1'
                }, 1500);
                $('#i_arrow').animate({
                    'opacity': '1'
                });
                indexBg(1);
                title(1);
                $('video').remove();
            }, 18000);
        });
    },
}
