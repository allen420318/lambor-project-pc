/* eslint-disable */
export function imgScroll(id) {
	var me = $(id);
	var box = me.children('.imgs-scroll-main');
	var list = box.children('li');
	var len = list.length;
	var ol = me.children('.imgs-scroll-dots');

	for(var d = 0; d < len; d++) {
		//生成dot
		ol.append('<li></li>');
	}
	var dots = ol.children('li');

	var current = 0;
	var flag = true;

	dots.click(function() {
		current = $(this).index();
		_animate(current);
	});

	var setIn = setInterval(function() {
		_setInterval();
	}, 3000);

	function _setInterval() {
		if(current >= len - 1) {
			current = -1;
		}
		if(flag && current < len - 1) {
			flag = false;
			current++;
			_animate(current);
		}
	}

	me.mouseenter(function() {
		clearInterval(setIn);
	}).mouseleave(function() {
		setIn = setInterval(function() {
			_setInterval();
		}, 3000);
	});

	function _animate(i) {
		list.eq(i).css({
			'z-index': '1'
		}).siblings().css({
			'z-index': '0'
		});
		box.animate({
			'left': '-100%'
		}, function() {
			_current(i);
			box.css('left', 0);
			flag = true;
		});
	}

	function _current(i) {
		list.eq(i).css({
			'left': '0'
		}).siblings().css({
			'left': '50%'
		});
		dots.eq(i).addClass('current').siblings().removeClass('current');
	}
	//初始化
	_current(0);
}