import {
    imgScroll
} from 'scripts/customJs/web/public'
import BLDef from 'scripts/common/BLDef'
import Vue from 'vue'
import CommonService from 'scripts/businessLogic/commonService'

export const indexActivity7 = {
    data() {
        return {
            ActivityList: [],
            IndexActivityObjList: [],
            DefaultActivityList: [],
        }
    },
    created: function () {
        // 优惠活动图片滚动
        this.SetDefaultActivityList()
    },
    mounted: function () {
        setTimeout(function () {
            imgScroll('#index-img-scoll')
        }, 100)
    },
    methods: {
        // 设定初始值
        SetDefaultActivityList: function () {
            this.DefaultActivityList = []
            // 设定默认的两个活动
            // 优惠活动--首存
            this.DefaultActivityList.push({
                ID: '-1',
                Name: '首存返利',
                StartTick: new Date().getTime(),
                EndTick: new Date().getTime(),
                ActLogo: this.$ResourceCDN + '/EditionImg/LamborFormal/images/web07/activities/activity_img1.jpg?VersionCode'
            })
            // 优惠活动--续存
            this.DefaultActivityList.push({
                ID: '-2',
                Name: '续存返利',
                StartTick: new Date().getTime(),
                EndTick: new Date().getTime(),
                ActLogo: this.$ResourceCDN + '/EditionImg/LamborFormal/images/web07/activities/activity_img2.jpg?VersionCode'
            })

            // 若有新的活动
            this.GetActivityData()
        },
        // 获取优惠活动信息
        GetActivityData: async function () {
            const inputObj = {
                WGUID: Vue.prototype.$WGUID
            }
            const data = await CommonService.Comm_GetBonusActivity(inputObj)
            if (data.Ret == 0) {
                this.IndexActivityObjList = data.Data.ActivityList
                this.DealUpdatedData()
            }
        },
        // 判定获取的优惠活动的数量
        DealUpdatedData: function () {
            if (this.IndexActivityObjList.length === 0) {
                this.ActivityList = this.DefaultActivityList
            } else if (this.IndexActivityObjList.length < 4) {
                this.ActivityList = this.IndexActivityObjList
            } else {
                // 超出长度 截取前面几个优惠活动进行展示
                this.ActivityList = this.IndexActivityObjList.slice(0, 4)
            }
        }
    }
};
