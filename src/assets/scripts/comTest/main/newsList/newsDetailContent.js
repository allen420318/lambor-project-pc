import InformationService from 'scripts/businessLogic/informationService';
import BLDef from 'scripts/common/BLDef';
import EventBus from 'scripts/common/EventBus';
import HttpService from 'scripts/common/HttpService';
import URLService from 'scripts/common/URLService';
import Vue from 'vue';

export const NewsDetailContent = {
    props: {
        ActiveID: undefined
    },
    data () {
        return {
            NewsDetail: {
                Title: '',
            },
            MessageCotentData: undefined,

            // 資源位置
            Resource: Vue.prototype.$ResourceURL,
            // 資源旗標
            ResourceFlag: false,
        };
    },

    created: function () {
        // if (this.$route.params.ID >= 0) {
        this.PageInit();
        // }
    },
    mounted () {
        // 新闻详情内容滚动条
        $('.news-detail-main-cont').perfectScrollbar();
    },
    methods: {
        ReturnBack: function () {
            if (localStorage.getItem('newsListAnchor') == undefined) {
                window.history.go(-1);
            } else {
                const tempObj = URLService.GetObjFromUrlParameter(localStorage.getItem('newsListAnchor'));
                tempObj.FromDetail = true;
                localStorage.setItem('newsListAnchor', URLService.GetUrlParameterFromObj(tempObj));
                this.$router.push({
                    name: 'NewsList'
                });
            } // end if
        },
        // 初始化
        PageInit: async function () {
            const dataObj = {
                WGUID: Vue.prototype.$WGUID,
                ID: this.$route.params.ID
            };
            const retData = await InformationService.GetNewsDetail(dataObj);

            // 失敗
            if (retData.Ret != 0) {
                const notifyData = {
                    NotifyMessage: retData.Message
                };
                EventBus.$emit('showNotifyMessage', notifyData);
                return;
            } // end if

            // 成功
            this.NewsDetail = retData.Data.Detail;
            const temp = await HttpService.GetContent(this.Resource + retData.Data.Detail.MessageCotent);
            this.MessageCotentData = temp;

            // 顯示資源
            this.ResourceFlag = true;
        }, // end PageInit
    },

};
