import NewsDetailContent from '@/components/sub/newsList/newsDetailContent';
import NewsDetailHeader from '@/components/sub/newsList/newsDetailHeader';
export const NewsDetail = {

    components: {
        NewsDetailContent,
        NewsDetailHeader
    }

};
