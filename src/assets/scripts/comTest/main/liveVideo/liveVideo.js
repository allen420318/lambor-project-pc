import Vue from 'vue';
import CommUtility from 'scripts/common/CommUtility';
import EventBus from 'scripts/common/EventBus';
import URLService from 'scripts/common/URLService';
import HttpService from 'scripts/common/HttpService';
// 列表内容
import livevideoContent from '@/components/general/liveVideo/liveVideoContent';
export const liveVideo = {
    components: {
        livevideoContent
    },
    data() {
        return {
            // livevideo List 2018.11.15 SeyoChen 遍历游戏
            GameList: [],
            VideoList: [],
            allVideoList: [],
            AddList: '', // 2018.11.15 SeyoChen 动态识别添加游戏模块
            PageCount: 0,
            pageNum: 1,
            pageSize: 27 // 分页每页9条
        };
    },

    mounted() {
        this.GatGame(); // 2019.03.28 SeyoChen 遍历游戏
    },
    methods: {
        // 遍历获取游戏列表
        GatGame: async function () {
            this.GameList = JSON.parse(localStorage.getItem('GameList'));

            const dataObj = {
                WGUID: '',
                GameTypeNo: ''
            };
            dataObj.WGUID = Vue.prototype.$WGUID;
            dataObj.GameTypeNo = '0'; // 0.全部 1.真人视讯 2.体育 3.彩票 4.电子
            const retData = await HttpService.PostDynamic(0, 214, dataObj);

            if (retData.Ret === 0 && this.GameList !== retData.Data.GameAPIVerdorList) {
                // 判断本地缓存是否与之前的相同
                this.GameList = retData.Data.GameAPIVerdorList;
                localStorage.setItem('GameList', JSON.stringify(retData.Data.GameAPIVerdorList));
            }

            for (let i = 0; i < this.GameList.length; i++) {
                // 2019.01.12 筛选游戏 并 判定状态 SeyoChen 启动中：702 维护中：703 关闭：704
                if (this.GameList[i].GameTypeNo === 1 && this.GameList[i].GameTypeState !== 704) {
                    this.allVideoList.push(this.GameList[i]);
                }
            }
            // 调试时使用，数据不够时随机插入1-20倍数据
            // let pauseList = JSON.parse(JSON.stringify(this.allVideoList));
            // let randomNum = 50 || Math.floor((Math.random() * 20) + 1);
            // console.log('randomNum', randomNum)
            // for (let i = 0; i < randomNum; i++) {
            //     this.allVideoList.push(...pauseList)
            // }

            // 判断总页数
            this.PageCount = Math.ceil(this.allVideoList.length / this.pageSize);
            // 只有一页时不需要显示换页
            this.PageCount = this.PageCount === 1 ? 0 : this.PageCount;
            // 初始计算第一页游戏条数
            this.$refs['livevideoContent'].computeGameList(1, this.allVideoList);
        },
        // 開新視窗
        PlayGame: function (gameList) {
            // 2018.11.15 SeyoChen 遍历游戏
            this.GameData.GameAPIVendor = gameList.GameApiNo;
            this.GameData.GameCode = gameList.GameCode;
            localStorage.setItem('PlayGamePostData', URLService.GetUrlParameterFromObj(this.GameData));
            // EventBus.$emit('showTransferMessage', this.GameData);
            CommUtility.OpenPlayGameWindow('webPlayGame', 'PlayGamePopUpWindow');
        }, // end PlayGame
    },
};
