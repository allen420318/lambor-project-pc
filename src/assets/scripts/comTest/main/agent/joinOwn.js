export const JoinOwn = {
    mounted() {
		// 23版 按钮点击后出现进入动画
		$('.enter-btn').click(function() {
			$('.join-content').animate({
				'left': '-100%'
			});
			$('.join-page-transition').animate({
				'height': '660px'
			});
        });
        // 23版滚动条
				$('.join-main-content').perfectScrollbar({wheelPropagation:true});
				
				// 02版轮播滚动
				this.load();
	},
	methods: {
		load: function() {
			this.$nextTick(function() {
				var bannerSlider = new Slider($('#banner-tabs'), {
					time: 5000,
					delay: 400,
					event: 'hover',
					auto: true,
					mode: 'fade',
					controller: $('#bannerCtrl'),
					activeControllerCls: 'active'
				});
			})
		},
	}
}