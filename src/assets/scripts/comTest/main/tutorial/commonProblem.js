import axios from 'axios'

export const commonProblem = {
    data () {
        return {
            msg: '',
            navs: 1
        }
    },
    methods: {
        getmsg () {
          let lang = this.$i18n.locale;
            axios.get('/static/json/tutorial/tutorialFAQ-'+lang+'.json').then((res) => {
                this.msg = res.data.data
            }).catch((error) => {
                console.log('tutorialFAQ.json')
            });
        },
        switchNav (i) {
            this.navs = i;

            $('.tutorial-inside').scrollTop(0)
            $('.tutorial-pull-down3').removeClass('fiveOpen')
        },
        cloneSpan (ev) {
            if ($(ev.target).text() == '收起详情') {
                $(ev.target).html('点击详情')
                $(ev.target).siblings('p').css('height', '30px')
            } else {
                $(ev.target).html('收起详情')
                $(ev.target).siblings('p').css('height', 'auto')
            }
        }
    },

    created () {
        this.getmsg()
    },
    mounted () {
        // 竖滚动条插件
        // $('#commonProblem-scroll').perfectScrollbar();

        // 33版
        var moveTo3 = 0;
        var comindex = 3;
        $('.problem-navs li').click(function () {
            moveTo3 = $(this).attr('data-num');
            showmore03(moveTo3);
            $(this).addClass('on').siblings().removeClass('on');
        });
        $('.nextbtn02').click(function () {
            if (moveTo3 < comindex) {
                moveTo3++;
                showmore03(moveTo3);
            } else {
                return moveTo3;
            }
            $('.problem-navs >li').eq(moveTo3).addClass('on').siblings().removeClass('on');
        });
        $('.prebtn02').click(function () {
            if (moveTo3 > 0) {
                moveTo3--;
                showmore03(moveTo3);
                $('.problem-box >div').eq(moveTo3).css('display', 'block').siblings().css('display', 'none');
            } else {
                return moveTo3;
            }
            $('.problem-navs >li').eq(moveTo3).addClass('on').siblings().removeClass('on');
        });

        function showmore03 (num) {
            //  var goleft = num * 971;
            //  $(".problem-box").animate({
            //  	"left": "-" + goleft + "px"
            //  });
            $('.problem-box >div').eq(moveTo3).css('display', 'block').siblings().css('display', 'none');
        }
    },
    watch: {
        '$i18n.locale' () {
            this.getmsg();
        }
    }
}
