import axios from 'axios'

export const bettingSkills = {
    data () {
        return {
            msg: '', // 存储本地json文字内容

            tutorialSports: '', // 存储本地tutorialSports json文字内容
            navs: 1,
            bettingSkill: 1, // 彩票投注标记
            sportsSkill: 1, // 体育博彩标记
            vHtml: [] // 段落内容
        }
    },
    methods: {
        // 菜单
        openLi (event) {
            if ($(event.target).siblings('ul').length != 0) {
                $(event.target).siblings('ul').show()
                $(event.target).parent().siblings().children('ul').hide()
            } else {
                var one = $(event.target).attr('one')
                var two = $(event.target).attr('two')
                var three = $(event.target).attr('three')
                    // console.log('tag',one,two,three)

                if (one != undefined && two != undefined && three != undefined) {
                    this.vHtml = this.tutorialSports.sportsShow.contant[one].rules[two].sonrules[three].grandsonrules
                } else if (one != undefined && two != undefined) {
                    this.vHtml = this.tutorialSports.sportsShow.contant[one].rules[two].sonrules
                } else if (one != undefined) {
                    this.vHtml = this.tutorialSports.sportsShow.contant[one].rules
                }

                $('.tutorial_abslut_2').hide()
                $('.tutorial-pull-down1').removeClass('fiveOpen2')
                $('.tutorial-inside').scrollTop(0)
            }
            // $('#bettingSkill-scroll').perfectScrollbar();
        },
        // 获取json内容
        getmsg () {
            let lang = this.$i18n.locale;
            axios.get('/static/json/tutorial/tutorialSkills-' + lang + '.json').then((res) => {
                this.msg = res.data.data
            });

            axios.get('/static/json/tutorial/tutorialSports-' + lang + '.json').then((res) => {
                this.tutorialSports = res.data.data
                this.vHtml = this.tutorialSports.sportsShow.contant[0].rules
            });
            console.log('this.tutorialSports', this.tutorialSports)
        },
        switchNav (i) {
            this.navs = i;
            $('.tutorial-inside').scrollTop(0);
            if (i == 'cp') {
                this.bettingSkill = 1;
                $('.tutorial_abslut_1').show()
            } else if (i == 'ty') {
                this.sportsSkill = 1;
                $('.tutorial_abslut_2').show()
            } else {
                $('.tutorial-pull-down1').removeClass('fiveOpen2')
            }
            // $('#bettingSkill-scroll').perfectScrollbar();
        },

        switchBettingSkill (i) { // 彩票投注标记修改
            this.bettingSkill = i;
            $('.tutorial-inside').scrollTop(0)
            $('.tutorial_abslut_1').hide()
            $('.tutorial-pull-down1').removeClass('fiveOpen2')
        },
        switchSportsSkill (i) { // 体育博彩标记修改
            if (i == 1) {
                $('.tutorial_abslut_2').hide()
            }
            this.sportsSkill = i;
            $('.tutorial-inside').scrollTop(0)
            $('.tutorial-pull-down1').removeClass('fiveOpen2')
        },
        cloneSpan (ev) {
            if ($(ev.target).text() == '收起详情') {
                $(ev.target).html('点击详情')
                $(ev.target).siblings('p').css('height', '30px')
            } else {
                $(ev.target).html('收起详情')
                $(ev.target).siblings('p').css('height', 'auto')
            }
        }
    },
    created () {
        this.getmsg()
    },
    mounted () {
        'use strict';

        // 竖滚动条插件
        // $('#bettingSkill-scroll').perfectScrollbar();

        // 33版
        var moveTo2 = 0;
        var skillindex = 5;
        $('.betaskill-navs >li').click(function () {
            moveTo2 = $(this).attr('data-num');
            showmore02(moveTo2);
            $(this).addClass('on').siblings().removeClass('on');
        });
        $('.nextbtn01').click(function () {
            if (moveTo2 < skillindex) {
                moveTo2++;
                showmore02(moveTo2);
            } else {
                return moveTo2;
            }
            $('.betaskill-navs >li').eq(moveTo2).addClass('on').siblings().removeClass('on');
        });
        $('.prebtn01').click(function () {
            if (moveTo2 > 0) {
                moveTo2--;
                showmore02(moveTo2);
            } else {
                return moveTo2;
            }
            $('.betaskill-navs >li').eq(moveTo2).addClass('on').siblings().removeClass('on');
        });

        function showmore02 (num) {
            // var goleft = num * 870;
            // $(".betaskill-box").animate({
            // 	"left": "-" + goleft + "px"
            // });
            $('.betaskill-box >div').eq(moveTo2).css('display', 'block').siblings().css('display', 'none');
        }
    },
    watch: {
        '$i18n.locale' () {
            this.getmsg();
        }
    }
}
