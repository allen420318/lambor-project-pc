import EventBus from 'scripts/common/EventBus'
import Vue from 'vue'
import BKBLDef from 'scripts/common/BLDef'
import CommonService from 'scripts/businessLogic/commonService'
import CommUtility from 'scripts/common/CommUtility'

export const registerSuccess = {
    data() {
        return {
            IsLogin: false,
            MemberInfo: {
                UnreadCount: 0
            },
            IsGetBalanceIng: false,
            VBalance: '',
            LogoImgPath: '',
            LogoName: ''
        }
    },
    // props: {
    // IsLogin: undefined
    // },
    mounted: function () {
        const self = this;
        EventBus.$on('SignalR_AddPrivateMessage', () => {
            self.MemberInfo.UnreadCount += 1;
        });
        EventBus.$on('SignalR_ReadPrivateMessage', (data) => {
            self.MemberInfo.UnreadCount = data.Data;
        });
        EventBus.$on('fundGetBalanceComplete', (SummaryAmount) => {
            self.VBalance = SummaryAmount;
            self.IsGetBalanceIng = false;
        });
        EventBus.$on('memberHeaderUpdateBalance', () => {
            self.GetBalance();
        });
        // this.LoadLangList();
        // this.BindingEvent();
    },
    created: function () {
        this.GetLogoData();
        this.CheckIsLogin()
        EventBus.$on('afterLogin', () => {
            this.CheckIsLogin()
        })
    },
    methods: {
        GetLogoData: async function () {
            const DataObj = {
                WGuid: Vue.prototype.$WGUID,
                platformType: BKBLDef.PlatformType.Web_PC
            };
            const retData = await CommonService.HomePage_LoadMainPage(DataObj);
            console.log('next');
            if (retData.Ret === 0) {
                this.LogoImgPath = this.$ResourceCDN + retData.Data.MasterLogo;
                this.LogoName = retData.Data.Name;
            } else {
                this.LogoImgPath = localStorage.getItem('LogoImgPath');
                this.LogoName = localStorage.getItem('LogoName');
            }
        },
        GetBalance: async function () {
            if (this.$route.name === 'webMemberFundManage') {
                // 更新資金管理餘額
                EventBus.$emit('updateFundBalance');
            } else {
                this.VBalance = await CommUtility.CalculateSummaryAmount();
            }
        },
        // 检查登录状态
        CheckIsLogin: async function () {
            const data = await CommonService.Comm_CheckPermission()
            console.log(data)
            switch (data.Status) {
                case BKBLDef.SysAccountStatus.NOT_LOGIN:
                    this.IsLogin = false
                    break
                case BKBLDef.SysAccountStatus.LOGINED_ENABLED:
                case BKBLDef.SysAccountStatus.LOGINED_FROZEN:
                    this.IsLogin = true
                    this.GetMemberInfo()
                    break
                default:
                    break
            }
        },
        GetMemberInfo: async function () {
            // 会员信息
            const data = await CommonService.Comm_GetMemberInfo()
            if (data.Ret === 0) {
                this.MemberInfo = data.Data.Member
            }
        },
    },
    watch: {
        IsLogin: function () {
            if (this.IsLogin === true) {
                this.GetMemberInfo()
                // this.GetBalance()
            }
        },
    },
}
