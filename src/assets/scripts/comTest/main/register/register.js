import EventBus from "scripts/common/EventBus"
import Vue from "vue"
import Regex from "scripts/common/CommDef"
import BKBLDef from "scripts/common/BLDef"
import CommService from "scripts/businessLogic/commonService"
import RegistService from "scripts/businessLogic/registerService"
import UserAgreement from "@/components/sub/register/userAgreement"
import CommUtility from "scripts/common/CommUtility"

export const initRegister = {
  data() {
    return {
      Flag: {
        AcctFocus: undefined,
        AcctErrorIsShow: undefined,
        PhoneNoFocus: undefined,
        PhoneNoErrorIsShow: undefined,
        PwdFocus: undefined,
        PwdValidateIsShow: undefined,
        // CheckPwdErrorIsShow: false,
        WithdrawPwdFocus: undefined,
        WithdrawPwdValidateIsShow: undefined,
        // CheckWithdrawPwdErrorIsShow: false,
        Focus: undefined,
        ErrorIsShow: undefined
      },
      LogoPath: localStorage.getItem("LogoImgPath"),
      // 正則表達式類別
      Reg: Regex.Regex,

      // 驗證碼
      AuthImg: "",

      // 登入資料物件
      RegistObj: {
        Acct: "",
        PhoneNo: "",
        Pwd: "",
        AuthCode: ""
      },

      // PreToken
      PreToken: "",
      IsBoxChecked: undefined,
      AcctAvailable: true,
      tempObj: false,

      // 驗證碼顏色設定
      RGB: {
        R: 255,
        G: 100,
        B: 255
      },
      LogoImgPath: localStorage.getItem("LogoImgPath"),
      LogoName: "",

      // 是否可以查看密码信息 20190812 SeyoChen
      IfPwd: {
        Password: false,
        WithdrawPwd: false
      },
      CheckPhoneNoAvailable: {
        PhoneNo: "",
        WGUID: ""
      },
      regisFlag: true,
      succFlagText: false
    }
  }, // end data

  created: function () {
    this.GetAuthCode()
    this.GetLogoData()
    console.log("注册页面11111111111111111111111")
    console.log(this.$route.params)
    console.log("注册页面11111111111111111111111")
  }, // end created
  mounted: function () {
    CommUtility.BindEnterTrigger("AddMemberForm", this.SumbitRegist)
  },
  methods: {
    // 点击可查看密码 20190812 SeyoChen
    ClickShowPwd: function (num) {
      switch (num) {
        case 0:
          this.IfPwd.Password = !this.IfPwd.Password
          break
        case 1:
          this.IfPwd.WithdrawPwd = !this.IfPwd.WithdrawPwd
          break
        default:
          break
      }
    },

    GetLogoData: async function () {
      const DataObj = {
        WGuid: Vue.prototype.$WGUID,
        platformType: BKBLDef.PlatformType.Web_PC
      }
      const retData = await CommService.HomePage_LoadMainPage(DataObj)
      console.log("next")
      if (retData.Ret === 0) {
        this.LogoImgPath = this.$ResourceCDN + retData.Data.MasterLogo
        this.LogoName = retData.Data.Name
      } else {
        this.LogoImgPath = localStorage.getItem("LogoImgPath")
        this.LogoName = localStorage.getItem("LogoName")
      }
    },

    // 取得驗證碼
    GetAuthCode: async function () {
      /* eslint-disable camelcase */
      const VCode_CharacterRGB = this.RGB
      const RGBData = {
        VCode_CharacterRGB
      }
      const retData = await CommService.VerifyCode_Gen(RGBData)
      this.AuthImg = retData.Data.VCode
      this.PreToken = retData.Data.ID
    }, // end GetAuthCode
    // 比較密碼
    // ComparePassword: function () {
    // this.Flag.CheckPwdErrorIsShow = CommUtility.ComparePassword(this.RegistObj.Pwd, this.RegistObj.CheckPwd);
    // }, // end ComparePassword

    // 比較取款密碼
    // CompareWithdrawPassword: function () {
    // this.Flag.CheckWithdrawPwdErrorIsShow = CommUtility.CompareWithdrawPassword(this.RegistObj.WithdrawPwd, this.RegistObj.CheckWithdrawPwd);
    // }, // end CompareWithdrawPassword

    // 驗證帳號是否重複
    CheckAcctAvailable: async function () {
      this.flag = !this.$validator.errors.has("Acct")

      if (this.flag && this.RegistObj.Acct.length > 0) {
        const obj = {
          Acct: this.RegistObj.Acct,
          WGUID: Vue.prototype.$WGUID
        }
        const retData = await CommService.CheckAcctAvailable(obj)
        this.AcctAvailable = retData.IsAvailable
      } else {
        this.AcctAvailable = true
      } // end if
    }, // end CheckAcctAvailable

    // 提交註冊申請
    SumbitRegist: async function () {
      if (this.regisFlag) {
        let anyInvalid = false
        // 是否通過各種欄位驗證
        const validateConfirm = await this.validate()
        if (!validateConfirm || this.Flag.CheckPwdErrorIsShow || this.Flag.CheckWithdrawPwdErrorIsShow) {
          anyInvalid = true
        } // end if
        // 是否閱讀過[用戶協議]
        // if (!this.IsBoxChecked) {
        //     this.IsBoxChecked = false;
        //     anyInvalid = true;
        // } // end if

        if (anyInvalid === true) {
          return
        } // end if

        // 注册手机号进行检测 会员手机号不能重复
        this.CheckPhoneNoAvailable.PhoneNo = this.RegistObj.PhoneNo
        this.CheckPhoneNoAvailable.WGUID = Vue.prototype.$WGUID
        const DataInput = {
          CheckPhoneNo: this.CheckPhoneNoAvailable
        }
        const CheckPhoneNo = await CommService.Comm_CheckPhoneNoAvailable(DataInput)

        if (CheckPhoneNo.Ret === 0 && CheckPhoneNo.Data.CheckPhoneNo.IsAvailable === false) {
          const notifyData = {
            NotifyMessage: this.$t("message.agent_alike_mobilePhone")
          }
          EventBus.$emit("showNotifyMessage", notifyData)
          this.regisFlag = true
          return
        } else if (CheckPhoneNo.Ret === 19) {
          const notifyData = {
            NotifyMessage: this.$t("message.member_phoneNumber_error"),
            NotifySubMessage: this.$t("message.agent_legal_mobilePhone")
          }
          EventBus.$emit("showNotifyMessage", notifyData)
          this.regisFlag = true
          return
        }
        // 注册手机号进行检测 会员手机号不能重复 20190830 SeyoChen end

        // 驗證帳號是否已被使用
        await this.CheckAcctAvailable()
        if (!this.AcctAvailable) {
          this.regisFlag = true
          return
        } // end if

        // 處理中則直接返回
        if (this.tempObj === true) {
          this.regisFlag = true
          return
        } // end if
        this.tempObj = true
        this.regisFlag = false
        // 註冊資料物件
        const DataObj = {
          VCodeID: this.PreToken,
          VCode: this.RegistObj.AuthCode,
          WGUID: Vue.prototype.$WGUID,
          PromotionUrl: window.location.href.split("/").slice(0, 3).join("/"), // 完整DomainName含參數
          PromotionInfo: this.$route.params.Data, // 參數
          Member: this.RegistObj
        }

        // 註冊服務
        EventBus.$emit("GlobalLoadingTrigger", true)
        const retData = await RegistService.Regist(DataObj)
        EventBus.$emit("GlobalLoadingTrigger", false)

        // 錯誤訊息物件
        const notifyData = {
          NotifyMessage: retData.Message
        }
        console.log("我进来了")
        console.log(DataObj)
        console.log(retData)
        console.log("我进来了")
        if (retData.Ret == 0) {
          this.succFlagText = true

          this.regisFlag = true
          // 清理缓存 20190808 SeyoChen
          localStorage.removeItem("Token")
          localStorage.removeItem("Auth")
          localStorage.removeItem("logoutTime")
          localStorage.removeItem("Acct") // 2018.09.20 SeyoChen
          localStorage.removeItem("CorrelationIBMemberId")
          localStorage.removeItem("userId")
          localStorage.removeItem("IBStatus")
          localStorage.removeItem("loginFlag")
          // 清理缓存 20190808 SeyoChen end

          // 成功
          EventBus.$emit("loginFlagCheck", "true")
          EventBus.$emit("afterLogin")
          localStorage.setItem("Token", retData.Data.LoginData.Token)
          localStorage.setItem("Auth", retData.Data.LoginData.Auth)
          localStorage.setItem("Acct", retData.Data.LoginData.Acct)
          localStorage.setItem("loginFlag", "true")
          localStorage.setItem("CorrelationIBMemberId", retData.Data.LoginData.CorrelationIBMemberId)
          localStorage.setItem("userId", retData.Data.LoginData.AcctID)
          localStorage.setItem("IBStatus", retData.Data.LoginData.IBStatus)
          // 存到local storage
          localStorage.setItem("RegisterSuccess_Account", retData.Data.LoginData.Acct)

          // 解决从推广链接的会员推广进去复制链接 注册成功后进入首页要弹出公告
          if (this.$route.params.Data) {
            sessionStorage.removeItem("IsFirstEnter")
          }
          // 成功並跳頁
          this.$router.push({
            name: "RegisterSuccess"
          })
          //  localStorage.setItem('Acct', this.LoginObj.Acct) // 2018.09.20 SeyoChen
          localStorage.setItem("logoutTime", new Date().getTime() + Vue.prototype.$SurviveMinutes * 60 * 1000)
          EventBus.$emit("afterLogin")
        } else if (retData.Ret === BKBLDef.ErrorRetType.COMM_VERIFY_CODE_INCORRECT) {
          this.succFlagText = true
          this.regisFlag = true
          // 驗證碼錯誤
          this.tempObj = false
          this.RegistObj.AuthCode = ""
          this.$validator.reset()
          this.GetAuthCode()
          EventBus.$emit("showNotifyMessage", notifyData)
        } else {
          this.succFlagText = true
          this.regisFlag = true
          // 失敗
          this.tempObj = false
          this.RegistObj = {}
          this.$validator.reset()
          this.GetAuthCode()
          EventBus.$emit("showNotifyMessage", notifyData)
        } // end if
        this.regisFlag = true
        setTimeout(() => {
          this.succFlagText = false
        }, 1000)
      } else {
        // EventBus.$emit('GlobalLoadingTrigger', true);
        if (this.succFlagText) {
          EventBus.$emit("GlobalLoadingTrigger", false)
        }
      }
    }, // end SumbitRegist

    // 所有欄位驗證
    validate: function () {
      Object.keys(this.fields).forEach((key) => {
        this.fields[key].touched = true
      })
      return this.$validator.validateAll().then((result) => {
        return result
      })
    }, // end validate

    // 彈出用戶協議視窗
    ShowUserAgreement: function () {
      UserAgreement.methods.OpenUserAgreement()
    } // end ShowUserAgreement
  }, // end methods

  components: {
    UserAgreement
  }, // end components
  mounted() {}
}
