import BLDef from 'scripts/common/BLDef'
import Vue from 'vue'
import CommUtility from 'scripts/common/CommUtility'
import EventBus from 'scripts/common/EventBus';
import URLService from 'scripts/common/URLService'
import HttpService from 'scripts/common/HttpService'

export const PokerKingLong = {
    data() {
        return {
            // poker List 2018.11.15 SeyoChen 遍历游戏
            GameKingList: [],
            KingPoKerList: [],
            AddKingList: '', // 2018.11.15 SeyoChen 动态识别添加游戏模块

            // 真人AG遊戲Post Model
            GameData: {
                GameAPIVendor: undefined,
                GameCode: '',
                GameCatlog: BLDef.GameCatlogType.Poker,
                PlayType: BLDef.IdentityType.FORMAL,
                Platform: BLDef.PlatformType.Web_PC
            },
        };
    },
    created: function () {
        this.GatKingGame(); // 2019.3.28 SeyoChen 遍历游戏
    },
    mounted: function () {
        // 2018.11.15 SeyoChen 动态识别添加游戏模块
        this.AjectKingAddBox(this.AddKingList);
    },
    methods: {
        // 遍历获取游戏列表
        GatKingGame: async function () {
            this.GameList = JSON.parse(localStorage.getItem('GameList'));
            const dataObj = {
                WGUID: '',
                GameTypeNo: ''
            };
            dataObj.WGUID = Vue.prototype.$WGUID;
            dataObj.GameTypeNo = '0'; // 0.全部 1.真人视讯 2.体育 3.彩票 4.电子
            const retData = await HttpService.PostDynamic(0, 214, dataObj);

            if (retData.Ret === 0 && this.GameList !== retData.Data.GameAPIVerdorList) {
                // 判断本地缓存是否与之前的相同
                this.GameList = retData.Data.GameAPIVerdorList;
                localStorage.setItem('GameList', JSON.stringify(retData.Data.GameAPIVerdorList));
            }

            for (let i = 0; i < this.GameList.length; i++) {
                // 2019.01.12 筛选游戏 并 判定状态 SeyoChen 启动中：702 维护中：703 关闭：704
                if (this.GameList[i].GameTypeNo === 5 && this.GameList[i].GameTypeState !== 704) {
                    this.KingPoKerList.push(this.GameList[i]);
                }
            }

            // 2018.11.15 SeyoChen 动态识别添加游戏模块
            this.AddKingList = parseInt(this.KingPoKerList.length);

            const self = this;
            setTimeout(function () {
                self.AjectKingAddBox(self.AddKingList);
            }, 50);
        },

        // 2018.11.15 SeyoChen 动态识别添加游戏模块
        AjectKingAddBox: function (num) {
            const boxSize = parseInt(3); // 页面中一行的box个数
            const boxHtml = '<li><a href="javascript:void(0);">敬请期待</a></li>';
            const box = $('#poker_box');

            if (num % boxSize === 0 && num !== 0) {

            } else {
                for (let i = 0; i < Math.abs((boxSize - (num % boxSize))); i++) {
                    box.append(boxHtml);
                }
            }
        },
        // 获取游戏列表 2018.11.15 SeyoChen end

        // 開新視窗
        PlayKingGame: function (gameList) {
            // 2018.11.15 SeyoChen 遍历游戏
            this.GameData.GameAPIVendor = gameList.GameApiNo;
            this.GameData.GameCode = gameList.GameCode;
            this.GameData.GameCatlog = BLDef.GameCatlogType.Poker; // 2018.12.01 传递游戏类型

            localStorage.setItem('PlayGamePostData', URLService.GetUrlParameterFromObj(this.GameData));
            // EventBus.$emit('showTransferMessage', this.GameList);
            CommUtility.OpenPlayGameWindow('webPlayGame', 'PlayGamePopUpWindow');
        }, // end PlayGame
    },
}
