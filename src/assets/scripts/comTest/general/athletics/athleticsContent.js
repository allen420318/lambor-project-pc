/**
 * @name 电子竞技内容组件
 * @author dingruijie
 * @time 2019-09-16
 * @augments athleticList 初始化数据列表 类型 [Array]
 * @event athleticsEnterGame 进入游戏点击事件 使用方法 @athleticsEnterGame= Function
 */

export default {
    name: 'athletics-content',
    props: {
        athleticList: {
            type: Array,
            default () {
                return []
            }
        },
        WattingGame: {
          type: Boolean,
          default: false
        }
    }
}
