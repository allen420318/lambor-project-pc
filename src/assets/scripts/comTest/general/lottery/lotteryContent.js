/**
 * @name 彩票投注内容组件
 * @author dingruijie
 * @time 2019-09-17
 * @augments PageCount 计算后的分页总数 类型：Number 默认0
 * @augments pageSize 分页每页条数 类型: Number 默认每页10条
 * @event 父组件请求完数据 初始化分页数据 this.$refs[refname].computeGameList(1，allList) 初始传1表示渲染第一页
 * @event PlayKGGame 进入游戏点击事件 使用方法 @PlayKGGame= Function
 */

import lotteryPaginate from '@/components/sub/paginate';
export default {
    name: 'lottery-content',
    components: {
        lotteryPaginate
    },
    props: {
        PageCount: {
            type: Number,
            default: 0
        },
        pageSize: {
            type: Number,
            default: 10
        }
    },
    data () {
        return {
            pageNum: 1,
            LotteryList: [],
            allLotteryList: []
        }
    },
    methods: {
        GetCurrentPageData: async function (pageNo) {
            this.computeGameList(pageNo);
        },
        // 计算每一页展示数据
        computeGameList (num, initList) {
            if (initList) this.allLotteryList = initList;
            this.pageNum = num;
            this.lotteryList = [];
            let gameList = [];
            let pageSize = this.pageSize; // 每页最大10条
            let lowerLimit = pageSize * (num - 1); // 从下限开始
            let upperLimit = pageSize * num; // 从下限结束
            let allList = this.allLotteryList; // 数据总列表
            if (allList.length > lowerLimit && allList.length > upperLimit) {
                // 游戏总数大于当前页最大序号
                for (let i = lowerLimit; i < upperLimit; i++) {
                    gameList.push(allList[i]);
                }
            } else {
                for (let i = lowerLimit; i < allList.length; i++) {
                    gameList.push(allList[i]);
                }
            }
            this.LotteryList = gameList;
            // 插入box之前清除原先已有box避免重复
            $('.lottery-expect').remove();
            setTimeout(() => {
                this.AjectAddBox(this.LotteryList.length);
            }, 50);
        },
        // 2018.11.15 SeyoChen 动态识别添加游戏模块
        AjectAddBox: function (num) {
            const boxSize = parseInt(5); // 页面中一行的box个数
            const boxHtml =
                `<li class="lottery-logo-li lottery-expect lotteryAbnormal">
                <i class="staytuned abnormalDefault"></i></li>`;
            const box = $('.lottery-logo-lists');
            if (num % boxSize === 0 && num !== 0) {} else {
                for (let i = 0; i < Math.abs(boxSize - (num % boxSize)); i++) {
                    box.append(boxHtml);
                }
            }
        }
    }
}
