import InformationService from 'scripts/businessLogic/informationService';
import BLDef from 'scripts/common/BLDef';
import EventBus from 'scripts/common/EventBus';
import HttpService from 'scripts/common/HttpService';
import CommUtility from 'scripts/common/CommUtility';
import URLService from 'scripts/common/URLService';
import Vue from 'vue';

export const NewsListContent = {
    props: {
        GetCurrentPageData: '',
    },
    data () {
        return {
            NewsList: {
                Title: '',
                TitlePic: '',
                MessageCotent: '',
                MessageTick: '',
            },

            MessageContentData: [],

            // 分頁資料
            PageInfo: {
                PageNo: 1,
                PageSize: 5,
                PageCount: 0,
                TotalCount: 0,
            },

            CommUtility: CommUtility,

            // 資源位置
            Resource: Vue.prototype.$ResourceURL,
            // 資源旗標
            ResourceFlag: false,

            // 錨點暫存物件
            AnchorDataObject: {
                Anchor: undefined,
                PageNo: undefined,
                FromDetail: false,
            },

            NewsListNull: false, // 2018.12.05 SeyoChen 当新闻条数为0时 隐藏
        };
    },

    created: function () {
        this.GetNews(1);
        this.GoToAnchor();
    },
    mounted: function () {
        this.GetNews(1);
    },
    methods: {

        // 獲取新聞清單
        GetNews: async function (pageNo) {
            this.PageInfo.PageNo = pageNo;
            const dataObj = {
                WGUID: Vue.prototype.$WGUID,
                PagingInfo: this.PageInfo,
            };
            const retData = await InformationService.GetNewsList(dataObj);

            // 失敗
            if (retData.Ret != 0) {
                const notifyData = {
                    NotifyMessage: retData.Message
                };
                EventBus.$emit('showNotifyMessage', notifyData);
                return;
            } // end if

            // 成功
            this.NewsList = retData.Data.NewsList;
            this.MessageContentData = [];
            for (let i = 0; i < this.NewsList.length; i++) {
                this.GetContentData(i);
            } // end for

            // 更新Paging
            this.PageInfo.PageCount = Math.ceil(retData.Data.PagingInfo.TotalCount / retData.Data.PagingInfo.PageSize);
            this.$parent.PageInfo.PageCount = this.PageInfo.PageCount;
            EventBus.$emit('Paginate_ResetPageNo', pageNo);
            // 資源顯示
            this.ResourceFlag = true;

            // 2018.12.05 SeyoChen 当新闻条数为0时不执行加载
            if (retData.Data.PagingInfo.TotalCount !== 0) {
                this.NewsListNull = true;
            }

            this.$nextTick(function () {
                // 新闻列表滚动条
                $('.news-main-cont').perfectScrollbar();
            });
        }, // end GetNews

        // 獲得內容
        GetContentData: async function (count) {
            const temp = await HttpService.GetContent(this.Resource + this.NewsList[count].MessageCotent);
            // this.MessageContentData.splice(count, 1, temp);
            this.MessageContentData.push(temp); // 修改首页信息最后一条为 undefined
        }, // end GetContentData

        //  跳至新聞詳細
        UrlRedirectDetail: function (target) {
            this.AnchorDataObject.Anchor = $(window).scrollTop();
            this.AnchorDataObject.PageNo = this.GetCurrentPageData == '' ? 1 : this.GetCurrentPageData;
            localStorage.setItem('newsListAnchor', URLService.GetUrlParameterFromObj(this.AnchorDataObject));
            this.$router.push({
                name: 'NewsListDetails',
                params: {
                    ID: target.ID
                }
            });
        }, // end UrlRedirectDetail

        // 將畫面移到指定錨點
        GoToAnchor: function () {
            if (localStorage.getItem('newsListAnchor')) {
                const tempObj = URLService.GetObjFromUrlParameter(localStorage.getItem('newsListAnchor'));
                if (tempObj.FromDetail) {
                    const self = this;
                    setTimeout(function () {
                        self.GetNews(tempObj.PageNo);
                        $(window).scrollTop(tempObj.Anchor);
                        localStorage.removeItem('newsListAnchor');
                    }, 0);
                } // end if
            } // end if
        }, // end GoToAnchor
    },

    watch: {
        GetCurrentPageData: function () {
            this.GetNews(this.GetCurrentPageData);
        },
    },
};
