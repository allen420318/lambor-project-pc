export const ActivityListOwn = {
	mounted() {
		// 23版 按钮点击后出现进入动画
		$('.enter-btn').click(function() {
			$('.activity-content').animate({
				'left': '-100%'
			});
			$('.activity-change-index').animate({
				'height': '601px'
			});
		});
	},
};