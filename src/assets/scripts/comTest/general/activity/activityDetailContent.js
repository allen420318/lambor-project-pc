import InformationService from 'scripts/businessLogic/informationService';
import CommonService from 'scripts/businessLogic/commonService';
import CommUtility from 'scripts/common/CommUtility';
import BLDef from 'scripts/common/BLDef';
import EventBus from 'scripts/common/EventBus';
import HttpService from 'scripts/common/HttpService';
import Vue from 'vue';

import PersonalCenterService from 'scripts/businessLogic/personalCenterService';

export const ActivityDetailContent = {

    // props: {
    //     ActiveID: undefined
    // },

    data () {
        return {
            PromotionDetail: {

            },

            // 在线支付
            OnlinePaymentAcctCnt: undefined,
            // 公司入款
            CompAcctCnt: undefined,

            // 圖片旗標
            ActLogoFlag: false,
            // 資源位置
            Resource: Vue.prototype.$ResourceURL,
            // 首存，续存
            OtherActivityData: false,
            // 传递活动详情到个人中心
            TransActivityData: {

            }
        };
    },

    created: function () {
        this.GetPaySty(); // 2018.11.30 判断 在线付款/公司入款 是否开启
        if (this.$route.params.ID >= 0) {
            this.OtherActivityData = true;
            this.GetPromotionDetail(this.$route.params.ID);
        }
        // if (this.ActiveID >= 0) {
        //     this.OtherActivityData = true;
        //     this.GetPromotionDetail(this.ActiveID);
        // }
    },
    mounted () {
        // $('.details-return').click(function() {
        //     $('.activity-detail-content').fadeOut();
        //     $('.activity-page-cont').fadeIn();
        // });
    },
    methods: {
        TriggerMemberUser: function (data) {
            this.$store.dispatch('setSwitchNameText', data);
            EventBus.$emit('personalInfo');
        },
        // 2018.11.30 判断 在线付款/公司入款 是否开启
        GetPaySty: async function () {
            const login = localStorage.getItem('Token');
            if (login == undefined) {
                return;
            }
            const retData = await PersonalCenterService.Deposit_LoadMainPage();
            if (retData.Ret == BLDef.ErrorRetType.SUCCESS) {
                this.OnlinePaymentAcctCnt = retData.Data.OnlinePaymentAcctCnt; // 在线支付号标
                this.CompAcctCnt = retData.Data.CompAcctCnt; // 公司入款号标
            }
        },
        // 2018.11.30 判断 在线付款/公司入款 是否开启 end

        // 獲得頁面資料
        GetPromotionDetail: async function (ID) {
            const dataObj = {
                WGUID: Vue.prototype.$WGUID,
                ID: ID
            };
            const retData = await InformationService.GetActivityDetail(dataObj);
            this.TransActivityData = retData.Data.Detail; // 2018.09.21 SeyoChen

            // 失敗
            if (retData.Ret != 0) {
                const notifyData = {
                    NotifyMessage: retData.Message
                };
                EventBus.$emit('showNotifyMessage', notifyData);
                return;
            } // end if

            // 成功
            this.PromotionDetail = retData.Data.Detail;
            const contentData = retData.Data.Detail.ActContent;
            this.PromotionDetail.ActContent = await HttpService.GetContent(this.Resource + retData.Data.Detail.ActContent);

            if (contentData === '') {
                return
            }
            alert(JSON.stringify(this.PromotionDetail.ActContent));
            // 修改进入显示资源路径问题 2019.1.25 SeyoChen
            $('.news-course-p').html(this.PromotionDetail.ActContent);
            // 修改进入显示资源路径问题 2019.1.25 SeyoChen end

            if (!(retData.Data.Detail.ActLogo == null)) {
                this.ActLogoFlag = true;
            } // end if

            // $('#activity-detail-main-cont').perfectScrollbar();
        }, // end GetPromotionDetail

        // 開啟視窗
        OpenWindow: function () {
            CommUtility.WebShowUniqueForm('informationForm');
        }, // end OpenWindow

        // 關閉視窗
        CloseWindow: function () {
            CommUtility.WebCloseUniqueForm('informationForm');
        }, // end CloseWindow

        // 传值到支付页面 2018.09.21 SeyoChen
        ToRechargePage: async function (num) {
            const retDataCheck = await CommonService.Comm_CheckPermission();
            console.log(retDataCheck.Status);
            if (retDataCheck.Status != 0) {
                this.CloseWindow();
                // this.$router.push( { name: 'webMemberDeposit', params: { ChargeType: num, ActivityData: this.TransActivityData } });
                this.$route.params.ActivityData = this.TransActivityData;
                this.$route.params.ChargeType = num;
                this.$store.dispatch('setSwitchNameText', 'webMemberDeposit');
                EventBus.$emit('personalInfo');
            } else {
                this.CloseWindow();
                this.$router.push({
                    name: 'Login'
                });
            }
        }
    },
    // watch: {
    //     ActiveID: function () {
    //         if (this.ActiveID >= 0) {
    //             this.OtherActivityData = true;
    //             this.GetPromotionDetail(this.ActiveID);
    //         }
    //     },
    // },

};
