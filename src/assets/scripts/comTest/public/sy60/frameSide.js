import BLDef from 'scripts/common/BLDef';
import Vue from 'vue';
import CommonService from 'scripts/businessLogic/commonService';
import CommUtility from 'scripts/common/CommUtility';

export const initService60 = {
    data() {
        return {
            LoadCompleted: false,
            IsCustomerServiceShowing: false,
            CustomerService: {}
        };
    },
    props: {
        IsLogin: undefined
    },
    created: function () {
        this.GetCustomerServiceInfo();
    },
    mounted() {
        scroll_top();

        function scroll_top() { // 置顶
            $('.s-right-top').click(function () {
                $('html,body').animate({
                    'scrollTop': '0'
                });
            });
            // s_t();
            $(window).scroll(function () {
                // s_t();
            });
            // function s_t() {
            // 	if($(window).scrollTop() > 0) {
            // 		$('.service').show();
            // 	} else {
            // 		$('.service').hide();
            // 	}
            // } //s_t();
        } // scroll_top();
        // 新开窗口并保持居中
        function openWin(win_url, win_w, win_h) {
            var url = win_url; // 转向页面的地址;
            var name = ''; // 页面名称，可为空;
            var iWidth = win_w; // 弹出窗口的宽度;
            var iHeight = win_h; // 弹出窗口的高度;
            // 获得窗口的垂直位置
            var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
            // 获得窗口的水平位置
            var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
            window.open(url, name, 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',status=no,toolbar=no,menubar=no,location=no,resizable=no,scrollbars=0,titlebar=no');
        }
        /* 开启对应新窗口 */
        $('.s-right-nav2').click(function () {
            openWin('/FreeTrial', '1900', '1080');
        });
    },
    methods: {
        // 弹出新窗口
        OpenNewBox60: function (aa) {
            window.open(aa, '客服', 'scrollbars=1,width=365,height=565,left=10,top=150');
            // return window.open(aa, 'newindow', 'height=565,width=357,top=0,left=0,toolbar=no,menubar=no,scrollbars=no,resizable=no,location=no,status=no');
        },
        // 開起此彈窗
        OpenWindow: function () {
            CommUtility.WebShowUniqueForm('forgetPasswordForm');
        }, // end OpenWindow

        GetCustomerServiceInfo: async function () {
            const inputObj = {
                WGUID: Vue.prototype.$WGUID
            };
            const data = await CommonService.Comm_GetCustomerServiceInfo(inputObj);
            if (data.Ret === BLDef.ErrorRetType.SUCCESS) {
                this.CustomerService = data.Data.CustomerServiceInfo;
            }
        },
        // 滑鼠移入右側浮動視窗
        serviceMouseEnter: function () {
            $('.service').addClass('hasblock');
        },
        // 滑鼠移出右側浮動視窗
        serviceMouseLeave: function () {
            if (this.IsCustomerServiceShowing === false) {
                $('.service').removeClass('hasblock');
            }
        },
        showCustomerService: function () {
            this.IsCustomerServiceShowing = true;
        },
        hideCustomerService: function () {
            this.IsCustomerServiceShowing = false;
        },
        CallQQClient: function (QQAcct) {
            const QQHref = 'http://wpa.qq.com/msgrd?v=3&uin=' + QQAcct + '&menu=yes';
            window.open(QQHref);
        }
    },
};
