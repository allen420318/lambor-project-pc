export const initNav23 = {
	data () {
		return {
				LogoPath: localStorage.getItem('LogoImgPath'),
		};
	},
	mounted () {
		function Second (b, p, d) {
			$(b).click(function () {
				$(p).animate({
					'left': '-100%'
				});
				$(d).animate({
					'height': '601px'
				});
			});
		}

		function btns (e, a, b) {
			var sb_true = 0;
			$(e).click(function () {
				$(this).hide().siblings().show();
				if(sb_true == 0) {
					$(a).fadeIn();
					$(b).fadeOut();
					sb_true = 1;
				} else {
					$(a).fadeOut();
					$(b).fadeIn();
					sb_true = 0;
				}
			});
		}
		//btns('.s-b-btn p', '.sb-cont-sbty', '.s-b-nav,.sb-tzgz-tysm');
		Animation();

		function Animation() {
			var list = $('.mainNav-u23 li'),
				mb_list = $('.m-b-nav ul li');
			var len = list.length;
			var i = 0,
				s = 0;
			var me = 0,
				mb_me = 0,
				a, b, x;
			/**左边线条**/
			var line_len = $('.mainNav-u23 li > a').length * 10;
			for(var line = 0; line < line_len; line++) {
				$('.all-line').append('<li></li>')
			}
			$('.all-line > li').each(function() {
				var $this = $(this).index() + 1;
				if($this % 2 != 0) {
					if($this % 5 == 0) {
						$(this).addClass('line-long');
					}
				}
			});
			list.click(function() {
				i = $(this).index();
				$(this).addClass('on').siblings().removeClass('on');
				animate(i);
				line_color();
				Return();
				if(i == me) {
					return false;
				} else {
					if(i > me) {
						Top('.all-main-cont > div', '.all-main-cont', i);
					} else {
						Bottom('.all-main-cont > div', '.all-main-cont', i);
					}
				}
				if(i == len - 1) {
					animate2(0);
				} else {
					animate2(3);
				}
				me = i;
			});
			$('.index-link').each(function() { //底部图片跳转
				$(this).find('a').click(function() {
					i = $(this).index() + 1;
					list.eq(i).addClass('on').siblings().removeClass('on');
					animate(i);
					line_color();
					Return();
					if(i == me) {
						return false;
					} else {
						if(i > me) {
							Top('.all-main-cont > div', '.all-main-cont', i);
						} else {
							Bottom('.all-main-cont > div', '.all-main-cont', i);
						}
					}
					me = i;
				});
			})
			$("#cont_ent").bind('mousewheel', function(event, delta) { //鼠标滚动,需引用jquery.mousewheel.js
				var $this = $(this),
					timeoutId = $this.data('timeoutId');
				if(timeoutId) {
					clearTimeout(timeoutId);
				}
				$this.data('timeoutId', setTimeout(function() {
					$('.all-main-cont > div').animate({
						'left': '0'
					});
					if(delta > 0) { //向上滚
						if(s <= 0) {
							if(i > 0) {
								i--;
								list.eq(i).addClass('on').siblings().removeClass('on');
								Bottom('.all-main-cont > div', '.all-main-cont', i);
								line_color(i);
								animate(i);
							}
						} else {
							if(s > 0) {
								s--;
								Right('.m-b-main > div', '.m-b-main', s);
								mb_list.eq(s).addClass('on').siblings().removeClass('on');
								animate2(s);
							}
						}
						if(i < len - 1) {
							animate2(3);
						}
						me = i;
						return false;
					} else if(delta < 0) { //向下滚
						if(i < len) {
							i++;
							if(i < len) {
								list.eq(i).addClass('on').siblings().removeClass('on');
								Top('.all-main-cont > div', '.all-main-cont', i);
								line_color(i);
								animate(i);
							}
						}
						if(i == len - 1) {
							animate2(0);
						} else {
							animate2(3);
						}
						if(i >= len) {
							if(s < 3) {
								s++;
								Left('.m-b-main > div', '.m-b-main', s);
								mb_list.eq(s).addClass('on').siblings().removeClass('on');
								animate2(s);
							}
							if(s > 0) {
								i = i - 1;
							}
						}
						me = i;
						return false;
					}
					$this.removeData('timeoutId');
					$this = null;
				}, 100));
				return false;
			});

			function Top(a, b, x) {
				$(a).eq(x).stop().css({
					'top': "100%",
					'z-index': '100'
				});
				$(b).stop().animate({
					'top': "-100%"
				}, 500, function() {
					$(b).stop().css('top', '0');
					$(a).eq(x).stop().css({
						'top': '0'
					});
					$(a).eq(x).siblings().stop().css({
						'top': "0",
						'z-index': '0'
					});
				});
			}

			function Bottom(a, b, x) {
				$(a).eq(x).stop().css({
					'top': "-100%",
					'z-index': '100'
				});
				$(b).stop().animate({
					'top': "100%"
				}, 500, function() {
					$(b).stop().css('top', '0');
					$(a).eq(x).stop().css({
						'top': '0'
					});
					$(a).eq(x).siblings().stop().css({
						'top': "0",
						'z-index': '0'
					});
				});
			}

			function Left(a, b, x) {
				$(a).eq(x).stop().css({
					'left': "50%",
					'z-index': '1'
				});
				$(b).stop().animate({
					'left': "-100%"
				}, 500, function() {
					$(b).stop().css('left', '0');
					$(a).eq(x).stop().css({
						'left': '0'
					});
					$(a).eq(x).siblings().stop().css({
						'left': "0",
						'z-index': '0'
					});
				});
			}

			function Right(a, b, x) {
				$(a).eq(x).stop().css({
					'left': "-50%",
					'z-index': '1'
				});
				$(b).stop().animate({
					'left': "100%"
				}, 500, function() {
					$(b).stop().css('left', '0');
					$(a).eq(x).stop().css({
						'left': '0'
					});
					$(a).eq(x).siblings().stop().css({
						'left': "0",
						'z-index': '0'
					});
				});
			}

			function animate(text) { //文字淡出
				$('.all-main-cont > div').eq(text).find('.bg .index-content .l-b-cont').css({
					'bottom': '55%',
					'opacity': '1'
				});
				$('.all-main-cont > div').eq(text).siblings().find('.bg .index-content .l-b-cont').css({
					'bottom': '30%',
					'opacity': '0'
				});
				if(text == 0) {
					$('.all-main-cont > div').eq(text).find('.bg .index-content .l-b-cont').css({
						'bottom': '60%'
					});
				}
				$('.all-main-cont > div').eq(text).siblings().find('.bg  > .transition').css({
					'height': '0'
				});
			}

			function animate2(text2) { //文字淡出
				$('.m-b-main > div').eq(text2).find('.index-content .l-b-cont').css({
					'bottom': '50%',
					'opacity': '1'
				});
				$('.m-b-main > div').eq(text2).siblings().find('.index-content .l-b-cont').css({
					'bottom': '30%',
					'opacity': '0'
				});
			}

			function Return() {
				$('.all-main-cont > div').animate({
					'left': '0'
				});
				s = 0;
				$('.m-b-main > div').eq(s).css({
					'z-index': '1'
				}).siblings().css({
					'z-index': '0'
				});
				mb_list.eq(s).addClass('on').siblings().removeClass('on');
			}
			line_color();

			function line_color() {
				var clr, clrs, lines = $('.all-line > li');
				lines.each(function() {
					lines.removeClass('on').css('width', '10px');
					for(var clr = 0; clr < 9; clr++) {
						clrs = i * 10 + clr;
						line_on();
						lines.eq(clrs).addClass('on');
					}

					function line_on() {
						if(clrs % 10 == 0 || clrs % 10 == 8) {
							lines.eq(clrs).css('width', '10px');
						}
						if(clrs % 10 == 1 || clrs % 10 == 7) {
							lines.eq(clrs).css('width', '20px');
						}
						if(clrs % 10 == 2 || clrs % 10 == 6) {
							lines.eq(clrs).css('width', '30px');
						}
						if(clrs % 10 == 3 || clrs % 10 == 5) {
							lines.eq(clrs).css('width', '40px');
						}
						if(clrs % 10 == 4) {
							lines.eq(clrs).css('width', '50px');
						}
					}
				});
			}
		}
		//}
	}
};