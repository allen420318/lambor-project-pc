import Vue from 'vue';
import BLDef from 'scripts/common/BLDef'
import CommonService from 'scripts/businessLogic/commonService'

export const initService23 = {
    props: {
        IsLogin: undefined
    },
    data() {
        return {
            IsCustomerServiceShowing: false,
            CustomerService: {},
            indexActivityObjList: [],
            DefaultActivityList: [],
            ActivityList: [],
            LoadCompleted: false,
        };
    },
    mounted() {
        //右侧鼠标滑过效果
        $('.lambor-right-main>div>a').mouseenter(function () {
            $(this).siblings('div').show();
        });
        $('.lambor-right-main>div').mouseleave(function () {
            $(this).find('>div').hide();
        });;
    },
    created: function () {
        this.GetCustomerServiceInfo();
        this.SetDefaultActivityList();
    },
    methods: {
        serviceMouseEnter: function () {},
        // 滑鼠移出右側浮動視窗
        serviceMouseLeave: function () {
            if (this.IsCustomerServiceShowing === false) {

            }
        },
        // 弹出新窗口
        OpenNewBox23: function (aa) {
            return window.open(aa, 'newindow', 'height=565,width=357,top=0,left=0,toolbar=no,menubar=no,scrollbars=no,resizable=no,location=no,status=no');
        },


        GetCustomerServiceInfo: async function () {
            const inputObj = {
                WGUID: Vue.prototype.$WGUID
            };
            const data = await CommonService.Comm_GetCustomerServiceInfo(inputObj);
            if (data.Ret === BLDef.ErrorRetType.SUCCESS) {
                this.CustomerService = data.Data.CustomerServiceInfo;
            }
        },
        CallQQClient: function (QQAcct) {
            const QQHref = 'http://wpa.qq.com/msgrd?v=3&uin=' + QQAcct + '&menu=yes';
            window.open(QQHref);
        },
        // 設定預設優惠
        SetDefaultActivityList: function () {
            this.DefaultActivityList = [];
            //優惠活動-首存
            this.DefaultActivityList.push({
                ID: '-1',
                Name: '首存返利',
                StartTick: new Date().getTime(),
                EndTick: new Date().getTime(),
                ActLogo: this.ResourceCDN + '/EditionImg/LamborFormal/images/web23/activity/activity_img1.jpg?VersionCode'
            });
            // 優惠活動-續存
            this.DefaultActivityList.push({
                ID: '-2',
                Name: '续存返利',
                StartTick: new Date().getTime(),
                EndTick: new Date().getTime(),
                ActLogo: this.ResourceCDN + '/EditionImg/LamborFormal/images/web23/activity/activity-img2.jpg?VersionCode'
            });
            this.GetEasyActivity();
        },
        // 取得輪播的優惠活動
        GetEasyActivity: async function () {
            const inputObj = {
                WGUID: Vue.prototype.$WGUID
            };
            const data = await CommonService.Comm_GetBonusActivity(inputObj);

            if (data.Ret == 0) {
                this.indexActivityObjList = data.Data.ActivityList;
                this.DealUpdatedData();
            }
        },
        // 設定輪播活動內容邏輯
        DealUpdatedData: function () {
            if (this.indexActivityObjList.length === 0) {
                this.ActivityList = this.DefaultActivityList;
            } else if (this.indexActivityObjList.length === 1) {
                this.ActivityList[0] = this.DefaultActivityList[0];
                this.ActivityList[1] = this.DefaultActivityList[1];
                this.ActivityList[2] = this.indexActivityObjList[0];
            } else if (this.indexActivityObjList.length === 2) {
                this.ActivityList[0] = this.DefaultActivityList[0];
                this.ActivityList[1] = this.indexActivityObjList[0];
                this.ActivityList[2] = this.indexActivityObjList[1];
            } else {
                this.ActivityList = this.indexActivityObjList;
            }
            this.LoadCompleted = true;
        },
    },
}
