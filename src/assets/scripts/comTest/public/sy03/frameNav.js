export const initNav3 = {
    data () {
        return {
            LogoPath: localStorage.getItem('LogoImgPath'),
        };
    },
    mounted () {
        // 首页# 导航按钮
        $('.main-navList').css({
            'left': '815px'
        });
        $('.main-cont-nav2 .main-navList').css({
            'left': '0'
        });
        var isclose = true;
        $('.nav_btn').click(function () {
            if (isclose) {
                $('.main-navList').stop();
                $('.main-navList').animate({
                    'left': '0'
                }, 500);
                isclose = false;
            } else {
                $('.main-navList').stop();
                $('.main-navList').animate({
                    'left': '815px'
                }, 500);
                isclose = true;
            }
        });
    }
};
