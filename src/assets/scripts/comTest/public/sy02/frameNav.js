export const initNav2 = {
    data () {
        return {
            LogoPath: localStorage.getItem('LogoImgPath'),
        };
    },
    mounted () {
        // 导航下划线
        $('.nav_item').mouseenter(function () {
            $(this).siblings('.nav_item_line').stop();
            $(this).siblings('.nav_item_line').css({
                'width': '0',
                'display': 'block !important'
            }).animate({
                'width': '62px'
            });
        }).mouseleave(function () {
            $(this).siblings('.nav_item_line').stop();
            $(this).siblings('.nav_item_line').css({
                'display': 'block !important'
            }).animate({
                'width': '0'
            });
        });
        // 热门游戏 下拉菜单
        $('.nav_item_no').mouseenter(function () {
            $(this).children('.nav_item_down').stop();
            $(this).children('.nav_item_down').slideDown();
        }).mouseleave(function () {
            $(this).children('.nav_item_down').stop();
            $(this).children('.nav_item_down').slideUp();
        });
        // 热门游戏 下划线
        $('.nav_item2').mouseenter(function () {
            $(this).children('.nav_item_line').stop();
            $(this).children('.nav_item_line').css({
                'width': '0',
                'display': 'block'
            }).animate({
                'width': '62px'
            });
        }).mouseleave(function () {
            $(this).children('.nav_item_line').stop();
            $(this).children('.nav_item_line').css({
                'display': 'block'
            }).animate({
                'width': '0'
            });
        });
    },
};
