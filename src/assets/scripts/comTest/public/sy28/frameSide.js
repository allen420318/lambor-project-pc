import BLDef from 'scripts/common/BLDef';
import Vue from 'vue';
import CommonService from 'scripts/businessLogic/commonService';

export const initService28 = {
    data() {
        return {
            indexActivityObjList: [],
            LoadCompleted: false,
            DefaultActivityList: [],
            ActivityList: [],
            SlideFlag: undefined,
            currentSlideIndex: 0
        };
    },
    props: {
        IsLogin: undefined
    },
    created: function () {
        this.SetDefaultActivityList();
    },
    methods: {
        // 設定預設優惠
        SetDefaultActivityList: function () {
            this.DefaultActivityList = [];
            // 優惠活動-首存
            this.DefaultActivityList.push({
                ID: '-1',
                Name: '首存返利',
                StartTick: new Date().getTime(),
                EndTick: new Date().getTime(),
                ActLogo: this.ResourceCDN + '/EditionImg/LamborFormal/images/web/activities/activity_img1.jpg?VersionCode'

            });
            // 優惠活動-續存
            this.DefaultActivityList.push({
                ID: '-2',
                Name: '续存返利',
                StartTick: new Date().getTime(),
                EndTick: new Date().getTime(),
                ActLogo: this.ResourceCDN + '/EditionImg/LamborFormal/images/web/activities/activity_img2.jpg?VersionCode'
            });

            this.GetEasyActivity();
        },
        // 取得輪播的優惠活動
        GetEasyActivity: async function () {
            const inputObj = {
                WGUID: Vue.prototype.$WGUID
            };
            const data = await CommonService.Comm_GetBonusActivity(inputObj);
            if (data.Ret == 0) {
                this.indexActivityObjList = data.Data.ActivityList;
                this.DealUpdatedData();
            }
        },
        // 設定輪播活動內容邏輯
        DealUpdatedData: function () {
            if (this.indexActivityObjList.length === 0) {
                this.ActivityList = this.DefaultActivityList;
            } else if (this.indexActivityObjList.length === 1) {
                this.ActivityList[0] = this.DefaultActivityList[0];
                this.ActivityList[1] = this.DefaultActivityList[1];
                this.ActivityList[2] = this.indexActivityObjList[0];
            } else if (this.indexActivityObjList.length === 2) {
                this.ActivityList[0] = this.DefaultActivityList[0];
                this.ActivityList[1] = this.indexActivityObjList[0];
                this.ActivityList[2] = this.indexActivityObjList[1];
            } else {
                this.ActivityList = this.indexActivityObjList;
            }
            this.LoadCompleted = true;
        },
    },
    mounted() {
        $(".SbtnActivity").mouseenter(function () {
            $(".in_s_carousel").show();
        });
        $(".in_twentyEight").mouseleave(function () {
            $(".in_s_carousel").hide();
        });
    }
};
