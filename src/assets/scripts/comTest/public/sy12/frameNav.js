 import BLDef from 'scripts/common/BLDef';
 import EventBus from 'scripts/common/EventBus';
 import CommonService from 'scripts/businessLogic/commonService';
 import CommUtility from 'scripts/common/CommUtility';

 export const initNav12 = {
     data () {
         return {
             IsLogin: undefined,
             MemberInfo: {},
             LogoPath: localStorage.getItem('LogoImgPath'),
         };
     },
     created: function () {
         this.CheckIsLogin();
     },
     methods: {
         // 收藏本站
         AddFavorite: function () {
             const notifyData = {};
             if (window.sidebar && window.sidebar.addPanel) { // Mozilla Firefox Bookmark
                 window.sidebar.addPanel(document.title, window.location.href, '');
             } else if (window.external && ('AddFavorite' in window.external)) { // IE Favorite
                 window.external.AddFavorite(location.href, document.title);
             } else if (window.opera && window.print) { // Opera Hotlist
                 this.title = document.title;
             } else { // webkit - safari/chrome
                 if (navigator.userAgent.toLowerCase().indexOf('mac') != -1) {
                     notifyData.NotifyMessage = `${this.$t('message.top_click')} Command/Cmd+D ${this.$t('message.top_favorite')}`;
                 } else {
                     notifyData.NotifyMessage = `${this.$t('message.top_click')} CTRL+D ${this.$t('message.top_favorite')}`
                 }
                 EventBus.$emit('showNotifyMessage', notifyData);
             }
         }, // end AddFavorite
         // 客服彈窗
         CustomerWindow: function () {
             CommUtility.WebShowUniqueForm('CustomerServiceWindow');
         }, // end  CustomerWindow
         // 檢查登入狀態
         CheckIsLogin: async function () {
             const data = await CommonService.Comm_CheckPermission();
             switch (data.Status) {
                 case BLDef.SysAccountStatus.NOT_LOGIN:
                     this.IsLogin = false;
                     break;
                 case BLDef.SysAccountStatus.LOGINED_ENABLED:
                 case BLDef.SysAccountStatus.LOGINED_FROZEN:
                     this.IsLogin = true;
                     this.GetMemberInfo();
                     break;
                 default:
                     break;
             }
         },
         GetMemberInfo: async function () {
             // 取得會員資訊
             const data = await CommonService.Comm_GetMemberInfo();
             if (data.Ret == 0) {
                 this.MemberInfo = data.Data.Member;
             }
         },
         // 檢查帳號狀態
         CheckLoginStatusBeforeRoute: async function (routeName) {
             const notifyData = {};
             const data = await CommonService.Comm_CheckPermission();

             switch (data.Status) {
                 case BLDef.SysAccountStatus.LOGINED_ENABLED:
                     this.$router.push({
                         name: routeName
                     });
                     break;
                 case BLDef.SysAccountStatus.LOGINED_FROZEN:
                     notifyData.NotifyMessage = this.$t('message.top_account_freezing');
                     EventBus.$emit('showNotifyMessage', notifyData);
                     break;
                 default:
                     break;
             }
         },
         // 弹出客服窗口
         OpenNewBox: function (serveLink) {
             window.open(serveLink, this.$t('message.top_customer_service'), 'scrollbars=1,width=365,height=565,left=10,top=150');
             this.CloseWindow();
         },
         // 開起此彈窗
         OpenWindow: function () {
             CommUtility.WebShowUniqueForm('forgetPasswordForm');
         }, // end OpenWindow
     },
     watch: {
         fastEntranceObj: function () {
             this.DealUpdatedData();
         }
     }
 };
