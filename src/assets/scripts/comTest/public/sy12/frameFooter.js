import CommonService from 'scripts/businessLogic/commonService';
import BKBLDef from 'scripts/common/BLDef';

export const initFooter12 = {
    data () {
        return {
            LogoPath: localStorage.getItem('LogoImgPath'),
            LogoName: localStorage.getItem('LogoName')
        };
    },
    created: function () {
        this.GetLogoData();
    },
    methods: {
        GetLogoData: async function () {
            const DataObj = {
                WGuid: this.$WGUID,
                platformType: BKBLDef.PlatformType.Web_PC
            };
            const retData = await CommonService.HomePage_LoadMainPage(DataObj);
            console.log('next');
            if (retData.Ret === 0) {
                this.LogoPath = this.$ResourceCDN + retData.Data.MasterLogo;
                this.LogoName = retData.Data.Name;
            } else {
                this.LogoPath = localStorage.getItem('LogoImgPath');
                this.LogoName = localStorage.getItem('LogoName');
            }
        },
        gotop: function () {
            $('body,html').animate({
                scrollTop: 0
            }, 1000);
        },
    },
};
