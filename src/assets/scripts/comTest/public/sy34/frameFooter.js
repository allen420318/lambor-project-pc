import CommonService from 'scripts/businessLogic/commonService';
import BKBLDef from 'scripts/common/BLDef';

export const initFooter34 = {
    name: 'HelloWorld',
    components: {},
    data () {
        return {
            SiteName: '',
            LogoPath: localStorage.getItem('LogoImgPath'),
            LogoName: localStorage.getItem('LogoName')
        };
    },
    mounted () {

    },
    created: function () {
        this.load();
        this.SiteName = sessionStorage.getItem('SiteName');
        this.GetLogoData();
    },
    methods: {
        GetLogoData: async function () {
            const DataObj = {
                WGuid: this.$WGUID,
                platformType: BKBLDef.PlatformType.Web_PC
            };
            const retData = await CommonService.HomePage_LoadMainPage(DataObj);
            console.log('next');
            if (retData.Ret === 0) {
                this.LogoPath = this.$ResourceCDN + retData.Data.MasterLogo;
                this.LogoName = retData.Data.Name;
            } else {
                this.LogoPath = localStorage.getItem('LogoImgPath');
                this.LogoName = localStorage.getItem('LogoName');
            }
        },
        load: function () {
            this.$nextTick(function () {

            })
        },
    }

};
