export const initNav34 = {
    name: 'HelloWorld',
    components: {},
    props: {
        barIndex: {
            type: String
        },
        phoneIndex: {
            type: String
        }
    },
    data () {
        return {
            loginFlag: true,
            navBars: [{
                url1: this.$ResourceCDN + '/EditionImg/lambor34.0/images/web/w_28.png',
                url2: this.$ResourceCDN + '/EditionImg/lambor34.0/images/web/w_28a.png',
                link: 'LiveVideo'
            }, {
                url1: this.$ResourceCDN + '/EditionImg/lambor34.0/images/web/w_29.png',
                url2: this.$ResourceCDN + '/EditionImg/lambor34.0/images/web/w_29a.png',
                link: 'Electronic'
            }, {
                url1: this.$ResourceCDN + '/EditionImg/lambor34.0/images/web/w_30.png',
                url2: this.$ResourceCDN + '/EditionImg/lambor34.0/images/web/w_30a.png',
                link: 'Sport'
            }, {
                url1: this.$ResourceCDN + '/EditionImg/lambor34.0/images/web/w_31.png',
                url2: this.$ResourceCDN + '/EditionImg/lambor34.0/images/web/w_31a.png',
                link: 'Lottery'
            }, {
                url1: this.$ResourceCDN + '/EditionImg/lambor34.0/images/web/s_32.png',
                url2: this.$ResourceCDN + '/EditionImg/lambor34.0/images/web/s_32a.png',
                link: 'Poker'
            }, {
                url1: this.$ResourceCDN + '/EditionImg/lambor34.0/images/web/w_95.png',
                url2: this.$ResourceCDN + '/EditionImg/lambor34.0/images/web/w_95a.png',
                link: 'Athletics'
            }],
            phoneFlag: true,
            favourableFlag: true,
            minFlag: true,
            newsFlag: true
            //              barIndex:0
        };
    },
    mounted () {

    },
    created: function () {
        this.load()
    },
    methods: {
        load: function () {
            this.$nextTick(function () {})
        },
        barTab (index) {
            let self = this
            self.navBars.forEach((item, key) => {
                if (index == key) {
                    self.$router.push({
                        name: item.link
                    })
                }
            })
        }
    }

};
