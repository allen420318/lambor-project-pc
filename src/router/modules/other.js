export default [
  {
    // 驗證電子信箱
    name: "webEmailVerifyInformation",
    path: "EmailVerifyInformation/:VerifyEmailCode",
    component: (resolve) => {
      require(["@/components/main/emailVerify/emailVerifyInformation"], resolve)
    }
  },
  {
    // 登录
    path: "/Login",
    name: "Login",
    component: (resolve) => {
      require(["@/components/main/login/login"], resolve)
    }
  },
  {
    // 注册
    path: "/Register/:Data?",
    name: "Register",
    component: (resolve) => {
      require(["@/components/main/register/register"], resolve)
    }
  },
  //              {
  //                  // 免费试玩
  //                  path: 'FreeTrial',
  //                  name: 'FreeTrial',
  //                  component: FreeTrial
  //              },
  {
    // pt-h5
    name: "webHtml5Lobby",
    path: "Html5Lobby",
    component: (resolve) => {
      require(["@/components/main/h5/html5Lobby"], resolve)
    }
  },
  {
    // 注册成功
    path: "/RegisterSuccess",
    name: "RegisterSuccess",
    component: (resolve) => {
      require(["@/components/main/register/registerSuccess"], resolve)
    }
  },
  {
    // 所有遊戲畫面
    name: "webPlayGame",
    path: "PlayGame",
    component: (resolve) => {
      require(["@/components/main/linkToGame/playGame"], resolve)
    }
  },
  {
    // 所有遊戲畫面
    name: "webPlayMyGame",
    path: "PlayMyGame",
    component: (resolve) => {
      require(["@/components/main/linkToGame/playMyGame"], resolve)
    }
  },
  {
    // 所有遊戲畫面--PNG
    name: "webPlayPngGame",
    path: "webPlayPngGame",
    component: (resolve) => {
      require(["@/components/main/linkToGame/playPngGame"], resolve)
    }
  },
  //              {
  //					// 支付方式-微信支付
  //					name: 'WeChatPayment',
  //					path: 'WeChatPayment',
  //					component: WeChatPayment,
  //				},
  //				{
  //					// 支付方式-支付宝支付
  //					name: 'AlipayPayment',
  //					path: 'AlipayPayment',
  //					component: AlipayPayment,
  //				},
  {
    // 在線支付頁面
    name: "webDepositPayment",
    path: "DepositPayment/:Data",
    component: (resolve) => {
      require(["@/components/main/memberNew/depositPayment"], resolve)
    }
  }
]
