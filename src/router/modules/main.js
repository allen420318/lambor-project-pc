export default [
  {
    // 首页
    path: "",
    name: "Index",
    component: (resolve) => {
      require(["@/components/main/index/index"], resolve)
    }
  },
  {
    // 真人视讯
    path: "LiveVideo",
    name: "LiveVideo",
    component: (resolve) => {
      require(["@/components/main/liveVideo/liveVideo"], resolve)
    }
  },
  {
    // 电子游戏
    path: "Electronic",
    name: "Electronic",
    component: (resolve) => {
      require(["@/components/main/electronic/electronic"], resolve)
    }
  },
  {
    // 电子竞技
    path: "Athletics",
    name: "Athletics",
    component: (resolve) => {
      require(["@/components/main/athletics/athletics"], resolve)
    }
  },
  {
    // 体育博彩
    path: "Sport",
    name: "Sport",
    component: (resolve) => {
      require(["@/components/main/sport/sport"], resolve)
    }
  },
  {
    // 彩票投注
    path: "Lottery",
    name: "Lottery",
    component: (resolve) => {
      require(["@/components/main/lottery/lottery"], resolve)
    }
  },
  {
    // 棋牌游戏
    name: "Poker",
    path: "Poker",
    component: (resolve) => {
      require(["@/components/main/poker/poker"], resolve)
    }
  },
  {
    // 优惠活动
    path: "ActivityList",
    name: "ActivityList",
    component: (resolve) => {
      require(["@/components/main/activity/activityList"], resolve)
    }
  },
  {
    // 优惠活动--详情
    path: "ActivityListDetails/:ID",
    name: "ActivityListDetails",
    component: (resolve) => {
      require(["@/components/main/activity/activityListDetails"], resolve)
    }
  },
  // {
  //     // 新手教程
  //     path: 'Tutorial',
  //     name: 'Tutorial',
  //     component: resolve => {
  //         require(['@/components/main/tutorial/tutorial'], resolve);
  //     }
  // },
  // {
  //     // 新手教程
  //     path: 'TutorialSkills',
  //     name: 'TutorialSkills',
  //     component: resolve => {
  //         require(['@/components/main/tutorial/tutorialSkill'], resolve);
  //     }
  // },
  // {
  //     // 新手教程
  //     path: 'TutorialSkillsDetail',
  //     name: 'TutorialSkillsDetail',
  //     component: resolve => {
  //         require(['@/components/main/tutorial/tutorialDetail'], resolve);
  //     }
  // },
  // {
  //     // 新手教程
  //     path: 'TutorialRules',
  //     name: 'TutorialRules',
  //     component: resolve => {
  //         require(['@/components/main/tutorial/tutorialRule'], resolve);
  //     }
  // },
  // {
  //     // 新手教程
  //     path: 'TutorialFAQ',
  //     name: 'TutorialFAQ',
  //     component: resolve => {
  //         require(['@/components/main/tutorial/tutorialFaq'], resolve);
  //     }
  // },
  {
    name: "Tutorial",
    path: "Tutorial",
    component: (resolve) => {
      require(["@/components/main/tutorial/tutorial"], resolve)
    },
    children: [
      // 新手教程 - 投注技巧
      {
        name: "TutorialSkills",
        path: "TutorialSkills",
        component: (resolve) => {
          require(["@/components/main/tutorial/tutorialSkill"], resolve)
        }
      },
      // 新手教程 - 投注技巧詳細
      {
        name: "TutorialSkillsDetail",
        path: "TutorialSkillsDetail",
        component: (resolve) => {
          require(["@/components/main/tutorial/tutorialDetail"], resolve)
        }
      },
      // 新手教程 - 遊戲規則
      {
        name: "TutorialRules",
        path: "TutorialRules",
        component: (resolve) => {
          require(["@/components/main/tutorial/tutorialRule"], resolve)
        }
      },
      // 新手教程 - 常見問題
      {
        name: "TutorialFAQ",
        path: "TutorialFAQ",
        component: (resolve) => {
          require(["@/components/main/tutorial/tutorialFaq"], resolve)
        }
      }
    ]
  },
  {
    // 新闻中心
    path: "NewsList",
    name: "NewsList",
    component: (resolve) => {
      require(["@/components/main/newsList/newsList"], resolve)
    }
  },
  {
    // 新闻中心--详情
    path: "NewsListDetails/:ID",
    name: "NewsListDetails",
    component: (resolve) => {
      require(["@/components/main/newsList/newsListDetails"], resolve)
    }
  },
  {
    // 手机APP
    path: "MobileAPP",
    name: "MobileAPP",
    component: (resolve) => {
      require(["@/components/main/mobileAPP/mobileAPP"], resolve)
    }
  },
  {
    // 关于我们
    path: "AboutUs",
    name: "AboutUs",
    component: (resolve) => {
      require(["@/components/main/aboutUs/aboutUs"], resolve)
    }
  },
  {
    // 厂商介绍
    path: "Partner",
    name: "Partner",
    component: (resolve) => {
      require(["@/components/main/partner/partner"], resolve)
    }
  },
  // {
  //     // 代理合作
  //     path: 'Agent',
  //     name: 'Agent',
  //     component: Agent
  // },
  {
    // 代理加盟
    path: "JoinUs/:Data?",
    name: "JoinUs",
    component: (resolve) => {
      require(["@/components/main/agent/joinUs"], resolve)
    }
  },
  {
    // 捕鱼
    path: "Catchfish",
    name: "Catchfish",
    component: (resolve) => {
      require(["@/components/main/catchfish/catchfish"], resolve)
    }
  },
  {
    // 联系我们
    path: "ContactUs",
    name: "ContactUs",
    component: (resolve) => {
      require(["@/components/main/contactUs/contactUs"], resolve)
    }
  }
]
