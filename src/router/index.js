import Vue from "vue"
import Router from "vue-router"
import PageRedirect from "@/router/redirect"
import routerSwitch from "@/routerSwitch.js"
import { store } from "@/store/index.js"
import { getLanguageCode } from "@/lang/helper.js"

Vue.use(Router)

const router = new Router({
  mode: "history",
  routes: routerSwitch
})
// 路由前檢查
router.beforeEach(async (to, from, next) => {
  const versionSetLanguageCode = Vue.prototype.$Lang
  const currentLanguageCode = getLanguageCode(versionSetLanguageCode)
  Vue.prototype.$MessageLang = currentLanguageCode

  store.dispatch("i18n/loadLanguage", { languageCode: currentLanguageCode })

  // 加時間防止被登出
  localStorage.setItem("logoutTime", new Date().getTime() + Vue.prototype.$SurviveMinutes * 60 * 1000)

  store.dispatch("user/setAgentId")
  store.dispatch("user/setUserId")

  // 確認是否需檢查路由
  if (PageRedirect.Web[to.name] !== undefined) {
    await store.dispatch("user/fetchUserStatus")

    const userStatus = store.state.user.userStatus

    // 避免Token殘留
    if (userStatus === 0) {
      localStorage.removeItem("Token")
    }

    const redirectData = PageRedirect.Web[to.name][userStatus]
    // 根據登入狀態判斷是否合法路由，若非法則進入指定路由
    if (redirectData.Ret === true) {
      next()
    } else {
      next({
        name: redirectData.RedirectName
      })
    }
  } else {
    next()
  }
})

router.afterEach(function () {
  // 跳轉頁面後回歸頂部
  document.body.scrollTop = 0
  document.documentElement.scrollTop = 0
})

export default router
