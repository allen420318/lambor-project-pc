export default {
    Web: {
        Member: {
            // 會員 - 個人中心總覽
            0: {
                Ret: false,
                RedirectName: 'Login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        MemberLevel: {
            // 會員 - 會員等級
            0: {
                Ret: false,
                RedirectName: 'Login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        Profile: {
            // 會員 - 個人資料
            0: {
                Ret: false,
                RedirectName: 'Login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        PrivateMessage: {
            // 會員 - 站內信息
            0: {
                Ret: false,
                RedirectName: 'Login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        BankData: {
            // 會員 - 銀行資料
            0: {
                Ret: false,
                RedirectName: 'Login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        ModifyPassword: {
            // 會員 - 修改密碼
            0: {
                Ret: false,
                RedirectName: 'Login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        FundManage: {
            // 會員 - 資金管理
            0: {
                Ret: false,
                RedirectName: 'Login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: false,
                RedirectName: 'Index'
            }
        },
        Deposit: {
            // 會員 - 存款
            0: {
                Ret: false,
                RedirectName: 'Login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: false,
                RedirectName: 'Index'
            }
        },             
        Withdrawal: {
            // 會員 - 取款
            0: {
                Ret: false,
                RedirectName: 'Login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: false,
                RedirectName: 'Index'
            }
        },                
        TransactionRecord: {
            // 會員 - 交易紀錄查詢
            0: {
                Ret: false,
                RedirectName: 'Login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },                    
        Login: {
            // 登入
            0: {
                Ret: true
            },
            3: {
                Ret: false,
                RedirectName: 'Index'
            },
            4: {
                Ret: false,
                RedirectName: 'Index'
            }
        },
        Register: {
            // 註冊
            0: {
                Ret: true
            },
            3: {
                Ret: false,
                RedirectName: 'Index'
            },
            4: {
                Ret: false,
                RedirectName: 'Index'
            }
        },
        RegisterSuccess: {
            // 註冊成功
            0: {
                Ret: false,
                RedirectName: 'Login'
            },
            3: {
                Ret: true
            },
            4: {
                Ret: true
            }
        },
        FreeTrial: {
            // 免費試玩
            0: {
                Ret: true
            },
            3: {
                Ret: false,
                RedirectName: 'Index'
            },
            4: {
                Ret: false,
                RedirectName: 'Index'
            }
        },        
    },
}