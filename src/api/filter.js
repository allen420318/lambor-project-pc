import CommUtility from 'scripts/common/CommUtility';
const filter = Vue => {
  // tick轉換為本地時間
  Vue.filter('utcTicksToLocalTime', function (utcDateTicks, format) {
    return CommUtility.UtcTicksToLocalTime(utcDateTicks, format);
  });

  Vue.filter('UtcTicksTime', function (utcDateTicks, format) {
    return CommUtility.UtcTicksTime(utcDateTicks, format);
  });

  Vue.filter('phoneNumberFormat', function (phoneNumber) {
    return CommUtility.PhoneNumberFormat(phoneNumber);
  });

  Vue.filter('limitTo', function (content, digit) {
    return CommUtility.LimitTo(content, digit);
  });

  Vue.filter('simpleContent', function (content, length) {
    return CommUtility.SimpleContent(content, length);
  });

  Vue.filter('moneyFormat', function (content) {
    return CommUtility.MoneyFormat(content);
  });

  Vue.filter('upperLimitDisplay', function (content, length) {
    return CommUtility.UpperLimitDisplay(content, length);
  });

  // 小数点格式化 20190814 SeyoChen
  Vue.filter('numCalFix', function (Data) {
    return CommUtility.numCalFix(Data);
  });
};
export default filter;
