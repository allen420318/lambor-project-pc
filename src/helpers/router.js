export const replaceRoutes = (targetList, sourceList) => {
  if (sourceList.length > 0) {
    return sourceList.map((s) => {
      const [target] = targetList.filter((t) => {
        return t.name === s.name
      })

      if (target) {
        return target
      } else {
        return s
      }
    })
  } else {
    return sourceList
  }
}
