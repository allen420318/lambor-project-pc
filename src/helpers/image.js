export const generateImageUrl = (relativePath, { isFromCDN = true, resourceCDN = "" } = {}) => {
  if (typeof relativePath === "string") {
    const resoucrPath = relativePath.startsWith("/") ? relativePath.substring(1) : relativePath

    if (isFromCDN === true) {
      return resourceCDN + "/" + resoucrPath
    } else {
      return require("images/" + resoucrPath)
    }
  } else {
    return null
  }
}
