import Vue from "vue"
import App from "@/App.vue"
import { fetcher } from "@/libs/fetcher.js"
import { store } from "@/store/index.js"
import router from "@/router/index.js"
import i18n from "@/lang/i18nLoader.js"
import { generateImageUrl } from "@/helpers/image.js"
import { isMobileAgent } from "@/libs/navigator.js"

export const mainLoader = () => {
  fetcher({ endpoint: "/static/customer/versionSet.json" }, "GET").then((result) => {
    Vue.prototype.$VersionNumber = result.VersionNumber // 版本号
    Vue.prototype.$BannerPath = result.BannerPath // Banner 资源
    Vue.prototype.$ServeLink = result.ServeLink // 客服链接
    Vue.prototype.$ServeQQ = result.ServeQQ // 客服QQ
    Vue.prototype.$SiteEmail = result.SiteEmail // 代理站长邮箱
    Vue.prototype.$WGUID = result.VersionSet.WGUID // 站长ID

    Vue.prototype.$Environment = result.Environment // 处于的环境 1：SGX 2：正式站

    // 语言 0:简体中文 1：繁体中文 2：英文 3：越南语
    Vue.prototype.$Lang = result.Lang ? Number(result.Lang) : 0

    Vue.prototype.$BKBLUrl = result.VersionSet.BKBLUrl // 后台接口地址
    Vue.prototype.$SignalRUrl = result.VersionSet.SignalRUrl // Signal地址

    const resourceCDN = result.VersionSet.ResourceCDN

    Vue.prototype.$ResourceCDN = resourceCDN

    Vue.prototype.getImage = (location, isFromCDN = false) => {
      const config = {
        isFromCDN,
        resourceCDN
      }

      return generateImageUrl(location, config)
    }

    if (result.SurviveMinutes) {
      Vue.prototype.$SurviveMinutes = result.SurviveMinutes
    } else {
      Vue.prototype.$SurviveMinutes = 3 * 24 * 60
    }

    let domainName = document.location.host

    // 配置文件
    Vue.prototype.$WebVersionHost = document.location.protocol + "//" + domainName // PC版地址
    Vue.prototype.$ResourceURL = document.location.protocol + "//" + domainName // Resource地址

    switch (Vue.prototype.$Environment) {
      case "1":
        Vue.prototype.$MobileVersionHost = document.location.protocol + "//m" + domainName // SGX移动端地址
        break
      case "2":
        Vue.prototype.$MobileVersionHost = document.location.protocol + "//" + domainName.replace("www", "mobile") // 正式站移动端地址
        break
      default:
        break
    }

    if (isMobileAgent() === true) {
      location.href = Vue.prototype.$MobileVersionHost + location.pathname
      return
    }

    new Vue({
      el: "#app",
      i18n,
      router,
      store,
      components: {
        App
      },
      template: "<App/>"
    })
  })
}
