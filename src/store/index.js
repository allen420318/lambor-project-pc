import Vue from "vue"
import Vuex from "vuex"
import site from "./module/site.js"
import user from "./module/user.js"
import i18n from "./module/i18n.js"

Vue.use(Vuex)

const initailAbortController = new AbortController()
const initailACSignal = initailAbortController.signal

export const store = new Vuex.Store({
  modules: { site, user, i18n },
  state: {
    LanguageData: 1,
    themecolor: "",
    switchName: "",
    agencyName: "",
    depositChannel: 0,
    announcementOpen: false,
    isFirstEnter: true, // 是否首次进入首页
    loginState: false, // 是否已经登录
    eleState: undefined, // 导航选择电子游戏类型
    fetchController: initailAbortController,
    fetchSignal: initailACSignal
  },
  getters: {
    agencyName(state) {
      return state.agencyName
    },
    getIsFirstEnter(state) {
      return state.isFirstEnter
    },
    getLoginState(state) {
      return state.loginState
    },
    geteleState(state) {
      return state.eleState
    }
  },
  mutations: {
    GetLanguageData(state, lang) {
      state.LanguageData = lang.Value
    },
    setThemeColor(state, num) {
      state.themecolor = num
    },
    setSwitchName(state, name) {
      state.switchName = name
    },
    setAgencyName(state, name) {
      state.agencyName = name
    },
    setDepositChannel(state, name) {
      state.depositChannel = name
    },
    setAnnouncementOpen(state, name) {
      state.announcementOpen = name
    },
    setIsFirstEnter(state, val) {
      state.isFirstEnter = val
    },
    setLoginState(state, val) {
      state.loginState = val
    },
    setEleState(state, val) {
      state.eleState = val
    },
    abortFetch(state) {
      state.fetchController.abort()

      const controller = new AbortController()
      const signal = controller.signal
      state.fetchController = controller
      state.fetchSignal = signal
    }
  },
  actions: {
    setThemeColorSkin: (context, num) => {
      context.commit("setThemeColor", num)
    },
    setSwitchNameText: (context, name) => {
      context.commit("setSwitchName", name)
    },
    setAgencyNameText: (context, name) => {
      context.commit("setAgencyName", name)
    },
    setDepositChannelText: (context, name) => {
      context.commit("setDepositChannel", name)
    },
    setAnnouncementOpenText: (context, name) => {
      context.commit("setAnnouncementOpen", name)
    },
    setIsFirstEnterVal: (context, val) => {
      context.commit("setIsFirstEnter", val)
    },
    setLoginState: (context, val) => {
      context.commit("setLoginState", val)
    },
    setEleState: (context, val) => {
      context.commit("setEleState", val)
    },
    abortFetch: (context, signal) => {
      context.commit("abortFetch", signal)
    }
  }
})

export default store
