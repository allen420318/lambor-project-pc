import { LOAD_LANGUAGE, SET_DEFAULT_LANGUAGE_CODE } from "../mutation-types.js"
import { i18n } from "@/lang/i18nLoader.js"
import { getLanguageFile } from "@/lang/helper.js"

const localStorageKey = "lang"

const state = {
  localStorageKey: localStorageKey,
  aliavableLanguages: new Map([
    ["cn", 0],
    ["tw", 1],
    ["en", 2],
    ["vi", 3],
    // Compatible keys
    ["zh", 0]
  ]),
  loadedLanguages: [],
  defaultLanguage: "en"
}

const loadLanguageFile = async (code) => {
  const fileCode = code === "zh" ? "cn" : code

  const result = await getLanguageFile(fileCode)

  i18n.setLocaleMessage(code, result)
}

const switchLanguage = (code) => {
  i18n.locale = code
}

const actions = {
  async loadLanguage({ commit, state }, { languageCode = undefined } = {}) {
    const code = languageCode
    const isAlreadyLoaded = state.loadedLanguages.includes(languageCode)

    if (isAlreadyLoaded === true) {
      switchLanguage(code)
    } else {
      await loadLanguageFile(code)
      switchLanguage(code)

      commit(LOAD_LANGUAGE, code)
    }
  },
  setDefaultLanguageCode({ commit }, languageCode) {
    if (languageCode) {
      commit(SET_DEFAULT_LANGUAGE_CODE, languageCode)
    } else {
      throw new Error("i18n / setDefaultLanguageCode error")
    }
  }
}

const mutations = {
  [LOAD_LANGUAGE](state, languageCode) {
    state.loadedLanguages.push(languageCode)
  },
  [SET_DEFAULT_LANGUAGE_CODE](state, languageCode) {
    state.defaultLanguage = languageCode
  }
}

const getters = {
  currentLanguage: () => {
    return i18n.locale
  },
  currentLanguageId: (state) => {
    return state.aliavableLanguages.get(i18n.locale)
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
