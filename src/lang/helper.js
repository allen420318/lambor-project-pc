/* global SITE_KEY */

import JsCookie from "js-cookie"

export const languageCodeMap = new Map([
  [0, "zh"],
  [1, "tw"],
  [2, "en"],
  [3, "vi"]
])

export const getLanguageCode = (codeId) => {
  const defaultLanguageId = JsCookie.getJSON("language")

  let finalLanguageId = 0

  if (defaultLanguageId >= 0) {
    finalLanguageId = defaultLanguageId
  } else if (codeId) {
    finalLanguageId = codeId
  }

  return languageCodeMap.get(finalLanguageId)
}

export const getLanguageFile = async (fileCode) => {
  let modLang = {}
  let elementLang = {}

  if (SITE_KEY !== "undefined") {
    modLang = await import(`@/mod/${SITE_KEY}/lang/${fileCode}.js`)
  }

  const elementLangMap = new Map([
    ["en", "en"],
    ["cn", "zh-CN"],
    ["tw", "zh-TW"],
    ["vi", "vi"]
  ])

  const elementLangCode = elementLangMap.get(fileCode)

  elementLang = await import(`element-ui/lib/locale/lang/${elementLangCode}.js`)

  const langMessages = await import(`@/lang/i18n/${fileCode}.js`)
  const result = { message: { ...langMessages.default, ...modLang.default }, ...elementLang.default }

  return result
}
