/* global SITE_KEY */

import otherRouter from "./router/modules/other"
import mainRouter from "./router/modules/main"

const defaultRoutes = [
  {
    // 找不到页面
    path: "/NotFoundComponent",
    name: "NotFoundComponent",
    component: (resolve) => {
      require(["@/components/base/notFound"], resolve)
    }
  },
  {
    path: "/", // 最外层框架（包含main，member）
    component: (resolve) => {
      require(["@/components/base/base"], resolve)
    },
    children: [
      ...otherRouter,
      {
        path: "/",
        component: (resolve) => {
          require(["@/components/base/baseWebLayout"], resolve)
        },
        children: [
          {
            // main Layout
            path: "",
            component: (resolve) => {
              require(["@/components/base/baseMainLayout"], resolve)
            },
            children: mainRouter
          }
        ]
      }
    ]
  }
]

let configRoutes = {}

if (SITE_KEY === "undefined" || !SITE_KEY) {
  configRoutes = defaultRoutes
} else {
  configRoutes = require(`@/mod/${SITE_KEY}/routers.js`).default
}

export default configRoutes
