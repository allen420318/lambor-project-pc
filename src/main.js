import Vue from "vue"
import VeeValidate from "vee-validate"
import "jquery"
import CommDef from "scripts/common/CommDef"
import JsCookie from "js-cookie"
import ElementUI from "element-ui"
import "element-ui/lib/theme-chalk/index.css"
import "swiper/dist/css/swiper.min.css"
import infiniteScroll from "vue-infinite-scroll"
import "perfect-scrollbar/css/perfect-scrollbar.css"
import directive from "./api/directive"
import filter from "./api/filter"

import { mainLoader } from "@/helpers/initialize.js"

Vue.config.productionTip = false
Vue.prototype.cookie = JsCookie

directive(Vue)
filter(Vue)

Vue.use(ElementUI)
Vue.use(VeeValidate)
Vue.use(infiniteScroll)

// 加入自訂信箱欄位驗證
VeeValidate.Validator.extend("lamborEmail", {
  getMessage: (field) => "The" + field + "value is not truthy.",
  validate: (value) => {
    return CommDef.Regex.Email1.test(value) && CommDef.Regex.Email2.test(value)
  }
})

mainLoader()
