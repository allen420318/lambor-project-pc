import { replaceRoutes } from "@/helpers/router.js"
import restRoutes from "@/router/modules/other.js"
import mainRoutes from "@/router/modules/main.js"

const changes = {
  main: [
    {
      path: "",
      name: "Index",
      component: () => import("@/mod/k05/components/main/index/index.vue")
    },
    {
    // 真人视讯
        path: 'LiveVideo',
        name: 'LiveVideo',
        component: () => import("@/mod/k05/components/main/liveVideo/liveVideo")
    },
    {
        // 电子竞技
        path: 'Athletics',
        name: 'Athletics',
        component: () => import("@/mod/k05/components/main/athletics/athletics")
    },
    {
        name: 'Tutorial',
        path: 'Tutorial',
        component:  () => import("@/mod/k05/components/main/tutorial/tutorial"),
        children: [
            {
                name: 'TutorialFAQ',
                path: 'TutorialFAQ',
                component: () => import("@/components/main/tutorial/tutorialFaq")
            }
        ]
    },
    {
        // 关于我们
        path: 'AboutUs',
        name: 'AboutUs',
        component:  () => import("@/mod/k05/components/main/aboutUs/aboutUs")
    }
  ],
  rest: [
    {
        path: '/Login',
        name: 'Login',
        component: () => import("@/mod/k05/components/main/login/login")
    },
    {
        // 注册
        path: '/Register/:Data?',
        name: 'Register',
        component: () => import("@/mod/k05/components/main/register/register")
    },
    {
        // 在線支付頁面
        name: 'webDepositPayment',
        path: 'DepositPayment/:Data',
        component: () => import("@/components/main/memberNew/depositPayment")
    }
  ]
}

const adds = {
  main: [],
  rest: []
}

const finalMainRoutes = [...replaceRoutes(changes.main, mainRoutes), ...adds.main]
const finalResetRoutes = [...replaceRoutes(changes.rest, restRoutes), ...adds.rest]

const result = [
  {
    path: "/NotFoundComponent",
    name: "NotFoundComponent",
    component: () => import("@/components/base/notFound")
  },
  {
    // 最外层框架（包含main，member）
    path: "/",
    component: () => import("@/mod/k05/components/base/base.vue"),
    children: [
      ...finalResetRoutes,
      {
        path: "/",
        component: () => import("@/components/base/baseWebLayout"),
        children: [
          {
            path: "",
            component: () => import("@/mod/k05/components/base/baseMainLayout"),
            children: finalMainRoutes
          }
        ]
      }
    ]
  }
]

export default result
