import messagesEn from './en';
import messagesZh from './zh';
import messagesTw from './tw';
import messagesVi from './vi';

const messages = {

    zh: {
        message: messagesZh
    },
    en: {
        message: messagesEn
    },
    tw: {
        message: messagesTw
    },
    vi: {
        message: messagesVi
    }
};

export default messages;
