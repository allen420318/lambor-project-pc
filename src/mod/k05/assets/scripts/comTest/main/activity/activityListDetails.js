// import ActivityDetailContent from '@/components/sub/activity/activityDetailContent';
import ActivityDetailContent from '@/mod/k05/components/sub/activity/activityDetailContent'
import ActivityDetailHeader from '@/components/sub/activity/activityDetailHeader'
export const ActivityDetails = {

    components: {
        ActivityDetailHeader,
        ActivityDetailContent,
    },

}
