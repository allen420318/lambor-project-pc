// 接口使用说明 获取优惠活动
// const data = await CommonService.Comm_GetBonusActivity(inputObj); const inputObj = {WGUID: Vue.prototype.$WGUID};

// 检查账号状态
//  const data = await CommonService.Comm_CheckPermission();
// 去的会员资讯
// const data = await CommonService.Comm_GetMemberInfo();
import Activity from '@/components/sub/index/activity'
// import FastEntrance from '@/components/sub/index/fastEntrance'
import FastEntrance from '@/mod/k05/components/sub/index/fastEntrance'

export const initIndex7 = {
	data () {
		return{
			IsLogin: false,
		}
	},
    methods: {
        Text: function () {
            console.log('1')
		},
		// 检测是否已经登陆成功
		JudgeLogin: function () {
			var token = localStorage.getItem('Token')
			if (token != null) {
				this.IsLogin = true
			}
		}
    },
	components: {
		Activity,
		FastEntrance
	},
    created: function () {
        this.Text()
    },
    mounted () {
		// 检测是否已经登录
		this.JudgeLogin()
	}
}