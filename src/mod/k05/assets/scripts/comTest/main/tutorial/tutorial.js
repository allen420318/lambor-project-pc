	// import BettingSkills from '@/components/sub/beginnerTutorial/BettingSkills';
	// import GameRules from '@/components/sub/beginnerTutorial/GameRules';
	import CommonProblem from '@/components/sub/beginnerTutorial/CommonProblem'
	import tutorialDetail from '@/components/sub/beginnerTutorial/tutorialDetail'
	import 'scripts/customJs/web/jparticle.jquery.min';
	import TutorialBanner from '@/components/sub/tutorial/tutorialBanner';
	import TutorialDetail from '@/components/main/tutorial/tutorialDetail';
	export const tutorial = {

	    data() {
	        return {
	            navs: 1,
	            tutorialContent: false,
	            detailTit: '',
	            detailCont: '',
	            TabType: 'CommProblem',
	            // 頁籤類型
	            TabTypeEnum: {
	                // 投注技巧
	                BettingTips: 'BettingTips',
	                // 遊戲規則
	                GameRule: 'GameRule',
	                // 常見規則
	                CommProblem: 'CommProblem'
	            }
	        }
	    },
	    // 竖滚动条插件
	    created: function () {
	        this.InitPage();
	        this.$nextTick(() => {
	            $('#bettingSkill-scroll').perfectScrollbar();
	        })
	    },
	    methods: {
	        InitPage: function () {
	            switch (this.$route.name) {
	                // case 'TutorialSkills':
	                //     this.TabType = 'BettingTips';
	                //     break;
	                // case 'TutorialRules':
	                //     this.TabType = 'GameRule';
	                //     break;
	                case 'TutorialFAQ':
	                    this.TabType = 'CommProblem';
	                    break;
	                default:
	                    this.TabType = 'CommProblem';
	                    break;
	            }
	        },
	        // 選擇區域
	        ChangeContent: function (type) {
	            switch (type) {
	                case 'BettingTips':
	                    this.TabType = 'BettingTips';
	                    type = 'TutorialSkills';
	                    break;
	                case 'GameRule':
	                    this.TabType = 'GameRule';
	                    type = 'TutorialRules';
	                    break;
	                case 'CommProblem':
	                    this.TabType = 'CommProblem';
	                    type = 'TutorialFAQ';
	                    break;
	                default:
	                    break;
	            }
	            this.$router.push({
	                name: type
	            });
	        }, //
	        switchNav(i) {
	            this.navs = i;
	            if (i == 1) {
	                $('.tutorial-pull-down1').addClass('fiveOpen2')
	            } else if (i == 2) {
	                $('.tutorial-pull-down2').addClass('fiveOpen')
	            } else if (i == 3) {
	                $('.tutorial-pull-down3').addClass('fiveOpen')
	            }

	            $('.tutorial-inside').scrollTop(0)
	        },
	        // 第二版banner页面按钮点击事件
	        tutorial_banner2(i) {
	            $('.tutorial_banner2').hide()
	            $('.tutorialbox').show()
	            this.navs = i;
	        }
	    },
	    mounted() {
	        // 粒子动画
	        $('.particlesWrap').jParticle({
	            particlesNumber: 60,
	            background: 'transparent',
	            color: '#afb0b5',
	            width: null,
	            height: null,
	            opacity: 0.7,
	            color: ['#FFF'],
	            maxSpeed: 2,
	            minSpeed: 2,
	        });

	        // $('#bettingSkill-scroll').perfectScrollbar();
	        var _this = this

	        $(document).ready(function () {
	            $('.banner_ul li').on('click', function () {
	                console.log('tag', $(this).text())
	                if ($(this).text() == '常见问题') {
	                    _this.navs = 3;
	                } else if ($(this).text() == '投注技巧') {
	                    _this.navs = 1;
	                } else if ($(this).text() == '游戏规则') {
	                    _this.navs = 2;
	                }

	                $('.tutorial_cont .banner').animate({
	                    left: '-100%'
	                }, 500)
	                $('.tutorialbox').animate({
	                    left: '0'
	                }, 500)
	                $('.tutorial-content').animate({
	                    height: '601px'
	                }, 1000)
	            })
	        })
	    },
	    components: {
	        // BettingSkills,
	        // GameRules,
	        CommonProblem,
	        tutorialDetail,
	        TutorialBanner,
	        TutorialDetail
	    }
	}
