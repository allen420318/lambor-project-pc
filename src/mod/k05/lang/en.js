const modMessages = {
  abouts_casino:
    "Registered in the beautiful capital of the Philippines, Manila, it is one of the largest and fastest growing online casinos in Asia.",
  agent_caps_look: "Case-insensitive",
  login_confirm_password: "Password is different from confirm password, please re-enter",
  reg_confirmPassword: "Indeed recognize Password"
}

export default modMessages
