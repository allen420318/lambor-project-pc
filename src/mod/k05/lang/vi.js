const modMessages = {
  agent_identifying_code: "Mã xác thực",
  login_account_number: "Tên đăng nhập",
  login_input_password: "Mật khẩu",
  reg_registered_account: "Tên đăng nhập",
  reg_mobile_number: "Số điện thoại",
  reg_cipher: "Mật khẩu",
  reg_verify_password: "xác nhận mật khẩu",
  reg_identifying_code: "Mã xác thực",
  member_bankAccount: "Tên đăng nhập",
  member_change_password: "Thay đổi mật khẩu",
  agent_caps_look: "Mã xác minh",
  login_confirm_password: "Mật khẩu và xác nhận mật khẩu không khớp , vui lòng nhập lại",
  reg_confirmPassword: "Trên thực tế nhận Mật khẩu"
}

export default modMessages
