const modMessages = {
  abouts_casino:
    "注册于美丽的菲律宾首都马尼拉，由业内七大博彩巨头联袂打造，是目前亚洲最具规模且成长最快的在线娱乐场之一。",
  agent_caps_look: "不区分大小写",
  login_confirm_password: "密码与确认密码不一致，请重新输入",
  reg_confirmPassword: "确认密码"
}

export default modMessages
