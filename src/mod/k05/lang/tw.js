const modMessages = {
  abouts_casino:
    "註冊於美麗的菲律賓首都馬尼拉，由業內七大博彩巨頭聯袂打造，是目前亞洲最具規模且成長最快的在線娛樂場之一。",
  agent_caps_look: "不區分大小寫",
  login_confirm_password: "密碼與確認密碼不一致，請重新輸入",
  reg_confirmPassword: "確認密碼"
}

export default modMessages
