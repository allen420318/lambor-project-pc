import { replaceRoutes } from "@/helpers/router.js"
import restRoutes from "@/router/modules/other.js"
import mainRoutes from "@/router/modules/main.js"

const changes = {
  main: [
    {
      // 电子游戏
      path: "Electronic",
      name: "Electronic",
      component: (resolve) => {
        require(["@/mod/k19/components/main/electronic/electronic"], resolve)
      }
    },
    {
      // 彩票投注
      path: "Lottery",
      name: "Lottery",
      component: (resolve) => {
        require(["@/mod/k19/components/main/lottery/lottery"], resolve)
      }
    },
    {
      // 优惠活动
      path: "ActivityList",
      name: "ActivityList",
      component: (resolve) => {
        require(["@/mod/k19/components/main/activity/activityList"], resolve)
      }
    },
    {
      // 优惠活动--详情
      path: "ActivityListDetails/:ID",
      name: "ActivityListDetails",
      component: (resolve) => {
        require(["@/mod/k19/components/main/activity/activityListDetails"], resolve)
      }
    },
    {
      // 联系我们
      path: "ContactUs",
      name: "ContactUs",
      component: (resolve) => {
        require(["@/mod/k19/components/main/contactUs/contactUs"], resolve)
      }
    },
    {
      // 新闻中心--详情
      path: "NewsListDetails/:ID",
      name: "NewsListDetails",
      component: (resolve) => {
        require(["@/mod/k19/components/main/newsList/newsListDetails"], resolve)
      }
    },
    {
      // 真人视讯
      path: "LiveVideo",
      name: "LiveVideo",
      component: (resolve) => {
        require(["@/mod/k19/components/main/liveVideo/liveVideo"], resolve)
      }
    },
    {
      // 棋牌游戏
      name: "Poker",
      path: "Poker",
      component: (resolve) => {
        require(["@/mod/k19/components/main/poker/poker"], resolve)
      }
    },
    {
      // 体育博彩
      path: "Sport",
      name: "Sport",
      component: (resolve) => {
        require(["@/mod/k19/components/main/sport/sport"], resolve)
      }
    },
    {
      // 代理加盟
      path: "JoinUs/:Data?",
      name: "JoinUs",
      component: (resolve) => {
        require(["@/mod/k19/components/main/agent/joinUs"], resolve)
      }
    },
    {
      // 电子竞技
      path: "Athletics",
      name: "Athletics",
      component: (resolve) => {
        require(["@/mod/k19/components/main/athletics/athletics"], resolve)
      }
    },
    {
      // 厂商介绍
      path: "Partner",
      name: "Partner",
      component: (resolve) => {
        require(["@/mod/k19/components/main/partner/partner"], resolve)
      }
    },
    {
      // 新闻中心
      path: "NewsList",
      name: "NewsList",
      component: (resolve) => {
        require(["@/mod/k19/components/main/newsList/newsList"], resolve)
      }
    },
    {
      // 关于我们
      path: "AboutUs",
      name: "AboutUs",
      component: (resolve) => {
        require(["@/mod/k19/components/main/aboutUs/aboutUs"], resolve)
      }
    },
    {
      // 首页
      path: "",
      name: "Index",
      component: (resolve) => {
        require(["@/mod/k19/components/main/index/index"], resolve)
      }
    },
    {
      // 新手教程
      path: "Tutorial",
      name: "Tutorial",
      component: (resolve) => {
        require(["@/mod/k19/components/main/tutorial/tutorial"], resolve)
      }
    },
    {
      // 捕鱼
      path: "Catchfish",
      name: "Catchfish",
      component: (resolve) => {
        require(["@/mod/k19/components/main/catchfish/catchfish"], resolve)
      }
    }
  ],
  rest: [
    {
      // 登录
      path: "/Login",
      name: "Login",
      component: (resolve) => {
        require(["@/mod/k19/components/main/login/login"], resolve)
      }
    },
    {
      // 注册
      path: "/Register/:Data?",
      name: "Register",
      component: (resolve) => {
        require(["@/mod/k19/components/main/register/register"], resolve)
      }
    },
    {
      // 所有遊戲畫面
      name: "webPlayMyGame",
      path: "PlayMyGame",
      component: (resolve) => {
        require(["@/mod/k19/components/main/linkToGame/playMyGame"], resolve)
      }
    },
    {
      // 注册成功
      path: "/RegisterSuccess",
      name: "RegisterSuccess",
      component: (resolve) => {
        require(["@/mod/k19/components/main/register/registerSuccess"], resolve)
      }
    },
    {
      // pt-h5
      name: "webHtml5Lobby",
      path: "Html5Lobby",
      component: (resolve) => {
        require(["@/mod/k19/components/main/h5/html5Lobby"], resolve)
      }
    }
  ]
}

const adds = {
  main: [],
  rest: []
}

const finalMainRoutes = [...replaceRoutes(changes.main, mainRoutes), ...adds.main]
const finalRestRoutes = [...replaceRoutes(changes.rest, restRoutes), ...adds.rest]

const result = [
  {
    path: "/NotFoundComponent",
    name: "NotFoundComponent",
    component: () => import("@/components/base/notFound")
  },
  {
    // 最外层框架（包含main，member）
    path: "/",
    component: () => import("@/mod/k19/components/base/base.vue"),
    children: [
      ...finalRestRoutes,
      {
        path: "/",
        component: () => import("@/components/base/baseWebLayout"),
        children: [
          {
            path: "",
            component: () => import("@/mod/k19/components/base/baseMainLayout"),
            children: finalMainRoutes
          }
        ]
      }
    ]
  }
]

export default result
