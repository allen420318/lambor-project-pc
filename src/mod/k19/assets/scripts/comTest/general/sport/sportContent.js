/**
 * @name 体育博彩内容组件
 * @author dingruijie
 * @time 2019-09-16
 * @augments PageCount 计算后的分页总数 类型：Number 默认0
 * @augments pageSize 分页每页条数 类型: Number 默认每页10条
 * @event 父组件请求完数据 初始化分页数据 this.$refs[refname].computeGameList(1，allList) 初始传1表示渲染第一页
 */

import sportPaginate from "@/mod/k19/components/sub/paginate"
import CommUtility from "scripts/common/CommUtility"
import EventBus from "scripts/common/EventBus"
import URLService from "scripts/common/URLService"
import BLDef from "scripts/common/BLDef"
export default {
  name: "sport-content",
  components: {
    sportPaginate
  },
  props: {
    PageCount: {
      type: Number,
      default: 0
    },
    pageSize: {
      type: Number,
      default: 10
    }
  },
  data() {
    return {
      pageNum: 1,
      SportList: [],
      allSportList: []
    }
  },
  methods: {
    // 计算每一页展示数据
    GetCurrentPageData: async function (pageNo) {
      this.computeGameList(pageNo)
    },
    // 计算每一页展示数据
    computeGameList(num, initList) {
      if (initList) this.allSportList = initList
      this.pageNum = num
      this.lotteryList = []
      let gameList = []
      let pageSize = this.pageSize // 每页最大12条
      let lowerLimit = pageSize * (num - 1) // 从下限开始
      let upperLimit = pageSize * num // 从下限结束
      let allList = this.allSportList // 数据总列表
      if (allList.length > lowerLimit && allList.length > upperLimit) {
        // 游戏总数大于当前页最大序号
        for (let i = lowerLimit; i < upperLimit; i++) {
          gameList.push(allList[i])
        }
      } else {
        for (let i = lowerLimit; i < allList.length; i++) {
          gameList.push(allList[i])
        }
      }
      this.SportList = gameList
      // 插入box之前清除原先已有box避免重复
      $(".sport-expect").remove()
      setTimeout(() => {
        this.AjectAddBox(this.SportList.length)
      }, 50)
    },
    // 2018.11.15 SeyoChen 动态识别添加游戏模块
    AjectAddBox: function (num) {
      const boxSize = parseInt(3) // 页面中一行的box个数
      const boxHtml = `<li class="sport-logo-li sport-expect sportAbnormal">
            <i class="staytuned abnormalDefault"></i>
            <div class="sport-logo-wrap"></div>
            </li>`
      const box = $(".sport-logo-lists")
      if (num % boxSize === 0 && num !== 0) {
      } else {
        for (let i = 0; i < Math.abs(boxSize - (num % boxSize)); i++) {
          box.append(boxHtml)
        }
      }
    },
    // 体育博彩特有
    PlaySportGame: function (list) {
      const dataObj = {
        GameAPIVendor: list.GameApiNo, // 游戏GameApiNo
        GameCode: list.GameCode,
        // GameCatlog: list.GameTypeNo, // 游戏类型
        GameCatlog: BLDef.GameCatlogType.Sport, // 2018.12.01 传递游戏类型
        PlayType: BLDef.IdentityType.FORMAL,
        PlatformType: BLDef.PlatformType.Web_PC
      }
      localStorage.setItem("PlayGamePostData", URLService.GetUrlParameterFromObj(dataObj))
      // EventBus.$emit('showTransferMessage', this.GameList);
      CommUtility.OpenPlayGameWindow("webPlayGame", "PlayGamePopUpWindow")
    }
  }
}
