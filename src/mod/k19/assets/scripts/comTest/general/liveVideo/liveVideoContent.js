/**
 * @name 真人视讯内容组件
 * @author dingruijie
 * @time 2019-09-16
 * @augments PageCount 计算后的分页总数 类型：Number 默认0
 * @augments pageSize 分页每页条数 类型: Number 默认每页10条
 * @event 父组件请求完数据 初始化分页数据 this.$refs[refname].computeGameList(1，allList) 初始传1表示渲染第一页
 */
import livevideoPaginate from "@/mod/k19/components/sub/paginate"
import CommUtility from "scripts/common/CommUtility"
import EventBus from "scripts/common/EventBus"
import URLService from "scripts/common/URLService"
import BLDef from "scripts/common/BLDef"
export default {
  name: "livevideo-content",
  components: {
    livevideoPaginate
  },
  props: {
    PageCount: {
      type: Number,
      default: 0
    },
    pageSize: {
      type: Number,
      default: 10
    }
  },
  data() {
    return {
      pageNum: 1,
      VideoList: [],
      allVideoList: [],
      // 真人AG遊戲Post Model
      GameData: {
        GameAPIVendor: undefined,
        GameCode: "",
        GameCatlog: BLDef.GameCatlogType.LiveVideo,
        PlayType: BLDef.IdentityType.FORMAL,
        Platform: BLDef.PlatformType.Web_PC
      }
    }
  },
  methods: {
    GetCurrentPageData: async function (pageNo) {
      this.computeGameList(pageNo)
    },
    // 计算每一页展示数据
    computeGameList(num, initList) {
      if (initList) this.allVideoList = initList
      // this.pageNum = num;
      // this.VideoList = [];
      // let gameList = [];
      // let pageSize = this.pageSize; // 每页最大9条
      // let lowerLimit = pageSize * (num - 1); // 从下限开始
      // let upperLimit = pageSize * num; // 从下限结束
      // let allList = this.allVideoList; // 数据总列表
      // if (allList.length > lowerLimit && allList.length > upperLimit) {
      //     // 游戏总数大于当前页最大序号
      //     for (let i = lowerLimit; i < upperLimit; i++) {
      //         gameList.push(allList[i]);
      //     }
      // } else {
      //     for (let i = lowerLimit; i < allList.length; i++) {
      //         gameList.push(allList[i]);
      //     }
      // }
      this.VideoList = this.allVideoList
      // 插入box之前清除原先已有box避免重复
      $(".live-expect").remove()
      setTimeout(() => {
        this.AjectAddBox(this.VideoList.length)
      }, 50)
    },
    // 动态识别添加游戏模块
    AjectAddBox(num) {
      const boxSize = parseInt(3) // 页面中一行的box个数
      const boxHtml = `<li class="live-logo-li live-expect liveAbnormal">
      <i class="staytuned abnormalDefault"></i></li>`
      const box = $(".live-logo-lists")
      if (num % boxSize === 0 && num !== 0) {
      } else {
        for (let i = 0; i < Math.abs(boxSize - (num % boxSize)); i++) {
          box.append(boxHtml)
        }
      }
    },
    // 開新視窗
    PlayGame: function (gameList) {
      // 遍历游戏
      this.GameData.GameAPIVendor = gameList.GameApiNo
      this.GameData.GameCode = gameList.GameCode
      localStorage.setItem("PlayGamePostData", URLService.GetUrlParameterFromObj(this.GameData))
      CommUtility.OpenPlayGameWindow("webPlayGame", "PlayGamePopUpWindow")
      // EventBus.$emit('showTransferMessage', this.GameData);
    } // end PlayGame
  }
}
