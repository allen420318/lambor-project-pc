import URLService from "scripts/common/URLService"
import EventBus from "scripts/common/EventBus"
import BLDef from "scripts/common/BLDef"
import CommUtility from "scripts/common/CommUtility"

export const initDeAl = {
  data() {
    return {
      DisplayActivityList: [{}],
      SelectItemID: undefined, // 选中活动的index
      SelectItem: undefined, // 选中的活动
      CommUtility: CommUtility, // 定义引入的CommUtility

      DefaultSelectItemID: undefined,
      DefaultSelectItem: undefined
    }
  },
  props: {
    OnlinePaymentAcctCnt: undefined,
    CompAcctCnt: undefined,
    DepositType: undefined,
    DepositAccountCount: undefined,
    ActivityList: {
      type: Array,
      default: function () {
        return []
      }
    },
    HaveNoSelectedBonus: undefined // 是否显示不参与活动按钮 2019.03.28
  },
  methods: {
    // 获取URL参数
    GetParams: function () {
      if (this.$route.params.Data != undefined) {
        // 取得注册成功页面带进来的选择资讯
        const selectData = URLService.GetObjFromUrlParameter(this.$route.params.Data)
        this.DefaultSelectItemID = this.DepositType + "_" + selectData.ActivityType + "_" + selectData.BonusSerialNo
        this.DefaultSelectItem = selectData
      }
    },
    // 选择活动
    SelectDepositActivity: function (index, item) {
      this.SelectItemID = index
      this.SelectItem = item
    },
    // 判断是否选择了优惠活动
    CheckSelectedBonus: function () {
      if (this.SelectItem == undefined) {
        CommUtility.WebShowUniqueForm("noSelectDepositActivity") // 若未选择则弹出未选择提示
      } else {
        this.DoDeposit() // 有则进入存款
      }
    },
    // 系统自动选择优惠
    AutoSelectActivity: function () {
      this.SelectItem = this.DisplayActivityList[0]
      CommUtility.WebCloseUniqueForm("noSelectDepositActivity")
      this.DoDeposit() // 进入存款
    },
    // 进入存款
    DoDeposit: function (HaveNoSelectedBonus) {
      if (HaveNoSelectedBonus === "yes") {
        const HaveNoSelectedBonusData = JSON.parse(localStorage.getItem("HaveNoSelectedBonusData"))
        // 不参与红利活动
        const SelectItem = {
          Name: HaveNoSelectedBonusData.Name,
          VBonusRatio: HaveNoSelectedBonusData.VBonusRatio,
          MinDepositAmt: HaveNoSelectedBonusData.MinDepositAmt,
          MaxDepositAmt: HaveNoSelectedBonusData.MaxDepositAmt,
          VRakeAmtRate: HaveNoSelectedBonusData.VRakeAmtRate,
          BonusSerialNo: HaveNoSelectedBonusData.BonusSerialNo,
          ActivityType: HaveNoSelectedBonusData.ActivityType,
          ActType: HaveNoSelectedBonusData.ActType
        }
        this.$parent.$parent.SwitchStep(2, this.DepositType, SelectItem)
      } else {
        this.$parent.$parent.SwitchStep(2, this.DepositType, this.SelectItem)
      }
    }
  },
  created: function () {
    const self = this
    EventBus.$on("resetSelectedDepositActivity", () => {
      self.SelectItemID = undefined
      self.SelectItem = undefined
      this.$parent.$parent.IsShow = false
    })
    this.GetParams()
    // console.log(this.DisplayActivityList);
  },
  watch: {
    ActivityList: function () {
      // 檢查是否有預設項目
      if (this.DefaultSelectItemID !== undefined) {
        this.SelectItemID = this.DefaultSelectItemID
        this.SelectItem = this.DefaultSelectItem
        this.DefaultSelectItemID = undefined
        this.DefaultSelectItem = undefined
      } else {
        this.SelectItemID = undefined
        this.SelectItem = undefined
      }

      this.DisplayActivityList = this.ActivityList
      // console.log(this.DisplayActivityList);

      for (let i = 0; i < this.DisplayActivityList.length; i++) {
        // 类型为红利设定
        // if (this.DisplayActivityList[i].ActivityType == BLDef.BonusWayType.DEPOSIT) {
        //     const DespTxt = this.DisplayActivityList[i].ActType == 1 ? '首存返利充100送' : '续存返利充100送';
        //     this.DisplayActivityList[i].Name = DespTxt + this.DisplayActivityList[i].VBonusRatio;
        //     this.DisplayActivityList[i].HtmlSmallImgUrl = this.DisplayActivityList[i].ActType == 1 ? this.$ResourceCDN + '/EditionImg/lambor60.0/images/web/activity/activity_img1.jpg' : this.$ResourceCDN + '/EditionImg/lambor60.0/images/web/activity/activity_img2.jpg';
        // }
        // 赋予UniqueID
        this.DisplayActivityList[i].PKID =
          this.DepositType +
          "_" +
          this.DisplayActivityList[i].ActivityType +
          "_" +
          this.DisplayActivityList[i].BonusSerialNo
      }
    }
  },
  beforeDestroy() {
    EventBus.$off("resetSelectedDepositActivity")
  }
}
