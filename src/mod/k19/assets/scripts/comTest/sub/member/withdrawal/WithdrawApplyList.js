import CommUtility from "scripts/common/CommUtility"
import EventBus from "scripts/common/EventBus"
import Paginate from "@/mod/k19/components/sub/paginate"
import PersonalCenterService from "scripts/businessLogic/personalCenterService"

export const initWdAt = {
  data() {
    return {
      CommUtility: CommUtility,
      WithdrawApplyLogList: [],
      ProcessingFlag: false,
      CurrentCancelItem: {},

      //  2018.12.03 SeyoChen 分页
      PageInfo: {
        PageNo: 1,
        PageSize: 10,
        PageCount: 0,
        TotalCount: 0
      },
      WithdrawPageList: {}
      //  2018.12.03 SeyoChen 分页 end
    }
  },
  created: function () {
    this.LoadApplyList(1) //  2018.12.03 SeyoChen 分页
    const self = this
    EventBus.$on("SignalR_UpdateWithdrawalList", () => {
      self.LoadApplyList()
    })
  },
  methods: {
    // 載入取款申請列表
    LoadApplyList: async function (pageNo) {
      const inputObj = {
        PagingInfo: {
          PageNo: -1,
          PageSize: -1
        }
      }
      const data = await PersonalCenterService.WithdrawApply_LoadMainPage(inputObj)
      if (data.Ret === 0) {
        for (let i = 0; i < data.Data.WithdrawApplyLogList.length; i++) {
          data.Data.WithdrawApplyLogList[i].IsCancel = false
        }
        this.WithdrawApplyLogList = data.Data.WithdrawApplyLogList
        console.log("WithdrawApplyLogList")
        console.log(this.WithdrawApplyLogList)
        console.log("WithdrawApplyLogList")
      }

      //  2018.12.03 SeyoChen 分页
      const start = (pageNo - 1) * this.PageInfo.PageSize
      const end = pageNo * this.PageInfo.PageSize

      this.PageInfo.PageCount = Math.ceil(this.WithdrawApplyLogList.length / this.PageInfo.PageSize)
      this.WithdrawPageList = this.WithdrawApplyLogList.slice(start, end)
      EventBus.$emit("Paginate_ResetPageNo", pageNo)
      //  2018.12.03 SeyoChen 分页 end
    },
    // 获取跳页 page  2018.12.03 SeyoChen
    GetCurrentPageData: function (page) {
      this.LoadApplyList(page)
    },
    //  2018.12.03 SeyoChen 分页 end

    // 觸發取款彈窗
    TriggerCancelForm: function (Item) {
      this.CurrentCancelItem = Item
      CommUtility.WebShowUniqueForm("withdrawCancelForm")
    },
    // 取消取款申請
    CancelWithdrawApply: async function () {
      CommUtility.WebCloseUniqueForm("withdrawCancelForm")
      if (this.ProcessingFlag === true) {
        return
      }

      this.ProcessingFlag = true

      const inputObj = {
        ID: this.CurrentCancelItem.ID,
        Reason: "玩家自行取消取款申请"
      }

      EventBus.$emit("GlobalLoadingTrigger", true)
      const data = await PersonalCenterService.WithdrawApply_Cancel(inputObj)
      EventBus.$emit("GlobalLoadingTrigger", false)

      this.ProcessingFlag = false
      if (data.Ret === 0) {
        this.CurrentCancelItem.IsCancel = true
        // EventBus.$emit('memberHeaderUpdateBalance');
      }

      const notifyData = {
        NotifyMessage: data.Message
      }

      EventBus.$emit("showNotifyMessage", notifyData)
    }
  },
  beforeDestroy() {
    EventBus.$off("SignalR_UpdateWithdrawalList")
  },
  components: {
    Paginate
  },
  mounted() {
    $("#withDraw_box").perfectScrollbar()
  }
}
