import CommUtility from "scripts/common/CommUtility"
import EventBus from "scripts/common/EventBus"
import Paginate from "@/mod/k19/components/sub/paginate"

export const initDsHy = {
  props: {
    DepositHistoryData: {
      type: Object,
      default: function () {
        return {}
      }
    }
  },
  data() {
    return {
      CommUtility: CommUtility,

      //  2018.12.03 SeyoChen 分页
      PageInfo: {
        PageNo: 1,
        PageSize: 10,
        PageCount: 0,
        TotalCount: 0
      },
      DepositHistorList: {}
      //  2018.12.03 SeyoChen 分页 end
    }
  },
  mounted: function () {
    EventBus.$on("depositHist", () => {
      this.ShowHistoryForm()
    })
  },
  methods: {
    ShowHistoryForm: function () {
      CommUtility.WebShowUniqueForm("withdrawActivityListForm")
    },
    // 获取分页对应页面的数据 2018.12.03 SeyoChen
    GetPageData: function (pageNo) {
      const start = (pageNo - 1) * this.PageInfo.PageSize
      const end = pageNo * this.PageInfo.PageSize

      this.PageInfo.PageCount = Math.ceil(this.DepositHistoryData.ActivityLogList.length / this.PageInfo.PageSize)
      this.DepositHistorList = this.DepositHistoryData.ActivityLogList.slice(start, end)
      EventBus.$emit("Paginate_ResetPageNo", pageNo)
    },
    // 倒序  2018.12.03 SeyoChen
    ReverseData: function () {
      this.DepositHistoryData.ActivityLogList = this.DepositHistoryData.ActivityLogList.reverse()
    },
    // 获取跳页 page  2018.12.03 SeyoChen
    GetCurrentPageData: function (page) {
      this.GetPageData(page)
    }
    //  2018.12.03 SeyoChen 分页 end
  },
  components: {
    Paginate
  },
  watch: {
    //  2018.12.03 SeyoChen 分页
    DepositHistoryData: function () {
      this.ReverseData()
      this.GetPageData(1)
    }
    //  2018.12.03 SeyoChen 分页 end
  }
}
