import CommUtility from "scripts/common/CommUtility"
import EventBus from "scripts/common/EventBus"

export const forgetPsFl = {
  mounted: function () {
    EventBus.$on("ForgetPasswordFailAlert", () => {
      this.OpenWindow()
    })
  },
  methods: {
    // 弹出新窗口
    OpenNewBox: function (aa) {
      return window.open(
        aa,
        "newindow",
        "height=565,width=357,top=0,left=0,toolbar=no,menubar=no,scrollbars=no,resizable=no,location=no,status=no"
      )
    },
    // 開起此彈窗
    // OpenWindow: function () {
    //     CommUtility.WebShowUniqueForm('forgetPasswordForm');
    // }, // end OpenWindow
    // 開啟視窗
    OpenWindow: function () {
      CommUtility.WebShowUniqueForm("failForm")
    }, // end OpenWindow

    // 關閉視窗
    CloseWindow: function () {
      CommUtility.WebCloseUniqueForm("failForm")
    }, // end CloseWindow

    // 客服彈窗
    CustomerWindow: function () {
      this.CloseWindow()
      //          CommUtility.WebShowUniqueForm('CustomerServiceWindow');
    } // end  CustomerWindow
  }
}
