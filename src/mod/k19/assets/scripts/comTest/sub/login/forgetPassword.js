import EventBus from "scripts/common/EventBus"
import Vue from "vue"
import Regex from "scripts/common/CommDef"
import CommUtility from "scripts/common/CommUtility"
import LoginService from "scripts/businessLogic/loginService"
import BLDef from "scripts/common/BLDef"

export const forgetPsd = {
  data() {
    return {
      // 正則表達式
      Reg: Regex.Regex,
      // 帳號
      Acct: undefined,
      // 電子信箱
      Email: undefined
    }
  },
  mounted: function () {
    CommUtility.BindEnterTrigger("forgetPasswordForm", this.SubmitRequest)
    EventBus.$on("ForgetPasswordAlert", () => {
      console.log("dsgdsg1111111111111111")
      this.OpenWindow()
    })
  },
  methods: {
    // 弹出新窗口
    OpenNewBox: function (aa) {
      return window.open(
        aa,
        "newindow",
        "height=565,width=357,top=0,left=0,toolbar=no,menubar=no,scrollbars=no,resizable=no,location=no,status=no"
      )
    },
    // 開起此彈窗
    OpenWindow: function () {
      CommUtility.WebShowUniqueForm("forgetPasswordForm")
    }, // end OpenWindow

    // 關閉此彈窗
    CloseWindow: function () {
      this.Acct = undefined
      this.Email = undefined
      this.$validator.reset()
      CommUtility.WebCloseUniqueForm("forgetPasswordForm")
    }, // end CloseWindow

    // 送出資料
    SubmitRequest: async function () {
      const result = await this.$validator.validateAll()
      if (result === false) {
        console.log("++++++++++++++++++++++++++++")
        console.log("++++++++++++++++++++++++++++")
        Object.keys(this.fields).forEach((key) => {
          this.fields[key].touched = true
        })
        return
      } // end if

      const dataObj = {
        ForgetPwdInfo: {
          WGUID: Vue.prototype.$WGUID,
          Acct: this.Acct,
          Email: this.Email,
          Domain: document.location.protocol + "//" + location.host
        }
      }

      EventBus.$emit("GlobalLoadingTrigger", true)
      const retData = await LoginService.ForgetPassword(dataObj)
      EventBus.$emit("GlobalLoadingTrigger", false)

      if (retData.Ret == BLDef.ErrorRetType.SUCCESS) {
        // 成功
        this.CloseWindow()
        this.$parent.TriggerForgetPasswordInformation()
      } else if (retData.Ret == BLDef.ErrorRetType.COMM_OUT_OF_RANGE) {
        // 額度已用完
        this.CloseWindow()
        this.$parent.TriggerForgetPasswordFail()
      } else {
        // retData.Ret == BLDef.ErrorRetType.DB_STATUS_DISABLED --账号已停用
        this.CloseWindow()
        const notifyData = {
          NotifyMessage: retData.Message
        }
        EventBus.$emit("showNotifyMessage", notifyData)
      }
    } // end SubmitRequest
  }
}
