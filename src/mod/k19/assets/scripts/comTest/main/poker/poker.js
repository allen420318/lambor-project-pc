import BLDef from "scripts/common/BLDef"
import Vue from "vue"
import CommUtility from "scripts/common/CommUtility"
import URLService from "scripts/common/URLService"
import EventBus from "scripts/common/EventBus"
import HttpService from "scripts/common/HttpService"
import { showMore } from "scripts/customJs/web/showMore"
// 列表内容
import pokerContent from "@/mod/k19/components/general/poker/pokerContent"
export const poker = {
  components: {
    pokerContent
  },
  data() {
    return {
      // livevideo List 2018.11.15 SeyoChen 遍历游戏
      GameList: [],
      PoKerList: [],
      allPoKerList: [],
      AddList: "", // 2018.11.15 SeyoChen 动态识别添加游戏模块

      // 彩票遊戲Post Model
      GameData: {
        GameAPIVendor: undefined,
        GameCode: "",
        GameCatlog: BLDef.GameCatlogType.Poker,
        PlayType: BLDef.IdentityType.FORMAL,
        PlatformType: BLDef.PlatformType.Web_PC
      },
      PageCount: 0,
      pageNum: 1,
      pageSize: 12 // // 分页每页12条
    }
  },

  mounted: function () {
    this.GatGame() // 2019.3.28 SeyoChen 遍历游戏
  },

  methods: {
    // 遍历获取游戏列表
    GatGame: async function () {
      this.GameList = JSON.parse(localStorage.getItem("GameList"))
      const dataObj = {
        WGUID: "",
        GameTypeNo: ""
      }
      dataObj.WGUID = Vue.prototype.$WGUID
      dataObj.GameTypeNo = "0" // 0.全部 1.真人视讯 2.体育 3.彩票 4.电子
      const retData = await HttpService.PostDynamic(0, 214, dataObj)

      if (retData.Ret === 0 && this.GameList !== retData.Data.GameAPIVerdorList) {
        // 判断本地缓存是否与之前的相同
        this.GameList = retData.Data.GameAPIVerdorList
        localStorage.setItem("GameList", JSON.stringify(retData.Data.GameAPIVerdorList))
      }

      for (let i = 0; i < this.GameList.length; i++) {
        // 2019.01.12 筛选游戏 并 判定状态 SeyoChen 启动中：702 维护中：703 关闭：704
        if (this.GameList[i].GameTypeNo === 5 && this.GameList[i].GameTypeState !== 704) {
          this.allPoKerList.push(this.GameList[i])
        }
      }
      // 调试时使用，数据不够时随机插入1-20倍数据
      // let pauseList = JSON.parse(JSON.stringify(this.allPoKerList));
      // let randomNum = 19 || Math.floor((Math.random() * 20) + 1);
      // console.log('randomNum', randomNum)
      // for (let i = 0; i < randomNum; i++) {
      //     this.allPoKerList.push(...pauseList)
      // }

      // 判断总页数
      this.PageCount = Math.ceil(this.allPoKerList.length / this.pageSize)
      // 只有一页时不需要显示换页
      this.PageCount = this.PageCount === 1 ? 0 : this.PageCount
      // 初始计算第一页游戏条数
      this.$refs["pokerContent"].computeGameList(1, this.allPoKerList)
    },
    // 获取游戏列表 2018.11.15 SeyoChen end
    // 棋牌遊戲
    PlayGame: function (gameList) {
      // 2018.11.15 SeyoChen 遍历游戏
      this.GameData.GameAPIVendor = gameList.GameApiNo
      this.GameData.GameCode = gameList.GameCode
      this.GameData.GameCatlog = BLDef.GameCatlogType.Poker // 2018.12.01 传递游戏类型
      localStorage.setItem("gameListGameApiNo", gameList.GameApiNo)
      localStorage.setItem("PlayGamePostData", URLService.GetUrlParameterFromObj(this.GameData))

      if (gameList.GameTypeNo == 5 && gameList.GameApiNo == "69") {
        console.log("sffdf")
        CommUtility.OpenPlayGameWindow("webPlayMyGame", "PlayGamePopUpWindow")
      } else if (gameList.GameTypeNo == 5 && gameList.GameApiNo == "70") {
        CommUtility.OpenPlayGameWindow("webPlayMyGame", "PlayGamePopUpWindow")
      } else if (gameList.GameTypeNo == 5 && gameList.GameApiNo == "72") {
        CommUtility.OpenPlayGameWindow("webPlayMyGame", "PlayGamePopUpWindow")
      } else if (gameList.GameTypeNo == 5 && gameList.GameApiNo == "73") {
        CommUtility.OpenPlayGameWindow("webPlayMyGame", "PlayGamePopUpWindow")
      } else {
        // EventBus.$emit('showTransferMessage', this.GameData);
        CommUtility.OpenPlayGameWindow("webPlayGame", "PlayGamePopUpWindow")
      }
      // CommUtility.OpenPlayGameWindow('webPlayGame', 'PlayGamePopUpWindow');
    } // end PlaySBGame
  }
}
