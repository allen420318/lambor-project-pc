import electronicContent from "@/mod/k19/components/general/electronic/electronicContent"

export const electronic = {
  name: "electronic",
  components: {
    electronicContent
  }
}
