import BLDef from "scripts/common/BLDef"
import Vue from "vue"
import CommUtility from "scripts/common/CommUtility"
import EventBus from "scripts/common/EventBus"
import URLService from "scripts/common/URLService"
import HttpService from "scripts/common/HttpService"
// 列表内容
import lotteryContent from "@/mod/k19/components/general/lottery/lotteryContent"
export const lottery = {
  components: {
    lotteryContent
  },
  data() {
    return {
      // 頁籤類型
      // TabType: {
      //     // 快樂彩說明
      //     HappyTab: '1',
      //     // 時時彩說明
      //     AlwaysTab: '2',
      //     // PK10
      //     PKTab: '3',
      //     // 香港樂透
      //     HongKongTab: '4',
      //
      // },
      // Lottery List 2018.11.15 SeyoChen 遍历游戏
      GameList: [],
      LotteryList: [],
      allLotteryList: [],
      AddList: "", // 2018.11.15 SeyoChen 动态识别添加游戏模块

      // 彩票遊戲Post Model
      GameData: {
        GameAPIVendor: undefined,
        GameCode: "",
        GameCatlog: BLDef.GameCatlogType.Lottery,
        PlayType: BLDef.IdentityType.FORMAL,
        PlatformType: BLDef.PlatformType.Web_PC
      },
      // 當下頁籤類型
      // TabNum: '1',
      PageCount: 0,
      pageNum: 1,
      pageSize: 10, // 分页每页10条

      // 世彩内部游戏 GameCode 爱国彩：256  北部（河内彩票）：197  中部（承天顺化彩票）：183  南部（胡志明彩票）：162  越南30秒（越南30秒彩）：223
      WorldGameCode: [256, 197, 183, 162, 223]
    }
  },

  mounted: function () {
    this.GatGame() // 2018.11.15 SeyoChen 遍历游戏
  },

  methods: {
    // 遍历获取游戏列表
    GatGame: async function () {
      this.GameList = JSON.parse(localStorage.getItem("GameList"))
      const dataObj = {
        WGUID: "",
        GameTypeNo: ""
      }
      dataObj.WGUID = Vue.prototype.$WGUID
      dataObj.GameTypeNo = "0" // 0.全部 1.真人视讯 2.体育 3.彩票 4.电子
      const retData = await HttpService.PostDynamic(0, 214, dataObj)

      if (retData.Ret === 0 && this.GameList !== retData.Data.GameAPIVerdorList) {
        // 判断本地缓存是否与之前的相同
        this.GameList = retData.Data.GameAPIVerdorList
        localStorage.setItem("GameList", JSON.stringify(retData.Data.GameAPIVerdorList))
      }

      for (let i = 0; i < this.GameList.length; i++) {
        // 2019.01.12 筛选游戏 并 判定状态 SeyoChen 启动中：702 维护中：703 关闭：704
        if (this.GameList[i].GameTypeNo === 3 && this.GameList[i].GameTypeState !== 704) {
          if (this.GameList[i].GameApiNo === "97") {
            for (let j = 0; j < 5; j++) {
              let Game = JSON.parse(JSON.stringify(this.GameList[i]))
              Game.GameCode = this.WorldGameCode[j]
              this.allLotteryList.push(Game)
              // this.allLotteryList[j].GameCode = this.WorldGameCode[j];
            }
          } else {
            this.allLotteryList.push(this.GameList[i])
          }
        }
      }

      console.log(this.allLotteryList)
      // 调试时使用，数据不够时随机插入1-20倍数据
      // let pauseList = JSON.parse(JSON.stringify(this.allLotteryList));
      // let randomNum = 50 || Math.floor((Math.random() * 20) + 1);
      // console.log('randomNum', randomNum)
      // for (let i = 0; i < randomNum; i++) {
      //     this.allLotteryList.push(...pauseList)
      // }

      // 判断总页数
      this.PageCount = Math.ceil(this.allLotteryList.length / this.pageSize)
      // 只有一页时不需要显示换页
      this.PageCount = this.PageCount === 1 ? 0 : this.PageCount
      // 初始计算第一页游戏条数
      this.$refs["lotteryContent"].computeGameList(1, this.allLotteryList)
    },
    // 彩票遊戲
    PlayKGGame: function (gameList) {
      console.log(gameList)
      // 2018.11.15 SeyoChen 遍历游戏
      this.GameData.GameAPIVendor = gameList.GameApiNo
      console.log(this.GameData.GameAPIVendor)
      this.GameData.GameCode = gameList.GameCode
      this.GameData.GameCatlog = BLDef.GameCatlogType.Lottery // 2018.12.01 传递游戏类型
      console.log(this.GameData)
      localStorage.setItem("PlayGamePostData", URLService.GetUrlParameterFromObj(this.GameData))
      // EventBus.$emit('showTransferMessage', this.GameData);
      CommUtility.OpenPlayGameWindow("webPlayGame", "PlayGamePopUpWindow")
    } // end PlaySBGame
  }
}
