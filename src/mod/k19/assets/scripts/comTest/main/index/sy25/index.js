import BLDef from "scripts/common/BLDef"
import Vue from "vue"
import mainHome from "scripts/customJs/web/mainHome"
import stopExecutionOnTimeout from "scripts/customJs/web/stopExecutionOnTimeout"
import EventBus from "scripts/common/EventBus"
import CommUtility from "scripts/common/CommUtility"
import LoginService from "scripts/businessLogic/loginService"
import CommonService from "scripts/businessLogic/commonService"
import SignalRService from "scripts/common/SignalRService"
import rotateLoopFun from "scripts/customJs/web/rotateLoop"
import PersonalCenterService from "scripts/businessLogic/personalCenterService"
import "scripts/customJs/web/script25/jquery-2.1.1.min.js"

// 引入25版首页轮播（但已弃用）
//  import 'scripts/customJs/web/script25/TweenMax.min.js';
//  import 'scripts/customJs/web/script25/index.js';

import Swiper from "swiper"

export const initIndex25 = {
  data() {
    return {
      MemberInfo: {},
      IsGetBalanceIng: false,
      VBalance: undefined,
      IsLogin: undefined,
      StateLogin: false
    }
  },
  created: function () {
    this.CheckIsLogin()
    this.$nextTick(() => {
      var swiper = new Swiper(".swiper-container", {
        mousewheelControl: true,
        direction: "vertical",
        mousewheelForceToAxis: true,
        pagination: ".swiper-pagination",
        on: {
          init: function () {
            swiperAnimateCache(this) // 隐藏动画元素
            swiperAnimate(this) // 初始化完成开始动画
          },
          slideChangeTransitionEnd: function () {
            swiperAnimate(this) // 每个slide切换结束时也运行当前slide动画
            // this.slides.eq(this.activeIndex).find('.ani').removeClass('ani'); 动画只展现一次，去除ani类名
          }
        }
      })
    })
  },
  mounted: function () {
    const self = this
    EventBus.$on("SignalR_AddPrivateMessage", () => {
      self.MemberInfo.UnreadCount += 1
    })
    EventBus.$on("SignalR_ReadPrivateMessage", (data) => {
      self.MemberInfo.UnreadCount = data.Data
    })
    EventBus.$on("fundGetBalanceComplete", (SummaryAmount) => {
      self.VBalance = SummaryAmount
      console.log("-------------------------------------------")
      console.log("self.VBalance")
      self.IsGetBalanceIng = false
    })
    EventBus.$on("memberHeaderUpdateBalance", () => {
      self.GetBalance()
    })

    this.GetMemberInfo()
    this.GetBalance()
  },

  methods: {
    // 新的跳转打开个人中心方式 2019.05.13 SeyoChen
    navClick: async function (name) {
      this.CheckLoginStatusBeforeRoute(name)
      if (this.StateLogin !== false) {
        this.$store.dispatch("setSwitchNameText", name)
        EventBus.$emit("personalInfo")
      }
    },

    // 檢查帳號狀態
    CheckLoginStatusBeforeRoute: async function (routeName) {
      const notifyData = {}
      const data = await CommonService.Comm_CheckPermission()
      console.log("+++++++++++++++++++")
      console.log(routeName)
      switch (data.Status) {
        case BLDef.SysAccountStatus.LOGINED_ENABLED:
          this.StateLogin = true
          this.$store.dispatch("setSwitchNameText", routeName)
          EventBus.$emit("personalInfo", routeName)
          break
        case BLDef.SysAccountStatus.LOGINED_FROZEN:
          this.StateLogin = false
          notifyData.NotifyMessage = "账户被冻结, 请联系客服人员"
          EventBus.$emit("showNotifyMessage", notifyData)
          break
        default:
          break
      }
    },

    // 弹出客服窗口
    OpenNewBox: function (serveLink) {
      window.open(serveLink, "客服", "scrollbars=1,width=365,height=565,left=10,top=150")
    },
    GetCustomerServiceInfo: async function () {
      const inputObj = {
        WGUID: Vue.prototype.$WGUID
      }
      const data = await CommonService.Comm_GetCustomerServiceInfo(inputObj)
      if (data.Ret === BLDef.ErrorRetType.SUCCESS) {
        this.CustomerService = data.Data.CustomerServiceInfo
      }
    },
    CallQQClient: function (QQAcct) {
      const QQHref = "http://wpa.qq.com/msgrd?v=3&uin=" + QQAcct + "&menu=yes"
      window.open(QQHref)
    },
    // 檢查登入狀態
    CheckIsLogin: async function () {
      const data = await CommonService.Comm_CheckPermission()
      switch (data.Status) {
        case BLDef.SysAccountStatus.NOT_LOGIN:
          this.IsLogin = false
          break
        case BLDef.SysAccountStatus.LOGINED_ENABLED:
        case BLDef.SysAccountStatus.LOGINED_FROZEN:
          this.IsLogin = true
          break
        default:
          break
      }
    },
    GetBalance: async function () {
      this.IsGetBalanceIng = true
      rotateLoopFun.rotateLoop() // 刷新按钮的旋转
      if (this.$route.name === "FundManage") {
        // 更新資金管理餘額
        EventBus.$emit("updateFundBalance")
      } else {
        // this.VBalance = await CommUtility.CalculateSummaryAmount();
        const inputObj = {}
        const retData = await PersonalCenterService.MemberCashFlowInfo_GetCWalletMoney(inputObj) // 新版获取账户金额 2019.03.28

        if (retData.Ret == 0) {
          this.VBalance = retData.Data.VAmount
          console.log("this.VBalance6666666666666666666666666")
          console.log(this.VBalance)
          console.log("this.VBalance666666666666666666666666666666")
        } else {
          this.VBalance = this.$t("message.top_under_maintenance")
        }
        this.IsGetBalanceIng = false
      }
    },
    GetMemberInfo: async function () {
      // 取得會員資訊
      const data = await CommonService.Comm_GetMemberInfo()
      if (data.Ret == 0) {
        this.MemberInfo = data.Data.Member
        rotateLoopFun.rotateLoop() // 刷新按钮的旋转
      }
    },
    // 登出
    Logout: async function () {
      const inputObj = {
        LogoutInfo: {}
      }
      EventBus.$emit("GlobalLoadingTrigger", true)
      await LoginService.LogOut(inputObj)
      // 移除登入相關資料
      localStorage.removeItem("Token")
      localStorage.removeItem("Auth")
      localStorage.removeItem("logoutTime")
      localStorage.removeItem("Acct") // 2018.09.20 SeyoChen

      // 若沒接收到signalr斷線訊息，則3秒後自行斷線
      setTimeout(function () {
        // 斷Signalr連線
        SignalRService.DisConnectServer()
        window.location.href = "/"
        EventBus.$emit("GlobalLoadingTrigger", false)
      }, 3000)
    }
  },
  watch: {
    IsLogin: function () {
      console.log(this.IsLogin)
      if (this.IsLogin === true) {
        this.GetMemberInfo()
        this.GetBalance()
      }
    }
  }
}
