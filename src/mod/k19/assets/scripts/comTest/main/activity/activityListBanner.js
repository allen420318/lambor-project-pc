import EventBus from "scripts/common/EventBus"
import InformationService from "scripts/businessLogic/informationService"
import CommonService from "scripts/businessLogic/commonService"
import BLDef from "scripts/common/BLDef"
import CommUtility from "scripts/common/CommUtility"
import Vue from "vue"

export const ActivityListBanner = {
  props: {
    GetCurrentPageData: ""
  },

  data() {
    return {
      // 載入畫面資料
      LoadMainPageDataModel: {
        BannerActivityList: [],
        ActivityList: []
      },

      // 分頁資料
      PageInfo: {
        PageNo: 1,
        PageSize: 6,
        PageCount: 0,
        TotalCount: 0
      },

      DefaultActivityList: []
    }
  },

  created: function () {
    this.GetPromotion(1)
  },

  methods: {
    // 取得畫面資料
    GetPromotion: async function (pageNo) {
      this.PageInfo.PageNo = pageNo
      const DataObj = {
        WGUID: Vue.prototype.$WGUID,
        PagingInfo: this.PageInfo
      }
      const retData = await InformationService.GetActivityList(DataObj)

      if (retData.Ret != 0) {
        const notifyData = {
          NotifyMessage: retData.Message
        }
        EventBus.$emit("showNotifyMessage", notifyData)
        return
      } // end if

      this.LoadMainPageDataModel.ActivityList = retData.Data.ActivityList
      this.LoadMainPageDataModel.BannerActivityList = retData.Data.ActivityList.slice(0, 3)
      // this.SetDefaultActivityList();

      // switch (retData.Data.PagingInfo.TotalCount) {
      //     case 0:
      //         this.LoadMainPageDataModel.BannerActivityList = Vue.util.extend([], this.DefaultActivityList);
      //         this.LoadMainPageDataModel.ActivityList = Vue.util.extend([], this.DefaultActivityList);
      //         break;
      //     case 1:
      //         this.LoadMainPageDataModel.BannerActivityList = Vue.util.extend([], this.DefaultActivityList);
      //         this.LoadMainPageDataModel.BannerActivityList = this.LoadMainPageDataModel.BannerActivityList.concat(retData.Data.BannerActivityList);
      //         this.LoadMainPageDataModel.ActivityList = Vue.util.extend([], this.DefaultActivityList);
      //         this.LoadMainPageDataModel.ActivityList = this.LoadMainPageDataModel.ActivityList.concat(retData.Data.ActivityList);
      //         break;
      //     case 2:
      //         this.LoadMainPageDataModel.BannerActivityList.push(this.DefaultActivityList[0]);
      //         this.LoadMainPageDataModel.BannerActivityList = this.LoadMainPageDataModel.BannerActivityList.concat(retData.Data.BannerActivityList);
      //         this.LoadMainPageDataModel.ActivityList.push(this.DefaultActivityList[0]);
      //         this.LoadMainPageDataModel.ActivityList = this.LoadMainPageDataModel.ActivityList.concat(retData.Data.ActivityList);
      //         break;
      //     default:
      //         this.LoadMainPageDataModel.ActivityList = retData.Data.ActivityList;
      //         this.LoadMainPageDataModel.BannerActivityList = retData.Data.ActivityList.slice( 0, 3);
      //         break;
      // }

      const self = this

      // 2018.09.20 SeyoChen
      const retDataLv = await CommonService.Comm_CheckPermission()
      if (retDataLv.Status != 0) {
        const dataObj = {
          WGUID: "",
          LoginID: "",
          PagingInfo: this.PageInfo
        }
        dataObj.WGUID = Vue.prototype.$WGUID
        dataObj.LoginID = localStorage.getItem("Acct")
        const ActivityData = await CommonService.Comm_GetBonusActivity_MemberLv(dataObj)

        this.LoadMainPageDataModel.BannerActivityList = ActivityData.Data.ActivityList.slice(0, 3) // 优惠活动页面 banner 跟随等级更新 2019.04.12 SeyoChen
        this.$parent.ActivityList = ActivityData.Data.ActivityList
        let pageCount = Math.ceil(ActivityData.Data.PagingInfo.TotalCount / ActivityData.Data.PagingInfo.PageSize)
        this.$parent.PageInfo.PageCount = pageCount === 1 ? 0 : pageCount
      } else {
        this.$parent.ActivityList = this.LoadMainPageDataModel.ActivityList
        let pageCount = Math.ceil(retData.Data.PagingInfo.TotalCount / retData.Data.PagingInfo.PageSize)
        this.$parent.PageInfo.PageCount = pageCount === 1 ? 0 : pageCount
      }

      // this.$parent.ActivityList = this.LoadMainPageDataModel.ActivityList;
      // let pageCount = Math.ceil(retData.Data.PagingInfo.TotalCount / retData.Data.PagingInfo.PageSize);
      // this.$parent.PageInfo.PageCount = pageCount === 1 ? 0 : pageCount;
      $(".activity-expect").remove()
      setTimeout(() => {
        // this.AjectAddBox(this.$parent.ActivityList.length);
      }, 50)
      // this.$parent.ActivityList = this.LoadMainPageDataModel.ActivityList;
      // 2018.09.20 SeyoChen end

      // this.$parent.ActivityList = this.LoadMainPageDataModel.ActivityList;

      // 更新Paging

      console.log(this.$parent.PageInfo.PageCount)
      console.log(retData.Data.PagingInfo.TotalCount)
      console.log(retData.Data.PagingInfo.PageSize)
      console.log(Math.ceil(retData.Data.PagingInfo.TotalCount / retData.Data.PagingInfo.PageSize))
      // this.PageInfo.PageCount = Math.ceil(retData.Data.PagingInfo.TotalCount / retData.Data.PagingInfo.PageSize);
      // if (this.PageInfo.PageCount == 0) {
      //     this.$parent.PageInfo.PageCount = 1; // 若無紅利活動也設定為1，使分頁套件可以顯示
      // } else {
      //     this.$parent.PageInfo.PageCount = this.PageInfo.PageCount;
      // }
    }, // end GetPromotion
    // 2018.11.15 SeyoChen 动态识别添加游戏模块
    AjectAddBox: function (num) {
      const boxSize = parseInt(3) // 页面中一行的box个数
      const boxHtml = `<li class="activity-li activity-li-JumpPage activity-expect">
        <div class="activity-expect-tips"><div><p>${this.$t(
          "message.live_expect"
        )}</p><span>IN MAINTENANCE</span></div></div></li>`
      const box = $(".activity-lists")
      if (num % boxSize === 0 && num !== 0) {
      } else {
        for (let i = 0; i < Math.abs(boxSize - (num % boxSize)); i++) {
          box.append(boxHtml)
        }
      }
    },
    // 設定預設的優惠活動(首存/續存)
    SetDefaultActivityList: function () {
      const Today = CommUtility.UtcTicksToLocalTime(new Date().getTime(), "MM/DD")
      // 優惠活動-首存
      const DefaultActivityList1 = {
        ID: "-1",
        Name: this.$t("message.activity_deposit_rebate"),
        StartTick: new Date().getTime(),
        EndTick: new Date().getTime(),
        ActLogo: this.$ResourceCDN + "/EditionImg/lambor60.0/images/web/activity/activity_banner1.png?VersionCode",
        ActListLogo: this.$ResourceCDN + "/EditionImg/lambor60.0/images/web/activity/activity_img1.jpg?VersionCode",
        ActContent:
          '<p class="p_1">' + this.$t("message.activity_deposit_rebate") + '</p><p class="p_2">' + Today + "</p>"
      }
      // 優惠活動-續存
      const DefaultActivityList2 = {
        ID: "-2",
        Name: this.$t("message.activity_survival_rebate"),
        StartTick: new Date().getTime(),
        EndTick: new Date().getTime(),
        ActLogo: this.$ResourceCDN + "/EditionImg/lambor60.0/images/web/activity/activity_banner2.png?VersionCode",
        ActListLogo: this.$ResourceCDN + "/EditionImg/lambor60.0/images/web/activity/activity_img2.jpg?VersionCode",
        ActContent:
          '<p class="p_1">' + this.$t("message.activity_survival_rebate") + '</p><p class="p_2">' + Today + "</p>"
      }

      this.DefaultActivityList.splice(0, 1, DefaultActivityList1)
      this.DefaultActivityList.splice(1, 1, DefaultActivityList2)
    }, // end SetDefaultActivityList

    //  跳至詳細
    UrlRedirectDetail: function (target) {
      this.$router.push({
        name: "webActivityDetail",
        params: {
          ID: target.ID
        }
      })
    } // end UrlRedirectDetail
  },

  watch: {
    GetCurrentPageData: function () {
      this.GetPromotion(this.GetCurrentPageData)
    }
  }
}
