// import BLDef from 'scripts/common/BLDef';
// import Vue from 'vue';

export const ActivityListContent = {
  data() {
    return {
      // 資源位置
      // Resource: Vue.prototype.$ResourceURL,
      // 資源旗標
      ResourceFlag: false,
      HaveActivity: true
    }
  },

  props: {
    ActivityList: {}
  },

  methods: {
    //  跳至詳細
    UrlRedirectDetail: function (target) {
      this.$router.push({
        name: "ActivityListDetails",
        params: {
          ID: target.ID
        }
      })
    } // end UrlRedirectDetail
  },

  watch: {
    ActivityList: function () {
      if (this.ActivityList.length <= 0) {
        this.HaveActivity = false
      }
      this.ResourceFlag = true
    }
  }
}
