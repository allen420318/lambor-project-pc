export const PartnerOwn = {
  name: "HelloWorld",
  components: {},
  data() {
    return {
      logoIndex: 0,
      frimNavs: ["星联盟", "AG亚游", "PT平台", "沙巴", "KG", "欧博"],
      navIndex: 0,
      loginFlag: true
    }
  },
  mounted() {
    // 23版 按钮点击后出现进入动画
    $(".enter-btn").click(function () {
      $(".partner-content").animate({
        left: "-100%"
      })
    })
  },
  created: function () {
    this.load()
  },
  methods: {
    load: function () {
      this.$nextTick(function () {
        memberLoad()
      })
    },
    navClick(index) {
      this.navIndex = index
    }
  }
}
