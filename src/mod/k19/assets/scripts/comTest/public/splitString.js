function SplitString(value, len, arr) {
  // 双字节字符个数
  let doubleByteLen = (value.match(/[^\x00-\xff]/g) || []).length * 2
  // 单字节字符个数
  let singleByteLen = (value.match(/[?!^\x00-\xff]/g) || []).length
  // 切割后的数组
  let splitArr = []
  if (arr) splitArr = arr
  if (doubleByteLen + singleByteLen > len) {
    // 处理字符串
    let newValue = processString(value, len, splitArr)
    SplitString(newValue, len, splitArr)
  } else {
    splitArr.push(value)
  }
  return splitArr
}
function processString(value, len, splitArr) {
  let sumByteLen = 0
  let stringArr = []
  let valueArr = value.split("")
  let newArr = JSON.parse(JSON.stringify(valueArr))
  valueArr.forEach((character) => {
    sumByteLen++
    if (/[^\x00-\xff]/.test(character)) sumByteLen++
    if (sumByteLen > len) return
    stringArr.push(newArr.splice(0, 1))
  })
  console.log(stringArr.join(""))
  splitArr.push(stringArr.join(""))
  return newArr.join("")
}

export default SplitString
