import BLDef from "scripts/common/BLDef"
import Vue from "vue"
import CommonService from "scripts/businessLogic/commonService"

export const initService34 = {
  data() {
    return {
      IsCustomerServiceShowing: false,
      CustomerService: {}
    }
  },
  props: {
    IsLogin: undefined
  },
  created: function () {
    this.GetCustomerServiceInfo()
  },
  methods: {
    // 弹出新窗口
    OpenNewBox34: function (serveLink) {
      window.open(serveLink, "客服", "scrollbars=1,width=365,height=565,left=10,top=150")
      // return window.open(aa, 'newindow', 'height=565,width=357,top=0,left=0,toolbar=no,menubar=no,scrollbars=no,resizable=no,location=no,status=no');
    },
    GetCustomerServiceInfo: async function () {
      const inputObj = {
        WGUID: Vue.prototype.$WGUID
      }
      const data = await CommonService.Comm_GetCustomerServiceInfo(inputObj)
      if (data.Ret === BLDef.ErrorRetType.SUCCESS) {
        this.CustomerService = data.Data.CustomerServiceInfo
      }
    },
    CallQQClient: function (QQAcct) {
      const QQHref = "http://wpa.qq.com/msgrd?v=3&uin=" + QQAcct + "&menu=yes"
      window.open(QQHref)
    }
  }
}
