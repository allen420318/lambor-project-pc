export default {
    /* eslint-disable */
    // 正則表達式
    Regex: {
        // 帳號欄位
        Account: /^[0-9A-Za-z]{4,12}$/,
        // 登录账号栏位
        LoginAccount: /^[0-9A-Za-z_-]{4,30}$/,
        // 密碼欄位
        Password: /^[a-zA-Z0-9!@#$%^&*()_+{}:"|<>?\-=\[\]'\;,./~`]{6,20}$/,
        // Password: /^[a-zA-Z0-9!@#$%^&*()_+{}:\"|<>?\-\\\\=\\[\\]'\;,\\\\.\\\\\/~`]{6,20}$/,
        // 驗證碼欄位
        VCode: /^[0-9A-Za-z]{4}$/,
        // 手機號碼
        PhoneNo: /^[0-9-+]{6,30}$/,
        // PhoneNo: /^1(3[0-9]|4[57]|5[0-35-9]|7[0135678]|8[0-9])\d{8}$/,
        // 遊戲關鍵字
        GameKeyword: /^[^\f\t\v]{0,20}$/,
        // 取款密碼
        WithdrawPwd: /^[0-9]{6}$/,
        // 暱稱/姓名
        NickName: /^.{2,100}$/,
        //银行资料姓名
        // NickBankName: /^[\u4e00-\u9fa5_a-zA-Z/\n\s*/g]{2,50}$/,
        NickBankName: /^[^0-9!@#$%^&*()_+{}:""|<>?\-=\[\]'\;,，……￥\\？《》【】·——‘’“”。、./~`]{2,50}$/,
        // 微信帳號
        WeChatID: /^[A-Za-z][A-Za-z0-9-_]{5,19}$/,
        // 信箱驗證1
        Email1: /^[0-9A-Za-z-_@\.]{5,100}$/,
        // 信箱驗證2
        Email2: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        // QQ帳號
        QQID: /^[0-9]{5,15}$/,
        // 銀行名稱
        BankName: /^.{2,100}$/,
        BankNickName: /^[^!@#$%^&*()+{}:""|<>?\=\[\]'\;,，……￥\\？《》【】·——‘’“”。、./~`]{2,50}$/,
        // 分行名稱
        BankBranchName: /^.{2,100}$/,
        BankExpensesName: /^.{2,50}$/,
        // 銀行帳號
        BankAccount: /^[0-9]{4,25}$/,

        // 金額
        MoneyAmount: /^[1-9][0-9]*$/,
        // 金额-允许输入小数点
        MoneyAmountPoint: /^(([1-9]{1}\d*)|(0{1}))(\.\d{1,2})?$/,
        // 備註
        Remark: /^[\s\S]{0,200}$/,
        // 站內信關鍵字
        //  PMKeyWord: /^[0-9A-Za-z-_\u4E00-\u9FA5+/.*!@#$%^&?\s]{3,30}$/,
        // 站內信關鍵字 可以支持各种标点符号 2018.12.20 SeyoChen 按需求走
        PMKeyWord: /^.{1,60}$/,
        UserKeyWord: /^.{3,60}$/,
        // 連繫我們-訊息
        Content: /^[\s\S]{2,1000}$/,
        // 收款地址欄位
        Address: /^[0-9A-Za-z]{10,100}$/,
        //代理中心-创建账号
        agencyUser: /^[0-9A-Za-z]{4,12}$/,
        // 代理中心-推广设置-方案名称
        PlanName: /^.{2,100}$/,
        //创建账号邮箱
        CreateEmail: /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(.[a-zA-Z0-9-]+)*.[a-zA-Z0-9]{2,6}$/,
        // 投注单号校验规则
        betNo: /^[^\f\t\v\u4E00-\u9FA5]{0,50}$/
    },
};
