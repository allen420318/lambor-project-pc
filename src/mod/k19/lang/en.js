const messagesEn = {
  tutorial_drawing_money: "Withdrawal related",
  tutorial_game_questions: "Game issues",
  tutorial_points_attention: "Precautions",
  tutorial_register_description: "Registration for new users is completely free.",
  tutorial_register_pay_problem: "Does it cost anything to register?",
  tutorial_register_descriptions: "Registering new users is completely free.",
  tutorial_register_attention:
    "Once the username is submitted, it cannot be changed. Please use a highly secure username and keep it in a safe place. The real name must be the same as the bank user name, otherwise no payment can be made. Please fill in a valid mobile phone number so that customer service can contact you as soon as a problem occurs.",
  information_modify: "Modify Personal Information",
  information_data_problem: "How to modify the data?",
  information_data_description:
    "Click the user name in the upper right to enter [Personal Center], [Personal Center]> [Personal Information], and you can modify your data on the [Personal Information] page. After modifying the data, click Save.",
  information_password_problem: "How do I change my password?",
  information_password_description:
    "Click the user name in the upper right to enter [Personal Center], [Personal Center]> [Change Password], select [Password Modify], and you can modify your password on the [Password Modify] page.",
  information_Withdrawal_password_problem: "How to change the withdrawal password?",
  information_Withdrawal_password_description:
    "Click the user name in the upper right to enter [Personal Center], [Personal Center]> [Change Password], and select [Withdraw Password Modify]. On the [Withdraw Password Modify] page, you can modify your withdrawal password.",
  information_name_problem: "How do I make a name change?",
  information_name_description:
    "If you need to change your name when registering because of a name change, please contact customer service.",
  information_forget_password_problem: "forget password?",
  information_forget_password_description: "Please contact customer service, someone will serve you.",
  recharge_related: "Recharge related",
  recharge_way_problem: "How do I top up?",
  recharge_way_description:
    "After logging in to your personal account, click [Recharge] in the upper right corner of the website to enter the recharge page. Select the recharge method you want and follow the prompts to recharge. Different recharge channels have different accounting times.",
  recharge_handlingfee_problem: "Handling fee?",
  recharge_handlingfee_description:
    "If you choose the company payment method, the bank will charge you a remittance fee; if you use the online payment method, there is no fee.",
  recharge_amount_problem: "Deposit limit?",
  recharge_amount_description:
    'Each bank has its own single maximum limit. When using online payment to select a bank, please first refer to the "Limit Description" at the bottom of this page, which is indicated in it.',
  recharge_attention_problem: "Precautions?",
  recharge_attention_description1:
    "The platform will change the payment card number from time to time. Please confirm the latest payment information before remittance. If the remittance is caused to a non-latest payment card, the loss is not related to the platform.",
  recharge_attention_description2:
    "Before confirming the account, please keep the relevant transfer information for comparison. (Eg: sender is name, card number, specific time and amount of remittance, bank documents, etc.)",
  wallet_related: "Withdrawal related",
  wallet_balance_problem: "Check your credit balance?",
  wallet_balance_description:
    "You can check the distribution of the current quota on each platform in [Fund Status] in [Personal Center].",
  wallet_conversion_problem: "How do I perform a balance conversion?",
  wallet_conversion_description:
    "You can perform quota conversion on the [One-Key Recall] page, [Personal Center]> [Fund Status]> [One-Key Recall], select transfer and transfer account numbers, enter the conversion amount, and press OK Conversion.",
  withdrawal_problem: "How do I withdraw money?",
  withdrawal_description:
    "You must complete your bank information for the first withdrawal. After logging in to your personal account, click the user name in the upper right to enter [Personal Center], [Personal Center]> [Withdrawal], fill in the withdrawal password, and you can apply for withdrawal.",
  withdrawal_fail_problem: "Withdrawal failed?",
  withdrawal_fail_description: "There are several reasons for the withdrawal failure:",
  withdrawal_fail_description1: "Withdrawal amount is greater than the actual amount in your account",
  withdrawal_fail_description2: "Incorrect withdrawal password",
  withdrawal_fail_description3: "Bank card information is illegal",
  withdrawal_fail_description4: "Incorrect bank card number, user name, bank refund",
  withdrawal_fail_description5:
    "If the withdrawal fails due to reasons other than the above, please contact customer service to assist you.",
  withdrawal_attention_problem: "Notes on applying for withdrawal?",
  withdrawal_attention_description:
    "Due to Philippine government regulation and the Anti-Money Laundering Act, you must meet the following conditions to make a payment:",
  withdrawal_attention_description1:
    '1. When withdrawing money, you must complete "Double Deposit Valid Bet" to withdraw money.',
  withdrawal_attention_description2:
    "2. If the new valid bet has not doubled the last deposit after the last withdrawal, the withdrawal cannot be made.",
  withdrawal_attention_description3: "3. The user name of the withdrawal must be the same as the bank.",
  withdrawal_handlingfee_problem: "Is there a handling fee for withdrawals?",
  withdrawal_handlingfee_description:
    "A maximum of 6 withdrawals per day, the first 3 times are free of handling fees, the next three times are charged 0.5%, and the single processing fee is up to 100 yuan.",
  withdrawal_arrivaltime_problem: "When does the withdrawal arrive?",
  withdrawal_arrivaltime_description:
    "All withdrawals arrive within 2 hours of bank working hours, and players can check the real-time situation in the member center.",
  inquire_related: "Query related",
  inquire_transaction_problem: "Recent transactions?",
  inquire_transaction_description: "In [Personal Center]> [Overview], you can see the recent transaction records.",
  inquire_financial_problem: "Inquiries about funds?",
  inquire_financial_description:
    "Search in [Personal Center]> [Record Query]> [Fund Transaction], you can freely set the query time and query type to search.",
  inquire_bet_problem: "Bet history query?",
  inquire_bet_description:
    "Search in [Personal Center]> [Record Search]> [Betting History]. You can freely set search time and search type to search.",
  activity_related: "Offer related",
  activity_update_problem: "How do I know the latest offers?",
  activity_update_description:
    "Please click [Promotions] on the homepage of the website to enter the event list page to view all offers. Click on each offer to enter the details list.",
  play_start_problem: "How to play?",
  play_start_description:
    "After logging in to your member account, click on the game items you are interested in at the top of the website, and the list of game manufacturers and games will jump to the bottom. Select the manufacturer or game you want to play and start playing.",
  play_dropped_problem: "Dropped after betting?",
  play_dropped_description:
    "If you suddenly drop the line after placing a bet, the confirmed bet slip is still valid, and the system will pay according to the actual results of the authorities. When you re-login to your account, please check the game betting history, balance and game results, etc. If you have any questions, please contact customer service to confirm.",
  play_loadflash_problem: "Can't load game flash?",
  down_app: "APP",
  tutorial_register_Description:
    'Click the [Register] button on the top right of the website to enter the registration page, read and agree to the "User Agreement", complete the registration information and submit it to complete the registration.',
  member_bank_notes:
    "Please fill in the correct bank name，Such as：ACB、BIDV、SACOMBANK、VIETINBANK、TECJCOM、BANKCOMMB、VIETCOMBANK、EXIMBANK",
  VND_LOTTERY_1: "Patriotic lottery",
  VND_LOTTERY_2: "North",
  VND_LOTTERY_3: "Middle",
  VND_LOTTERY_4: "South",
  VND_LOTTERY_5: "VN 30s",
  agency_download: "download"
}

export default messagesEn
