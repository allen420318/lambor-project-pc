const messagesZh = {
  tutorial_drawing_money: "取款相关",
  member_extra_dividend: "红利百分比",
  member_bank_notes:
    "请填写正确银行的名称，如：ACB、BIDV、SACOMBANK、VIETINBANK、TECJCOM、BANKCOMMB、VIETCOMBANK、EXIMBANK",
  VND_LOTTERY_1: "爱国彩",
  VND_LOTTERY_2: "北部",
  VND_LOTTERY_3: "中部",
  VND_LOTTERY_4: "南部",
  VND_LOTTERY_5: "越南30秒彩"
}

export default messagesZh
