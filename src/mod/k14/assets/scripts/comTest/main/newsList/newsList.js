import InformationService from '@/mod/k14/assets/scripts/businessLogic/informationService.js';
import BLDef from '@/assets/scripts/common/BLDef.js';
import EventBus from '@/assets/scripts/common/EventBus.js';
import HttpService from '@/assets/scripts/common/HttpService.js';
import NewsListContent from '@/mod/k14/components/sub/newsList/newsListContent.vue';
import NewsListPaging from '@/mod/k14/components/sub/paginate.vue';
import Vue from 'vue';

export const NewsList = {
    data () {
        return {
            PageInfo: {
                PageCount: 0,
            },
            CurrentPageData: '',
            NewsDetail: {
                Title: '',
            },
            MessageCotentData: undefined,

            // 資源位置
            Resource: Vue.prototype.$ResourceURL,
            // 資源旗標
            ResourceFlag: false,
        };
    },
    mounted () {
        // 60版 弹窗关闭
        $('.news-close').click(function () {
            $('#n-p-main').hide();
        });
        // 23版隐藏详情显示列表
        $('.news-popup-back').click(function () {
            $('#n-p-main').fadeOut();
            $('.news-page').fadeIn();
        });
        // 60版 弹窗滚动条
        $('.news-scroll-box').perfectScrollbar();

        // 23版 弹窗滚动条
        $('.news-popup-detail').perfectScrollbar();
    },
    methods: {
        // 弹窗弹出详情页面
        showDetail: function (ID) {
            this.PageInit(ID);
            $('#n-p-main').show();
        },
        //  跳至新聞詳細
        UrlRedirectDetailAnimation: function (ID) {
            this.PageInit(ID)
            $('.news-page').fadeOut();
            $('#n-p-main').fadeIn();
        }, // end UrlRedirectDetail
        // 初始化
        PageInit: async function (ID) {
            const dataObj = {
                WGUID: Vue.prototype.$WGUID,
                ID: ID
            };
            const retData = await InformationService.GetNewsDetail(dataObj);

            // 失敗
            if (retData.Ret != 0) {
                const notifyData = {
                    NotifyMessage: retData.Message
                };
                EventBus.$emit('showNotifyMessage', notifyData);
                return;
            } // end if

            // 成功
            this.NewsDetail = retData.Data.Detail;
            const temp = await HttpService.GetContent(this.Resource + retData.Data.Detail.MessageCotent);
            this.MessageCotentData = temp;

            // 顯示資源
            this.ResourceFlag = true;
        }, // end PageInit
        GetCurrentPageData: function (page) {
            this.CurrentPageData = page;
        },
    },
    components: {
        NewsListContent,
        NewsListPaging
    },

};
