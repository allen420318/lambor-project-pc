// import '@/assets/scripts/customJs/web/showdate.js';
import FrameFooter from '@/mod/k14/components/public/frameFooter.vue'
import FrameHeader from '@/components/public/frameHeader.vue'
import FrameNav from '@/components/public/mainNavigator.vue'
import floatService from '@/mod/k14/components/public/floatService.vue'
import MemberFillTip from '@/mod/k14/components/public/memberFillTip.vue';
import BLDef from '@/assets/scripts/common/BLDef.js';
import CommonService from '@/mod/k14/assets/scripts/businessLogic/commonService.js';
import EventBus from '@/assets/scripts/common/EventBus.js';
import rotateLoopFun from '@/assets/scripts/customJs/web/rotateLoop.js';
import PersonalCenterService from '@/mod/k14/assets/scripts/businessLogic/personalCenterService.js';
export const baseMbLy = {
    components: {
        FrameFooter,
        FrameHeader,
        FrameNav,
        floatService,
        MemberFillTip
    },
    data() {
        return {
            IsLogin: undefined,
            IsGetBalanceIng: false,
            VBalance: '',
            MemberInfo: {},
        };
    },
    mounted() {
        const self = this;
        EventBus.$on('fundGetBalanceComplete', (SummaryAmount) => {
            self.VBalance = SummaryAmount;
            self.IsGetBalanceIng = false;
        });
        EventBus.$on('memberHeaderUpdateBalance', () => {
            self.GetBalance();
        });
        // 每10分钟刷新一次钱包 2019.03.28 SeyoChen
        EventBus.$on('GBKBalance', (Balance) => {
            self.VBalance = Balance;
        });
    },
    created: function () {
        this.CheckIsLogin();
        // this.$nextTick(function () {
        //   function winScroll() {
        //     const topLeft = document.documentElement.scrollLeft;
        //     $('.header').css('left', -topLeft + 'px');
        //   }
        //   window.onscroll = winScroll;
        // });
    },
    methods: {
        // 檢查登入狀態
        CheckIsLogin: async function () {
            const data = await CommonService.Comm_CheckPermission();
            switch (data.Status) {
                case BLDef.SysAccountStatus.NOT_LOGIN:
                    this.IsLogin = false;
                    break;
                case BLDef.SysAccountStatus.LOGINED_ENABLED:
                case BLDef.SysAccountStatus.LOGINED_FROZEN:
                    this.IsLogin = true;
                    break;
                default:
                    break;
            }
        },
        // 檢查帳號狀態
        CheckLoginStatusBeforeRoute: async function (routeName) {
            const notifyData = {};
            const data = await CommonService.Comm_CheckPermission();
            switch (data.Status) {
                case BLDef.SysAccountStatus.LOGINED_ENABLED:
                    this.$router.push({
                        name: routeName
                    });
                    break;
                case BLDef.SysAccountStatus.LOGINED_FROZEN:
                    notifyData.NotifyMessage = '账户被冻结, 请联系客服人员';
                    EventBus.$emit('showNotifyMessage', notifyData);
                    break;
                default:
                    break;
            }
        },
        GetBalance: async function () {
            this.IsGetBalanceIng = true;
            rotateLoopFun.rotateLoop(); // 刷新按钮的旋转
            if (this.$route.name === 'FundManage') {
                // 更新資金管理餘額
                EventBus.$emit('updateFundBalance');
            } else {
                const inputObj = {};
                const retData = await PersonalCenterService.MemberCashFlowInfo_GetCWalletMoney(inputObj); // 新版获取账户金额 2019.03.28

                if (retData.Ret == 0) {
                    this.VBalance = retData.Data.VAmount
                } else {
                    this.VBalance = this.$t('message.top_under_maintenance')
                }
                this.IsGetBalanceIng = false;
            }
        },
        GetMemberInfo: async function () {
            // 取得會員資訊
            const data = await CommonService.Comm_GetMemberInfo();
            if (data.Ret == 0) {
                this.MemberInfo = data.Data.Member;
                rotateLoopFun.rotateLoop(); // 刷新按钮的旋转
            }
        },
    },
    watch: {
        IsLogin: function () {
            if (this.IsLogin === true) {
                this.GetMemberInfo();
                this.GetBalance();
            }
        },
    },
    beforeDestroy() {
        EventBus.$off('fundGetBalanceComplete');
        EventBus.$off('memberHeaderUpdateBalance');
    }
}
