import axios from "axios"
import Vue from "vue"
//  import '@/assets/scripts/customJs/web/jquery.js';
import "@/assets/scripts/customJs/web/slider.js"
import "@/assets/scripts/customJs/web/public.js"
import "@/assets/scripts/customJs/web/mousewheel.js"
import "@/assets/scripts/customJs/web/perfect-scrollbar.js"
// import '@/assets/scripts/customJs/web/jquery.SuperSlide.2.1.2'
import "@/assets/scripts/customJs/web/jquery.shCircleLoader"
import "@/assets/scripts/customJs/web/script25/swiper.min.js" // 25版引入swiper轮播
import "@/assets/scripts/customJs/web/script25/swiper.animate1.0.3.min.js"

// 导航引入文件
// import '@/assets/scripts/customJs/web/jquery-1.11.0';
// import '@/assets/scripts/customJs/web/jquery.SuperSlide.2.1.2';
import "@/assets/scripts/customJs/web/waypoints.min"
// import '@/assets/scripts/customJs/web/main.js';
// 导航引入文件结束

import EventBus from "@/assets/scripts/common/EventBus.js"
import BLDef from "@/assets/scripts/common/BLDef.js"
import LoginService from "@/mod/k14/assets/scripts/businessLogic/loginService.js"
import CommonService from "@/mod/k14/assets/scripts/businessLogic/commonService.js"
import SignalrService from "@/assets/scripts/common/SignalRService.js"
import HttpService from "@/assets/scripts/common/HttpService.js"
import MessageWindow from "@/mod/k14/alerMessage/messageWindow.vue"
import SystemNotifyWindow from "@/mod/k14/components/sub/systemNotifyWindow.vue"
import CustomerService from "@/mod/k14/components/general/sidebar/sidebar.vue"
import GlobalLoading from "@/alerMessage/globalLoading.vue"
import EmailVerifySuccess from "@/mod/k14/components/sub/memberNew/profile/emailVerifySuccess.vue"
import PersonalCenterService from "@/mod/k14/assets/scripts/businessLogic/personalCenterService.js"
import MemberMain from "@/mod/k14/components/base/memberMain.vue"
import MemberAgency from "@/mod/k14/components/base/memberAgency.vue"
import ForgetPassword from "@/mod/k14/components/sub/login/forgetPassword.vue"
import ForgetPasswordSuccess from "@/mod/k14/components/sub/login/forgetPasswordSuccess.vue"
import ForgetPasswordFail from "@/mod/k14/components/sub/login/forgetPasswordFail.vue"
import NewIndexService from "@/mod/k14/assets/scripts/businessLogic/newIndexService.js"
import AnnouncementWindow from "@/mod/k14/components/sub/announcementWindow.vue"
import CommUtility from "@/assets/scripts/common/CommUtility.js"
import { deflate } from "zlib"

export const baseInfo = {
  name: "app",
  data() {
    return {
      CTTimer: undefined,
      HBTimer: undefined,
      LoginTimer: undefined,
      EventData: false,
      IfLogin: false,
      IsLoginFlag: false,
      OnlinePeople: undefined
    }
  },
  components: {
    MessageWindow,
    SystemNotifyWindow,
    CustomerService,
    GlobalLoading,
    EmailVerifySuccess,
    MemberMain,
    MemberAgency,
    ForgetPassword,
    AnnouncementWindow,
    ForgetPasswordSuccess,
    ForgetPasswordFail
  },
  mounted() {
    // document.getElementById('app').className = 'theme' + this.themecolor;
    const self = this
    EventBus.$on("loginFlagCheck", (data) => {
      self.IsLoginFlag = data
      localStorage.setItem("loginFlag", data)
    })
    const loginCheck = localStorage.getItem("loginFlag")
    if (localStorage.getItem("loginFlag")) {
      self.IsLoginFlag = loginCheck
    }
    // 弹出公告信息只在首页显示
    let route = this.$route
    if (route.name == "Index" && sessionStorage.getItem("IsFirstEnter") !== "NO") {
      EventBus.$emit("showAnnouncementWindow")
    }
    // 修改密码，退出重新登录
    EventBus.$on("Logout_ChangePassword", () => {
      localStorage.setItem("loginFlag", "false")
      // const reData = await LoginService.LogOut(inputObj);
      // 移除登入相關資料
      localStorage.removeItem("Token")
      localStorage.removeItem("Auth")
      localStorage.removeItem("logoutTime")
      localStorage.removeItem("Acct") // 2018.09.20 SeyoChen
      localStorage.removeItem("CorrelationIBMemberId")
      localStorage.removeItem("IBStatus")
      localStorage.removeItem("loginFlag")
      SignalrService.DisConnectServer()
      // 修改登录密码成功后退出当前用户 20190830 SeyoChen
      const notifyData = {
        NotifyMessage: this.$t("message.main_change_password"),
        CloseFunction: self.SingleLogout
      }
      // 关闭个人中心、代理中心弹窗再打开登出提示
      CommUtility.WebCloseUniqueForm("memberAgencyFrom")
      CommUtility.WebCloseUniqueForm("memberMainFrom")
      EventBus.$emit("showNotifyMessage", notifyData)
    })
  },
  computed: {
    themecolor() {
      return this.$store.state.themecolor // 获取store的最新themecolor
    }
  },
  created: function () {
    // this._getLess("60");
    this.GetLogoData()
    this.EventFun() // 监听触发 呼叫事件
    this.GatGame() // 遍历获取游戏列表 2018.11.15 SeyoChen
    this.CheckLoginStatus()
    this.showNumberOnline()
    const self = this
    EventBus.$on("afterLogin", () => {
      clearInterval(self.CTTimer)
      self.SignalRConnect()
      self.LoginCount()
    })

    setInterval(function () {
      if (self.EventData == true && self.IfLogin == true) {
        self.EventData = false
        self.GBKgetBalance()
      }
    }, 600000)
    EventBus.$on("skinHeadCon", (num) => {
      self.skinClick(num)
    })

    EventBus.$on("personalInfo", (data) => {
      self.memberLayout(data)
    })
    EventBus.$on("personalAgency", (data) => {
      self.memberAgency(data)
    })
  },
  methods: {
    // 显示在线人数
    showNumberOnline: async function () {
      const DataObj = {
        WGUID: Vue.prototype.$WGUID
      }
      const retData = await NewIndexService.HomePageLoadRankAndOnlineNumPage(DataObj)

      if (retData.Ret === 0) {
        this.OnlinePeople = retData.Data.OnlineNum
        // sessionStorage.setItem('OnlinePeopleNum', this.OnlinePeople)
        // EventBus.$emit('OnlinePeopleNum', this.OnlinePeople);
      }
    },
    GetLogoData: async function () {
      const DataObj = {
        WGuid: Vue.prototype.$WGUID,
        platformType: BLDef.PlatformType.Web_PC
      }
      const retData = await CommonService.HomePage_LoadMainPage(DataObj)

      if (retData.Ret === 0) {
        localStorage.setItem("LogoName", retData.Data.Name)
        localStorage.setItem("LogoImgPath", this.$ResourceCDN + retData.Data.MasterLogo)
        localStorage.setItem("IconLogoUrl", retData.Data.IconLogoUrl) // 浏览器页签icon 20190814 SeyoChen
      }
    },
    // 切换皮肤
    skinClick: function (num) {
      // 把className theme1，theme2，theme3挂载在app.vue的<div id="app"></div>上
      document.getElementById("app").className = "theme" + num
      this.$store.dispatch("setThemeColorSkin", num)
    },
    memberLayout: function (data) {
      MemberMain.methods.ShowEditPhone(data)
    },
    memberAgency: function (data) {
      MemberAgency.methods.ShowAgency(data)
    },
    // GBK钱包查询余额
    GBKgetBalance: async function () {
      const inputObj = {}
      const retData = await PersonalCenterService.MemberCashFlowInfo_BackToCWallet(inputObj)
      if (retData.Ret == 0) {
        EventBus.$emit("GBKBalance", retData.Data.VAmount)
      }
    },
    // 监听键盘，鼠标事件 2019.03.28 SeyoChen
    EventFun: function () {
      const self = this
      window.onkeydown = function () {
        self.EventData = true
      }
      document.onmousemove = function () {
        self.EventData = true
      }
    },
    // 遍历获取游戏列表 2018.11.15 SeyoChen
    GatGame: async function () {
      // 判断本地是否已经有缓存--获取
      const GameData = localStorage.getItem("GameList")

      const dataObj = {
        WGUID: "",
        GameTypeNo: ""
      }
      dataObj.WGUID = Vue.prototype.$WGUID
      dataObj.GameTypeNo = "0" // 0.全部 1.真人视讯 2.体育 3.彩票 4.电子
      const retData = await HttpService.PostDynamic(0, 214, dataObj)

      if (retData.Ret === 0) {
        // 判断本地缓存是否与之前的相同
        if (GameData === JSON.stringify(retData.Data.GameAPIVerdorList)) {
        } else {
          localStorage.setItem("GameList", JSON.stringify(retData.Data.GameAPIVerdorList))
        }
      }
    },
    // 遍历获取游戏列表 2018.11.15 SeyoChen end

    // 檢查登入狀態
    CheckLoginStatus: async function () {
      const data = await CommonService.Comm_CheckPermission()
      switch (data.Status) {
        case BLDef.SysAccountStatus.NOT_LOGIN:
          this.IfLogin = false
          this.LocalCheckToken()
          break
        case BLDef.SysAccountStatus.LOGINED_ENABLED:
        case BLDef.SysAccountStatus.LOGINED_FROZEN:
          this.IfLogin = true
          this.LoginHeartBeat()
          this.SignalRConnect()
          this.LoginCount()
          break
        default:
          break
      }
    },

    // 開始登出計時
    LoginCount: async function () {
      const self = this
      const logoutTime = localStorage.getItem("logoutTime")
      // 時間到就登出
      if (logoutTime > new Date().getTime() || logoutTime == undefined) {
        setTimeout(function () {
          self.LoginCount()
        }, 60000)
      } else {
        // 移除登入相關資料
        localStorage.removeItem("Token")
        localStorage.removeItem("Auth")
        localStorage.removeItem("logoutTime")
        localStorage.removeItem("Acct")
        EventBus.$emit("SignalR_DisConnectServer") // 20190726 Synchro SeyoChen
        // 若沒接收到signalr斷線訊息，則3秒後自行斷線
        setTimeout(function () {
          // 斷Signalr連線
          SignalrService.DisConnectServer()
          // window.location.href = '/';
          // this.$store.dispatch('setLoginState', false);
        }, 3000)
      } // end if
    }, // end LoginCount

    // 建立SignalR連線
    SignalRConnect: function () {
      SignalrService.ConnectServer(this.SignalRConnect)
    },
    // 登入後心跳包機制
    LoginHeartBeat: function () {
      this.PostHeartBeat()
      // this.HBTimer = setInterval(this.PostHeartBeat, 1000 * 60 * 1);
    },
    // 發送心跳包到後端
    PostHeartBeat: async function () {
      async function PostAction() {
        const newTick = new Date().getTime() + 1000 * 59
        await LoginService.UpdateSurviveTime()
        localStorage.setItem("HBTick", newTick)
      }

      const HBTick = localStorage.getItem("HBTick")
      if (HBTick) {
        // 有Tick但比現在舊
        if (HBTick < new Date().getTime()) {
          PostAction()
        }
      } else {
        // 沒有心跳包
        PostAction()
      }
    },
    // 檢查本地Token
    LocalCheckToken: function () {
      this.CTTimer = setInterval(async function () {
        const currentTK = localStorage.getItem("Token")

        // 本地端有token則進到後端檢查
        if (currentTK) {
          const data = await CommonService.Comm_CheckPermission()
          if (
            data.Status == BLDef.SysAccountStatus.LOGINED_ENABLED ||
            data.Status == BLDef.SysAccountStatus.LOGINED_FROZEN
          ) {
            window.location.reload()
          } else {
            localStorage.removeItem("Token")
          }
        }
      }, 3000)
    },
    // 開啟忘記密碼視窗
    TriggerForgetPassword: function () {
      // ForgetPassword.methods.OpenWindow();
      EventBus.$emit("ForgetPasswordAlert")
    }, // end TriggerForgetPassword
    // 開啟忘記密碼訊息視窗
    TriggerForgetPasswordInformation: function () {
      // ForgetPasswordSuccess.methods.OpenWindow();
      EventBus.$emit("ForgetPasswordSuccessAlert")
    }, // end TriggerForgetPasswordInformation

    // 開啟忘記密碼訊息視窗
    TriggerForgetPasswordFail: function () {
      // ForgetPasswordFail.methods.OpenWindow();
      EventBus.$emit("ForgetPasswordFailAlert")
    } // end Trigg
  },
  beforeDestroy() {
    EventBus.$off("afterLogin")
  },
  watch: {
    themecolor: {
      handler() {
        document.getElementById("app").className = "theme" + this.themecolor
      }
    }
  }
}
