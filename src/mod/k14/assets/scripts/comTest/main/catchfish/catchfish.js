/**
 * @name 捕鱼
 * @author dingruijie
 * @time 2019-09-12
 */
// 列表内容
import catchfishContent from '@/mod/k14/components/general/catchfish/catchfishContent.vue';
export default {
    name: 'catchfish',
    components: {
        catchfishContent
    },
    data () {
        return {
            PageCount: 0,
            pageSize: 9 // // 分页每页9条
        }
    },
    mounted () {
        this.getGameList();
    },
    methods: {
        // 获取当游戏列表
        async getGameList () {
            // 无后台数据暂时使用其他模块的数据
            let allCatchfishList = [] | JSON.parse(localStorage.getItem('GameList'));
            // 调试时使用，数据不够时随机插入1-20倍数据
            // let pauseList = JSON.parse(JSON.stringify(allCatchfishList));
            // let randomNum = Math.floor((Math.random() * 20) + 1);
            // for (let i = 0; i < randomNum; i++) {
            //     allCatchfishList.push(...pauseList)
            // }
            // 判断总页数
            this.PageCount = Math.ceil(allCatchfishList.length / this.pageSize);
            // 只有一页时不需要显示换页
            this.PageCount = this.PageCount === 1 ? 0 : this.PageCount;
            // 初始计算第一页游戏条数
            this.$refs['catchfishcontent'].computeGameList(1, allCatchfishList);
        }
    }
}
