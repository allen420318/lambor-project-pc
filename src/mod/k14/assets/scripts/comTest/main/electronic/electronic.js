import electronicContent from '@/mod/k14/components/general/electronic/electronicContent.vue';

export const electronic = {
    name: 'electronic',
    components: {
        electronicContent
    }
};
