import BLDef from '@/assets/scripts/common/BLDef.js';
import CommUtility from '@/assets/scripts/common/CommUtility.js';

export const mobileApp15 = {
    mounted () {
        function load () {
            // 打开弹框
            $('.goplay').click(function () {
                $('.md-overlay').addClass('md-show');
                $('.md-modal').addClass('md-show');
            });
            // 关闭弹框
            $('.close_btn,.confirm_btn').click(function () {
                $('.md-overlay').removeClass('md-show');
                $('.md-modal').removeClass('md-show');
            });
            /*
             * 页面进入效果
             * obj：model块（object）
             * scrollTop：当前滚动条位置（int）
             * _h：显示器高度（int）
             */
            function scrollSpecial (obj, scrollTop, _h) {
                var that = obj;
                var itemList = that.find('[into_special]');
                for (var i = 0; i < itemList.length; i++) {
                    var item = $(itemList[i]);
                    var spe_h = parseInt(item.attr('into_height')); // 进入高度(int)
                    var spe_diret = item.attr('into_special'); // 进入方向（left,right）

                    // 页面隐藏
                    if (item.hasClass('onshow')) {
                        if (scrollTop < spe_h - (_h + 200) && spe_diret == 'left') {
                            item.css({
                                'left': '0',
                                'opacity': '1'
                            }).animate({
                                'left': '-100%',
                                'opacity': '0'
                            }, 100);
                            item.removeClass('onshow');
                        } else if (scrollTop < spe_h - (_h + 200) && spe_diret == 'right') {
                            item.css({
                                'right': '0',
                                'opacity': '1'
                            }).animate({
                                'right': '-100%',
                                'opacity': '0'
                            }, 100);
                            item.removeClass('onshow');
                        }
                    }
                    // 展开
                    else {
                        if (scrollTop > spe_h - _h && spe_diret == 'left') {
                            item.css({
                                'left': '-100%',
                                'opacity': '0'
                            }).animate({
                                'left': '0',
                                'opacity': '1'
                            }, 750);
                            item.addClass('onshow');
                        } else if (scrollTop > spe_h - _h && spe_diret == 'right') {
                            item.css({
                                'right': '-100%',
                                'opacity': '0'
                            }).animate({
                                'right': '0',
                                'opacity': '1'
                            }, 750);
                            item.addClass('onshow');
                        }
                    }
                }
            }
            // 初始页面效果
            scrollSpecial($('.mobile_section'), 1, 0);
            // 滚动条滑动进入效果
            $(window).scroll(function () {
                var scrollTop = $(window).scrollTop();
                var h = $(window).height();

                scrollSpecial($('.mobile_section'), scrollTop, h);
            });
        }
        load();
    },

    methods: {
        // 玩AG Html5 Game
        PlayAGHtml5Game: function () {
            const dataObj = {
                GameAPIVendor: BLDef.GameApiType.AG,
                GameCode: '',
                GameCatlog: BLDef.GameCatlogType.LiveVideonHtml5,
                PlayType: BLDef.IdentityType.FORMAL,
                Platform: BLDef.PlatformType.Web_PC
            };
            CommUtility.OpenPlayGameWindow('PlayGame', 'PlayGamePopUpWindow', dataObj);
        }, // end PlayAGHtml5Game

        // 導頁到pt html5大廳
        RedirectToPTHtml5Lobby: function () {
            CommUtility.OpenPlayGameWindow('webHtml5Lobby', 'PlayGamePopUpWindow');
        }, // end RedirectToPTHtml5Lobby

        // 玩SB Html5 Game
        PlaySBHtml5Game: function () {
            const dataObj = {
                GameAPIVendor: BLDef.GameApiType.SABA,
                GameCode: '',
                GameCatlog: BLDef.GameCatlogType.ElectronicGameHtml5,
                PlayType: BLDef.IdentityType.FORMAL,
                Platform: BLDef.PlatformType.Web_PC
            };
            CommUtility.OpenPlayGameWindow('PlayGame', 'PlayGamePopUpWindow', dataObj);
        }, // end PlaySBHtml5Game
    },
};
