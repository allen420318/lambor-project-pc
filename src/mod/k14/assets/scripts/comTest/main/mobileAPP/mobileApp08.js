	import BLDef from '@/assets/scripts/common/BLDef.js'
	import CommUtility from '@/assets/scripts/common/CommUtility.js'

	export const mobileApp08 = {
	    data () {
	        return {
	            actived: 0,
	            items: [{
	                    'tab': 'AG'
	                },
	                {
	                    'tab': 'PT'
	                },
	                {
	                    'tab': '沙巴'
	                }
	            ],
	            selectList: 0,
	        }
	    },
	    methods: {
	        // tab
	        CurrentTab: function (index) {
	            const tabContent = document.getElementById('tab_content')
	            this.actived = index
	            this.selectList = index
	        },

	        // 玩AG Html5 Game
	        PlayAGHtml5Game: function () {
	            const dataObj = {
	                GameAPIVendor: BLDef.GameApiType.AG,
	                GameCode: '',
	                GameCatlog: BLDef.GameCatlogType.LiveVideonHtml5,
	                PlayType: BLDef.IdentityType.FORMAL,
	                Platform: BLDef.PlatformType.Web_PC
	            }
	            CommUtility.OpenPlayGameWindow('PlayGame', 'PlayGamePopUpWindow', dataObj)
	        }, // end PlayAGHtml5Game

	        // 導頁到pt html5大廳
	        RedirectToPTHtml5Lobby: function () {
	            CommUtility.OpenPlayGameWindow('PTH5', 'PlayGamePopUpWindow')
	        }, // end RedirectToPTHtml5Lobby

	        // 玩SB Html5 Game
	        PlaySBHtml5Game: function () {
	            const dataObj = {
	                GameAPIVendor: BLDef.GameApiType.SABA,
	                GameCode: '',
	                GameCatlog: BLDef.GameCatlogType.ElectronicGameHtml5,
	                PlayType: BLDef.IdentityType.FORMAL,
	                Platform: BLDef.PlatformType.Web_PC
	            }
	            CommUtility.OpenPlayGameWindow('PlayGame', 'PlayGamePopUpWindow', dataObj)
	        }, // end PlaySBHtml5Game
	    },
	}
