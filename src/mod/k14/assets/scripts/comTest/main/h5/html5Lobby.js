import CommService from '@/mod/k14/assets/scripts/businessLogic/commonService.js';
import BLDef from '@/assets/scripts/common/BLDef.js';
import EventBus from '@/assets/scripts/common/EventBus.js';
import Regex from '@/assets/scripts/common/CommDef.js';
import CommUtility from '@/assets/scripts/common/CommUtility.js';
import PtHtml5LobbyHeader from '@/components/sub/ptHtml5Lobby/ptHtml5LobbyHeader.vue';
import PtHtml5LobbySearchbar from '@/mod/k14/components/sub/ptHtml5Lobby/ptHtml5LobbySearchbar.vue';
import PtHtml5LobbyContent from '@/mod/k14/components/sub/ptHtml5Lobby/ptHtml5LobbyContent.vue';
import PtHtml5LobbyPaging from '@/mod/k14/components/sub/paginate.vue';

export const html5By = {
    data () {
        return {
            // 分頁頁數
            PageInfo: {
                PageCount: 0,
            },

            // 載入畫面資料
            LoadMainPageDataModel: {

            },
            // 正則表達式類別
            Reg: Regex.Regex,
            // 遊戲搜尋關鍵字
            Keyword: undefined,
            // 當前頁數
            CurrentPageData: '',
            SearchPage: undefined,
            PTHtml5GameList: undefined,
        };
    },

    created: function () {
        this.RootGetLoginStatus();
    },
    mounted () {
        $('.H5-navs li').click(function () {
            $(this).addClass('on').siblings().removeClass('on');
        });
        $('.H5-lists li').click(function () {
            $(this).addClass('on').siblings().removeClass('on');
        });
        CommUtility.BindEnterTrigger('H5SearchForm', this.GetPTHtml5GameList);
    },
    methods: {

        // 取得分頁當前頁數
        GetCurrentPageData: function (page) {
            this.CurrentPageData = page;
        }, // end GetCurrentPageData

        // 取得登入狀態
        RootGetLoginStatus: async function () {
            const retData = await CommService.Comm_CheckPermission();
            if (retData.Status != BLDef.SysAccountStatus.LOGINED_ENABLED) {
                const notifyData = {
                    NotifyMessage: (BLDef.SysAccountStatus.NOT_LOGIN == retData.Status || BLDef.SysAccountStatus.NOT_LOGIN_DISABLED == retData.Status || BLDef.SysAccountStatus.NOT_LOGIN_DELETED == retData.Status) ? this.$t('message.top_login_account') : this.$t('message.top_account_freezing'),
                    NotifySubMessage: undefined,
                    CloseFunction: function () {
                        window.close();
                    },
                };
                EventBus.$emit('showNotifyMessage', notifyData);
            }
        }, // end RootGetLoginStatus
        // 根據條件篩選遊戲資料
        GetPTHtml5GameList: async function (pageNo) {
            if (pageNo === undefined) {
                pageNo = 1;
            }

            // 進行關鍵字欄位驗證
            if (this.$validator.errors.has('ReqKeyWord')) {
                return;
            } // end if
            this.PageInfo.PageNo = pageNo;
            this.$parent.SearchPage = pageNo;

            const dataObj = {
                GameAPIVendor: BLDef.GameApiType.PT,
                Keyword: this.Keyword,
                Filter: {
                    Value: '-1',
                },
                PagingInfo: this.PageInfo,
            };
            this.GameFilter.Value = undefined;
            EventBus.$emit('GlobalLoadingTrigger', true);
            const retData = await GameService.GameMobileBet_Query(dataObj);
            EventBus.$emit('GlobalLoadingTrigger', false);

            // 成功
            if (retData.Ret == 0) {
                this.$parent.PTHtml5GameList = retData.Data.GameList;

                // 更新Paging
                if (retData.Data.PagingInfo) {
                    this.PageInfo.PageCount = Math.ceil(retData.Data.PagingInfo.TotalCount / retData.Data.PagingInfo.PageSize);
                    this.$parent.PageInfo.PageCount = this.PageInfo.PageCount;
                } else {
                    this.PageInfo.PageCount = 0;
                } // end if
            }
        }, // end GetPTHtml5GameList

    },

    components: {
        PtHtml5LobbyHeader,
        PtHtml5LobbySearchbar,
        PtHtml5LobbyContent,
        PtHtml5LobbyPaging,
    },
}
