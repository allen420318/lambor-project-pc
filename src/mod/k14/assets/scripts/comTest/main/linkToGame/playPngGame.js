// import BLDef from '@/assets/scripts/common/BLDef.js';
import Vue from 'vue';
// import URLService from '@/assets/scripts/common/URLService.js';
import CommonService from '@/mod/k14/assets/scripts/businessLogic/commonService.js';
// import GameService from '@/mod/k14/assets/scripts/businessLogic/gameService.js';
import EventBus from '@/assets/scripts/common/EventBus.js';

export const playPe = {
    data() {
        return {
            GamePlayUrl: undefined,
            PlayGameData: {},

            UrlData: {}
        };
    },
    created: function () {
        this.GamePlayUrl = JSON.parse(localStorage.getItem('PlayUrl'));
    },
    mounted: function () {
        this.PageInit();
        this.PlayGame();
        // this.GetParams();
    },
    methods: {
        // 初始化
        PageInit: function () {
            window.history.forward(1);
            $(window).resize(function () {
                $('#Iframe_Game').css({
                    height: window.innerHeight + 'px'
                });
            });
        },
        // 獲取URL參數
        // GetParams: async function () {
        //     if (localStorage.getItem('PlayGamePostData') != undefined) {
        //         this.PlayGameData = URLService.GetObjFromUrlParameter(localStorage.getItem('PlayGamePostData'));
        //         localStorage.removeItem('PlayGamePostData');
        //         if (this.PlayGameData.PlayType != BLDef.IdentityType.TRIAL) {
        //             // 檢查權限:凍結不可進行遊戲
        //             const permissionData = await this.CheckAcctStatus();
        //             if (permissionData.Status != BLDef.SysAccountStatus.LOGINED_ENABLED) {
        //                 const notifyData = {
        //                     NotifyMessage: (BLDef.SysAccountStatus.NOT_LOGIN == permissionData.Status || BLDef.SysAccountStatus.NOT_LOGIN_DISABLED == permissionData.Status || BLDef.SysAccountStatus.NOT_LOGIN_DELETED == permissionData.Status) ? '请登录账号' : '账户被冻结, 请联系客服人员!',
        //                     CloseFunction: this.CloseCurrentWindow
        //                 };
        //                 EventBus.$emit('showNotifyMessage', notifyData);
        //                 return;
        //             }
        //             this.PlayGame();
        //         } else {
        //             this.CheckAuthCode(this.PlayGameData.VCodeObj);
        //         }
        //     } else {
        //         this.CloseCurrentWindow();
        //     }
        // },
        // 檢查登入狀態
        CheckAcctStatus: async function () {
            const retData = await CommonService.Comm_CheckPermission();
            return retData;
        },
        // 檢查驗證碼
        CheckAuthCode: async function (VCodeObj) {
            const inputObj = VCodeObj;
            const retData = await CommonService.VerifyCode_Verify(inputObj);
            if (retData.Ret != 0) {
                const notifyData = {
                    NotifyMessage: retData.Message,
                    CloseFunction: this.CloseCurrentWindow
                };
                EventBus.$emit('showNotifyMessage', notifyData);
            } else {
                this.PlayGame();
            }
        },
        // 進行遊戲
        PlayGame: async function () {
            // const token = this.PlayGameData.PlayType == BLDef.IdentityType.TRIAL ? this.PlayGameData.CToken : undefined;
            // const BaseUrl = window.location.href.split('/').slice(0, 3).join('/');
            // const inputObj = {
            //     WGUID: Vue.prototype.$WGUID,
            //     GameAPIVendor: this.PlayGameData.GameAPIVendor,
            //     GameCatlog: this.PlayGameData.GameCatlog,
            //     GameCode: this.PlayGameData.GameCode,
            //     PlatformType: BLDef.PlatformType.Web_PC,
            //     PlayType: this.PlayGameData.PlayType,
            //     Url: BaseUrl
            // };

            // EventBus.$emit('GlobalLoadingTrigger', true);
            // const gameData = await GameService.Game_Player(inputObj, token);
            // EventBus.$emit('GlobalLoadingTrigger', false);

            // // 请求频繁，后端返回限制进入游戏  2019.04.02
            // if (gameData.Ret == 22) {
            //     const notifyData = {
            //         NotifyMessage: '请求过于频繁，请30秒后重试',
            //         CloseFunction: this.CloseCurrentWindow
            //     }
            //     EventBus.$emit('showNotifyMessage', notifyData);
            //     return;
            // }
            // // 请求频繁，后端返回限制进入游戏  2019.04.02 end

            // if (gameData.Ret != 0) {
            //     const notifyData = {
            //         // NotifyMessage: gameData.Message,
            //         NotifyMessage: '线路维护中',
            //         CloseFunction: this.CloseCurrentWindow
            //     };
            //     EventBus.$emit('showNotifyMessage', notifyData);
            //     return;
            // }

            // this.GamePlayUrl = gameData.Data.GamePlayUrl;
            this.UrlData = this.GamePlayUrl.split('&&&');

            const scriptUrl = '<script src=\'' + this.UrlData[0] + '&width=1500px&height=800px\' type=\'text/javascript\'><script type=\'text/javascript\'> function PlayForReal() { window.location = \'' + this.UrlData[1] + '\'; } function Logout() { window.close(); } function reloadgame(gameId, user) { }';
            // const scriptUrl = '<script src="https://csistage.playngonetwork.com/Casino/js?div=pngCasinoGame&lang=en_GB&pid=459&practice=1&gameId=199&username=15-283D602D846J294P&gt;?div=pngCasinoGame&gid=firejoker&lang=en_GB&pid=1&username=123-ahxa94-az6134d&practice=1&width=1500px&height=800px" type="text/javascript"><script type=\'text/javascript\'>function PlayForReal() {window.location = \'https://csistage.playngonetwork.com/Casino/GameLoader?pid=459&practice=1&gameId=199&username=15-283D602D846J294P\';}function Logout() {window.close();}function reloadgame(gameId, user) {}';
            $('head').append(scriptUrl);

            console.log(scriptUrl);
            // location.href = this.UrlData[1];
        },
        // 關閉視窗
        CloseCurrentWindow: function () {
            window.close();
        }
    },
};
