import 'bootstrap'
import BLDef from '@/assets/scripts/common/BLDef.js'
import Vue from 'vue'
import CommonService from '@/mod/k14/assets/scripts/businessLogic/commonService.js'

export const indexActivity = {
    data() {
        return {
            indexActivityObjList: [],
            LoadCompleted: false,
            DefaultActivityList: [],
            ActivityList: [],
            SlideFlag: undefined,
            currentSlideIndex: 0,

            // 分頁資料
            PageInfo: {
                PageNo: 1,
                PageSize: 9,
                PageCount: 0,
                TotalCount: 0
            }
        };
    },
    created: function () {
        this.SetDefaultActivityList();
    },
    methods: {
        // 設定預設優惠
        SetDefaultActivityList: function () {
            this.DefaultActivityList = [];
            // 優惠活動-首存
            //   this.DefaultActivityList.push({
            //     ID: '-1',
            //     Name: '首存返利',
            //     StartTick: new Date().getTime(),
            //     EndTick: new Date().getTime(),
            //     ActLogo:
            //       this.$ResourceCDN +
            //       '/EditionImg/Lambor1.0/images/web/promotion/activity_img1.jpg?VersionCode'
            //   });
            //   // 優惠活動-續存
            //   this.DefaultActivityList.push({
            //     ID: '-2',
            //     Name: '续存返利',
            //     StartTick: new Date().getTime(),
            //     EndTick: new Date().getTime(),
            //     ActLogo:
            //       this.$ResourceCDN +
            //       '/EditionImg/Lambor1.0/images/web/promotion/activity_img2.jpg?VersionCode'
            //   });

            this.GetEasyActivity();
        },
        // 取得輪播的優惠活動
        GetEasyActivity: async function () {
            // 2018.09.20 SeyoChen
            const retDataLv = await CommonService.Comm_CheckPermission();
            if (retDataLv.Status != 0) {
                const dataObj = {
                    WGUID: '',
                    LoginID: '',
                    PagingInfo: this.PageInfo
                };
                dataObj.WGUID = Vue.prototype.$WGUID;
                dataObj.LoginID = localStorage.getItem('Acct');
                const data = await CommonService.Comm_GetBonusActivity_MemberLv(
                    dataObj
                );
                if (data.Ret == 0) {
                    this.indexActivityObjList = data.Data.ActivityList.slice(0, 3); // 优惠活动页面 banner 跟随等级更新 2019.04.12 SeyoChen
                    console.log(JSON.stringify(this.indexActivityObjList));
                    this.DealUpdatedData();
                }
            } else {
                const inputObj = {
                    WGUID: Vue.prototype.$WGUID
                };
                const data = await CommonService.Comm_GetBonusActivity(inputObj);
                if (data.Ret == 0) {
                    this.indexActivityObjList = data.Data.ActivityList;
                    this.DealUpdatedData();
                }
            }
            // this.$parent.ActivityList = this.LoadMainPageDataModel.ActivityList;
            // 2018.09.20 SeyoChen end
        },
        // 設定輪播活動內容邏輯
        DealUpdatedData: function () {
            if (this.indexActivityObjList.length === 0) {
                this.LoadCompleted = false;
                // this.ActivityList = this.DefaultActivityList;
                return;
            } else if (this.indexActivityObjList.length === 1) {
                this.LoadCompleted = true;
                this.ActivityList = this.indexActivityObjList;
                return;
            } else {
                this.LoadCompleted = true;
                this.ActivityList = this.indexActivityObjList;
            }
            console.log(this.ActivityList);
            setTimeout(
                function () {
                    this.SildePromotionList();
                }.bind(this),
                100
            );
        },
        // 啟用活動輪播
        SildePromotionList: function () {
            this.AutoPlay();
            $('#myCarousel').on('slide.bs.carousel', this.SlideEvent);
        },
        // 指定輪播到特定index
        SildeTo: function (item) {
            $('#myCarousel').carousel(item);
            $('#myCarousel').carousel('pause');
        },
        // 輪播事件偵測
        SlideEvent: function (event) {
            this.currentSlideIndex = $(event.relatedTarget).index();
        },
        // 進行自動播放
        AutoPlay: function () {
            $('#myCarousel').carousel('cycle');
            console.log(this.ActivityList);
        }
    }
};
