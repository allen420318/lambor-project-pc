// import AthleticsFirm from '@/components/sub/athletics/athleticsFirm.vue';
// import AthleticsTable from '@/components/sub/athletics/athleticsTable.vue';
// import page from '@/mod/k14/components/sub/paginate.vue';
import Swiper from 'swiper';
import BLDef from '@/assets/scripts/common/BLDef.js';
import CommUtility from '@/assets/scripts/common/CommUtility.js';
import EventBus from '@/assets/scripts/common/EventBus.js';
import URLService from '@/assets/scripts/common/URLService.js';
import HttpService from '@/assets/scripts/common/HttpService.js';
import Vue from 'vue';
// 列表内容
import athleticsContent from '@/components/general/athletics/athleticsContent.vue';
export default {
    components: {
        athleticsContent
    },
    data() {
        return {
            PageInfo: {
                PageCount: 0
            },
            LoadMainPageDataModel: {},
            CurrentPageData: '',
            SearchPage: undefined,
            GameNum: null,
            GameData: {
                GameAPIVendor: undefined,
                GameCode: '',
                GameCatlog: BLDef.GameCatlogType.LiveVideo,
                PlayType: BLDef.IdentityType.FORMAL,
                Platform: BLDef.PlatformType.Web_PC
            },
            GameList: [],
            athleticList: [], // 电子竞技游戏数据列 20190921 SeyoChen
            WattingGame: false // 判断是否有游戏列表 20190917 SeyoChen
        };
    },
    created: function () {
        this.GatGame();
     
    },
    methods: {
        // 遍历获取游戏列表
        GatGame: async function () {
            this.GameList = JSON.parse(localStorage.getItem('GameList'));

            const dataObj = {
                WGUID: '',
                GameTypeNo: ''
            };
            dataObj.WGUID = Vue.prototype.$WGUID;
            dataObj.GameTypeNo = '0'; // 0.全部 1.真人视讯 2.体育 3.彩票 4.电子
            const retData = await HttpService.PostDynamic(0, 214, dataObj);

            if (
                retData.Ret === 0 &&
                this.GameList !== retData.Data.GameAPIVerdorList
            ) {
                // 判断本地缓存是否与之前的相同
                this.GameList = retData.Data.GameAPIVerdorList;
                localStorage.setItem(
                    'GameList',
                    JSON.stringify(retData.Data.GameAPIVerdorList)
                );
                // this.getAthleticList();
            }

            for (let i = 0; i < this.GameList.length; i++) {
                // 2019.01.12 筛选游戏 并 判定状态 SeyoChen 启动中：702 维护中：703 关闭：704
                if (
                    this.GameList[i].GameTypeNo === 6 &&
                    this.GameList[i].GameTypeState !== 704
                ) {
                    this.athleticList.push(this.GameList[i]);
                }
            }

            // 判断是否有游戏列表 20190917 SeyoChen
            if (this.athleticList.length === 0) {
                this.WattingGame = true;
            }
            this.$nextTick(() => {
                var swiper = new Swiper('.swiper-container', {
                    mousewheelControl: true,
                    direction: 'horizontal', //
                    mousewheelForceToAxis: true,
                    pagination: {
                        el: '.swiper-pagination',
                        clickable: true
                    },
                    autoplay: 5000, // 可选选项，自动滑动
                    paginationClickable: true
                });
            });
        },
        GetCurrentPageData: function (page) {
            this.CurrentPageData = page;
        },
        setGameNum: function (numb) {
            this.GameNum = numb;
        },
        athleticsEnterGame: async function (GameTypeNo, GameApiNo, GameCode) {
            // const GetNavLists = JSON.parse(localStorage.getItem('GameList'));
            for (let i = 0; i < this.GameList.length; i++) {
                if (
                    this.GameList[i].GameApiNo == GameApiNo &&
                    this.GameList[i].GameTypeNo == GameTypeNo
                ) {
                    this.PlayGame(GameTypeNo, GameApiNo, GameCode);
                }
            }
        },
        // 開新視窗
        PlayGame: function (GameTypeNo, GameApiNo, GameCode) {
            console.log(GameApiNo)
            this.GameData.GameAPIVendor = GameApiNo;
            this.GameData.GameCode = GameCode;
            this.GameData.GameCatlog = GameTypeNo;
            localStorage.setItem('PlayGamePostData', URLService.GetUrlParameterFromObj(this.GameData));

            // EventBus.$emit('showTransferMessage', this.GameData);
            CommUtility.OpenPlayGameWindow('webPlayGame', 'PlayGamePopUpWindow');
        }, // end PlayGame
    },
    watch: {
        GameNum: function (numb) {
            this.GameNum = numb;
        }
    }
};
