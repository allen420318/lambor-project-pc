/**
 * @name 电子游戏内容组件
 * @author dingruijie
 * @time 2019-09-17
 * @augments PageCount 计算后的分页总数 类型：Number 默认0
 * @augments pageSize 分页每页条数 类型: Number 默认每页10条
 * @event 父组件请求完数据 初始化分页数据 this.$refs[refname].computeGameList(1，allList) 初始传1表示渲染第一页
 */

// 分页组件
import electronicPaginate from '@/mod/k14/components/sub/paginate.vue';
import GameFirm from '@/mod/k14/components/general/electronic/gameFirm.vue';
import GameTable from '@/mod/k14/components/general/electronic/gameTable.vue';
export default {
    name: 'electronic-content',
    components: {
        electronicPaginate,
        GameFirm,
        GameTable
    },
    data () {
        return {
            PageInfo: {
                PageCount: 0,
            },
            LoadMainPageDataModel: {

            },
            CurrentPageData: '',
            SearchPage: undefined,
            GameNum: null
        };
    },
    methods: {
        GetCurrentPageData (page) {
            this.CurrentPageData = page;
        },
        setGameNum (numb) {
            this.GameNum = numb;
        },
        // 加载完成触发
        loadStatus(status) {
          this.$refs.gameTable.loadStatus(status)
        }
    },
    watch: {
        GameNum (numb) {
            this.GameNum = numb;
        }
    }
};
