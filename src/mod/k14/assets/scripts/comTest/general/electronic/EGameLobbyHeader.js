import BLDef from '@/assets/scripts/common/BLDef.js';
import Vue from 'vue';
import CommUtility from '@/assets/scripts/common/CommUtility.js';
import GameService from '@/mod/k14/assets/scripts/businessLogic/gameService.js';
import HttpService from '@/assets/scripts/common/HttpService.js';
import Regex from '@/assets/scripts/common/CommDef.js';
import EventBus from '@/assets/scripts/common/EventBus.js';

export const EGameLobbyHeader = {
    props: {
        GetCurrentPageData: ''
        // GetGameApiNo: '',
    },
    data () {
        return {
            // 頁面DataModel
            LoadMainPageDataModel: {
                GameTypeFilterList: {}
            },

            // EGameList 2018.11.15 SeyoChen 遍历游戏
            GameList: [],
            EgameList: [],

            // 查詢 Post DataModel
            PostDataModel: {
                GameAPIVendor: {
                    Value: ''
                },
                GameTypeFilter: {
                    Value: '-1'
                },
                SlotLineFilter: {
                    Value: '-1'
                },
                Keyword: ''
            },

            // 分頁資料
            PageInfo: {
                PageNo: 1,
                PageSize: 18,
                PageCount: 0
            },

            Reg: Regex.Regex,

            GameNum: 0,
            GameApiName: '',
            currentPage: 0
        };
    },

    created: function () {
        this.GatGame(); // 2018.11.15 SeyoChen 遍历游戏
        // this.GetGameApiType();
        // this.BindGameTypeEvt(1);  // 载入第一页
        // this.LoadMainPage();
    },
    mounted: function () {
        CommUtility.BindEnterTrigger('ElectronicGameForm', this.Search);

        // 电子游戏 游戏图标的切换
        // // const that = this; // 2018.12.04
        // $('.ele-vendor-lists').on('click', '.ele-vendor-li ', function () {
        //     $(this).addClass('on').siblings('.ele-vendor-li').removeClass('on');
        // const logoUrl = $(this).children().children('img').attr('src');
        // const index = ($(this).index()) + 1; // 2018.12.04
        // that.GameNum = index; // 2018.12.04
        // $('.logo-show').children('img').attr('src', logoUrl);
        // });
        // this.$emit('getGameNum', this.GameNum);
    },
    methods: {
        // 遍历获取游戏列表
        GatGame: async function () {
            this.GameList = JSON.parse(localStorage.getItem('GameList'));

            const dataObj = {
                WGUID: '',
                GameTypeNo: '',
                PlatformType: BLDef.PlatformType.Web_PC
            };
            dataObj.WGUID = Vue.prototype.$WGUID;
            dataObj.GameTypeNo = '0'; // 0.全部 1.真人视讯 2.体育 3.彩票 4.电子
            const retData = await HttpService.PostDynamic(0, 214, dataObj);

            if (
                retData.Ret === 0 &&
                this.GameList !== retData.Data.GameAPIVerdorList
            ) {
                // 判断本地缓存是否与之前的相同
                this.GameList = retData.Data.GameAPIVerdorList;
                localStorage.setItem(
                    'GameList',
                    JSON.stringify(retData.Data.GameAPIVerdorList)
                );
            }

            for (let i = 0; i < this.GameList.length; i++) {
                // 2为 欧博游戏 2019.01.12 筛选游戏 并 判定状态 SeyoChen
                if (
                    this.GameList[i].GameApiNo !== '2' &&
                    this.GameList[i].GameTypeNo === 4 &&
                    this.GameList[i].GameTypeState !== '704'
                ) {
                    this.EgameList.push(this.GameList[i]);
                }
            }
            // 插入box之前清除原先已有box避免重复
            $('.ele-vendor-expect').remove();
            setTimeout(() => {
                this.AjectAddBox(this.EgameList.length);
            }, 50);
            // 默认显示第几页，如果第一页维护中，默认选中页按顺序往下取
            for (let i = 0; i < this.EgameList.length; i++) {
                if (this.EgameList[i].GameTypeState !== '703') {
                    this.BindGameTypeEvt(
                        this.EgameList[i].GameApiNo,
                        this.EgameList[i].GameApiName,
                        i
                    ); // 载入第一页 this.EgameList[0].ApiStatus
                    return;
                }
            }
            // this.GetGameApiNo = this.EgameList[0].GameApiNo;

            // GameTypeState == '703'
        },

        // 电子游戏点击事件
        // GetGameApiType: function (list) {
        //     this.PostDataModel.GameAPIVendor.Value = list;
        // },
        // 获取游戏列表 2018.11.15 SeyoChen end

        clearLoading () {
            EventBus.$emit('GlobalLoadingTrigger', false);
        },
        // 动态识别添加游戏模块
        AjectAddBox (num) {
            const boxSize = parseInt(6); // 页面中一行的box个数
            const boxHtml = `<li class="ele-vendor-li ele-vendor-expect">
            <div><span>COMMING SOON</span><p>${this.$t('message.live_expect')}</p></div></li>`;
            const box = $('.ele-vendor-lists');
            if (num % boxSize === 0 && num !== 0) {} else {
                for (let i = 0; i < Math.abs(boxSize - (num % boxSize)); i++) {
                    box.append(boxHtml);
                }
            }
        },
        BindGameTypeEvt: function (gameApiType, GameApiName, index) {
            this.currentPage = index;
            this.GameApiName = GameApiName;
            this.PostDataModel.GameAPIVendor.Value = gameApiType;
            this.PostDataModel.GameTypeFilter.Value = -1; // 將過濾器設置回全部
            this.clearLoading();
            // this.GetElectronicGameList(1);

            // 2019.01.12 电子游戏详细游戏显示维护 SeyoChen
            // if (ApiStatus === 703) {
            //     $('.electronic-maintain').show();
            // } else {
            //     $('.electronic-maintain').hide();
            // }
            // 2019.01.12 电子游戏详细游戏显示维护 SeyoChen end

            // this.$emit('getGameNum', gameApiType);
            this.GameNum = gameApiType; // 2018.12.04 SeyoChen 图片类型对比，传递对应API编号
            this.LoadMainPage();
            // this.GetElectronicGameList(1);
            // this.GetElectronicGameList();
        },

        // 獲得GameApi
        GetGameApiType: function () {
            let typeSet = {
                num1: 'AG',
                num3: 'PT',
                num7: 'QT',
                num8: 'DT',
                num9: 'SG',
                num14: 'MG',
                num15: 'BBIN',
                num16: 'TTG',
                num17: 'PG',
                num18: 'WG',
                num20: 'RT',
                num21: 'PS',
                num30: 'BP',
                num31: 'PNG1',
                num23: 'KingLong',
                num37: 'RTG1',
                num39: 'TopTG'
            };
            this.PostDataModel.GameAPIVendor.Value =
                BLDef.GameApiType[typeSet['num' + this.GameNum]];
            // switch (this.GameNum) {
            //     case '1':
            //         this.PostDataModel.GameAPIVendor.Value = BLDef.GameApiType.AG;
            //         break;
            //     case '3':
            //         this.PostDataModel.GameAPIVendor.Value = BLDef.GameApiType.PT;
            //         break;
            //     case '7':
            //         this.PostDataModel.GameAPIVendor.Value = BLDef.GameApiType.QT;
            //         break;
            //     case '8':
            //         this.PostDataModel.GameAPIVendor.Value = BLDef.GameApiType.DT;
            //         break;
            //     case '9':
            //         this.PostDataModel.GameAPIVendor.Value = BLDef.GameApiType.SG;
            //         break;
            //     case '14':
            //         this.PostDataModel.GameAPIVendor.Value = BLDef.GameApiType.MG;
            //         break;
            //     case '15':
            //         this.PostDataModel.GameAPIVendor.Value = BLDef.GameApiType.BBIN;
            //         break;
            //     case '16':
            //         this.PostDataModel.GameAPIVendor.Value = BLDef.GameApiType.TTG;
            //         break;
            //     case '17':
            //         this.PostDataModel.GameAPIVendor.Value = BLDef.GameApiType.PG;
            //         break;
            //     case '18':
            //         this.PostDataModel.GameAPIVendor.Value = BLDef.GameApiType.WG;
            //         break;
            //     case '20':
            //         this.PostDataModel.GameAPIVendor.Value = BLDef.GameApiType.RT;
            //         break;
            //     case '21':
            //         this.PostDataModel.GameAPIVendor.Value = BLDef.GameApiType.PS;
            //         break;
            //     case '22':
            //         this.PostDataModel.GameAPIVendor.Value = BLDef.GameApiType.PNG;
            //         break;
            //     case '30':
            //         this.PostDataModel.GameAPIVendor.Value = BLDef.GameApiType.BP;
            //         break;
            //     case '31':
            //         this.PostDataModel.GameAPIVendor.Value = BLDef.GameApiType.PNG1;
            //         break;
            //     case '33':
            //         this.PostDataModel.GameAPIVendor.Value = BLDef.GameApiType.KingLong;
            //         break;
            //     case '37':
            //         this.PostDataModel.GameAPIVendor.Value = BLDef.GameApiType.RTG1;
            //         break;
            //     case '39':
            //         this.PostDataModel.GameAPIVendor.Value = BLDef.GameApiType.TopTG;
            //         break;
            //     default:
            //         break;
            // } // end switch
        }, // end GetGameApiType

        // 載入頁面
        LoadMainPage: async function (pageNo) {
            // this.LoadMainPageDataModel = {
            //     GameTypeFilterList: {}
            // };

            // const dataObj = {
            //     GameAPIVendor: this.PostDataModel.GameAPIVendor
            // };
            // const retData = await GameService.LoadMainPage(dataObj);

            // if (retData.Ret != 0) {
            //     return;
            // }

            // this.LoadMainPageDataModel = retData.Data;
            this.GetElectronicGameList(pageNo);
        }, // end LoadMainPage

        // 取得電子遊戲List
        GetElectronicGameList: async function (pageNo) {
            let PageNo = pageNo || 1;
            if (!pageNo) {
                this.$emit('loadStatus', 'init');
            }
            // 更新Paging
            this.PageInfo.PageNo = pageNo;
            this.$parent.SearchPage = pageNo;
            EventBus.$emit('Paginate_ResetPageNo', pageNo);
            // 獲得頁面資料
            const loadDataObj = {
                GameAPIVendor: this.PostDataModel.GameAPIVendor
            };

            const tempRetData = await GameService.LoadMainPage(loadDataObj);

            if (tempRetData.Ret !== 0) {
                return;
            }
            this.LoadMainPageDataModel.GameTypeFilterList = tempRetData.Data.GameTypeFilterList;

            const dataObj = {
                WGUID: Vue.prototype.$WGUID,
                GameAPIVendor: this.PostDataModel.GameAPIVendor, // 遊戲供應商
                GameTypeFilter: this.PostDataModel.GameTypeFilter, // 遊戲類型過濾條件
                Keyword: '', // 關鍵字
                PagingInfo: this.PageInfo,
                PlatformType: 1 // 2019.01.28 SeyoChen 添加识别PC端
            };
            // EventBus.$emit('GlobalLoadingTrigger', true);
            const retData = await GameService.GameQuery(dataObj);
            // EventBus.$emit('GlobalLoadingTrigger', false);
            // 成功
            // this.LoadMainPageDataModel.GameList = retData.Data.GameList;
            let LoadMainPageDataModelSet = {}
            LoadMainPageDataModelSet.GameTypeFilterList = this.LoadMainPageDataModel.GameTypeFilterList;
            if (!pageNo && this.$parent.CurrentPageData > 1) {
                this.$parent.CurrentPageData = 1
            } else {
                if (this.$parent.LoadMainPageDataModel.GameList && pageNo > 1) {
                    this.$parent.LoadMainPageDataModel.GameList.push(...retData.Data.GameList)
                    LoadMainPageDataModelSet.GameList = this.$parent.LoadMainPageDataModel.GameList
                    this.$set(this.$parent, 'LoadMainPageDataModel', LoadMainPageDataModelSet)
                } else {
                    LoadMainPageDataModelSet.GameList = retData.Data.GameList
                    this.$set(this.$parent, 'LoadMainPageDataModel', LoadMainPageDataModelSet)
                }
            }

            // 加载完成
            this.$emit('loadStatus', 'loading');
            this.PostDataModel.Keyword = '';
            // 更新Paging
            if (retData.Data.PagingInfo) {
                this.PageInfo.PageCount = Math.ceil(
                    retData.Data.PagingInfo.TotalCount /
                    retData.Data.PagingInfo.PageSize
                );
                this.$parent.PageInfo.PageCount = this.PageInfo.PageCount;
            } else {
                this.PageInfo.PageCount = 0;
            } // end if
        }, // end GetElectronicGameList

        // 搜尋功能
        Search: async function () {
            const flag = await this.validate();

            if (flag === false) {
                return;
            } // end if

            // 初始在第一頁
            this.PageInfo.PageNo = 1;
            this.$parent.SearchPage = 1;

            // 獲得頁面資料
            const loadDataObj = {
                GameAPIVendor: this.PostDataModel.GameAPIVendor
            };

            const tempRetData = await GameService.LoadMainPage(loadDataObj);

            if (tempRetData.Ret !== 0) {
                return;
            }
            this.LoadMainPageDataModel = tempRetData.Data;

            // 將過濾器設置回全部
            this.PostDataModel.GameTypeFilter.Value = -1;

            const dataObj = {
                WGUID: Vue.prototype.$WGUID,
                GameAPIVendor: this.PostDataModel.GameAPIVendor, // 遊戲供應商
                GameTypeFilter: this.PostDataModel.GameTypeFilter, // 遊戲類型過濾條件
                Keyword: this.PostDataModel.Keyword === undefined
                    ? ''
                    : this.PostDataModel.Keyword, // 關鍵字
                PagingInfo: this.PageInfo
            };
            EventBus.$emit('GlobalLoadingTrigger', true);
            const retData = await GameService.GameQuery(dataObj);
            EventBus.$emit('GlobalLoadingTrigger', false);
            // 成功
            this.LoadMainPageDataModel.GameList = retData.Data.GameList;
            this.$parent.LoadMainPageDataModel = this.LoadMainPageDataModel;

            // 更新Paging
            if (retData.Data.PagingInfo) {
                this.PageInfo.PageCount = Math.ceil(
                    retData.Data.PagingInfo.TotalCount /
                    retData.Data.PagingInfo.PageSize
                );
                this.$parent.PageInfo.PageCount = this.PageInfo.PageCount;
            } else {
                this.PageInfo.PageCount = 0;
            } // end if
        }, // end Search

        // 所有欄位驗證
        validate: function () {
            Object.keys(this.fields).forEach(key => {
                this.fields[key].touched = true;
            });
            return this.$validator.validateAll().then(result => {
                return result;
            });
        } // end validate
    },

    watch: {
        GetCurrentPageData: function (value) {
            this.GetElectronicGameList(this.GetCurrentPageData);
        },
        GameNum: function (numb) {
            this.$emit('getGameNum', numb);
        }
    }
};
