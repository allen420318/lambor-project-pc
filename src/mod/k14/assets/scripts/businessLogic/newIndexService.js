import HttpService from '@/assets/scripts/common/HttpService.js';
import BLDef from '@/assets/scripts/common/BLDef.js';

export default {
    // 盈利榜
    async HomePageLoadRankAndOnlineNumPage (DataObj) {
        const retData = await HttpService.PostDynamic(BLDef.NewIndexService.ServiceType, BLDef.NewIndexService.ActType.HomePage_LoadRankAndOnlineNum, DataObj);
        return retData;
    },

};
