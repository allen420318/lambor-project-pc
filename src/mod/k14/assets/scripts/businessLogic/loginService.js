import HttpService from '@/assets/scripts/common/HttpService.js'
import BLDef from '@/assets/scripts/common/BLDef.js'

export default {
    // 取得PreToken
    async LoadPage() {
        const retData = await HttpService.PostDynamic(BLDef.LoginService.ServiceType, BLDef.LoginService.ActType.LoadPage)
        return retData.Data.LoginData.Token
    },

    // 取得圖形驗證碼
    async ReGenVCode(preToken) {
        console.log('33333333333333' + preToken)
        const retData = await HttpService.PostDynamic(BLDef.LoginService.ServiceType, BLDef.LoginService.ActType.RegenVCode, null, preToken)
        return retData.Data.LoginData.VCodeImg
    },

    // 登入
    async LoginConfirm(preToken, dataObj) {
        const retData = await HttpService.PostDynamic(BLDef.LoginService.ServiceType, BLDef.LoginService.ActType.LoginConfirm, dataObj, preToken)
        return retData
    },
    // 登出
    async LogOut(dataOb) {
        const retData = await HttpService.PostAes(BLDef.LoginService.ServiceType, BLDef.LoginService.ActType.LogOut, dataOb)
        return retData
    },
    // 心跳包更新存活時間
    async UpdateSurviveTime() {
        const retData = await HttpService.PostAes(BLDef.LoginService.ServiceType, BLDef.LoginService.ActType.UpdateSurviveTime, null, true)
        return retData
    },
    // 忘記密碼
    async ForgetPassword(dataOb) {
        const retData = await HttpService.PostDynamic(BLDef.LoginService.ServiceType, BLDef.LoginService.ActType.ForgetLoginPassword, dataOb)
        return retData
    }
}
