const modMessages = {
  member_number_blank: '長度限制3-60',
  reg_account_registration: '帳號注册',
  agency_new_registration: '新注册',
  member_canal: '渠道',
  agency_winning_percentage: '中獎金額+占成+碼傭+紅利活動',
  agency_dividend_betting: '+紅利派發+返水-有效投注',
  abouts_safe_enough: '平臺足够安全',
  abouts_fair_play: '公平遊戲',
  abouts_brief_introduction: '簡介',

}

export default modMessages
