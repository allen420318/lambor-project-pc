const modMessages = {
  member_activity_name: 'Tên khuyến mãi:',
  member_activity_time: 'Thời gian khuyến mãi:',
  member_extra_dividend: 'Tỷ lệ phần trăm Lợi nhuận:',
  member_minimum_income: 'Tiền gửi tối thiểu:',
  member_maximum_dividend: 'Phần thưởng cao nhất:',
  member_document_rate: 'Ghi đè tốc độ giặt :',
  member_number_blank: 'Chiều dài của những hạn chế 3-60',
  member_sumbitAccount_number: 'Xác nhận tài khoản ngân hàng:',
  agency_winning_percentage: 'Số thắng lợi Phần trăm +Mã Ủy ban +các hoạt động thưởng',
  agency_dividend_betting: 'Trình phân chia Hàm lượng',

}

export default modMessages
