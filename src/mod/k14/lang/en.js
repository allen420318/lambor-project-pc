const modMessages = {
  member_activity_name: 'promotion name:',
  member_activity_time: 'promotion time:',
  member_extra_dividend: 'Percentage of Bonus:',
  member_minimum_income: 'Minimum deposit:',
  member_maximum_dividend: 'Maximum Bonus:',
  member_document_rate: 'Bet rate:',
  member_number_blank: 'Limitation of length 3-60',
  member_exercise: 'promotion',
  agency_operation: 'operation',
  member_sumbitAccount_number: 'Confirm bank account:',
  agency_winning_percentage: 'Winning amount + percentage + Code Commission + bonus activities',
  agency_dividend_betting: '+Dividend distribution + backwater - effective betting',
  agency_download: 'download',

}

export default modMessages
