const list = [
  "webMemberSummary",
  "webMemberBankData",
  "webMemberDeposit",
  "webMemberFundManage",
  "webMemberModifyPassword",
  "webMemberPrivateMessage",
  "webMemberProfile",
  "webMemberTransactionRecord",
  "webMemberWithdrawal",
  "webBetRecord",
  "webMemberWithdrawalRecords",
  "webMemberDepositRecords",
  "webMemberAccountChangeRecords"
]

export default list
