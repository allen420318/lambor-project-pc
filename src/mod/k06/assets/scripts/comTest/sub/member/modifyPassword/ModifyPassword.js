import BLDef from "scripts/common/BLDef"
import CommonService from "@/assets/scripts/businessLogic/commonService"
import Regex from "@/assets/scripts/common/CommDef"
import CommUtility from "scripts/common/CommUtility"
import PersonalCenterService from "@/assets/scripts/businessLogic/personalCenterService"
import EventBus from "@/assets/scripts/common/EventBus"
// import LoginService from 'scripts/businessLogic/loginService';
// import SignalRService from 'scripts/common/SignalRService';
export const initMoPa = {
  props: {
    ModifyType: undefined
  },
  data() {
    return {
      ProcessingFlag: false,
      PasswordObj: {},
      Reg: {
        Password: Regex.Regex.Password,
        PasswordErrorMsg: this.$t("message.member_number_range")
      },

      // 判断是否在取款中已经封锁账号 2018.12.17 SeyoChen
      CommUtility: CommUtility,
      WithdrawData: {
        WithdrawPwd: ""
      }
      // 判断是否在取款中已经封锁账号 2018.12.17 SeyoChen end
    }
  },
  mounted: function () {
    CommUtility.BindEnterTrigger("passwordModifyForm", this.SubmitChange)
  },
  methods: {
    // 關閉視窗
    CloseWindow: function () {
      CommUtility.WebCloseUniqueForm("informationForm")
    }, // end CloseWindow

    // 弹出客服窗口
    OpenNewBox: function (serveLink) {
      window.open(
        serveLink,
        this.$t("message.top_customer_service"),
        "scrollbars=1,width=365,height=565,left=10,top=150"
      )
      this.CloseWindow()
    },
    // 送出修改密碼
    SubmitChange: async function () {
      Object.keys(this.fields).forEach((key) => {
        this.fields[key].touched = true
      })
      const result = await this.$validator.validateAll()
      if (result === false) {
        return
      }

      // 處理中則直接返回
      if (this.ProcessingFlag === true) {
        return
      }

      this.ProcessingFlag = true

      const inputObj = {
        CPType: this.ModifyType,
        ChangePwdInfo: {
          OriginPwd: this.PasswordObj.OriginPassword,
          NewPwd: this.PasswordObj.NewPassword,
          ConfirmNewPwd: this.PasswordObj.ConfirmPassword
        }
      }

      // 判断是否在取款中已经封锁账号 2018.12.17 SeyoChen
      if (this.ModifyType === 4) {
        // 2019.01.12 判定是否锁定账号
        const lookingData = await PersonalCenterService.WithdrawApply_GetWithdrawLimitInfo()
        this.ProcessingFlag = false

        if (lookingData.Data.WithdrawPwdErrCnt >= 3) {
          CommUtility.WebShowUniqueForm("pwdErrorForm2")
          return
        }
        // 2019.01.12 判定是否锁定账号 end

        // 取得登入狀態 -- 判断账户状态 是否被冻结 2018.12.26
        const retData = await CommonService.Comm_CheckPermission()
        this.ProcessingFlag = false
        if (retData.Status != BLDef.SysAccountStatus.LOGINED_ENABLED) {
          const notifyData = {
            NotifyMessage: this.$t("message.top_account_freezing"),
            NotifySubMessage: undefined,
            CloseFunction: function () {
              window.close()
            }
          }
          EventBus.$emit("showNotifyMessage", notifyData)
          return
        }
        // 取得登入狀態 -- 判断账户状态 是否被冻结 2018.12.26 end

        this.WithdrawData.WithdrawPwd = this.PasswordObj.OriginPassword
        const data = await PersonalCenterService.WithdrawApply_Add(this.WithdrawData)
        this.ProcessingFlag = false

        if (data.Data.PwdErrCnt >= 3) {
          CommUtility.WebShowUniqueForm("pwdErrorForm")
          return
        }
      }
      // 判断是否在取款中已经封锁账号 2018.12.17 SeyoChen end

      EventBus.$emit("GlobalLoadingTrigger", true)
      const data = await PersonalCenterService.ChangePassword_Mod(inputObj)
      EventBus.$emit("GlobalLoadingTrigger", false)

      let notifyData = {
        NotifyMessage: data.Message
      }

      if (data.Ret === 0 && this.ModifyType === 4) {
        notifyData.CloseFunction = this.ResetPwdForm
        // 顯示訊息彈窗
        EventBus.$emit("showNotifyMemberMessage", notifyData)
      } else if (data.Ret === 0 && this.ModifyType === 2) {
        // 修改登录密码成功后退出当前用户 20190830 SeyoChen
        notifyData = {
          NotifyMessage: this.$t("message.main_change_password"),
          CloseFunction: this.Logout
        }
        EventBus.$emit("showNotifyMemberMessage", notifyData)
        // 修改登录密码成功后退出当前用户 20190830 SeyoChen end
      } else {
        // 顯示訊息彈窗
        EventBus.$emit("showNotifyMemberMessage", notifyData)
      }

      this.ProcessingFlag = false
    },
    // 清除畫面驗證與資料
    ResetPwdForm: function () {
      this.PasswordObj = {}
      this.$validator.reset()
    },
    // 切換密碼類型時更新正則與驗證訊息
    UpdateRegex: function () {
      if (this.ModifyType === 2) {
        this.Reg.Password = Regex.Regex.Password
        this.Reg.PasswordErrorMsg = this.$t("message.member_number_range")
      } else if (this.ModifyType === 4) {
        this.Reg.Password = Regex.Regex.WithdrawPwd
        this.Reg.PasswordErrorMsg = this.$t("message.member_use_numbers")
      }
    },
    // 登出
    Logout: async function () {
      // const inputObj = {
      //   LogoutInfo: {}
      // };
      EventBus.$emit("loginFlagCheck", false)
      localStorage.setItem("loginFlag", "false")
      EventBus.$emit("GlobalLoadingTrigger", true)
      // const reData = await LoginService.LogOut(inputObj);
      // 移除登入相關資料
      localStorage.removeItem("Token")
      localStorage.removeItem("Auth")
      localStorage.removeItem("logoutTime")
      localStorage.removeItem("Acct") // 2018.09.20 SeyoChen
      localStorage.removeItem("CorrelationIBMemberId")
      localStorage.removeItem("IBStatus")
      localStorage.removeItem("loginFlag")
      EventBus.$emit("GlobalLoadingTrigger", false)

      // SignalRService.DisConnectServer();

      CommUtility.WebCloseUniqueForm("memberMainFrom") // 页面切换时隐藏弹窗

      this.$router.push({
        name: "Login"
      })
    }
  },
  watch: {
    ModifyType: function () {
      this.ResetPwdForm()
      this.UpdateRegex()
    }
  }
}
