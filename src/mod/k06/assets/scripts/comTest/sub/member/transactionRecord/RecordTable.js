import BLDef from "scripts/common/BLDef"
import PersonalCenterService from "@/assets/scripts/businessLogic/personalCenterService"
import MemberRecordDetailNormal from "@/components/sub/member/transactionRecord/memberRecordDetailNormal"
import MemberRecordDetailRake from "@/mod/k06/components/sub/member/transactionRecord/memberRecordDetailRake"

export const initRoTl = {
  data() {
    return {
      RakeSettleDetail: {
        RakeList: [],
        Summary: {}
      },
      OtherRecordDetail: {},
      RecordCondition: {},
      TransType: {
        TransType_Deposit: 1,
        TransType_EWallet: 2,
        TransType_BonusActivity: 3,
        TransType_RakeSettled: 4,
        TransType_Withdraw: 5
      }
    }
  },
  props: {
    RecordList: {
      type: Array,
      default: function () {
        return []
      }
    }
  },
  methods: {
    ShowDetail: async function (Record) {
      const inputObj = {
        ID: Record.ID,
        Type: Record.TransType
      }
      this.RecordCondition = inputObj
      const data = await PersonalCenterService.CashFlowLog_LoadDetailPage(inputObj)
      if (data.Ret === BLDef.ErrorRetType.SUCCESS) {
        if (Record.TransType === this.TransType.TransType_RakeSettled) {
          // 反水類型
          this.RakeSettleDetail = data.Data.Detail
        } else {
          // 其他類型
          this.OtherRecordDetail = data.Data.Detail
        }
      }
    }
  },
  components: {
    MemberRecordDetailNormal,
    MemberRecordDetailRake
  }
  // mounted() {
  // 	// 滚动条
  // 	$('#record2-scroll').perfectScrollbar();
  // }
}
