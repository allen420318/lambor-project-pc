/**
 * @name 彩票投注内容组件
 * @author dingruijie
 * @time 2019-09-17
 * @augments PageCount 计算后的分页总数 类型：Number 默认0
 * @augments pageSize 分页每页条数 类型: Number 默认每页10条
 * @event 父组件请求完数据 初始化分页数据 this.$refs[refname].computeGameList(1，allList) 初始传1表示渲染第一页
 * @event PlayKGGame 进入游戏点击事件 使用方法 @PlayKGGame= Function
 */

import lotteryPaginate from "@/mod/k06/components/sub/paginate"
export default {
  name: "lottery-content",
  components: {
    lotteryPaginate
  },
  props: {
    PageCount: {
      type: Number,
      default: 0
    },
    pageSize: {
      type: Number,
      default: 10
    }
  },
  data() {
    return {
      pageNum: 1,
      LotteryList: [],
      allLotteryList: []
    }
  },
  methods: {
    GetCurrentPageData: async function (pageNo) {
      this.computeGameList(pageNo)
    },
    // 计算每一页展示数据
    computeGameList(num, initList) {
      if (initList) this.allLotteryList = initList
      this.LotteryList = this.allLotteryList
      this.LotteryList.forEach((item) => {
        switch (item.GameApiNo) {
          case "86":
            item.photoUrl = "/static/images/lottery/GamingWorld.png"
            break
          default:
            break
        }
      })
      // 插入box之前清除原先已有box避免重复
      $(".lottery-expect").remove()
      // setTimeout(() => {
      //     this.AjectAddBox(this.LotteryList.length);
      // }, 50);
    },
    // 2018.11.15 SeyoChen 动态识别添加游戏模块
    AjectAddBox: function (num) {
      const boxSize = parseInt(5) // 页面中一行的box个数
      const boxHtml = `<li class="lottery-logo-li lottery-expect lotteryAbnormal">
                <i class="staytuned abnormalDefault"></i></li>`
      const box = $(".lottery-logo-lists")
      if (num % boxSize === 0 && num !== 0) {
      } else {
        for (let i = 0; i < Math.abs(boxSize - (num % boxSize)); i++) {
          box.append(boxHtml)
        }
      }
    }
  }
}
