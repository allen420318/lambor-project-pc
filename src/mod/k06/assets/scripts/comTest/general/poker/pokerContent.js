/**
 * @name 棋牌游戏内容组件
 * @author dingruijie
 * @time 2019-09-17
 * @augments PageCount 计算后的分页总数 类型：Number 默认0
 * @augments pageSize 分页每页条数 类型: Number 默认每页10条
 * @event 父组件请求完数据 初始化分页数据 this.$refs[refname].computeGameList(1，allList) 初始传1表示渲染第一页
 * @event PlayGame 进入游戏点击事件 使用方法 @PlayGame= Function
 */

import pokerPaginate from "@/mod/k06/components/sub/paginate"
export default {
  name: "poker-content",
  components: { pokerPaginate },
  props: {
    PageCount: {
      type: Number,
      default: 0
    },
    pageSize: {
      type: Number,
      default: 10
    }
  },
  data() {
    return {
      pageNum: 1,
      PoKerList: [],
      allPoKerList: []
    }
  },
  methods: {
    GetCurrentPageData: async function (pageNo) {
      this.computeGameList(pageNo)
    },
    // 计算每一页展示数据
    computeGameList(num, initList) {
      if (initList) this.allPoKerList = initList
      this.PoKerList = this.allPoKerList
      this.PoKerList.forEach((item, index) => {
        switch (item.GameApiNo) {
          case "45":
            item.photoUrl = "/static/images/poker/jdb.png"
            break
          case "106":
            item.photoUrl = "/static/images/poker/bailu.png"
            break
          case "111":
            item.photoUrl = "/static/images/poker/lc.png"
            break
          default:
            break
        }
      })
      // 插入box之前清除原先已有box避免重复
      $(".poker-expect").remove()
      // setTimeout(() => {
      //     this.AjectAddBox(this.PoKerList.length);
      // }, 50);
    },
    // 2018.11.15 SeyoChen 动态识别添加游戏模块
    AjectAddBox: function (num) {
      const boxSize = parseInt(3) // 页面中一行的box个数
      if (num === 0) return
      const boxHtml =
        '<li class="poker-logo-li poker-expect pokerAbnormal"><i class="staytuned abnormalDefault"></i></li>'
      const box = $("#poker-logo-lists")
      if (num % boxSize === 0 && num !== 0) {
      } else {
        for (let i = 0; i < Math.abs(boxSize - (num % boxSize)); i++) {
          box.append(boxHtml)
        }
      }
    }
  }
}
