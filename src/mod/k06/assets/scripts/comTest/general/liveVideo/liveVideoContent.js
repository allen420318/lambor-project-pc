/**
 * @name 真人视讯内容组件
 * @author dingruijie
 * @time 2019-09-16
 * @augments PageCount 计算后的分页总数 类型：Number 默认0
 * @augments pageSize 分页每页条数 类型: Number 默认每页10条
 * @event 父组件请求完数据 初始化分页数据 this.$refs[refname].computeGameList(1，allList) 初始传1表示渲染第一页
 */
import livevideoPaginate from "@/mod/k06/components/sub/paginate"
import CommUtility from "scripts/common/CommUtility"
import EventBus from "@/assets/scripts/common/EventBus"
import URLService from "@/assets/scripts/common/URLService"
import BLDef from "scripts/common/BLDef"
import JsCookie from "js-cookie"
export default {
  name: "livevideo-content",
  components: {
    livevideoPaginate
  },
  props: {
    PageCount: {
      type: Number,
      default: 0
    },
    pageSize: {
      type: Number,
      default: 10
    }
  },
  data() {
    return {
      pageNum: 1,
      VideoList: [],
      allVideoList: [],
      // 真人AG遊戲Post Model
      GameData: {
        GameAPIVendor: undefined,
        GameCode: "",
        GameCatlog: BLDef.GameCatlogType.LiveVideo,
        PlayType: BLDef.IdentityType.FORMAL,
        Platform: BLDef.PlatformType.Web_PC
      }
    }
  },
  methods: {
    GetCurrentPageData: async function (pageNo) {
      this.computeGameList(pageNo)
    },
    // 计算每一页展示数据
    computeGameList(num, initList) {
      if (initList) this.allVideoList = initList
      this.VideoList = this.allVideoList
      this.VideoList.forEach((item) => {
        switch (item.GameApiNo) {
          case "123":
            item.photoUrl = "/static/images/live/sexy.png"
            break
          case "1":
            item.photoUrl = "/static/images/live/ag.png"
            break
          case "112":
            item.photoUrl = "/static/images/live/s128.png"
            break
          case "67":
            item.photoUrl = "/static/images/live/eBET.png"
            break
          case "15":
            item.photoUrl = "/static/images/live/BBIN.png"
            break
          case "66":
            item.photoUrl = "/static/images/live/og.png"
            break
          case "94":
            item.photoUrl = "/static/images/live/salon.png"
            break
          default:
            break
        }
      })
      // 插入box之前清除原先已有box避免重复
      $(".live-expect").remove()
      // setTimeout(() => {
      //     this.AjectAddBox(this.VideoList.length);
      // }, 50);
    },
    // 动态识别添加游戏模块
    AjectAddBox(num) {
      const boxSize = parseInt(3) // 页面中一行的box个数
      const boxHtml = `<li class="live-logo-li live-expect liveAbnormal">
      <i class="staytuned abnormalDefault"></i></li>`
      const box = $(".live-logo-lists")
      if (num % boxSize === 0 && num !== 0) {
      } else {
        for (let i = 0; i < Math.abs(boxSize - (num % boxSize)); i++) {
          box.append(boxHtml)
        }
      }
    },
    // 開新視窗
    PlayGame: function (gameList) {
      // 遍历游戏
      this.GameData.GameAPIVendor = gameList.GameApiNo
      this.GameData.GameCode = gameList.GameCode
      localStorage.setItem("PlayGamePostData", URLService.GetUrlParameterFromObj(this.GameData))
      CommUtility.OpenPlayGameWindow("webPlayGame", "PlayGamePopUpWindow")
      // EventBus.$emit('showTransferMessage', this.GameData);
    } // end PlayGame
  }
}
