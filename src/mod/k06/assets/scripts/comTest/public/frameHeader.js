import EventBus from "@/assets/scripts/common/EventBus"
import CommUtility from "scripts/common/CommUtility"
import LoginService from "@/assets/scripts/businessLogic/loginService"
import CommonService from "@/assets/scripts/businessLogic/commonService"
import SignalRService from "@/assets/scripts/common/SignalRService"
import rotateLoopFun from "@/assets/scripts/customJs/web/rotateLoop"
import BLDef from "scripts/common/BLDef"
import PersonalCenterService from "@/assets/scripts/businessLogic/personalCenterService"
import LamborMarquee from "@/mod/k06/omponents/public/marquee"

export const initHeader = {
  data() {
    return {
      MemberInfo: {
        UnreadCount: 0
      },
      IsGetBalanceIng: false,
      VBalance: undefined,

      // 語系選單
      LangList: [],
      // 被選定的語系
      LangSelected: {
        Name: "简体中文",
        Value: 1
      },
      skinNum: "",
      skinData: [
        {
          id: 1,
          num: "02"
        },
        {
          id: 2,
          num: "03"
        },
        {
          id: 3,
          num: "07"
        },
        {
          id: 4,
          num: "08"
        },
        {
          id: 5,
          num: "15"
        },
        {
          id: 6,
          num: "23"
        },
        {
          id: 7,
          num: "25"
        },
        {
          id: 8,
          num: "28"
        },
        {
          id: 9,
          num: "33"
        },
        {
          id: 10,
          num: "34"
        },
        {
          id: 11,
          num: "60"
        }
      ],
      language: "zh"
    }
  },

  props: {
    IsLogin: undefined
  },
  mounted: function () {
    const self = this
    if (self.cookie.getJSON("language")) {
      if (self.cookie.getJSON("language") == 0) {
        self.language = "zh"
      } else if (self.cookie.getJSON("language") == 1) {
        self.language = "tw"
      } else if (self.cookie.getJSON("language") == 2) {
        self.language = "en"
      } else {
        self.language = "vi"
      }
    } else {
      self.language = self.$i18n.locale
    }
    EventBus.$on("SignalR_AddPrivateMessage", () => {
      self.MemberInfo.UnreadCount += 1
    })
    EventBus.$on("SignalR_ReadPrivateMessage", (data) => {
      self.MemberInfo.UnreadCount = data.Data
    })
    EventBus.$on("fundGetBalanceComplete", (SummaryAmount) => {
      self.VBalance = SummaryAmount
      self.IsGetBalanceIng = false
    })
    EventBus.$on("memberHeaderUpdateBalance", () => {
      self.GetBalance()
    })

    // 修改密码，退出重新登录
    EventBus.$on("Logout_ChangePassword", () => {
      localStorage.setItem("loginFlag", "false")
      // const reData = await LoginService.LogOut(inputObj);
      // 移除登入相關資料
      localStorage.removeItem("Token")
      localStorage.removeItem("Auth")
      localStorage.removeItem("logoutTime")
      localStorage.removeItem("Acct") // 2018.09.20 SeyoChen
      localStorage.removeItem("CorrelationIBMemberId")
      localStorage.removeItem("IBStatus")
      localStorage.removeItem("loginFlag")

      SignalRService.DisConnectServer()
      // 修改登录密码成功后退出当前用户 20190830 SeyoChen
      const notifyData = {
        NotifyMessage: this.$t("message.main_change_password"),
        CloseFunction: self.SingleLogout
      }
      EventBus.$emit("showNotifyMessage", notifyData)
    })

    // 每10分钟刷新一次钱包 2019.03.28 SeyoChen
    EventBus.$on("GBKBalance", (Balance) => {
      self.VBalance = Balance
    })

    this.LoadLangList()
    this.BindingEvent()
  },
  computed: {
    themecolor() {
      return this.$store.state.themecolor // 获取store的最新themecolor
    }
  },
  methods: {
    // 后台通知登出 20190919 SeyoChen
    SingleLogout: function () {
      this.$router.push({
        name: "Login"
      })
    },

    changeColor(num) {
      EventBus.$emit("skinHeadCon", num)
    },
    TriggerMemberUser: function (data) {
      console.log(data)
      this.$store.dispatch("setSwitchNameText", data)
      EventBus.$emit("personalInfo", data)
    },
    CheckAcctStatus: async function () {
      const retData = await CommonService.Comm_CheckPermission()
      return retData
    },
    agencyCenter: function (data) {
      CommUtility.WebCloseUniqueForm("memberMainFrom") // 页面切换时隐藏弹窗
      this.$store.dispatch("setAgencyNameText", "webProfitLoss")
      EventBus.$emit("personalAgency", data)
    },
    // 檢查權限:凍結不可進行遊戲
    CheckLogin: async function () {
      const permissionData = await this.CheckAcctStatus()
      if (permissionData.Status != BLDef.SysAccountStatus.LOGINED_ENABLED) {
        const notifyData = {
          NotifyMessage:
            BLDef.SysAccountStatus.NOT_LOGIN == permissionData.Status ||
            BLDef.SysAccountStatus.NOT_LOGIN_DISABLED == permissionData.Status ||
            BLDef.SysAccountStatus.NOT_LOGIN_DELETED == permissionData.Status
              ? this.$t("message.top_login_account")
              : this.$t("message.top_account_freezing"),
          CloseFunction: this.CloseCurrentWindow
        }
        EventBus.$emit("showNotifyMessage", notifyData)
      } else {
        // this.$router.push({
        //     name: 'Deposit'
        // });
        this.$store.dispatch("setSwitchNameText", "webMemberDeposit")
        EventBus.$emit("personalInfo")
      }
    },

    // 載入下拉式選單
    LoadLangList: async function () {
      const retData = await CommonService.Comm_GetLanguages()
      if (retData.Ret == 0) {
        this.LangList = retData.Data.LangSelectList
        this.LangSelected = this.LangList[0]
      } // end if
    }, // end LoadLangList

    // 切換語系
    ChangeLang: function (targetLang) {
      /* *******************************************************切換語系動作( 2017/11/9  -- 未完成 --) */
      this.LangSelected = targetLang
      this.$store.commit("GetLanguageData", targetLang)
    },

    GetBalance: async function () {
      this.IsGetBalanceIng = true
      rotateLoopFun.rotateLoop() // 刷新按钮的旋转
      if (this.$route.name === "FundManage") {
        // 更新資金管理餘額
        EventBus.$emit("updateFundBalance")
      } else {
        // this.VBalance = await CommUtility.CalculateSummaryAmount();
        const inputObj = {}
        const retData = await PersonalCenterService.MemberCashFlowInfo_GetCWalletMoney(inputObj) // 新版获取账户金额 2019.03.28

        if (retData.Ret == 0) {
          this.VBalance = retData.Data.VAmount
        } else {
          this.VBalance = this.$t("message.top_under_maintenance")
        }
        this.IsGetBalanceIng = false
      }
    },

    GetMemberInfo: async function () {
      // 取得會員資訊
      const data = await CommonService.Comm_GetMemberInfo()
      if (data.Ret == 0) {
        this.MemberInfo = data.Data.Member
        rotateLoopFun.rotateLoop() // 刷新按钮的旋转
      }
    },
    // 登出
    Logout: async function () {
      const inputObj = {
        LogoutInfo: {}
      }
      EventBus.$emit("loginFlagCheck", false)
      localStorage.setItem("loginFlag", "false")
      EventBus.$emit("GlobalLoadingTrigger", true)
      const reData = await LoginService.LogOut(inputObj)
      // 移除登入相關資料
      // localStorage.removeItem('Token');
      // localStorage.removeItem('Auth');
      // localStorage.removeItem('logoutTime');
      // localStorage.removeItem('Acct'); // 2018.09.20 SeyoChen

      // 清理缓存 20190808 SeyoChen
      localStorage.removeItem("Token")
      localStorage.removeItem("Auth")
      localStorage.removeItem("logoutTime")
      localStorage.removeItem("Acct") // 2018.09.20 SeyoChen
      localStorage.removeItem("CorrelationIBMemberId")
      localStorage.removeItem("IBStatus")
      localStorage.removeItem("loginFlag")
      // 清理缓存 20190808 SeyoChen end

      if (reData.Ret === 0) {
        // localStorage.clear();
        window.location.href = "/"
        EventBus.$emit("GlobalLoadingTrigger", false)
      } else {
        // localStorage.clear();
        // 若沒接收到signalr斷線訊息，則3秒後自行斷線
        setTimeout(function () {
          // 斷Signalr連線
          SignalRService.DisConnectServer()
          window.location.href = "/"
          EventBus.$emit("GlobalLoadingTrigger", false)
        }, 3000)
      }
    },
    // 收藏本站
    AddFavorite: function () {
      const notifyData = {}
      if (window.sidebar && window.sidebar.addPanel) {
        // Mozilla Firefox Bookmark
        window.sidebar.addPanel(document.title, window.location.href, "")
      } else if (window.external && "AddFavorite" in window.external) {
        // IE Favorite
        window.external.AddFavorite(location.href, document.title)
      } else if (window.opera && window.print) {
        // Opera Hotlist
        this.title = document.title
      } else {
        // webkit - safari/chrome
        if (navigator.userAgent.toLowerCase().indexOf("mac") != -1) {
          notifyData.NotifyMessage = this.$t("message.top_clickBookmark2")
        } else {
          notifyData.NotifyMessage = this.$t("message.top_clickBookmark1")
        }
        EventBus.$emit("showNotifyMessage", notifyData)
      }
    }, // end AddFavorite

    // 綁定隨機點擊事件(讓下拉選單恢復)
    BindingEvent: function () {
      document.addEventListener("click", this.HideDropDown)
    }, // end BindingEvent

    // 顯示紀錄類型選單
    ShowDropDown: function (targetClass) {
      $(".lang_down_box").slideUp(100)
      if (this.DropDowning !== targetClass) {
        $("." + targetClass).slideDown(300)
        this.DropDowning = targetClass
      } else {
        $("." + targetClass).slideUp(100)
        this.DropDowning = ""
      }
    }, // end ShowDropDown

    // 隱藏紀錄類型選單
    HideDropDown: function (event) {
      const target = $(event.target)
      if (target.closest(".d_course").length === 0) {
        $(".lang_down_box").slideUp(100)
        this.DropDowning = ""
      }
    }, // end HideDropDown
    // 多语言切换
    handleLanguage: function (lang) {
      const self = this
      console.log(lang.langNum)
      console.log(lang, "lang")

      if (lang.langNum == 0) {
        self.language = "zh"
        self.$i18n.locale = "zh"
      } else if (lang.langNum == 1) {
        self.language = "tw"
        self.$i18n.locale = "tw"
      } else if (lang.langNum == 2) {
        self.language = "en"
        self.$i18n.locale = "en"
      } else {
        self.language = "vi"
        self.$i18n.locale = "vi"
      }
      self.cookie.set("language", lang.langNum)
      EventBus.$emit("GlobalLoadingTrigger", true)
      setTimeout(() => {
        EventBus.$emit("GlobalLoadingTrigger", false)
      }, 1000)
      window.location.reload()
    }
  },
  watch: {
    IsLogin: function () {
      if (this.IsLogin === true) {
        this.GetMemberInfo()
        this.GetBalance()
      }
    }
  },
  components: {
    LamborMarquee
  },
  beforeDestroy() {
    EventBus.$off("SignalR_AddPrivateMessage")
    EventBus.$off("SignalR_ReadPrivateMessage")
    EventBus.$off("fundGetBalanceComplete")
    EventBus.$off("memberHeaderUpdateBalance")
  }
}
