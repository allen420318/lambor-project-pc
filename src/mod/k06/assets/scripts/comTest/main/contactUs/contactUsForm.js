import RegEx from "@/assets/scripts/common/CommDef"
import Vue from "vue"
import CommService from "@/assets/scripts/businessLogic/commonService"
import BLDef from "scripts/common/BLDef"
import ContactUsConfirmForm from "@/mod/k06/components/sub/contactUs/contactUsConfirmForm"
import CommUtility from "scripts/common/CommUtility"

export const ContactUsForm = {
  props: {
    TypeList: undefined,
    Account: undefined,
    LoginStatus: undefined
  },

  data() {
    return {
      // 正則表達式
      Reg: RegEx.Regex,

      // 類型下拉選單錯誤旗標
      TypeErrorFlag: undefined,

      // 驗證碼圖片
      AuthImg: "",

      // post用的model
      PostDataModel: {
        ContactUs: {
          Name: "",
          PhoneNo: "",
          Email: "",
          Type: {},
          QQAcct: "",
          Content: ""
        },
        Email: undefined,
        WGUID: Vue.prototype.$WGUID,
        VCodeID: undefined,
        VCode: undefined
      },

      // 下拉式選單的下拉判斷旗標
      DropDowning: "",

      // 驗證碼顏色設定
      RGB: {
        R: 255,
        G: 100,
        B: 255
      },
      defaultImg: 'this.src="' + this.$ResourceCDN + '/EditionImg/LamborFormal/images/public/refresh.png"'
    }
  },

  mounted: function () {
    CommUtility.BindEnterTrigger("contactUsForm", this.Submit)
    this.BindingEvent()
    this.GetAuthCode()
  },

  methods: {
    // 取得驗證碼
    GetAuthCode: async function () {
      /* eslint-disable camelcase */
      const VCode_CharacterRGB = this.RGB
      const RGBData = {
        VCode_CharacterRGB
      }
      const retData = await CommService.VerifyCode_Gen(RGBData)
      this.AuthImg = retData.Data.VCode
      this.PostDataModel.VCodeID = retData.Data.ID
      this.PostDataModel.VCode = undefined
    }, // end GetAuthCode

    // 綁定隨機點擊事件(讓下拉選單恢復)
    BindingEvent: function () {
      document.addEventListener("click", this.HideDropDown)
    }, // end BindingEvent

    // 顯示紀錄類型選單
    ShowDropDown: function (targetClass) {
      $(".down_box").slideUp(100)
      if (this.DropDowning !== targetClass) {
        $("." + targetClass).slideDown(300)
        this.DropDowning = targetClass
      } else {
        $("." + targetClass).slideUp(100)
        this.DropDowning = ""
      }
    }, // end ShowDropDown

    // 隱藏紀錄類型選單
    HideDropDown: function (event) {
      const target = $(event.target)
      if (target.closest(".d_course").length === 0) {
        $(".down_box").slideUp(100)
        this.DropDowning = ""
      }
    }, // end HideDropDown

    // 根據下拉選單選項改變類型
    ChangeType: function (type) {
      this.PostDataModel.ContactUs.Type = type
      // 驗證類型是否有選
      if (this.PostDataModel.ContactUs.Type.Value != -2) {
        this.TypeErrorFlag = false
      } // end if
    }, // end ChangeType

    // 所有欄位驗證
    validate: function () {
      Object.keys(this.fields).forEach((key) => {
        this.fields[key].touched = true
      })
      return this.$validator.validateAll().then((result) => {
        return result
      })
    }, // end validate

    Submit: async function () {
      let anyInvalid = false

      const validate = await this.validate()

      // 驗證所有欄位
      if (!validate) {
        anyInvalid = true
      } // end if

      // 驗證類型是否有選
      if (this.PostDataModel.ContactUs.Type.Value == -2) {
        this.TypeErrorFlag = true
        anyInvalid = true
      } // end if

      if (anyInvalid === true) {
        return
      } // end if
      ContactUsConfirmForm.mixins[0].methods.OpenWindow()
    }
  },

  components: {
    ContactUsConfirmForm
  },

  watch: {
    TypeList: function () {
      this.PostDataModel.ContactUs.Type = this.TypeList[0]
    }
  }
}
