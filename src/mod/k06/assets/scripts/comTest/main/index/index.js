// 接口使用说明 获取优惠活动
// const data = await CommonService.Comm_GetBonusActivity(inputObj); const inputObj = {WGUID: Vue.prototype.$WGUID};

// 检查账号状态
//  const data = await CommonService.Comm_CheckPermission();
// 去的会员资讯
// const data = await CommonService.Comm_GetMemberInfo();
import Activity from "@/mod/k06/components/sub/index/activity"
import FastEntrance from "@/mod/k06/components/sub/index/fastEntrance"

export const initIndex = {
  components: {
    Activity,
    FastEntrance
  },
  mounted() {
    $(".news_show").hover(function () {
      $(this).addClass("hasblock").siblings(".news_show").removeClass("hasblock")
    })
    // 首页热门游戏
    $(".hot_box .hot_lists li")
      .mouseenter(function () {
        $(this).animate(
          {
            "margin-top": "-20px"
          },
          300
        )
      })
      .mouseleave(function () {
        $(this).animate(
          {
            "margin-top": "0"
          },
          300
        )
      })
    // 新开窗口并保持居中
    function openWin(win_url, win_w, win_h) {
      var url = win_url // 转向页面的地址;
      var name = "" // 页面名称，可为空;
      var iWidth = win_w // 弹出窗口的宽度;
      var iHeight = win_h // 弹出窗口的高度;
      // 获得窗口的垂直位置
      var iTop = (window.screen.availHeight - 30 - iHeight) / 2
      // 获得窗口的水平位置
      var iLeft = (window.screen.availWidth - 10 - iWidth) / 2
      window.open(
        url,
        name,
        "height=" +
          iHeight +
          ",,innerHeight=" +
          iHeight +
          ",width=" +
          iWidth +
          ",innerWidth=" +
          iWidth +
          ",top=" +
          iTop +
          ",left=" +
          iLeft +
          ",status=no,toolbar=no,menubar=no,location=no,resizable=no,scrollbars=0,titlebar=no"
      )
    }
    /*开启对应新窗口*/
    $(".free-sbtn").click(function () {
      openWin("/FreeTrial", "1900", "1080")
    })
  }
}
