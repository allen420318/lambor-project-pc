import BLDef from "scripts/common/BLDef"
import EventBus from "@/assets/scripts/common/EventBus"
import CommonService from "@/assets/scripts/businessLogic/commonService"

export const indexFast = {
  data() {
    return {
      IsLogin: undefined,
      MemberInfo: {},

      StateLogin: false
    }
  },
  created: function () {
    this.CheckIsLogin()
  },
  methods: {
    // 新的跳转打开个人中心方式 2019.05.13 SeyoChen
    navClick: async function (name) {
      this.CheckLoginStatusBeforeRoute(name)
      if (this.StateLogin !== false) {
        this.$store.dispatch("setSwitchNameText", name)
        EventBus.$emit("personalInfo")
      }
    },
    // 檢查登入狀態
    CheckIsLogin: async function () {
      const data = await CommonService.Comm_CheckPermission()
      switch (data.Status) {
        case BLDef.SysAccountStatus.NOT_LOGIN:
          this.IsLogin = false
          break
        case BLDef.SysAccountStatus.LOGINED_ENABLED:
        case BLDef.SysAccountStatus.LOGINED_FROZEN:
          this.IsLogin = true
          this.GetMemberInfo()
          break
        default:
          break
      }
    },
    GetMemberInfo: async function () {
      // 取得會員資訊
      const data = await CommonService.Comm_GetMemberInfo()
      if (data.Ret === 0) {
        this.MemberInfo = data.Data.Member
      }
    },
    // 檢查帳號狀態
    CheckLoginStatusBeforeRoute: async function (routeName) {
      const notifyData = {}
      const data = await CommonService.Comm_CheckPermission()

      switch (data.Status) {
        case BLDef.SysAccountStatus.LOGINED_ENABLED:
          // this.$router.push({
          //     name: routeName
          // });
          this.StateLogin = true
          this.$store.dispatch("setSwitchNameText", routeName)
          EventBus.$emit("personalInfo", routeName)
          break
        case BLDef.SysAccountStatus.LOGINED_FROZEN:
          this.StateLogin = false
          notifyData.NotifyMessage = "账户被冻结, 请联系客服人员"
          EventBus.$emit("showNotifyMessage", notifyData)
          break
        default:
          break
      }
    }
  },
  watch: {
    fastEntranceObj: function () {
      this.DealUpdatedData()
    }
  }
}
