import electronicContent from "@/mod/k06/components/general/electronic/electronicContent"

export const electronic = {
  name: "electronic",
  components: {
    electronicContent
  }
}
