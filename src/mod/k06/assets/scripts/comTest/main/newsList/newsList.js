import InformationService from "@/assets/scripts/businessLogic/informationService"
import BLDef from "scripts/common/BLDef"
import EventBus from "@/assets/scripts/common/EventBus"
import HttpService from "@/assets/scripts/common/HttpService"
import NewsListContent from "@/mod/k06/components/sub/newsList/newsListContent"
import NewsListPaging from "@/mod/k06/components/sub/paginate"
import Vue from "vue"

export const NewsList = {
  data() {
    return {
      PageInfo: {
        PageCount: 0
      },
      CurrentPageData: "",
      NewsDetail: {
        Title: ""
      },
      MessageCotentData: undefined,

      // 資源位置
      Resource: Vue.prototype.$ResourceURL,
      // 資源旗標
      ResourceFlag: false
    }
  },
  mounted() {
    // 23版 弹窗关闭
    $(".news-close").click(function () {
      $("#n-p-main").hide()
    })

    // 23版 弹窗滚动条
    $(".news-popup-detail").perfectScrollbar()
  },
  methods: {
    // 弹窗弹出详情页面
    showDetail: function (ID) {
      this.PageInit(ID)
      $("#n-p-main").show()
    },
    //  跳至新聞詳細
    UrlRedirectDetailAnimation: function (ID) {
      this.PageInit(ID)
      $(".news-page").fadeOut()
      $("#n-p-main").fadeIn()
    }, // end UrlRedirectDetail
    // 初始化
    PageInit: async function (ID) {
      const dataObj = {
        WGUID: Vue.prototype.$WGUID,
        ID: ID
      }
      // 点击切换详情内容时内容滚动到顶部，在请求内容前注销滚动事件禁止滚动
      $(".news-popup-detail").scrollTop(0)
      $(".news-popup-detail").perfectScrollbar("destroy")
      const retData = await InformationService.GetNewsDetail(dataObj)
      // 失敗
      if (retData.Ret != 0) {
        const notifyData = {
          NotifyMessage: retData.Message
        }
        EventBus.$emit("showNotifyMessage", notifyData)
        return
      } // end if

      // 成功
      this.NewsDetail = retData.Data.Detail
      const temp = await HttpService.GetContent(this.Resource + retData.Data.Detail.MessageCotent)
      this.MessageCotentData = temp
      this.$nextTick(() => {
        // 内容渲染后重新计算滚动
        $(".news-popup-detail").perfectScrollbar()
      })
      // 顯示資源
      this.ResourceFlag = true
    }, // end PageInit
    GetCurrentPageData: function (page) {
      this.CurrentPageData = page
    }
  },
  components: {
    NewsListContent,
    NewsListPaging
  }
}
