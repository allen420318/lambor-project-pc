import EventBus from "@/assets/scripts/common/EventBus"
import CommUtility from "scripts/common/CommUtility"
import PersonalCenterService from "@/assets/scripts/businessLogic/personalCenterService"
import BLDef from "scripts/common/BLDef"
import MemberDepositSelectActivity from "@/components/sub/member/deposit/memberDepositSelectActivity"
import MemberDepositSelectedBonus from "@/components/sub/member/deposit/memberDepositSelectedBonus"
import MemberDepositOnlineSelectPay from "@/components/sub/member/deposit/memberDepositOnlineSelectPay"
import MemberDepositRemittance from "@/mod/k06/components/sub/memberNew/deposit/memberDepositRemittance"
import MemberDepositRecord from "@/components/sub/member/deposit/memberDepositRecord"
import MemberDepositAmountLimit from "@/components/sub/member/deposit/memberDepositAmountLimit"

export const initDeOt = {
  data() {
    return {
      OnlinePaymentAcctCnt: undefined, // 在线支付号标
      CompAcctCnt: undefined, // 公司入款号标
      SelectedPaymentType: 1, // 选择的存款类型，默认在线支付
      ActivityName: undefined, // 选择的活动名称
      ActivityDetail: {
        type: Array,
        default: function () {
          return []
        }
      },
      IsShow: false,
      CurSelectedActivity: {
        // 当前从活动选择那边带进来的活动
        BonusSerialNo: undefined, // 红利对应的资料序号
        ActivityType: undefined, // 红利活动方式 ==》 -1：未定义; 1：存款(红利设定); 2：红利活动
        ActType: undefined // 红利方式细项分类 ==》 1: 首存 2: 续存
      },
      CurActivityDetail: {},
      HaveNoSelectedBonus: undefined, // 是否显示不参与活动按钮 2019.03.28

      HasActivity: false // 会员分级的优惠活动 存在:false 不存在:true 20190903 SeyoChen
    }
  },
  mounted: async function () {
    this.LoadMainPage()
    // 2018.11.22 SeyoChen 最终版优惠活动传值进入存款
    if (this.$route.params.ActivityData != undefined) {
      this.IsShow = true
      const dataObj = {
        ActType: -1,
        BonusSerialNo: this.$route.params.ActivityData.ID,
        ActivityType: 2
      }

      if (this.$route.params.ChargeType == 1) {
        const ParaData = await PersonalCenterService.OnlinePaymentDeposit_LoadMainPage(dataObj) // 在线支付获取详情

        // 设置上面组件间传递的数据
        this.SelectedPaymentType = 1
        this.ActivityName = ParaData.Data.ActivityDetail.Name
        this.CurActivityDetail = ParaData.Data.ActivityDetail

        this.OutSideActivity(ParaData.Data.ActivityDetail, 1) // 1:在线支付 2:公司入款
      } else if (this.$route.params.ChargeType == 2) {
        const ParaData = await PersonalCenterService.CompanyDeposit_LoadMainPage(dataObj) // 公司入款获取详情
        // 设置上面组件间传递的数据
        this.SelectedPaymentType = 2
        this.ActivityName = ParaData.Data.ActivityDetail.Name
        this.CurActivityDetail = ParaData.Data.ActivityDetail

        if (ParaData.Ret != 0) {
          this.$route.params.ChargeType = 1
          const ParaData2 = await PersonalCenterService.OnlinePaymentDeposit_LoadMainPage(dataObj)
          this.OutSideActivity(ParaData2.Data.ActivityDetail, 1) // 1:在线支付 2:公司入款
          this.OpenWindow()
          return
        }
        this.$emit("CurrentPageData", ParaData.Data)
        this.OutSideActivity(ParaData.Data.ActivityDetail, 2) // 1:在线支付 2:公司入款
      }
    }
    // 2018.11.22 SeyoChen 最终版优惠活动传值进入存款 end
  },

  methods: {
    // 获取存款记录 2019.04.18 SeyoChen
    CloseDepositRecord: async function () {
      this.RecordShow = true
    },

    // 从外部优惠活动进入 2018.11.22 SeyoChen
    OutSideActivity: function (ParaData, params2) {
      this.$route.params.ChargeType = params2 // 支付方式
      const transActivity = {
        Name: this.$route.params.ActivityData.Name,
        StartTime: this.$route.params.ActivityData.StartTime,
        EndTime: this.$route.params.ActivityData.EndTime,
        StartTick: this.$route.params.ActivityData.StartTick,
        EndTick: this.$route.params.ActivityData.EndTick,
        BonusSerialNo: this.$route.params.ActivityData.ID,
        ActType: -1,
        ActivityType: 2,
        MinDepositAmt: ParaData.MinDepositAmt,
        MaxDepositAmt: ParaData.MaxDepositAmt,
        BonusRatio: ParaData.BonusRatio,
        VBonusRatio: ParaData.VBonusRatio
      }

      this.SelectedPaymentType = params2
      this.ActivityDetail = transActivity
      this.ActivityName = transActivity.Name
      this.CurSelectedActivity.Name = transActivity.Name
      this.CurSelectedActivity.BonusSerialNo = transActivity.BonusSerialNo
      this.CurSelectedActivity.ActivityType = transActivity.ActivityType
      this.CurSelectedActivity.ActType = transActivity.ActType
      this.CurActivityDetail = ParaData
      this.CurrentPageData(ParaData)
      this.IsShow = true
      EventBus.$emit("ActivityData", ParaData)
    },
    // 从外部优惠活动进入 2018.11.22 SeyoChen end

    // 開啟視窗
    OpenWindow: function () {
      CommUtility.WebShowUniqueForm("informationForm")
    }, // end OpenWindow

    // 關閉視窗
    CloseWindow: function () {
      CommUtility.WebCloseUniqueForm("informationForm")
    }, // end CloseWindow
    // 2018.09.27 SeyoChen end

    // 子级 memberDepositOnlineSelectPay 传过来的活动详情
    CurrentPageData: function (CurrentPageData) {
      this.CurActivityDetail = CurrentPageData
    },
    // 载入存款页面
    LoadMainPage: async function () {
      const retData = await PersonalCenterService.Deposit_LoadMainPage()
      // 若载入成功
      if (retData.Ret == BLDef.ErrorRetType.SUCCESS) {
        this.OnlinePaymentAcctCnt = retData.Data.OnlinePaymentAcctCnt // 在线支付号标
        this.CompAcctCnt = retData.Data.CompAcctCnt // 公司入款号标

        if (this.OnlinePaymentAcctCnt == 0) {
          this.SelectedPaymentType = 2
        }

        // 判断是否显示不参与活动按钮 2019.03.28
        this.HaveNoSelectedBonus = retData.Data.PartakeBonusModel.Status
        // 保存不参与活动的洗码量倍率
        localStorage.setItem("HaveNoSelectedBonusData", JSON.stringify(retData.Data.PartakeBonusModel))

        // 若没有符合会员分级的优惠活动 20190903 SeyoChen
        if (retData.Data.RemittanceDepositActivityList.length === 1) {
          this.HasActivity = retData.Data.RemittanceDepositActivityList[0].PresetValue.Checked
        } else {
          this.HasActivity = false
        }
        // 若没有符合会员分级的优惠活动 20190903 SeyoChen end
      } else {
        // 若载入失败
        const notifyData = {
          NotifyMessage: retData.Message
        }
        EventBus.$emit("showNotifyMessage", notifyData)
      }
    },
    // 进入存款
    SwitchStep: function (param1, param2, param3) {
      // param1：可能是步骤几的意思; param2:选择的存款方式(1是在线支付，2是公司入款); param3：当前选中的优惠活动
      if (param1 == 2) {
        // 当进行到步骤二，则进入存款方式，隐藏优惠活动的选择
        this.IsShow = true
      } else {
        this.IsShow = false
      }

      // 2018.11.30 SeyoChen 仅开启公司入款 默认在线支付不需要设置
      if (this.OnlinePaymentAcctCnt == 0) {
        this.SelectedPaymentType = 2
        EventBus.$emit("ShowTitleFonts", "公司入款")
      } else {
        this.SelectedPaymentType = param2
      }
      // 2018.11.30 SeyoChen 仅开启公司入款 end

      this.ActivityDetail = param3
      this.ActivityName = param3.Name
      this.CurSelectedActivity.BonusSerialNo = param3.BonusSerialNo
      this.CurSelectedActivity.ActivityType = param3.ActivityType
      this.CurSelectedActivity.ActType = param3.ActType
    }
  },
  components: {
    MemberDepositSelectActivity,
    MemberDepositSelectedBonus,
    MemberDepositOnlineSelectPay,
    MemberDepositRemittance,
    MemberDepositAmountLimit,
    MemberDepositRecord
  },
  watch: {
    SelectedPaymentType: function () {
      // console.log('-----选择的存款方式(1是在线支付，2是公司入款)-----');
      // console.log(this.SelectedPaymentType);
      // console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
      return this.SelectedPaymentType
    }
  }
}
