import moment from "moment"
import EventBus from "@/assets/scripts/common/EventBus"
import RegEx from "@/assets/scripts/common/CommDef"
import Datepicker from "@/components/main/common/datePicker"
import BLDef from "scripts/common/BLDef"
import PersonalCenterService from "@/assets/scripts/businessLogic/personalCenterService"
import CommUtility from "scripts/common/CommUtility"
import Paginate from "@/mod/k06/components/sub/paginate"
import MemberPrivateMessageTable from "@/components/sub/member/privateMessage/memberPrivateMessageTable"
import MemberPrivateMessageDetail from "@/components/sub/member/privateMessage/memberPrivateMessageDetail"
export const initPtMg = {
  data() {
    return {
      // 正則表達式
      Reg: RegEx.Regex,
      DisplayMode: 1,
      Submitted: false,
      DropDowning: false,
      TimeSequenceInvalid: false,
      DisplayQueryObj: {
        StartTime: moment(new Date()).add(-1, "month").utcOffset(480).format("YYYY/MM/DD"),
        EndTime: moment(new Date()).utcOffset(480).format("YYYY/MM/DD"),
        MessageType: {}
      },
      QueryConditions: {
        PrivateMessageType: -1,
        TitleKeyWord: "",
        PagingInfo: {
          PageSize: -1,
          PageNo: 1
        }
      },
      MessageTypeList: [],
      ReadStatusList: [],
      CalendarDisabled: {
        from: new Date(moment().utcOffset(480).format("YYYY/MM/DD")) // Disable all dates after specific date
      },
      PageCount: 0,
      PageSize: 10
    }
  },
  mounted: function () {
    this.BindingEvent()
    this.GetMessageType()
    CommUtility.BindEnterTrigger("PrivateMessageQueryForm", this.UpdateQueryCondition)
  },
  methods: {
    BindingEvent: function () {
      document.addEventListener("click", this.HideDropDown)
    },
    GetMessageType: async function () {
      const data = await PersonalCenterService.PrivateMessage_LoadMainPage()

      if (data.Ret === BLDef.ErrorRetType.SUCCESS) {
        this.MessageTypeList = data.Data.PrivateMessageTypeList
        this.ReadStatusList = data.Data.PrivateMessageReadStatusList
        this.DisplayQueryObj.MessageType = data.Data.PrivateMessageTypeList[0]
        this.QueryConditions.PrivateMessageType = data.Data.PrivateMessageTypeList[0]
        this.QueryConditions.StartTick = CommUtility.LocalTimeToUtcTicks(this.DisplayQueryObj.StartTime)
        this.QueryConditions.EndTick = CommUtility.LocalTimeToUtcTicks(this.DisplayQueryObj.EndTime, true)
        this.GetDataList()
        this.$refs.messageTable.AddBorderState(0)
      }
    },
    // 顯示紀錄類型選單
    ShowDropDown: function () {
      if (this.DropDowning === false) {
        $(".down_box").slideDown(300)
        this.DropDowning = true
      } else {
        $(".down_box").slideUp(300)
        this.DropDowning = false
      }
    },
    // 隱藏紀錄類型選單
    HideDropDown: function (event) {
      const target = $(event.target)
      if (target.closest(".d_course").length === 0) {
        $(".down_box").slideUp(300)
        this.DropDowning = false
      }
    },
    // 選取紀錄類型
    SelectType: function (MessageType) {
      this.DisplayQueryObj.MessageType = MessageType
    },
    // 點擊搜索，更新搜尋條件
    UpdateQueryCondition: async function () {
      this.Submitted = true
      Object.keys(this.fields).forEach((key) => {
        this.fields[key].touched = true
      })
      const result = await this.$validator.validateAll()
      const anyFieldInvalid = this.CheckFied()

      if (anyFieldInvalid === true || result === false) {
        return
      }
      this.QueryConditions.PagingInfo.PageNo = 1
      this.QueryConditions.PrivateMessageType = this.DisplayQueryObj.MessageType
      this.QueryConditions.StartTick = CommUtility.LocalTimeToUtcTicks(this.DisplayQueryObj.StartTime)
      this.QueryConditions.EndTick = CommUtility.LocalTimeToUtcTicks(this.DisplayQueryObj.EndTime, true)
      this.GetMessageType()
      this.GetDataList()
    },
    // 驗證搜尋欄位正確性
    CheckFied: function () {
      let anyInvalid = false
      this.TimeSequenceInvalid = false
      // 開始日期大於結束日期
      if (new Date(this.DisplayQueryObj.StartTime) > new Date(this.DisplayQueryObj.EndTime)) {
        this.TimeSequenceInvalid = true
        anyInvalid = true
      }
      return anyInvalid
    },
    // 切換頁面時進行原條件搜尋
    /* GetCurrentPageData: async function (pageNo) {
            if (pageNo !== undefined) {
                this.QueryConditions.PagingInfo.PageNo = pageNo;
            }
            EventBus.$emit('GlobalLoadingTrigger', true);
            const data = await PersonalCenterService.PrivateMessage_Query(this.QueryConditions);
            EventBus.$emit('GlobalLoadingTrigger', false);
            EventBus.$emit('PrivateMessage_UpdateList', data.Data.PrivateMessageList);
            this.PageCount = Math.ceil(data.Data.PagingInfo.TotalCount / this.QueryConditions.PagingInfo.PageSize);
        }, // end GetCurrentPageData */

    // ********************************************************************************************
    GetCurrentPageData: async function (pageNo) {
      EventBus.$emit("PrivateMessage_UpdatePageNo", pageNo)
    }, // end GetCurrentPageData

    // 獲得清單
    GetDataList: async function () {
      EventBus.$emit("GlobalLoadingTrigger", true)
      const data = await PersonalCenterService.PrivateMessage_Query(this.QueryConditions)
      EventBus.$emit("GlobalLoadingTrigger", false)
      EventBus.$emit("PrivateMessage_UpdateList", data.Data.PrivateMessageList)
      this.PageCount = Math.ceil(data.Data.PagingInfo.TotalCount / this.PageSize)
    }, // end GetDataList
    // ***********************************************************************************************

    SwitchMode: function (Mode) {
      this.DisplayMode = Mode
    }
  },
  components: {
    MemberPrivateMessageTable,
    MemberPrivateMessageDetail,
    Datepicker,
    Paginate
  }
}
