const list = [
  "webMemberSummary",
  "webMemberDeposit",
  "webMemberFundManage",
  "webMemberPrivateMessage",
  "webMemberProfile",
  "webMemberTransactionRecord",
  "webMemberWithdrawal",
  "webBetRecord",
  "webMemberWithdrawalRecords",
  "webMemberDepositRecords",
  "webMemberAccountChangeRecords"
]

export default list
