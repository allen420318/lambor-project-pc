const modMessages = {
  foot_about_us: "Giới thiệu",
  foot_novice_course: "HƯỚNG DẪN",
  nav_live: "Live Casino",
  nav_game: "Điện tử",
  nav_lottery: "Thể thao",
  nav_betting: "Xổ số",
  nav_poker: "Poker",
  nav_promotions: "Khuyến mãi",
  nav_news: "Tin nóng",
  activity_enter_deposits: "Nạp tiền",
  activity_online_payment: "Thanh toán trực tuyến",
  activity_company_income: "Nạp qua thẻ ngân hàng",
  activity_not_yet: "Trang web chưa cập nhật chương trình khuyến mãi",
  abouts_casino:
    'Công ty luôn thực hiện một lý tưởng kể từ khi thành lập: "Thế giới sẽ mở đường cho những người có tầm nhìn và mơ ước", Vvn là tương lai của Việt Nam, Tương lai có nghĩa là "ước mơ," tầm nhìn xa ", hoặc" Viễn cảnh ", và ý nghĩa của thương hiệu Vvnbet chính là VISION IN VIETNAM , BEST BET IN VVNbet .  "Ước mơ và viễn cảnh chính là Người chơi Việt có thể hưởng thụ chiến thắng tại Vvn"',
  abouts_legal:
    "Công ty chúng tôi đã tham gia vào lĩnh vực giải trí từ năm 2010 và sắp bước vào năm thứ 10. Mặc dù mười năm không đặc biệt dài, đạt được cột mốc này cũng là một vinh dự và sự công nhận đối với chúng tôi. ",
  abouts_text1:
    'So với thị trường mười năm trước, chúng ta có thể nói rằng mình đã bắt đầu hơi muộn, nhưng theo quyết định và sự lãnh đạo chính xác của nhà lãnh đạo, chúng tôi luôn tuân thủ tư duy "thương hiệu độc nhất" có thể cạnh tranh với các thương hiệu trong ngành. Do đó, chúng tôi đã tạo ra một trang web game trực tuyến và liên tục tối ưu hóa đồng thời liên tục cải thiện các thông số kỹ thuật dịch vụ để  có thể đặt nền tảng sâu rộng trong một thị trường cạnh tranh cao. ',
  abouts_text2:
    "Chúng tôi không chỉ có một Đội ngũ chuyên gia ưu tú mà còn tập hợp được các lập trình viên chuyên nghiệp đến từ các nước khác nhau. Đối với vấn đề huấn luyện bồi dưỡng nhân tài thì chúng tôi áp dụng mô hình giáo dục hợp tác của các trường đại học nổi tiếng trên thế giới, đưa ra những khoản thưởng tài trợ và mức lương cho các nhân tài ưu tú, để những nhân tài này sau khi tốt nghiệp có thể học cách tận dụng hết khả năng vào đội ngũ của chúng tôi để phát huy sở trường, và trở thành hậu thuẫn mạnh nhất của chúng tôi để đưa ra các ý tưởng sáng tạo, ngoài ra trong bộ phận khai thác còn một nhóm nhân viên trực tuyến am hiểu sâu sắc về ngành giải trí luôn luôn lắng nghe nhu cầu của khách hàng và có sự tương tác trao đổi với đội ngũ lập trình, giúp cho sản phẩm của chúng tôi càng trở nên đa dạng và tiếp cận thị trường một cách thiết thực, khiến cho sản phẩm có thể giành được lòng tin cậy của khách hàng bấy lâu nay. ",
  agent_full_name: "Tên đầy đủ",
  agent_authentic: "Vui lòng nhập Tên đầy đủ của bạn",
  agent_legal_mobilePhone: "Chỉ có thể sử dụng số , dấu trừ, dấu cộng, phạm vi từ 6-30 ký tự ",
  agent_identifying_code: "Mã xác thực",
  agent_code_error: "Lỗi mã xác thực",
  agent_remarks: "Ghi chú",
  login_forget_pwd_rang: "Chỉ có thể sử dụng chữ cái và chữ số, phạm vi từ 4-12 ký tự",
  reg_enter_account: "Vui lòng nhập tài khoản của bạn",
  reg_use_numbers: "Chỉ có thể sử dụng số, không quá 6 ký tự",
  reg_identifying_code: "Mã xác thực",
  reg_update_information:
    "Thành sau khi các thành viên có hiệu quả, khách hàng chịu trách nhiệm về Hỗ trợ online, e-mail, hãy liên hệ chúng tôi trên trang web bất cứ lúc nào để cung cấp thông tin cá nhân được cập nhật cho Công ty.",
  user_irresistible:
    "8. Do trường hợp thảm họa bất khả kháng hoặc cố tình xâm nhập phá hoại, gây ra bởi những thiệt hại về trang web hoặc hỏng dữ liệu, dữ liệu trang web sẽ dựa vào dữ liệu cuối cùng sau khi được xử lý, vì vậy mỗi hội viên cố gắng lưu hoặc in dữ liệu nếu có thể, như vậy chúng tôi có thể đối chiếu khi có khiếu nại. Để tránh mọi tranh chấp, mỗi người dùng phải kiểm tra thông tin của tài khoản thành viên trong bất kỳ trò chơi nào tham gia trang web hoặc trước khi kết thúc trò chơi. Nếu có bất kỳ tình huống bất thường nào được tìm thấy, hãy liên hệ kịp thời với Hỗ trợ online. Nếu không, người dùng sẽ được coi là đồng ý và chấp nhận. Tất cả dữ liệu hoặc dữ liệu lịch sử của tài khoản phải tuân theo dữ liệu trong cơ sở dữ liệu của công ty và người dùng sẽ không phản đối. Và công ty có quyền sửa đổi tất cả các quy tắc mà không cần thông báo trước. ",
  tutorial_common_problem: "CÂU HỎI THƯỜNG GẶP",
  tutorial_game_questions: "Câu Hỏi Game",
  tutorial_withdraw_money: "Cách Thức Rút Tiền Như Thế Nào?",
  tutorial_withdrawal_commission: "Rút Tiền Có Mất Phí Giao Dịch Không?",
  tutorial_account_stolen:
    "Để ngăn chặn hành vi trộm cắp tài khoản, vui lòng thực hiện cài đặt mật khẩu phức tạp hơn và thay đổi mật khẩu thường xuyên; vui lòng tránh chơi trò chơi trên máy tính công cộng (quán cà phê Internet và khách sạn). Chẳng hạn như số tài khoản bị đánh cắp vui lòng liên hệ 24- Xiao trong khi Hỗ trợ online, nhân viên dịch vụ khách hàng sẽ giúp bạn lần đầu tiên để có những biện pháp cần thiết để khôi phục lại tài khoản bị đánh cắp.",
  member_optimal_activities:
    "Dựa trên tình hình thực tế tài khoản của bạn , hệ thống khuyến nghị các tùy chọn khuyến mãi tốt nhất như sau:",
  member_participating_activities: "Không tham gia vào Ưu đãi",
  member_order_number: "Số đơn lệnh",
  member_state: "Trạng thái",
  member_capital_status: "Trạng thái số dư",
  member_recreation: "Trò chơi",
  member_clause: "Cách thức nạp",
  member_sum_money: "Số tiền",
  member_amount_status: "Trạng thái số dư của các game",
  member_number_range: "Chỉ dùng chữ số và ký hiệu thường, phạm vi từ 6-20 ký tự",
  member_exchange_inquiry: "Kiểm tra số dư xuất nhập khoản",
  member_record_query: "Kiểm tra kỷ lục đặt cược",
  member_unassigned_color: "Kiểm tra kỷ lục chưa phát thưởng",
  member_BBS_informations: "Tin nhắn",
  member_change_password: "Sửa đổi mật khẩu",
  member_pen: "Lần",
  member_in_process: "đang được xữ lý",
  member_bank_cards: "Liên kết thẻ ngân hàng",
  member_platform_interior:
    "Thẻ ngân hàng đã liên kết có thể sử dụng để rút tiền, chuyển nội bộ trong các tài khoản game và các chức năng khác.",
  member_game_name: "Tên trò chơi",
  member_update_time: "Thời gian cập nhập kỷ lục đặt cược trên trang web sẽ tùy vào nhà phát hành game.",
  member_beijing_time: "Thời gian trên trang web sẽ tính theo múi giờ Bắc Kinh (GMT+8)",
  member_transaction_amount: "Số tiền giao dịch",
  member_page_before: "Trang trước",
  member_latter_page: "Trang tiếp theo",

  float_online_service: "Hỗ trợ online",
  tutorial_register_problem: "CÁCH ĐĂNG KÝ",
  tutorial_register_Description:
    'Nhấp vào nút Đăng Ký ngay góc phải trên cùng của trang chủ Nền tảng, vui lòng đọc và nhấn Đồng Ý "Điều Khoản Sử Dụng" Điền Thông Tin Đăng Ký tài khoản và nhấn Đăng Ký để hoàn tất.',
  tutorial_register_pay_problem: "Đăng Ký tài khoản có mất phí không?",
  tutorial_register_description: "Đăng Ký tài khoản Nền tảng Hoàn Toàn Miễn Phí.",
  tutorial_register_attention:
    "Quý Khách không thể thay đổi tên Truy Cập một khi việc đăng ký đã hoàn tất, Quý Khách vui lòng tự bảo quản và sử dụng tên Truy Cập một cách an toàn. Họ tên được cập nhật tại trang RÚT TIỀN phải trùng với chủ tài khoản ngân hàng, nếu không sẽ không thể tiến hành Rút tiền. Vui lòng cung cấp số điện thoại hợp lệ để bộ phận CSKH liên lạc một cách nhanh chóng.",
  information_modify: "CHỈNH SỬA THÔNG TIN CÁ NHÂN",
  information_data_problem: "Làm Cách Nào Để Chỉnh Sửa Thông Tin Cá Nhân?",
  information_data_description:
    'Nhấn vào tên Truy Cập trên cùng bên phải trang chủ, "THÔNG TIN TÀI KHOẢN" > "THÔNG TIN CÁ NHÂN" , để tiến hành chỉnh sửa thông tin, nhấn Cập Nhật để lưu thông tin.',
  information_password_problem: "Làm Cách Nào Để Đổi Mật Khẩu Đăng Nhập?",
  information_password_description:
    'Nhấn vào tên Truy Cập trên cùng bên phải trang chủ, "THÔNG TIN TÀI KHOẢN" > "ĐỔI MẬT KHẨU", chọn mục "MẬT KHẨU ĐĂNG NHẬP" tiến hành thay đổi Mật Khẩu.',
  information_Withdrawal_password_problem: "Làm Cách Nào để đổi Mật Khẩu Rút tiền?",
  information_Withdrawal_password_description:
    'Nhấp vào tên Truy Cập trên cùng bên phải trang chủ, "THÔNG TIN TÀI KHOẢN" > "ĐỔI MẬT KHẨU", chọn mục "MẬT KHẨU RÚT TIỀN" tiến hành thay đổi Mật Khẩu.',
  information_name_problem: "Làm Cách Nào Chỉnh Sửa Họ tên?",
  information_name_description: "Nếu Quý Khách muốn sửa đổi Họ tên vui lòng liên hệ bộ phận CSKH để tư vấn.",
  information_forget_password_problem: "Quên Mật Khẩu?",
  information_forget_password_description: "Quý Khách vui lòng liên hệ bộ phận CSKH để được hỗ trợ.",
  recharge_related: "GỬI TIỀN",
  recharge_way_problem: "Cách Gửi Tiền Vào Tài Khoản Như Thế Nào?",
  recharge_way_description:
    'Quý Khách sau khi Truy Cập vào Tài Khoản, nhấn vào "GỬI TIỀN" ngay góc phải trên cùng của trang chủ. Chọn cách thức Gửi tiền và làm theo hướng dẫn. Thời gian số dư được cập nhật vào tài khoản tuỳ vào cách thức Gửi tiền.',
  recharge_handlingfee_problem: "Phí Gửi Tiền?",
  recharge_handlingfee_description:
    "Quý Khách khi gửi tiền vào tài khoản, Nền tảng sẽ không thu bất kì phí giao dịch nào, Phí giao dịch do bên Ngân hàng thu.",
  recharge_amount_problem: "Hạn Mức Gửi Tiền?",
  recharge_amount_description:
    "Số tiền tối đa cho 1 lần giao dịch tuỳ vào mỗi Ngân hàng, Quý Khách khi giao dịch Gửi tiền vui lòng tham khảo hạn mức gửi tiền hiển thị bên dưới.",
  recharge_attention_problem: "LƯU Ý",
  recharge_attention_description1:
    "Số tài khoản Nền tảng luôn thay đổi thường xuyên, Quý Khách cần kiểm tra số tài khoản Nền tảng  trước khi gửi tiền, nếu Quý Khách chuyển tiền sai chủ tài khoản,Nền tảng sẽ không chịu trách nhiệm.",
  recharge_attention_description2:
    "Quý khách vui lòng lưu lại thông tin chuyển khoản trước khi xác nhận Gửi tiền thành công (như họ tên, số thẻ ngân hàng, thời gian và số tiền gửi, hoá đơn...)",
  wallet_related: "SỐ DƯ ",
  wallet_balance_problem: "KIỂM TRA SỐ DƯ",
  wallet_balance_description:
    'Nhấn vào tên Truy Cập trên cùng bên phải trang chủ, "THÔNG TIN TÀI KHOẢN" > "SỐ DƯ" để kiểm tra số dư các sản phẩm Game.',
  wallet_conversion_problem: "Chuyển Đổi Số Dư Bằng Cách Nào?",
  withdrawal_description:
    'Quý Khách sau khi đăng nhập, Nhấn vào tên Truy Cập trên cùng bên phải trang chủ, "THÔNG TIN TÀI KHOẢN" > "TÀI KHOẢN NGÂN HÀNG" > "THÔNG TIN NGÂN HÀNG, vui lòng điền đầy đủ thông tin chủ Tài Khoản.RÚT TIỀN: "THÔNG TIN TÀI KHOẢN" > "RÚT TIỀN" > "LỆNH RÚT TIỀN", Chọn Thẻ/Nhập Số Tiền/Xác Nhận/Nhập Mật Khẩu Rút Tiền/Xác Nhận.',
  withdrawal_fail_problem: "Rút Tiền Thất Bại",
  withdrawal_fail_description: "Nguyên nhân Rút tiền thất bại:",
  withdrawal_fail_description1: "Số tiền Rút nhiều hơn số dư tài khoản hiện có",
  withdrawal_fail_description2: "Sai mật khẩu",
  withdrawal_fail_description3: "Thông Tin Tài khoản Ngân hàng không hợp lệ",
  withdrawal_fail_description4: "Sai Số thẻ, Sai Họ tên,  Ngân hàng trả lệnh",
  withdrawal_fail_description5: "Nếu Quý Khách Rút tiền thất bại xin vui lòng liên hệ bộ phận CSKH để được tư vấn.",
  withdrawal_attention_problem: "Lưu Ý Khi Rút Tiền",
  withdrawal_attention_description:
    'Dưới sự Giám Sát của Chính Phủ PHILIPPINES và Chính Sách "Phòng Chống Rửa Tiền", Quý Khách phải phù hợp các điều kiện sau mới có thể tiến hành Rút tiền:',
  withdrawal_attention_description1:
    '1. Điều kiện Rút Tiền: "Tổng tiền Cược phải bằng Tổng tiền Nạp"Ví dụ: Nạp 200.000 phải Cược đủ 200.000 ',
  withdrawal_attention_description2:
    "2. Sau khi Rút tiền, Tổng tiền ván Cược mới chưa bằng Tổng tiền lần Nạp tiếp theo, không thể tiến hành Rút tiền.Lưu ý: Sau khi đạt điều kiện Rút tiền nhưng Quý Khách không thực hiện giao dịch Rút tiền ngay, mà tiếp tục Nạp thêm 300.000, lúc này muốn Rút tiền thì cần phải Cược đủ 300.000 mới Nạp vào mới có thể Rút tiền.",
  withdrawal_attention_description3:
    '3. Tài khoản Ngân hàng của người nhận tiền phải trùng khớ với mục "ThÔNG TIN NGÂN HÀNG" đã cập nhật trước đó tại Tài Khoản Nền tảng.',
  withdrawal_handlingfee_description:
    "Khi người chơi đạt đến Số tiền cược quy định, Rút tiền sẽ không bị mất phí, số lần giao dịch Rút tiền trong ngày không bị giới hạn.",
  withdrawal_arrivaltime_problem: "Thời Gian Rút Tiền Thành Công",
  withdrawal_arrivaltime_description:
    'Trong giờ làm việc hành chính của ngân hàng, tất cả giao dịch Rút tiền được xử lý thành công trong vòng 2 tiếng, Quý Khách có thể theo dõi trạng thái ở mục "RÚT TIỀN"',
  inquire_related: "KIỂM TRA LỊCH SỬ",
  inquire_transaction_problem: "Lịch Sử Giao Dịch",
  inquire_transaction_description:
    '"THÔNG TIN TÀI KHOẢN" > "TÓM TẮT" có thể kiểm tra tổng thể thông tin của tài khoản.',
  inquire_financial_problem: "Lịch Sử Giao Dịch",
  inquire_financial_description:
    ' "THÔNG TIN TÀI KHOẢN" > "SAO KÊ" > "LỊCH SỬ GIAO DỊCH" Quý Khách có thể điều chỉnh Thời Gian và Loại Giao Dịch để tiến hành kiểm tra Lịch Sử Giao Dịch.',
  inquire_bet_problem: "Lịch Sử Cược",
  inquire_bet_description:
    '"THÔNG TIN TÀI KHOẢN" > "SAO KÊ" > "LỊCH SỬ CƯỢC" Quý Khách có thể điều chỉnh thời gian và Loại Giao Dịch để tiến hành kiểm tra Lịch Sử Cược.',
  activity_related: "KHUYẾN MÃI",
  activity_update_problem: "Cập Nhật Thông Tin Khuyến Mãi Mới Nhất",
  play_start_problem: "Cách Vào Game",
  play_start_description:
    "Quý Khách sau khi đăng nhập, nhấn chọn các sản phẩm Game hiển thị tại trang chủ, phía dưới sẽ hiện ra hàng loạt các sản phẩm.",
  play_dropped_problem: "Mất Kết Nối",
  play_dropped_description:
    "Nếu người chơi bị mất kết nối khỏi hệ thống sau khi Cược, Ván Cược đó sẽ được tính khi mã số Cược được hiển thị, hệ thống sẽ dựa vào kết quả ván bài đó để phát thưởng. Khi đăng nhập lại, Quý Khách vui lòng kiểm tra Lịch Sử Cược, Số dư và kết quả Cược, nếu có thắc mắc vui lòng liên hệ bộ phận CSKH để được hỗ trợ.",
  play_loadflash_problem: "Không Thể Tải FLASH GAME",
  play_loadflash_description:
    "Quý Khách vui lòng kiểm tra phiên bản Adobe Flash Player trên trình duyệt để tiến hành cập nhật phiên bản mới nhất.",
  agent_alliance: "Gia nhập đại lý",
  top_agentJoin: "Đại lý",
  agent_qq: "TK zalo",
  top_cash_advance: "Rút tiền ngay",
  hot_game: "Trò nóng",
  go_top: "Quay lại đỉnh",
  fair_play: "Chơi đẹp",
  agent_word_remark: "Chỉ có thể sử dụng chữ cái, số, khoảng trống và ký hiệu phổ biến . Giới hạn từ 100 ký tự",
  agent_contant_us_remark: "Chỉ có thể sử dụng chữ cái, số, khoảng trống và ký hiệu phổ biến . Giới hạn từ 2-500 ký tự",
  index_technical_service: "Dịch vụ kỹ thuật",
  index_hour: "Giờ",
  index_safe_technology: "Kỹ thuật an toàn vượt trội",
  index_average_time: "Trung bình thời gian",
  news_understanding_details: "Tìm hiểu chi tiết",
  activity_lists: "Danh sách khuyển mãi",
  latest_news: "Tin mới nhất",
  news_details: "Chi tiết tin nhắn",
  index_ultimate_experience: "Trải nghiệm tuyệt đỉnh",
  index_do_best: "Làm tốt hơn",
  index_brilliant: "Tuyệt vời",
  index_open_new_era: "Kỷ nguyên mới về cá cược",
  information_recall:
    'Nhấn vào tên Truy Cập trên cùng bên phải trang chủ, "THÔNG TIN TÀI KHOẢN" > "SỐ DƯ" > "THU HỒI SỐ DƯ", thực hiện thao tác này toàn bộ Số dư trong các sản phẩm Game sẽ được Thu Hồi về Tài Khoản chính.',
  information_common: "chung",
  information_problem: "vấn đề",
  index_most_popular_game: "Trang wed game hiện đại và sản phẩm trò chơi bùng cháy nhất",
  understand_more: "Hiểu thêm",
  our_strengths: "Lợi thế của chúng tôi",
  most_entertaining_online_chess:
    "Thương hiệu cờ vua trực tuyến thú vị nhất thế giới, nơi tụ tập của hai triệu người yêu thích poker",
  commission: "Ủy ban",
  website_home_page: "trang chủ",
  home: "Nhà",
  gather_details: "Thu thập chi tiết",
  member_sumbitAccount_number: "Xác nhận tài khoản ngân hàng:"
}

export default modMessages
