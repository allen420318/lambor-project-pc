// import 'jquery.marquee';
// import swiper from 'swiper';
// import 'swiper/dist/css/swiper.min.css';
import "@/assets/scripts/customJs/web/jquery.SuperSlide.2.1.3"
import moment from "moment"
import Vue from "vue"
import BLDef from "@/assets/scripts/common/BLDef"
import CommUtility from "@/assets/scripts/common/CommUtility"
import EventBus from "scripts/common/EventBus"
import CommonService from "scripts/businessLogic/commonService"
// import SplitString from "./splitString"
export const initMarquee = {
  data() {
    return {
      MQTimer: undefined,
      MarqueeKey: "webMarquee",
      MarqueeFlag: undefined,
      MarqueeStartTick: +Infinity,
      marqueeShowList: []
    }
  },
  // props: {
  //     IsLogin: undefined
  // },
  created: async function () {
    const self = this
    EventBus.$on("SignalR_MaintenanceFinish_Notify", (data) => {
      self.MaintenanceFinishNotify(data)
    })
    EventBus.$on("SignalR_UpdateMarquee", (data) => {
      this.MarqueeStartTick = +Infinity
      self.UpdateMarquee(data)
    })

    // 2018.10.26 SeyoChen 修改需求--没有登录依然可以看跑马灯
    await this.GetMarqueeData()
  },
  mounted: function () {},
  methods: {
    BindMarqueeEvent: function () {
      const self = this
      self.MarqueeFlag = true
      const continueFlag = self.checkMarquee()
      if (continueFlag === false) {
        self.MarqueeFlag = false
        self.DealMarqueeData()
      } else {
        this.$nextTick(function () {
          $(".slideTxtBox").slide({
            mainCell: ".bd ul",
            effect: "topLoop",
            autoPlay: true,
            interTime: 5000
          })
        })
      }
    },
    GetMarqueeData: async function () {
      $(".slideTxtBox .db ul").html("")
      const DataObj = {
        WGUID: Vue.prototype.$WGUID
      }
      const data = await CommonService.Comm_GetMarqueeInfo(DataObj)
      if (data.Ret === BLDef.ErrorRetType.SUCCESS) {
        localStorage.setItem(this.MarqueeKey, JSON.stringify(data.Data.MarqueeList))
        this.DealMarqueeData()
      } else {
      }
    },
    // 處理跑馬燈資料
    DealMarqueeData: function () {
      const self = this
      // 取得storage中的跑馬燈資訊
      const playFlag = self.checkMarquee()

      if (playFlag === true) {
        this.BindMarqueeEvent()
      } else {
        if (this.MarqueeStartTick != +Infinity) {
          this.MQTimer = setTimeout(this.DealMarqueeData, this.MarqueeStartTick)
        }
      }
    },
    // 檢查跑馬燈
    checkMarquee: function () {
      const marqueeStr = localStorage.getItem(this.MarqueeKey)
      this.marqueeShowList = []
      $(".slideTxtBox .db ul").html("")

      if (marqueeStr) {
        const marqueeList = JSON.parse(marqueeStr)
        const marqueeKeepArray = []
        // 組成跑馬燈內容
        for (let i = 0, len = marqueeList.length; i < len; i += 1) {
          const Item = marqueeList[i]
          if (
            (Item.StartTick < moment().valueOf() && Item.EndTick > moment().valueOf()) ||
            Item.IsRemoveByTime === false
          ) {
            switch (Item.Type) {
              // 緊急公告
              case BLDef.BroadcastMsgType.EMERGENCY:
                Item.Msg = Item.Msg
                break
              // 維護
              case BLDef.BroadcastMsgType.MAINTAIN:
                Item.Msg = Item.Msg.replace(
                  "{StartTime}",
                  CommUtility.UtcTicksToLocalTime(Item.StartTick, "YYYY/MM/DD HH:mm:ss")
                ).replace("{EndTime}", CommUtility.UtcTicksToLocalTime(Item.EndTick, "YYYY/MM/DD HH:mm:ss"))
                break
              default:
                break
            } // end switch
            let len = 80 // 跑马灯每条数据最长150个字节
            let splitArr = SplitString(Item.Msg, len)
            splitArr.forEach((str) => {
              let obj = JSON.parse(JSON.stringify(Item))
              obj.Msg = str
              marqueeKeepArray.push(obj)
            })
            // if (Item.Msg.length > 33) {
            //     let obj1 = {
            //         EndTick: Item.EndTick,
            //         ID: Item.ID,
            //         IsRemoveByTime: Item.IsRemoveByTime,
            //         Msg: Item.Msg.slice(0, 33),
            //         Priority: Item.Priority,
            //         StartTick: Item.StartTick,
            //         Type: Item.Type,
            //         UpdateTick: Item.UpdateTick
            //     }
            //     let obj2 = {
            //         EndTick: Item.EndTick,
            //         ID: Item.ID + '_Excess part',
            //         IsRemoveByTime: Item.IsRemoveByTime,
            //         Msg: Item.Msg.slice(33),
            //         Priority: Item.Priority,
            //         StartTick: Item.StartTick,
            //         Type: Item.Type,
            //         UpdateTick: Item.UpdateTick
            //     }
            //     marqueeKeepArray.push(obj1);
            //     marqueeKeepArray.push(obj2);
            // } else {
            //     marqueeKeepArray.push(Item);
            // }
          } else if (Item.StartTick > moment().valueOf()) {
            let len = 150 // 跑马灯每条数据最长150个字节
            let splitArr = SplitString(Item.Msg, len)
            splitArr.forEach((str) => {
              let obj = JSON.parse(JSON.stringify(Item))
              obj.Msg = str
              marqueeKeepArray.push(obj)
            })
            // if (Item.Msg.length > 33) {
            //     let obj1 = {
            //         EndTick: Item.EndTick,
            //         ID: Item.ID,
            //         IsRemoveByTime: Item.IsRemoveByTime,
            //         Msg: Item.Msg.slice(0, 33),
            //         Priority: Item.Priority,
            //         StartTick: Item.StartTick,
            //         Type: Item.Type,
            //         UpdateTick: Item.UpdateTick
            //     }
            //     let obj2 = {
            //         EndTick: Item.EndTick,
            //         ID: Item.ID + '_Excess part',
            //         IsRemoveByTime: Item.IsRemoveByTime,
            //         Msg: Item.Msg.slice(33),
            //         Priority: Item.Priority,
            //         StartTick: Item.StartTick,
            //         Type: Item.Type,
            //         UpdateTick: Item.UpdateTick
            //     }
            //     marqueeKeepArray.push(obj1);
            //     marqueeKeepArray.push(obj2);
            // } else {
            //     marqueeKeepArray.push(Item);
            // }

            // 判斷最近要觸發的跑馬燈倒數時間
            if (this.MarqueeStartTick > Item.StartTick - moment().valueOf()) {
              this.MarqueeStartTick = Item.StartTick - moment().valueOf() + 1000
            } // end if
          }
        }

        localStorage.setItem(this.MarqueeKey, JSON.stringify(marqueeKeepArray))

        // 设置定时器到点进行加入相关的跑马灯 20190828 SeyoChen
        const nowTime = Date.parse(new Date())
        // let setTime = 0;
        let lastTime = 10000000000000
        for (let i = 0; i < marqueeKeepArray.length; i++) {
          if (
            lastTime > marqueeKeepArray[i].StartTick &&
            marqueeKeepArray[i].StartTick - nowTime > 0 &&
            marqueeKeepArray[i].EndTick - nowTime > 0
          ) {
            if (lastTime > marqueeKeepArray[i].EndTick) {
              lastTime = marqueeKeepArray[i].EndTick
            } else {
              lastTime = marqueeKeepArray[i].StartTick
            }
          }
          if (marqueeKeepArray[i].StartTick - nowTime < 0 && marqueeKeepArray[i].EndTick - nowTime > 0) {
            this.marqueeShowList.push(marqueeKeepArray[i])
            // setTimeout(() => {
            //     this.checkMarquee();
            // }, marqueeKeepArray[i].StartTick - nowTime);
          }
          // else {
          //     this.marqueeShowList.push(marqueeKeepArray[i]);
          // }
        }
        // console.log('||||||||||||||||||||||||||||||||||||||||||||||||22222yes');
        // console.log(setTime);

        // setTimeout(() => {
        //   this.DealMarqueeData();
        // }, setTime - nowTime);

        // 设置定时器到点进行加入相关的跑马灯 20190828 SeyoChen end

        setTimeout(() => {
          this.checkMarquee()
        }, lastTime)

        // 將文字丟到跑馬燈上
        // this.marqueeShowList = marqueeKeepArray;

        // 更新storage跑馬燈資訊
        if (marqueeKeepArray.length > 0) {
          return true
        }

        // 沒有跑馬燈就移除localStorage
        if (marqueeKeepArray.length == 0) {
          this.MarqueeStartTick = +Infinity
          localStorage.removeItem(this.MarqueeKey)
        } // end if
      }

      return false
    },

    // signalr通知新的跑馬燈部分
    UpdateMarquee: async function () {
      // await this.GetMarqueeData();
      if (this.MarqueeFlag !== true) {
        clearTimeout(this.MQTimer)
        this.DealMarqueeData()
      }
    },
    // signalr結束維護通知
    MaintenanceFinishNotify: function (data) {
      if (data.Data.MarqueeMessage.ID && data.Data.MarqueeMessage.Type == BLDef.BroadcastMsgType.MAINTAIN) {
        const playArray = []
        let marqueeList = []
        const marqueeStr = localStorage.getItem(this.MarqueeKey)
        if (marqueeStr) {
          marqueeList = JSON.parse(marqueeStr)
        }

        // 更新跑馬燈列表
        for (let i = 0; i < marqueeList.length; i++) {
          if (!marqueeList[i].ID || marqueeList[i].ID != data.Data.MarqueeMessage.ID) {
            playArray.push(marqueeList[i])
          }
        }

        localStorage.setItem(this.MarqueeKey, JSON.stringify(playArray))
      }
    }
  },
  watch: {
    // IsLogin: async function () {
    //     if (this.IsLogin === true) {
    //         await this.GetMarqueeData();
    //         this.DealMarqueeData();
    //     }
    // }
  },
  beforeDestroy() {
    EventBus.$off("SignalR_MaintenanceFinish_Notify")
    EventBus.$off("SignalR_UpdateMarquee")
  }
}
