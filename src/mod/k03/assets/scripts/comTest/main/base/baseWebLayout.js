import BLDef from "@/assets/scripts/common/BLDef"
import EventBus from "scripts/common/EventBus"
import CommonService from "scripts/businessLogic/commonService"

export const baseWeLa = {
  data() {
    return {
      NotifyObj: {},
      IsLogin: undefined
    }
  },
  created: function () {
    this.StopCtrlKey()
    this.CheckIsLogin()
    const self = this
    EventBus.$on("SignalR_Maintenance_Notify", (data) => {
      self.MaintenanceNotify(data)
    })
    EventBus.$on("SignalR_Logout_OtherPageLogout", () => {
      self.GlobalLogout()
    })
  },
  methods: {
    // 檢查登入狀態
    CheckIsLogin: async function () {
      const data = await CommonService.Comm_CheckPermission()
      switch (data.Status) {
        case BLDef.SysAccountStatus.NOT_LOGIN:
          this.IsLogin = false
          break
        case BLDef.SysAccountStatus.LOGINED_ENABLED:
        case BLDef.SysAccountStatus.LOGINED_FROZEN:
          this.IsLogin = true
          break
        default:
          break
      }
    },
    // 維護通知
    MaintenanceNotify: function (data) {
      switch (data.Data.MaintainType) {
        case BLDef.MaintainTypesEnum.AllSYS:
        case BLDef.MaintainTypesEnum.GameFrontEnd:
          window.location.href = data.Data.NotifyData.Url
          break
        case BLDef.MaintainTypesEnum.GameApi:
          break
        case BLDef.MaintainTypesEnum.CashFlowProvider:
          break
        default:
          break
      }
    },
    // signalr通知進行登出
    GlobalLogout: function () {
      window.location.href = "/"
      //   this.$store.dispatch('setLoginState', false);
    },
    StopCtrlKey: function () {
      // 禁止滚轮缩放
      const isFirefox = /Firefox/i.test(navigator.userAgent)
      const unMouseBtn = function (e) {
        const aftere = e || window.event
        if (aftere.wheelDelta && aftere.ctrlKey) {
          // IE/Opera/Chrome e.wheelDelta±120
          if (aftere.preventDefault) {
            aftere.preventDefault()
          } else {
            aftere.returnValue = false
          }
        } else if (aftere.detail && aftere.ctrlKey) {
          // Firefox e.detail±3
          if (aftere.preventDefault) {
            aftere.preventDefault()
          } else {
            aftere.returnValue = false
          }
        }
      }
      const mousewheelevt = isFirefox ? "DOMMouseScroll" : "mousewheel"
      if (document.attachEvent) {
        // IE/Opera/Chrome
        document.attachEvent("on" + mousewheelevt, unMouseBtn)
      } else if (document.addEventListener) {
        // Firefox
        document.addEventListener(mousewheelevt, unMouseBtn, false)
      }
      const unCtrl = function (e) {
        const aftere = e || window.event
        const code = isFirefox ? "109,107,173,61" : "109,107,189,187"
        if (aftere.ctrlKey && code.indexOf(aftere.keyCode) != -1) {
          if (aftere.preventDefault) {
            aftere.preventDefault()
          } else {
            aftere.returnValue = false
          }
        }
      }
      document.onkeydown = unCtrl
    }
  },
  watch: {
    $route(to, from) {
      $(".bg").hide()
      console.log(to + from)
    }
  },
  beforeDestroy() {
    EventBus.$off("SignalR_Maintenance_Notify")
    EventBus.$off("SignalR_Logout_OtherPageLogout")
  }
}
