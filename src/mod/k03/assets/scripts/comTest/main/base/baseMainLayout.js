import FrameFooter from "@/mod/k03/components/public/frameFooter.vue"
import rssHeader from "@/mod/k03/components/general/header/header.vue"
import LamborSide from "@/mod/k03/components/public/lamborSide.vue"
import BLDef from "@/assets/scripts/common/BLDef"
import CommonService from "scripts/businessLogic/commonService"
import LamborMarquee from "@/mod/k03/components/public/marquee"
import ForgetPassword from "@/mod/k03/components/sub/login/forgetPassword"
import ForgetPasswordSuccess from "@/components/sub/login/forgetPasswordSuccess"
import ForgetPasswordFail from "@/components/sub/login/forgetPasswordFail"
import NewIndexService from "scripts/businessLogic/newIndexService"
import Vue from "vue"
export const baseMnLt = {
  components: {
    FrameFooter,
    // MainHeader,
    // FrameNav,
    rssHeader,
    // floatService,
    LamborSide,
    LamborMarquee,
    ForgetPassword,
    ForgetPasswordSuccess,
    ForgetPasswordFail
  },
  data() {
    return {
      IsLogin: undefined,
      OnlinePeople: undefined
    }
  },
  created: function () {
    this.CheckIsLogin()
    this.showNumberOnline()
    // this.$nextTick(function () {
    //   function winScroll() {
    //     const topLeft = document.documentElement.scrollLeft;
    //     $('.header').css('left', -topLeft + 'px');
    //   }
    //   window.onscroll = winScroll;
    // });
  },
  methods: {
    // 檢查登入狀態
    CheckIsLogin: async function () {
      const data = await CommonService.Comm_CheckPermission()
      switch (data.Status) {
        case BLDef.SysAccountStatus.NOT_LOGIN:
          this.IsLogin = false
          break
        case BLDef.SysAccountStatus.LOGINED_ENABLED:
        case BLDef.SysAccountStatus.LOGINED_FROZEN:
          this.IsLogin = true
          break
        default:
          break
      }
    },
    // 显示在线人数
    showNumberOnline: async function () {
      console.log("#333")
      const DataObj = {
        WGUID: Vue.prototype.$WGUID
      }
      const retData = await NewIndexService.HomePageLoadRankAndOnlineNumPage(DataObj)
      console.log("在线人数")
      console.log(DataObj)
      console.log(retData)
      console.log("在线人数")
      if (retData.Ret === 0) {
        this.OnlinePeople = retData.Data.OnlineNum
        // sessionStorage.setItem('OnlinePeopleNum', this.OnlinePeople)
        // EventBus.$emit('OnlinePeopleNum', this.OnlinePeople);
      }
    }
  }
}
