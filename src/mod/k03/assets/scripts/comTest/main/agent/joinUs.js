import RegEx from "scripts/common/CommDef"
import RegisterService from "scripts/businessLogic/registerService"
// import BLDef from "scripts/common/BLDef"
import Vue from "vue"
import CommService from "scripts/businessLogic/commonService"
import Datepicker from "@/components/main/common/datePicker"
// import Datepicker from 'vuejs-datepicker';
import EventBus from "scripts/common/EventBus"
import moment from "moment"
import CommUtility from "@/assets/scripts/common/CommUtility"
import JoinUsSuccess from "@/components/sub/agent/joinUsSuccess"

export const JoinUs = {
  data() {
    return {
      activeSex: undefined,
      // 正則表達式
      Reg: RegEx.Regex,

      // post用的model
      PostDataModel: {
        Name: "",
        PhoneNo: "",
        Email: "",
        CardName: "",
        BankName: "",
        BankProvince: "",
        BankCity: "",
        BankDistrict: "",
        BankBranchName: "",
        BankAccnt: "",
        WeChatNo: "",
        QQNo: "",
        Remark: "",
        AuthCode: ""
      },

      // 載入頁面
      LoadAddPageModel: {
        ProvinceSelectList: {},
        TownSelectList: {},
        DistSelectList: {}
      },

      // 判斷性別未填寫的錯誤旗標
      PostDataModelSexErrorIsShow: undefined,
      // 銀行欄位未填寫的錯誤旗標
      PostDataModelBankDropdownErrorIsShow: false,
      // 下拉式選單的下拉判斷旗標
      DropDowning: "",
      // 下拉選單是否有未填
      DropDownInvalid: undefined,
      // 驗證碼
      AuthImg: "",
      // PreToken
      PreToken: "",

      CalendarDisabled: {
        from: new Date(moment().add(1, "days").format("YYYY/MM/DD")) // Disable all dates after specific date
      },

      FlagObj: false,
      joinUsAddFlag: true,

      // 驗證碼顏色設定
      RGB: {
        R: 255,
        G: 100,
        B: 255
      },
      CheckPhoneNoAvailable: {
        PhoneNo: "",
        WGUID: ""
      },
      SiteEmail: Vue.prototype.$SiteEmail,
      defaultImg: 'this.src="' + this.$ResourceCDN + '/EditionImg/LamborFormal/images/public/refresh.png"'
    }
  },

  created: async function () {
    // this.BindingEvent();
    this.LoadPage()
    this.GetAuthCode()
  },
  mounted: function () {
    CommUtility.BindEnterTrigger("JoinUsForm", this.AddIB)
    $(".qe_d_course").click(function () {
      $(this).parent(".qe_down").siblings(".qe_down").find(".qe_down_box").slideUp(500)
      $(this).siblings(".qe_down_box").slideToggle(500)
      $(this)
        .siblings(".qe_down_box")
        .children(".qe_down_list")
        .click(function () {
          var course = $(this).html()
          $(this).parent(".qe_down_box").siblings(".qe_d_course").html(course)
          $(this).parent(".qe_down_box").slideUp(500)
        })
    })
    $(".a-j-sex li").click(function () {
      $(this).addClass("on").siblings().removeClass("on")
    })
  },
  methods: {
    // 取得驗證碼
    GetAuthCode: async function () {
      /* eslint-disable camelcase */
      const VCode_CharacterRGB = this.RGB
      const RGBData = {
        VCode_CharacterRGB
      }
      const retData = await CommService.VerifyCode_Gen(RGBData)
      this.AuthImg = retData.Data.VCode
      this.PreToken = retData.Data.ID
    }, // end GetAuthCode

    // 載入頁面
    LoadPage: async function () {
      const dataObj = {}
      const retData = await RegisterService.LoadIBRegisterMainPage(dataObj)

      // 失敗
      if (retData.Ret != 0) {
        return
      } // end if

      // 成功
      this.LoadAddPageModel = retData.Data
      // this.PostDataModel.BankProvince = this.LoadAddPageModel.ProvinceSelectList[0];
      // this.PostDataModel.BankCity = this.LoadAddPageModel.TownSelectList[0];
      // this.PostDataModel.BankDistrict = this.LoadAddPageModel.DistSelectList[0];
      this.PostDataModel.Sex = {
        Value: -1
      } // default value
    }, // end LoadPage

    // 綁定隨機點擊事件(讓下拉選單恢復)
    BindingEvent: function () {
      document.addEventListener("click", this.HideDropDown)
    }, // end BindingEvent

    // 顯示紀錄類型選單
    ShowDropDown: function (targetClass) {
      $(".down_box").slideUp(100)
      if (this.DropDowning !== targetClass) {
        $("." + targetClass).slideDown(300)
        this.DropDowning = targetClass
      } else {
        $("." + targetClass).slideUp(100)
        this.DropDowning = ""
      }
    }, // end ShowDropDown

    // 隱藏紀錄類型選單
    HideDropDown: function (event) {
      const target = $(event.target)
      if (target.closest(".d_course").length === 0) {
        $(".down_box").slideUp(100)
        this.DropDowning = ""
      }
    }, // end HideDropDown

    // 選擇省並變更其他選單資料
    // ChangeProvince: async function (item) {
    //     const dataObj = {
    //         SType: BLDef.SelectTypesDef.CITIES,
    //         InputValues: [item.Value]
    //     };
    //     const retData = await CommService.GetCityTypes(dataObj);
    //     if (retData.Ret == 0) {
    //         this.PostDataModel.BankProvince = item;
    //         this.LoadAddPageModel.TownSelectList = retData.Data.SelectList;
    //         this.PostDataModel.BankCity = retData.Data.SelectList[0];
    //         this.PostDataModel.BankDistrict = this.LoadAddPageModel.DistSelectList[0];
    //         this.LoadAddPageModel.DistSelectList = [this.LoadAddPageModel.DistSelectList[0]];
    //     } // end if
    // }, // end ChangeProvince

    // 選擇城市並變更其他選單資料
    // ChangeTown: async function (item) {
    //     const dataObj = {
    //         SType: BLDef.SelectTypesDef.DISTS,
    //         InputValues: [item.Value]
    //     };
    //     const retData = await CommService.GetDistTypes(dataObj);
    //     if (retData.Ret === 0) {
    //         this.PostDataModel.BankCity = item;
    //         this.LoadAddPageModel.DistSelectList = retData.Data.SelectList;
    //         this.PostDataModel.BankDistrict = retData.Data.SelectList[0];
    //         if (this.LoadAddPageModel.DistSelectList.length === 1) {
    //             this.DropDownInvalid = true;
    //         } // end if
    //     } // end if
    // }, // end ChangeTown

    // 選擇區域
    // ChangeDist: function (item) {
    //     this.PostDataModel.BankDistrict = item;
    //     if (item.Value !== '-1') {
    //         this.DropDownInvalid = true;
    //     }
    // }, // end ChangeDist

    // 檢查性別RadioButton
    CheckSexRadio: function (index) {
      this.activeSex = index
      this.PostDataModelSexErrorIsShow = true
    }, // end CheckSexRadio

    // 所有欄位驗證
    validate: function () {
      Object.keys(this.fields).forEach((key) => {
        this.fields[key].touched = true
      })
      return this.$validator.validateAll().then((result) => {
        return result
      })
    }, // end validate

    AddIB: async function () {
      if (this.joinUsAddFlag) {
        $(":focus").blur()
        let anyInvalid = false

        const validate = await this.validate()

        // 驗證所有欄位
        if (!validate) {
          anyInvalid = true
        } // end if

        // // 驗證開戶網點下拉式選單
        // if (!this.DropDownInvalid || this.PostDataModel.BankProvince.Value == -1 || this.PostDataModel.BankCity.Value == -1 || (this.PostDataModel.BankDistrict.Value == -1 && !this.DropDownInvalid)) {
        //     this.DropDownInvalid = false;
        //     anyInvalid = true;
        // } // end

        // 驗證性別欄位
        if (!this.PostDataModelSexErrorIsShow) {
          this.PostDataModelSexErrorIsShow = false
          anyInvalid = true
        } // end if

        if (anyInvalid === true) {
          return
        } // end if

        if (this.FlagObj === true) {
          return
        } // end if
        this.FlagObj = true
        // 注册手机号进行检测 会员手机号不能重复
        this.CheckPhoneNoAvailable.PhoneNo = this.PostDataModel.PhoneNo
        this.CheckPhoneNoAvailable.WGUID = this.$WGUID
        const DataInput = {
          CheckPhoneNo: this.CheckPhoneNoAvailable
        }
        this.joinUsAddFlag = false
        const CheckPhoneNo = await CommService.Comm_CheckPhoneNoAvailable(DataInput)

        if (CheckPhoneNo.Ret === 0 && CheckPhoneNo.Data.CheckPhoneNo.IsAvailable === false) {
          const notifyData = {
            NotifyMessage: this.$t("message.agent_alike_mobilePhone")
          }
          EventBus.$emit("showNotifyMessage", notifyData)
          this.FlagObj = false
          this.joinUsAddFlag = true
          return
        } else if (CheckPhoneNo.Ret === 19) {
          const notifyData = {
            NotifyMessage: this.$t("message.member_phoneNumber_error"),
            NotifySubMessage: this.$t("message.agent_legal_mobilePhone")
          }
          EventBus.$emit("showNotifyMessage", notifyData)
          this.FlagObj = false
          this.joinUsAddFlag = true
          return
        }
        // 注册手机号进行检测 会员手机号不能重复

        // 時間資料轉換
        if (this.PostDataModel.Birth !== undefined) {
          this.PostDataModel.Birth = moment(this.PostDataModel.Birth).format("YYYY/MM/DD")
        }

        const dataObj = {
          VCodeID: this.PreToken,
          VCode: this.PostDataModel.AuthCode,
          WGUID: Vue.prototype.$WGUID,
          PromotionUrl: window.location.href, // new $window.URL($location.absUrl()).origin, // 完整DomainName含參數
          PromotionInfo: this.$route.params.Data,
          IB: this.PostDataModel,
          RegisterWay: 1 // 来源方式：1、链接，2、QRCode
        }
        this.joinUsAddFlag = false
        EventBus.$emit("GlobalLoadingTrigger", true)
        const retData = await RegisterService.AddIB(dataObj)
        EventBus.$emit("GlobalLoadingTrigger", false)

        const notifyData = {
          NotifyMessage: retData.Message
        }

        // 失敗
        if (retData.Ret != 0) {
          this.FlagObj = false
          this.joinUsAddFlag = true

          // 秀出錯誤訊息
          // 站长被停用 2019.06.25 ququ
          if (retData.Ret === 100014) {
            notifyData.NotifyMessage = this.$t("message.agent_stop_using")
          }
          // 站长被停用 2019.06.25 ququ end
          EventBus.$emit("showNotifyMessage", notifyData)
          this.PostDataModel.AuthCode = ""
          this.$validator.reset()
          this.GetAuthCode()
          return
        } // end if

        this.Success()
        this.joinUsAddFlag = true
      }
    }, // end AddIB

    // 代理註冊成功跳頁
    Success: function () {
      this.$refs.joinUsSuccessFun.OpenWindow()
    } // end Success
  },

  components: {
    Datepicker,
    JoinUsSuccess
  }
}
