import electronicContent from "@/mod/k03/components/general/electronic/electronicContent"

export const electronic = {
  name: "electronic",
  components: {
    electronicContent
  }
}
