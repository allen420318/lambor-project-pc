import BLDef from "@/assets/scripts/common/BLDef"
import Vue from "vue"
// import mainHome from "scripts/customJs/web/mainHome"
// import stopExecutionOnTimeout from "scripts/customJs/web/stopExecutionOnTimeout"
import EventBus from "scripts/common/EventBus"
import CommUtility from "@/assets/scripts/common/CommUtility"
import LoginService from "scripts/businessLogic/loginService"
import CommonService from "scripts/businessLogic/commonService"
import SignalRService from "scripts/common/SignalRService"
import rotateLoopFun from "scripts/customJs/web/rotateLoop"
import InformationService from "scripts/businessLogic/informationService"
import HttpService from "@/assets/scripts/common/HttpService"
import PersonalCenterService from "scripts/businessLogic/personalCenterService"
import "scripts/customJs/web/script25/jquery-2.1.1.min.js"
import * as swiperAnimate from "@/assets/scripts/customJs/swiper.animate.min.js"
import "@/assets/scripts/customJs/animate.min.css"

// 引入25版首页轮播（但已弃用）
//  import 'scripts/customJs/web/script25/TweenMax.min.js';
//  import 'scripts/customJs/web/script25/index.js';

import Swiper from "swiper"

export const initIndex25 = {
  data() {
    return {
      MemberInfo: {},
      IsGetBalanceIng: false,
      VBalance: undefined,
      IsLogin: undefined,
      StateLogin: false,
      showID: undefined,
      OtherActivityData: false,
      PromotionDetail: {},
      // 圖片旗標
      ActLogoFlag: false,
      // 資源位置
      Resource: Vue.prototype.$ResourceURL,
      // 传递活动详情到个人中心
      TransActivityData: {}
    }
  },
  created: function () {
    this.CheckIsLogin()
    this.$nextTick(() => {
      var swiper = new Swiper(".swiper-container2", {
        pagination: ".swiper-p2",
        direction: "vertical",
        slidesPerView: 1,
        paginationClickable: true,
        spaceBetween: 30,
        mousewheelControl: true,
        on: {
          init: function () {
            swiperAnimate.swiperAnimateCache(this) // 隐藏动画元素
            swiperAnimate.swiperAnimate(this) // 初始化完成开始动画
          },
          slideChangeTransitionEnd: function () {
            swiperAnimate.swiperAnimate(this) // 每个slide切换结束时也运行当前slide动画
            // this.slides.eq(this.activeIndex).find('.ani').removeClass('ani'); 动画只展现一次，去除ani类名
          }
        }
      })
    })
  },
  mounted: function () {
    const self = this
    EventBus.$on("SignalR_AddPrivateMessage", () => {
      self.MemberInfo.UnreadCount += 1
    })
    EventBus.$on("SignalR_ReadPrivateMessage", (data) => {
      self.MemberInfo.UnreadCount = data.Data
    })
    EventBus.$on("fundGetBalanceComplete", (SummaryAmount) => {
      self.VBalance = SummaryAmount
      console.log("-------------------------------------------")
      console.log("self.VBalance")
      self.IsGetBalanceIng = false
    })
    EventBus.$on("memberHeaderUpdateBalance", () => {
      self.GetBalance()
    })
    $(".activity-close").click(function () {
      $("#a-p-main").hide()
    })
    this.GetMemberInfo()
    this.GetBalance()
  },

  methods: {
    // 跳转到电子游戏页面选中外部点中的游戏 20191028 SeyoChen
    LinkElectronic: function (GameNo) {
      this.$router.push({
        name: "Electronic",
        params: {
          GameNo: GameNo
        }
      })
    },

    // 新的跳转打开个人中心方式 2019.05.13 SeyoChen
    navClick: async function (name) {
      this.CheckLoginStatusBeforeRoute(name)
      if (this.StateLogin !== false) {
        this.$store.dispatch("setSwitchNameText", name)
        EventBus.$emit("personalInfo")
      }
    },

    // 檢查帳號狀態
    CheckLoginStatusBeforeRoute: async function (routeName) {
      const notifyData = {}
      const data = await CommonService.Comm_CheckPermission()
      console.log("+++++++++++++++++++")
      console.log(routeName)
      switch (data.Status) {
        case BLDef.SysAccountStatus.LOGINED_ENABLED:
          this.StateLogin = true
          this.$store.dispatch("setSwitchNameText", routeName)
          EventBus.$emit("personalInfo", routeName)
          break
        case BLDef.SysAccountStatus.LOGINED_FROZEN:
          this.StateLogin = false
          notifyData.NotifyMessage = "账户被冻结, 请联系客服人员"
          EventBus.$emit("showNotifyMessage", notifyData)
          break
        default:
          break
      }
    },

    // 弹出客服窗口
    OpenNewBox: function (serveLink) {
      window.open(serveLink, "客服", "scrollbars=1,width=365,height=565,left=10,top=150")
    },
    GetCustomerServiceInfo: async function () {
      const inputObj = {
        WGUID: Vue.prototype.$WGUID
      }
      const data = await CommonService.Comm_GetCustomerServiceInfo(inputObj)
      if (data.Ret === BLDef.ErrorRetType.SUCCESS) {
        this.CustomerService = data.Data.CustomerServiceInfo
      }
    },
    CallQQClient: function (QQAcct) {
      const QQHref = "http://wpa.qq.com/msgrd?v=3&uin=" + QQAcct + "&menu=yes"
      window.open(QQHref)
    },
    // 檢查登入狀態
    CheckIsLogin: async function () {
      const data = await CommonService.Comm_CheckPermission()
      switch (data.Status) {
        case BLDef.SysAccountStatus.NOT_LOGIN:
          this.IsLogin = false
          break
        case BLDef.SysAccountStatus.LOGINED_ENABLED:
        case BLDef.SysAccountStatus.LOGINED_FROZEN:
          this.IsLogin = true
          break
        default:
          break
      }
    },
    GetBalance: async function () {
      this.IsGetBalanceIng = true
      rotateLoopFun.rotateLoop() // 刷新按钮的旋转
      if (this.$route.name === "FundManage") {
        // 更新資金管理餘額
        EventBus.$emit("updateFundBalance")
      } else {
        // this.VBalance = await CommUtility.CalculateSummaryAmount();
        const inputObj = {}
        const retData = await PersonalCenterService.MemberCashFlowInfo_GetCWalletMoney(inputObj) // 新版获取账户金额 2019.03.28

        if (retData.Ret == 0) {
          this.VBalance = retData.Data.VAmount
          console.log("this.VBalance6666666666666666666666666")
          console.log(this.VBalance)
          console.log("this.VBalance666666666666666666666666666666")
        } else {
          this.VBalance = this.$t("message.top_under_maintenance")
        }
        this.IsGetBalanceIng = false
      }
    },
    GetMemberInfo: async function () {
      // 取得會員資訊
      const data = await CommonService.Comm_GetMemberInfo()
      if (data.Ret == 0) {
        this.MemberInfo = data.Data.Member
        rotateLoopFun.rotateLoop() // 刷新按钮的旋转
      }
    },
    // 登出
    Logout: async function () {
      const inputObj = {
        LogoutInfo: {}
      }
      EventBus.$emit("GlobalLoadingTrigger", true)
      await LoginService.LogOut(inputObj)
      // 移除登入相關資料
      localStorage.removeItem("Token")
      localStorage.removeItem("Auth")
      localStorage.removeItem("logoutTime")
      localStorage.removeItem("Acct") // 2018.09.20 SeyoChen

      // 若沒接收到signalr斷線訊息，則3秒後自行斷線
      setTimeout(function () {
        // 斷Signalr連線
        SignalRService.DisConnectServer()
        window.location.href = "/"
        EventBus.$emit("GlobalLoadingTrigger", false)
      }, 3000)
    },
    // 跳至詳細
    UrlRedirectDetail: function (target) {
      // this.$router.push({
      //     name: 'ActivityListDetails',
      //     params: {
      //         ID: target.ID
      //     }
      // });
      if (target.ID >= 0) {
        this.showID = 0
        this.OtherActivityData = true
        this.GetPromotionDetail(target.ID)
      } else if (target.ID == -1) {
        this.showID = 1
      } else if (target.ID == -2) {
        this.showID = 2
      }
      $("#a-p-main").show()
    }, // end UrlRedirectDetail
    // 獲得頁面資料
    GetPromotionDetail: async function (ID) {
      const dataObj = {
        WGUID: Vue.prototype.$WGUID,
        ID: ID
      }
      const retData = await InformationService.GetActivityDetail(dataObj)
      this.TransActivityData = retData.Data.Detail // 2018.09.21 SeyoChen

      // 失敗
      if (retData.Ret != 0) {
        const notifyData = {
          NotifyMessage: retData.Message
        }
        EventBus.$emit("showNotifyMessage", notifyData)
        return
      } // end if

      // 成功
      this.PromotionDetail = retData.Data.Detail
      const contentData = retData.Data.Detail.ActContent

      this.PromotionDetail.ActContent = await HttpService.GetContent(this.Resource + retData.Data.Detail.ActContent)
      if (contentData === "") {
        return
      }
      // 修改进入显示资源路径问题 2019.1.25 SeyoChen
      $("#a-p-main-detail").html(this.PromotionDetail.ActContent)
      // 修改进入显示资源路径问题 2019.1.25 SeyoChen end

      if (!(retData.Data.Detail.ActLogo == null)) {
        this.ActLogoFlag = true
      } // end if

      $(".activity-popup-detail").perfectScrollbar()
    }, // end GetPromotionDetail
    // 開啟視窗
    OpenWindow: function () {
      $("#a-p-main").hide()
      CommUtility.WebShowUniqueForm("informationForm")
    }, // end OpenWindow

    // 开启弹窗但不隐藏优惠活动详情
    OpenAndNoHide: function () {
      CommUtility.WebShowUniqueForm("informationForm")
    }, // end OpenAndNoHide
    // 關閉視窗
    CloseWindow: function () {
      CommUtility.WebCloseUniqueForm("informationForm")
    }, // end CloseWindow

    // 传值到支付页面 2018.09.21 SeyoChen
    ToRechargePage: async function (num) {
      const retDataCheck = await CommonService.Comm_CheckPermission()
      console.log(retDataCheck.Status)
      if (retDataCheck.Status != 0) {
        // this.CloseWindow();
        $("#a-p-main").hide()
        // this.$router.push( { name: 'webMemberDeposit', params: { ChargeType: num, ActivityData: this.TransActivityData } });
        this.$route.params.ActivityData = this.TransActivityData
        this.$route.params.ChargeType = num
        this.$store.dispatch("setSwitchNameText", "webMemberDeposit")
        EventBus.$emit("personalInfo")
      } else {
        // this.CloseWindow();
        $("#a-p-main").hide()
        this.$router.push({
          name: "Login"
        })
      }
    }
  },
  watch: {
    IsLogin: function () {
      console.log(this.IsLogin)
      if (this.IsLogin === true) {
        this.GetMemberInfo()
        this.GetBalance()
      }
    }
  }
}
