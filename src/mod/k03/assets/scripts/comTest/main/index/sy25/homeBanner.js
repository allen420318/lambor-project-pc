import EventBus from "scripts/common/EventBus"
import InformationService from "scripts/businessLogic/informationService"
import CommonService from "scripts/businessLogic/commonService"
import Vue from "vue"
import Swiper from "swiper"

export const HomeBanner = {
  props: {
    GetCurrentPageData: ""
  },

  data() {
    return {
      // 載入畫面資料
      LoadMainPageDataModel: {
        BannerActivityList: [],
        ActivityList: []
      },

      // 分頁資料
      PageInfo: {
        PageNo: 1,
        PageSize: 9,
        PageCount: 0,
        TotalCount: 0
      },
      defaultImg: 'this.src="' + this.$ResourceCDN + '/Customized/VVN/images/index/bannerDefault.png?VersionCode"',

      DefaultActivityList: []
    }
  },

  created: function () {
    this.GetPromotion(1)
  },

  methods: {
    // 取得畫面資料
    GetPromotion: async function (pageNo) {
      this.PageInfo.PageNo = pageNo
      const DataObj = {
        WGUID: Vue.prototype.$WGUID,
        PagingInfo: this.PageInfo
      }
      const retData = await InformationService.GetActivityList(DataObj)

      console.log(retData)
      console.log("retData99999999999999999")
      if (retData.Ret != 0) {
        const notifyData = {
          NotifyMessage: retData.Message
        }
        EventBus.$emit("showNotifyMessage", notifyData)
        return
      } // end if

      this.LoadMainPageDataModel.ActivityList = retData.Data.ActivityList
      // this.LoadMainPageDataModel.BannerActivityList = []
      this.LoadMainPageDataModel.BannerActivityList = retData.Data.ActivityList.slice(0, 5)

      const self = this
      // bind輪播事件
      setTimeout(function () {
        self.Slider()
      }, 500)

      // 2018.09.20 SeyoChen
      const retDataLv = await CommonService.Comm_CheckPermission()
      if (retDataLv.Status != 0) {
        const dataObj = {
          WGUID: "",
          LoginID: "",
          PagingInfo: this.PageInfo
        }
        dataObj.WGUID = Vue.prototype.$WGUID
        dataObj.LoginID = localStorage.getItem("Acct")
        const ActivityData = await CommonService.Comm_GetBonusActivity_MemberLv(dataObj)

        this.LoadMainPageDataModel.BannerActivityList = ActivityData.Data.ActivityList.slice(0, 5) // 优惠活动页面 banner 跟随等级更新 2019.04.12 SeyoChen
        this.$parent.ActivityList = ActivityData.Data.ActivityList
      } else {
        this.$parent.ActivityList = this.LoadMainPageDataModel.ActivityList
      }
      $(".activity-expect").remove()

      // this.PageInfo.PageCount = Math.ceil(retData.Data.PagingInfo.TotalCount / retData.Data.PagingInfo.PageSize);
      // if (this.PageInfo.PageCount == 0) {
      //     this.$parent.PageInfo.PageCount = 1; // 若無紅利活動也設定為1，使分頁套件可以顯示
      // } else {
      //     this.$parent.PageInfo.PageCount = this.PageInfo.PageCount;
      // }
    }, // end GetPromotion
    // 2018.11.15 SeyoChen 动态识别添加游戏模块
    AjectAddBox: function (num) {
      const boxSize = parseInt(3) // 页面中一行的box个数
      const boxHtml = `<li class="activity-li activity-li-PopUp activity-expect">
        <div class="activity-expect-tips"><div><p>${this.$t(
          "message.live_expect"
        )}</p><span>IN MAINTENANCE</span></div></div></li>`
      const box = $(".activity-lists")
      if (num % boxSize === 0 && num !== 0) {
      } else {
        for (let i = 0; i < Math.abs(boxSize - (num % boxSize)); i++) {
          box.append(boxHtml)
        }
      }
    },
    // 優惠活動輪播事件
    Slider: function () {
      this.$nextTick(() => {
        var swiper1 = new Swiper(".swiper-container1", {
          mousewheelControl: true,
          direction: "horizontal",
          mousewheelForceToAxis: true,
          pagination: ".banner_point",
          autoplayDisableOnInteraction: false,
          paginationClickable: true,
          autoplay: 5000 // 可选选项，自动滑动
        })
        $(".swiper-button-next").click(function () {
          swiper1.slidePrev()
        })
        $(".swiper-button-prev").click(function () {
          swiper1.slideNext()
        })
      })
    } // end Silder
  },

  watch: {
    GetCurrentPageData: function () {
      this.GetPromotion(this.GetCurrentPageData)
    }
  }
}
