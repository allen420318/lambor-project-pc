import EventBus from "scripts/common/EventBus"
import BKBLDef from "@/assets/scripts/common/BLDef"
import Vue from "vue"
import Regex from "scripts/common/CommDef"
import LoginService from "scripts/businessLogic/loginService"
import CommUtility from "@/assets/scripts/common/CommUtility"
import ForgetPassword from "@/mod/k03/components/sub/login/forgetPassword"
import ForgetPasswordSuccess from "@/components/sub/login/forgetPasswordSuccess"
import ForgetPasswordFail from "@/components/sub/login/forgetPasswordFail"
import CommonService from "scripts/businessLogic/commonService"
// 引入base64
const Base64 = require("js-base64").Base64
export const initLogin = {
  data() {
    return {
      // 正则表达式
      Reg: Regex.Regex,
      // PreToken (用于获取验证码)
      PreToken: "",
      // 验证码图片
      AuthImg: "",

      // 登入信息
      LoginObj: {
        Acct: "",
        Pwd: "",
        VCodeImg: undefined,
        remember: true
      },
      LogoImgPath: "",
      LogoName: "",

      defaultImg: 'this.src="' + this.$ResourceCDN + '/EditionImg/LamborFormal/images/public/refresh.png"'
    }
  },
  mounted: function () {
    CommUtility.BindEnterTrigger("LoginForm", this.LoginSubmit)
    // 在页面加载时从cookie获取登录信息
    let account = this.cookie.getJSON("account")
    let password = Base64.decode(this.cookie.getJSON("password"))
    // 如果存在赋值给表单，并且将记住密码勾选
    console.log("account存储数据")
    console.log(password)
    console.log("account存储数据")
    if (password) {
      this.LoginObj.Acct = account
      this.LoginObj.Pwd = password
      this.LoginObj.remember = true
    }
  },
  methods: {
    GetLogoData: async function () {
      const DataObj = {
        WGuid: Vue.prototype.$WGUID,
        platformType: BKBLDef.PlatformType.Web_PC
      }
      const retData = await CommonService.HomePage_LoadMainPage(DataObj)
      console.log("next")
      if (retData.Ret === 0) {
        this.LogoImgPath = this.$ResourceCDN + retData.Data.MasterLogo
        this.LogoName = retData.Data.Name
      } else {
        this.LogoImgPath = localStorage.getItem("LogoImgPath")
        this.LogoName = localStorage.getItem("LogoName")
      }
    },
    // 获取ToKen
    GetPreToKen: async function () {
      const retData = await LoginService.LoadPage()
      if (retData !== "") {
        this.PreToken = retData
        this.GetAuthCode()
      }
      console.log("11111111:" + this.PreToken)
    }, // end GetPreToKen

    // 获取验证码
    GetAuthCode: async function () {
      console.log("22222222222" + this.PreToken)
      const retData = await LoginService.ReGenVCode(this.PreToken)
      this.AuthImg = retData
    }, // end GetAuthCode

    // 登入
    LoginSubmit: async function () {
      this.flag = await this.validate()

      // 處理中則直接返回
      if (this.tempObj === true) {
        return
      } // end if
      this.tempObj = true

      if (this.flag === true) {
        const dataObj = {
          LoginData: {},
          WGUID: ""
        }
        dataObj.LoginData = this.LoginObj
        dataObj.WGUID = Vue.prototype.$WGUID
        EventBus.$emit("GlobalLoadingTrigger", true)
        const retData = await LoginService.LoginConfirm(this.PreToken, dataObj)
        EventBus.$emit("GlobalLoadingTrigger", false)

        // 失敗
        if (retData.Ret !== 0) {
          const notifyData = {
            NotifyMessage: retData.Message
          }
          EventBus.$emit("showNotifyMessage", notifyData)

          // 針對錯誤重置欄位
          switch (retData.Ret) {
            // 驗證碼錯誤
            case 30:
              this.LoginObj.VCodeImg = ""
              this.$validator.reset()
              break
            default:
              this.LoginObj.Acct = ""
              this.LoginObj.Pwd = ""
              this.LoginObj.VCodeImg = ""
              this.$validator.reset()
              break
          } // end switch

          this.GetAuthCode()
          this.tempObj = false
        } else {
          // localStorage.clear();

          // 清理缓存 20190808 SeyoChen
          localStorage.removeItem("Token")
          localStorage.removeItem("Auth")
          localStorage.removeItem("logoutTime")
          localStorage.removeItem("Acct") // 2018.09.20 SeyoChen
          localStorage.removeItem("CorrelationIBMemberId")
          localStorage.removeItem("userId")
          localStorage.removeItem("IBStatus")
          localStorage.removeItem("loginFlag")
          // 清理缓存 20190808 SeyoChen end

          localStorage.setItem("Token", retData.Data.LoginData.Token)
          localStorage.setItem("Auth", retData.Data.LoginData.Auth)
          localStorage.setItem("Acct", this.LoginObj.Acct) // 2018.09.20 SeyoChen
          localStorage.setItem("CorrelationIBMemberId", retData.Data.LoginData.CorrelationIBMemberId)
          localStorage.setItem("logoutTime", new Date().getTime() + Vue.prototype.$SurviveMinutes * 60 * 1000)
          localStorage.setItem("userId", retData.Data.LoginData.AcctID)
          localStorage.setItem("IBStatus", retData.Data.LoginData.IBStatus)

          EventBus.$emit("afterLogin")
          // 储存登录信息
          this.setUserInfo()
          this.$router.push({
            name: "Index"
          })
          EventBus.$emit("loginFlagCheck", "true")
          localStorage.setItem("loginFlag", "true")
          this.tempObj = false
        }
      } else {
        this.tempObj = false
      } // edn if
    }, // end  LoginSubmit
    // 储存表单信息
    setUserInfo: function () {
      // 判断用户是否勾选记住密码，如果勾选，向cookie中储存登录信息，
      // 如果没有勾选，储存的信息为空
      if (this.LoginObj.remember) {
        this.cookie.set("account", this.LoginObj.Acct)
        // base64加密密码
        let passWord = Base64.encode(this.LoginObj.Pwd)
        this.cookie.set("password", passWord)
      } else {
        this.cookie.set("account", "")
        this.cookie.set("password", "")
      }
    },
    forgetClick: function () {
      // console.log(this.checked)
    },
    // 所有欄位驗證
    validate: function () {
      Object.keys(this.fields).forEach((key) => {
        this.fields[key].touched = true
      })
      return this.$validator.validateAll().then((result) => {
        return result
      })
    }, // end validate
    // 開啟忘記密碼視窗
    TriggerForgetPassword: function () {
      // ForgetPassword.methods.OpenWindow();
      EventBus.$emit("ForgetPasswordAlert")
    }, // end TriggerForgetPassword
    // 開啟忘記密碼訊息視窗
    TriggerForgetPasswordInformation: function () {
      // ForgetPasswordSuccess.methods.OpenWindow();
      EventBus.$emit("ForgetPasswordSuccessAlert")
    }, // end TriggerForgetPasswordInformation

    // 開啟忘記密碼訊息視窗
    TriggerForgetPasswordFail: function () {
      // ForgetPasswordFail.methods.OpenWindow();
      EventBus.$emit("ForgetPasswordFailAlert")
    } // end Trigg
  },
  created: function () {
    // 重置个人中心是否为代理状态 SeyoChen 20190718
    localStorage.setItem("loginFlag", "false")
    EventBus.$emit("loginFlagCheck", "false")
    // 重置个人中心是否为代理状态 SeyoChen 20190718 end
    this.GetPreToKen()
    this.GetLogoData()
  },
  components: {
    ForgetPassword,
    ForgetPasswordSuccess,
    ForgetPasswordFail
  }
}
