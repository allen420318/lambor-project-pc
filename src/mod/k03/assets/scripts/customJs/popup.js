export function popup() {
	var id = null;
	$('*').click(function() {
		var me = $(this);
		if(me.attr('data-type') == "openPopup") {
			id = me.attr('data-target');
			$(id).fadeIn();
		}
		if(me.attr('data-close')) {
			if(id) {
				$(id).fadeOut();
			} else {
				$(this).parents()[0].remove();
			}
		}
	});
}