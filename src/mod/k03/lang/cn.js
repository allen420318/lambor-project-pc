const modMessages = {
  activity_company_income: "银行卡入款",
  abouts_text1: "",
  abouts_text2: "",
  verify_membership_account: "会员帐号",
  login_forget_password: "忘记密码",
  hot_game: "热门游戏",
  go_top: "回到顶部",
  fair_play: "公平 游戏",
  agent_word_remark: "仅可以使用中英文字母,数字,空白与一般常用符号, 字数限制100",
  agent_contant_us_remark: "仅可以使用中英文字母,数字,空白与一般常用符号, 字数限制2-500",
  index_technical_service: "技术服务",
  index_hour: "小时",
  index_safe_technology: "安全卓越技术",
  index_average_time: "平均时间",
  news_understanding_details: "了解详情",
  activity_lists: "活动列表",
  latest_news: "最新消息",
  news_details: "消息详情",
  index_ultimate_experience: "极致体验",
  index_do_best: "用心做最好",
  index_brilliant: "精彩纷呈",
  index_open_new_era: "开启博彩新时代",
  information_recall:
    "您可以在【一键收回】页面进行额度转换的动作，【个人中心】＞【资金状态】＞【一键收回】，选择转出与转入账号后，填入转换金额，按下确认即可转换。",
  information_common: "常见",
  information_problem: "问题",
  index_most_popular_game: "网罗时下最潮，最火爆的游戏精品",
  understand_more: "了解更多",
  our_strengths: "我们的优势",
  most_entertaining_online_chess: "全球最娱乐的线上棋牌品牌，两百万扑克爱好者的聚集地",
  commission: "佣",
  website_home_page: "网站首页",
  home: "回首页",
  gather_details: "收起详情",
  agency_winning_percentage: "损益+占成+码佣+红利活动",
  agency_dividend_betting: "+红利派发+返水"
}

export default modMessages
