const modMessages = {
  agent_QQ: '請輸入您的Zalo',
  agent_digits: '僅可以使用2—11個數字',
  tutorial_drawing_money: '取款相關',
  member_extra_dividend: '紅利百分比',
  reg_account_registration: '帳號注册',
  agency_new_registration: '新注册',
  member_canal: '渠道',
  agent_qq: 'Zalo',
  down_app: 'APP下載',
  member_bank_notes: '請填寫正確銀行的名稱，如：ACB、BIDV、SACOMBANK、VIETINBANK、TECJCOM、BANKCOMMB、VIETCOMBANK、EXIMBANK',
  VND_LOTTERY_1: '愛國彩',
  VND_LOTTERY_2: '北部',
  VND_LOTTERY_3: '中部',
  VND_LOTTERY_4: '南部',
  VND_LOTTERY_5: '越南30秒彩',
};

export default modMessages;
