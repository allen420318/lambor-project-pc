/* eslint-disable */
import Vue from 'vue';
import moment from 'moment';
import URLService from '@/assets/scripts/common/URLService.js';
import GameService from '@/mod/k04/assets/scripts/businessLogic/gameService.js';
import PersonalCenterService from '@/mod/k04/assets/scripts/businessLogic/personalCenterService.js';
import Router from '@/router';

export default {
    // 計算總資產金額
    async CalculateSummaryAmount() {
        let retSummaryAmount = 0;
        let retBool = false;
        const data = await GameService.GetGameMaintainStatus();

        if (data.Ret == 0) {
            for (let i = 0; i < data.Data.GameAPIVendorMaintainStatusList.length; i++) {
                if (data.Data.GameAPIVendorMaintainStatusList[i].IsMaintaining === '702') {
                    const inputObj = {
                        APIVendorID: data.Data.GameAPIVendorMaintainStatusList[i].GameAPIVendor
                    };
                    // eslint-disable-next-line no-await-in-loop
                    const balanceData = await PersonalCenterService.EWalletTransfer_GetBalance(inputObj);
                    if (balanceData.Ret == 0) {
                        retSummaryAmount = this.NumberAdd(retSummaryAmount, balanceData.Data.GameApiBalance.Amount);
                        retBool = true;
                    }
                }
            }

            if (retBool === '703') {
                retSummaryAmount = '维护中';
            }
        } else {
            retSummaryAmount = '维护中';
        }
        return retSummaryAmount;
    },
    NumberAdd(arg1, arg2) {
        let r1;
        let r2;
        let m = 0;
        let c = 0;
        try {
            r1 = arg1.toString().split('.')[1].length;
        } catch (e) {
            r1 = 0;
        }
        try {
            r2 = arg2.toString().split('.')[1].length;
        } catch (e) {
            r2 = 0;
        }
        c = Math.abs(r1 - r2);
        //eslint-disable-next-line
        m = Math.pow(10, Math.max(r1, r2));
        if (c > 0) {
            //eslint-disable-next-line
            const cm = Math.pow(10, c);
            if (r1 > r2) {
                arg1 = Number(arg1.toString().replace('.', ''));
                arg2 = Number(arg2.toString().replace('.', '')) * cm;
            } else {
                arg1 = Number(arg1.toString().replace('.', '')) * cm;
                arg2 = Number(arg2.toString().replace('.', ''));
            }
        } else {
            arg1 = Number(arg1.toString().replace('.', ''));
            arg2 = Number(arg2.toString().replace('.', ''));
        }
        return (arg1 + arg2) / m;
    },
    GenGuid() {
        let d = new Date().getTime();
        const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            const r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    }, // end GenGuid

    GenRandomInteger(MinNum, MaxNum) {
        // 取得 minNum(最小值) ~ maxNum(最大值) 之間的亂數
        return Math.floor(Math.random() * (MinNum - MaxNum + 1)) + MinNum;
    }, // end GenRandomInteger

    // tick轉換為本地時間
    UtcTicksToLocalTime(utcDateTicks, format) {
        if (!utcDateTicks) {
            return undefined;
        }
        return moment(utcDateTicks).utcOffset(8 * 60).format(format);
    },

    // tick轉換為时分秒
    UtcTicksTime(utcDateTicks, format) {
        if (!utcDateTicks) {
            return undefined;
        }
        return moment(utcDateTicks).utcOffset(0 * 60).format(format);
    },

    // 本地時間轉換為tick
    LocalTimeToUtcTicks(orgLocalDateTime, IsEndTime, IsContainTime) {
        if (!orgLocalDateTime) {
            return undefined;
        }

        let localDateTime;
        if (IsContainTime === true) {
            localDateTime = orgLocalDateTime;
        } else {
            // 將時分秒格式去除, 保留日期部份避免受當地時間影響
            localDateTime = moment(orgLocalDateTime).format('YYYY/MM/DD');
        }
        // 計算與UTC+8的時差
        const timeoffset = ((new Date().getTimezoneOffset() / 60) + 8) * -1;

        // 補正同一目標日期時間，與UTC+8差距的tick
        const utcEightFixTick = timeoffset * 3600 * 1000;

        if (IsEndTime === true) {
            return new Date(localDateTime).getTime() + utcEightFixTick + (86400 * 1000 - 1); // 結束日期以當天的23:59:59.999
        }

        return new Date(localDateTime).getTime() + utcEightFixTick;
    },

    // 手機格式隱藏部分號碼
    PhoneNumberFormat(phoneNumber) {
        if (!phoneNumber) {
            return phoneNumber;
        }

        const phoneNumberString = phoneNumber.toString();
        const phoneNumberLength = phoneNumberString.length;
        if (phoneNumberLength <= 4) {
            return phoneNumberString;
        }

        const tmpArray = phoneNumberString.split('');
        if (phoneNumberLength <= 8) {
            let toAddChars = '';
            for (let index = 0; index < phoneNumberLength - 4; index += 1) {
                toAddChars += '*';
            }
            tmpArray.splice(0, phoneNumberLength - 4, toAddChars);
        } else {
            tmpArray.splice(phoneNumberLength - 8, 4, '****');
        }
        return tmpArray.join('');
    },

    // 金錢轉換格式
    MoneyFormat(content) {
        if (content !== undefined && (!isNaN(parseFloat(content)) && isFinite(content))) {
            return content.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        }
        return content;
    },

    // 僅顯示內容前幾位
    LimitTo(content, digit) {
        return content.substring(0, digit);
    },

    // 簡化顯示字元
    SimpleContent(orgText, displayLength) {
        // 過濾html標籤
        const tmp = document.createElement('DIV');
        tmp.innerHTML = orgText;
        const pureText = tmp.textContent || tmp.innerText || '';
        // 過濾ASCII不可見字元
        let finalText = pureText.replace(/[\x00-\x1F]/g, '');
        // 純字串若長度超過則省略
        if (finalText.length > displayLength) {
            finalText = finalText.substring(0, displayLength) + '...';
        }
        return finalText;
    },

    // 超過的數字顯示..+
    UpperLimitDisplay(content, limitNumber) {
        if (content !== undefined && isNaN(content) === false && content > limitNumber) {
            return limitNumber + '+';
        }
        return content;
    },

    // 顯示特殊彈窗
    WebShowUniqueForm(ID) {
        switch (ID) {
            case 'freeTrialForm':
                $('.free_bg').fadeIn();
                break;
            case 'SystemNotifyWindow':
                $('.system_Bg').fadeIn();
                break;
            default:
                $('.normal_Bg').fadeIn();
                break;
        }

        $('#' + ID).slideDown(200);
        $('body').css('overflow', 'hidden');
    },
    // 个人中心嵌套弹框
    WebShowAlertboxForm(ID) {
        $('#' + ID).removeClass('slideOutRight');
        $('#' + ID).addClass('slideInRight');
        //  $('#' + ID).show();
        $('.alert_Bg').fadeIn();
        $('body').css('overflow', 'hidden');
    },
    // 關閉特殊彈窗
    WebCloseUniqueForm(ID) {
        $('.bg').fadeOut();
        $('#' + ID).slideUp(200);
        $('body').css('overflow', 'visible');
    },

    // 关闭个人中心嵌套弹窗
    WebClosealertboxForm(ID) {
        $('#' + ID).removeClass('slideInRight');
        $('#' + ID).addClass('slideOutRight');
        //  $('#' + ID).hide();
        $('.alert_Bg').fadeOut();
        // $('body').css('overflow', 'hidden');
        setTimeout(() => {
            $('#' + ID).removeClass('slideOutRight');
        }, 500);
    },

    // 顯示訊息彈窗
    WebShowMessageForm() {
        $('.normal_Bg').fadeIn();
        $('#MessageWindow').slideDown(200);
        $('body').css('overflow', 'hidden');
    },

    // 關閉訊息彈窗
    WebCloseMessageForm() {
        $('.bg').fadeOut();
        $('#MessageWindow').slideUp(200);
        $('body').css('overflow', 'visible');
    },


    // 另開統一的遊戲視窗
    OpenPlayGameWindow(routerName, windowName, Data) {
        let url = '';
        if (Data == undefined) {
            url = Router.resolve({
                name: routerName
            }).href;
        } else {
            url = Router.resolve({
                name: routerName,
                params: {
                    Data: URLService.GetUrlParameterFromObj(Data)
                }
            }).href;
        }
        window.open(url, windowName, 'scrollbars=1,width=1500,height=800,left=10,top=150');
    }, // end OpenPlayGameWindow

    // 另開統一的遊戲視窗
    OpenDepositWindow(routerName, windowName, Data) {
        let url = '';
        if (Data == undefined) {
            url = Router.resolve({
                name: routerName
            }).href;
        } else {
            url = Router.resolve({
                name: routerName,
                params: {
                    Data: URLService.GetUrlParameterFromObj(Data)
                }
            }).href;
        }
        Vue.prototype.$DepositWindow = window.open(url, windowName, 'scrollbars=1,width=1500,height=800,left=10,top=150');
    }, // end OpenPlayGameWindow

    // 綁定按下Enter事件
    BindEnterTrigger(TriggerElement, TriggerFunction) {
        $('#' + TriggerElement).keypress(function (e) {
            if (e.target.nodeName == 'TEXTAREA') {
                return;
            }

            if (e.which == 13) {
                TriggerFunction();
                $(':focus').blur();
            }
        });
    },

    // 格式化小数点后两位
    numCalFix(Data) {
        let isZheng = 1;
        if (Data * 1 >= 0) {
            isZheng = 1;
        } else {
            isZheng = -1;
        }
        Data = Math.abs(Data * 1);
        const toFixedNum = function (num, s) {
            const times = 10 ** s;
            let des = num * times + 0.5;
            des = parseInt(des, 10) / times;
            return des * isZheng + '';
        };
        let rtn = toFixedNum(Data, 5);
        rtn *= 1;
        return rtn;
    },

    // 文本文档空格转换
    enterChange(Data) {
        if (Data !== null && Data !== '' && Data !== undefined) {
            const rtn = Data.replace(/\n|\r\n/g, '<br/>');
            return rtn;
        } else {
            return;
        }
    }
};
