/* eslint-disable */
import axios from "axios";
import Vue from "vue";
import Security from "@/assets/scripts/common/Security.js";
import VersionManagement from '@/assets/scripts/common/versionManagement.js';
import { messages } from '@/lang/i18nLoader.js';
var CancelToken = axios.CancelToken;
var source = CancelToken.source();
import fetch from "@/api/fetch";
// import BLDef from '@/assets/scripts/common/BLDef.js';

export default {
    // 多语言
    $t(name) {
        let Language = Vue.prototype.cookie.getJSON('language') !== undefined ? Vue.prototype.cookie.getJSON('language') : Vue.prototype.$Lang;
        Language = Language || 0;
        let langList = ['zh', 'tw', 'en', 'vi'];
        return messages[langList[Language]][name.split('.')[0]][name.split('.')[1]];
    },
    // 動態加解密request
    PostDynamic(ST, AT, DataObj, CustToken) {
        let _this = this;
        if (DataObj === undefined) {
            DataObj = null;
        } // end if
        // 加時間防止被登出
        localStorage.setItem(
            "logoutTime",
            new Date().getTime() + Vue.prototype.$SurviveMinutes * 60 * 1000
        );
        console.log("ST:" + ST + ", AT:" + AT);
        console.log(DataObj);
        const tmpToken =
            CustToken === undefined ? localStorage.getItem("Token") : CustToken;
        const Language = Vue.prototype.cookie.getJSON('language') !== undefined ? Vue.prototype.cookie.getJSON('language') : Vue.prototype.$Lang;
        return axios
            .post(Vue.prototype.$BKBLUrl, {
                AT: AT,
                ST: ST,
                TK: tmpToken,
                LG: Language === null ? Vue.prototype.$Lang : Language, // 多语言选填传输属性，默认为简体中文，可不传输 0:简体中文 1：繁体中文 2：英文 3：越南语
                Data: Security.DynamicEncryptWithBase64(
                    DataObj,
                    Security.KeyGenSNKey(1),
                    Security.KeyGenGuidKey("1")
                )
            })
            .then(rsp => {
                try {
                    const viewObj = Security.DynamicDecryptToObj(rsp.data.Rep);
                    console.log("ST:" + ST + ", AT:" + AT);
                    console.log(viewObj);

                    // 获取后端回传的版本号进行对比，然后缓存在RD页面进行查看
                    const num = 'SA' + ST.toString() + AT.toString();
                    // if (VersionManagement.VersionNum[num].split('_')[1] !== '0001') {
                    //     const Version = {
                    //         ST: ST,
                    //         AT: AT,
                    //         NO: VersionManagement.VersionNum[num]
                    //     };
                    //     this.SelectNum(Version);
                    // }

                    // viewObj 為空
                    if (viewObj === undefined || viewObj == null) {
                        return {
                            Ret: "-1",
                            Message: _this.$t('message.system_is_busy'),
                            Data: null
                        };
                    }

                    // viewObj.Data 為空
                    if (viewObj.Data === undefined || viewObj.Data == null) {
                        return viewObj;
                    }

                    return viewObj;
                } catch (ex) {
                    // 获取后端回传的版本号进行对比，然后缓存在RD页面进行查看
                    const num = 'SA' + ST.toString() + AT.toString();
                    if (VersionManagement.VersionNum[num].split('_')[1] !== '0001') {
                        const Version = {
                            ST: ST,
                            AT: AT,
                            NO: VersionManagement.VersionNum[num]
                        };
                        this.SelectNum(Version);
                    }

                    return {
                        Ret: "-1",
                        Message: _this.$t('message.system_is_busy'),
                        Data: null
                    };
                }
            });
    },
    // 清除请求队列
    ClearRequestQueue() {
        fetch.cancelRequest();
    },
    // AES加密request
    PostAes(ST, AT, DataObj, isUpdateSurviveTime) {
        let _this = this;
        window.requestQueue = [];
        if (DataObj === undefined) {
            DataObj = null;
        } // end if
        // 加時間防止被登出
        if (!isUpdateSurviveTime) {
            localStorage.setItem(
                "logoutTime",
                new Date().getTime() + Vue.prototype.$SurviveMinutes * 60 * 1000
            );
        }
        // console.log('ST:' + ST + ', AT:' + AT);
        // console.log(DataObj);
        const key = localStorage.getItem("Auth");
        const Language = Vue.prototype.cookie.getJSON('language') !== undefined ? Vue.prototype.cookie.getJSON('language') : Vue.prototype.$Lang;
        return fetch
            .post(Vue.prototype.$BKBLUrl, {
                ST: ST,
                AT: AT,
                TK: localStorage.getItem("Token"),
                LG: Language === null ? Vue.prototype.$Lang : Language, // 多语言选填传输属性，默认为简体中文，可不传输 0:简体中文 1：繁体中文 2：英文
                Data: Security.AESEncryptWithBase64(
                    JSON.stringify(DataObj),
                    key
                )
            }, {
                cancelToken: source.token
            })
            .then(rsp => {
                try {
                    const viewObj = Security.AESDecryptToObj(rsp.data.Rep, key);
                    // console.log('ST:' + ST + ', AT:' + AT);
                    // console.log(viewObj);
                    // viewObj 為空

                    // 获取后端回传的版本号进行对比，然后缓存在RD页面进行查看
                    const num = 'SA' + ST.toString() + AT.toString();
                    // if (VersionManagement.VersionNum[num].split('_')[1] !== '0001') {
                    //     const Version = {
                    //         ST: ST,
                    //         AT: AT,
                    //         NO: VersionManagement.VersionNum[num]
                    //     };
                    //     this.SelectNum(Version);
                    // }

                    if (viewObj === undefined || viewObj == null) {
                        return {
                            Ret: '-1',
                            Message: _this.$t('message.system_is_busy'),
                            Data: null
                        };
                    }

                    if (viewObj === undefined || viewObj == null) {
                        return {
                            Ret: "-1",
                            Message: _this.$t('message.system_is_busy'),
                            Data: null
                        };
                    }

                    // viewObj.Data 為空
                    if (viewObj.Data === undefined || viewObj.Data == null) {
                        return viewObj;
                    }
                    return viewObj;
                } catch (ex) {
                    return {
                        Ret: "-1",
                        Message: _this.$t('message.system_is_busy'),
                        Data: null
                    };
                }
            })
            .catch(err => {
                return {
                    Ret: "-1",
                    Message: _this.$t('message.system_is_busy'),
                    Data: null
                };
            });
    },

    // http get html內容
    GetContent(serviceUrl) {
        let _this = this;
        return axios({
            url: serviceUrl,
            dataType: "text"
        }).then(
            function success(rsp) {
                console.log(rsp.data);
                return rsp.data;
            },
            function error() {
                return _this.$t('message.system_is_busy');
            }
        );
    },

    // 匯出Excel
    ExportExcel(exportFileName, ST, AT, DataObj) {
        if (DataObj == undefined) {
            DataObj = null;
        }
        const key = localStorage.getItem("Auth");

        return axios
            .post(
                Vue.prototype.$BKBLUrl, {
                    ST: ST,
                    AT: AT,
                    TK: localStorage.getItem("Token"),
                    Data: Security.AESEncryptWithBase64(
                        JSON.stringify(DataObj),
                        key
                    )
                }, {
                    responseType: "arraybuffer"
                }
            )
            .then(rsp => {
                const filename = exportFileName + ".xlsx";
                const contentType = rsp.headers["content-type"];

                try {
                    const blob = new Blob([rsp.data], {
                        type: contentType
                    });

                    if (window.navigator.msSaveOrOpenBlob) {
                        // for IE
                        window.navigator.msSaveOrOpenBlob(blob, filename);
                    } else {
                        const url = window.URL.createObjectURL(blob);
                        const linkElement = document.createElement("a");
                        linkElement.setAttribute("href", url);
                        linkElement.setAttribute("download", filename);

                        const clickEvent = new MouseEvent("click", {
                            view: window,
                            bubbles: true,
                            cancelable: false
                        });

                        linkElement.dispatchEvent(clickEvent);
                    }
                } catch (ex) {
                    console.log(ex);
                }
            });
    },

    // 取得IP及位置資訊
    GetLocation() {
        return axios.get("http://freegeoip.net/json/").then(res => {
            try {
                console.log(res.data);
                return res.data;
            } catch (ex) {
                console.log(ex);
                return ex;
            }
        });
    },

    // 存储版本号不一的接口数据 20191022 SeyoChen
    SelectNum(data) {
        let unm = [];
        if (localStorage.getItem('VersionNum') == '' || localStorage.getItem('VersionNum') == undefined) {
            unm = [];
        } else {
            unm = JSON.parse(localStorage.getItem('VersionNum'));
        }

        // 判断是否存在重复接口
        for (let i = 0; i < unm.length; i++) {
            console.log(unm[i]);
            if (unm[i].ST === data.ST && unm[i].AT === data.AT) {
                return;
            }
        }

        const VersionData = unm;
        VersionData.push(data);
        localStorage.setItem('VersionNum', JSON.stringify(VersionData));
    }
};
