import HttpService from '@/assets/scripts/common/HttpService.js'
import BLDef from '@/assets/scripts/common/BLDef.js'

export default {
    // 註冊
    async Regist (DataObj) {
        const retData = await HttpService.PostDynamic(BLDef.RegisterService.ServiceType, BLDef.RegisterService.ActType.MemberRegister_Add, DataObj)
        return retData
    }, // end Regist

    // 載入註冊成功頁面
    async LoadMainPage (DataObj) {
        const retData = await HttpService.PostAes(BLDef.RegisterService.ServiceType, BLDef.RegisterService.ActType.MemberRegister_LoadSuccessPage, DataObj)
        return retData
    }, // end LoadMainPage

    // 載入加盟我們頁面
    async LoadIBRegisterMainPage (DataObj) {
        const retData = await HttpService.PostDynamic(BLDef.RegisterService.ServiceType, BLDef.RegisterService.ActType.IBRegister_LoadMainPage, DataObj)
        return retData
    }, // end LoadIBRegisterMainPage

    // 新增代理
    async AddIB (DataObj) {
        const retData = await HttpService.PostDynamic(BLDef.RegisterService.ServiceType, BLDef.RegisterService.ActType.IBRegister_Add, DataObj);
        return retData;
    }, // end AddIB
    // 会员申请成为代理
    async UserIBRegister_Add (inputData) {
        const retData = await HttpService.PostDynamic(BLDef.RegisterService.ServiceType, BLDef.RegisterService.ActType.UserIBRegister_Add, inputData);
        return retData;
    }, // end AddI
}
