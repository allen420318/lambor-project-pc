import electronicContent from '@/mod/k04/components/general/electronic/electronicContent.vue';

export const electronic = {
    name: 'electronic',
    components: {
        electronicContent
    }
};
