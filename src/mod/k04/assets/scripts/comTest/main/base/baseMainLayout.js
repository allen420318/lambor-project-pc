    // import '@/assets/scripts/customJs/web/showdate.js'
    import FrameFooter from '@/mod/k04/components/public/frameFooter.vue'
    import FrameHeader from '@/mod/k04/components/public/frameHeader.vue'
    import FrameNav from '@/mod/k04/components/public/frameNav.vue'
    import LamborSide from '@/mod/k04/components/public/lamborSide.vue'
    import floatService from '@/mod/k04/components/public/floatService.vue'
    import MemberMain from '@/mod/k04/components/public/memberMain.vue'
    import BLDef from '@/assets/scripts/common/BLDef.js'
    import CommonService from '@/mod/k04/assets/scripts/businessLogic/commonService.js'
    import LamborMarquee from '@/mod/k04/components/public/marquee.vue';
    import ForgetPassword from '@/mod/k04/components/sub/login/forgetPassword.vue';
    import ForgetPasswordSuccess from '@/mod/k04/components/sub/login/forgetPasswordSuccess.vue';
    import ForgetPasswordFail from '@/mod/k04/components/sub/login/forgetPasswordFail.vue';
    import NewIndexService from '@/mod/k04/assets/scripts/businessLogic/newIndexService.js';
    import Vue from 'vue';
    export const baseMnLt = {
        components: {
            FrameFooter,
            // MainHeader,
            // FrameNav,
            floatService,
            MemberMain,
            LamborMarquee,
            ForgetPassword,
            ForgetPasswordSuccess,
            ForgetPasswordFail,
            FrameHeader,
            FrameNav,
            LamborSide,
        },
        data() {
            return {
                IsLogin: undefined,
                OnlinePeople: undefined
            }
        },
        created: function () {
            this.CheckIsLogin()
            this.showNumberOnline()
            // this.$nextTick(function () {
            //   function winScroll() {
            //     const topLeft = document.documentElement.scrollLeft;
            //     $('.header').css('left', -topLeft + 'px');
            //   }
            //   window.onscroll = winScroll;
            // });
        },
        methods: {
            // 檢查登入狀態
            CheckIsLogin: async function () {
                const data = await CommonService.Comm_CheckPermission()
                switch (data.Status) {
                    case BLDef.SysAccountStatus.NOT_LOGIN:
                        this.IsLogin = false
                        break
                    case BLDef.SysAccountStatus.LOGINED_ENABLED:
                    case BLDef.SysAccountStatus.LOGINED_FROZEN:
                        this.IsLogin = true
                        break
                    default:
                        break
                }
            },
            // 显示在线人数
            showNumberOnline: async function () {
                console.log('#333');
                const DataObj = {
                    WGUID: Vue.prototype.$WGUID
                };
                const retData = await NewIndexService.HomePageLoadRankAndOnlineNumPage(
                    DataObj
                );
                console.log('在线人数');
                console.log(DataObj);
                console.log(retData);
                console.log('在线人数');
                if (retData.Ret === 0) {
                    this.OnlinePeople = retData.Data.OnlineNum;
                    // sessionStorage.setItem('OnlinePeopleNum', this.OnlinePeople)
                    // EventBus.$emit('OnlinePeopleNum', this.OnlinePeople);
                }
            },
        }
    }

