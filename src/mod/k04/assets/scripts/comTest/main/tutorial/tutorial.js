import rssPage from '@/mod/k04/components/main/common/page.vue'
import '@/assets/scripts/comTest/main/index/sy34/load.js';
export const tutorial = {
    name: 'HelloWorld',
    components: {
        rssPage
    },
    data() {
        return {
            SiteName: '',
            loginFlag: true,
            logoIndex: 0,
            frimNavs: [],
            navIndex: 0,
            skillNavs: [{
                name: this.$t('message.tutorial_baccarat')
            }, {
                name: this.$t('message.tutorial_roulette')
            }, {
                name: this.$t('message.tutorial_cussec')
            }, {
                name: this.$t('message.tutorial_other')
            }],
            skillIndex: 0,
            ruleNavs: [this.$t('message.tutorial_baccarat'), this.$t('message.tutorial_dragon_tiger'), this.$t('message.tutorial_roulette'), this.$t('message.tutorial_cussec')],
            ruleIndex: 0,
            matterNavs: [this.$t('message.top_register'), this.$t('message.information_modify'), this.$t('message.recharge_related'), this.$t('message.wallet_related'), this.$t('message.tutorial_drawing_money'), this.$t('message.inquire_related'), this.$t('message.activity_related'), this.$t('message.tutorial_game_questions')],
            matterIndex: 0
        };
    },
    created: function () {
        this.load();
        this.showWhat();
        this.SiteName = localStorage.getItem('LogoName');
    },
    mounted() {},
    methods: {
        load: function () {
            this.$nextTick(function () {
                memberLoad()
            })
        },
        navClick(index) {
            this.navIndex = index
        },
        skillTab(index) {
            this.skillIndex = index
        },
        ruleTab(index) {
            this.ruleIndex = index
        },
        matterTab(index) {
            this.matterIndex = index
        },
        // 根据路由参数判断该显示哪个展示项
        showWhat() {
            const navShow = this.$route.query.navIndex;
            const skillShow = this.$route.query.skillIndex;
            const ruleShow = this.$route.query.ruleIndex;
            const matterShow = this.$route.query.matterIndex;
            switch (navShow) {
                case 1:
                    this.navIndex = 1;
                    break;
                case 2:
                    this.navIndex = 2;
                    break;
                case 3:
                    this.navIndex = 3;
                    break;
                default:
                    this.navIndex = 0;
            }
            switch (skillShow) {
                case 1:
                    this.skillIndex = 1;
                    break;
                case 2:
                    this.skillIndex = 2;
                    break;
                case 3:
                    this.skillIndex = 3;
                    break;
                default:
                    this.skillIndex = 0;
            }
            switch (ruleShow) {
                case 1:
                    this.ruleIndex = 1;
                    break;
                case 2:
                    this.ruleIndex = 2;
                    break;
                case 3:
                    this.ruleIndex = 3;
                    break;
                default:
                    this.ruleIndex = 0;
            }
            switch (matterShow) {
                case 1:
                    this.matterIndex = 1;
                    break;
                case 2:
                    this.matterIndex = 2;
                    break;
                case 3:
                    this.matterIndex = 3;
                    break;
                case 4:
                    this.matterIndex = 4;
                    break;
                case 5:
                    this.matterIndex = 5;
                    break;
                case 6:
                    this.matterIndex = 6;
                    break;
                case 7:
                    this.matterIndex = 7;
                    break;
                default:
                    this.matterIndex = 0;
            }
        }
        // 根据路由参数判断该显示哪个展示项 end
    }

};
