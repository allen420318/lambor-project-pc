import axios from 'axios'
export const gameRules = {

    data () {
        return {
            msg: localStorage.getItem('LogoName'),
            navs: 1
        }
    },
    methods: {
        getmsg () {
            this.msg = localStorage.getItem('LogoName');
        },
        switchNav (i) {
            this.navs = i;
            $('.tutorial-inside').scrollTop(0)
            $('.tutorial-pull-down2').removeClass('fiveOpen')
        }
    },
    created () {
        this.getmsg()
    },
    mounted () {
        'use strict';
        // 竖滚动条插件
        // $('#gameRules-scroll').perfectScrollbar();

        // 33版
        var moveTo = 0;
        var index = $('.gamerule-box >div').length - 1;
        $('.gamerule-navs >li').click(function () {
            moveTo = $(this).attr('data-num');
            showmore(moveTo);
            $(this).addClass('on').siblings().removeClass('on');
        });
        $('.nextbtn').click(function () {
            if (moveTo < index) {
                moveTo++;
                showmore(moveTo);
            } else {
                return moveTo;
            }
            $('.gamerule-navs li').eq(moveTo).addClass('on').siblings().removeClass('on');
        });
        $('.prebtn').click(function () {
            if (moveTo > 0) {
                moveTo--;
                showmore(moveTo);
            } else {
                return moveTo;
            }
            $('.gamerule-navs li').eq(moveTo).addClass('on').siblings().removeClass('on');
        });

        function showmore (num) {
            //  var goleft = num * 870;
            //  $(".gamerule-box").animate({
            //  	"left": "-" + goleft + "px"
            //  });
            $('.gamerule-box >div').eq(moveTo).css('display', 'block').siblings().css('display', 'none');
        }
    }
}
