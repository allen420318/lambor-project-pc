import Vue from 'vue';
import CommUtility from '@/assets/scripts/common/CommUtility.js';
import EventBus from '@/assets/scripts/common/EventBus.js';
import URLService from '@/assets/scripts/common/URLService.js';
import BLDef from '@/assets/scripts/common/BLDef.js';
import HttpService from '@/assets/scripts/common/HttpService.js';
// 列表内容
import sportContent from '@/mod/k04/components/general/sport/sportContent.vue';

export const sport = {
    components: {
        sportContent
    },
    data () {
        return {
            // 頁籤類型
            // TabType: {
            //     // 投注說明
            //     BettingRule: '1',
            //     // 體育說明
            //     SportDescription: '2'
            // },

            // 當下頁籤類型
            // TabNum: '1',

            // sport List 2018.11.15 SeyoChen 遍历游戏
            GameList: [],
            SportList: [],
            allSportList: [],
            AddList: '', // 2018.11.15 SeyoChen 动态识别添加游戏模块

            // 對應瀏覽器顯示PDF的旗標
            IsShowFrame: undefined,
            PageCount: 0,
            pageNum: 1,
            pageSize: 12 // // 分页每页12条
        };
    },
    mounted: function () {
        this.GatGame(); // 2019.03.28 SeyoChen 遍历游戏
    },
    methods: {

        // 遍历获取游戏列表
        GatGame: async function () {
            this.GameList = JSON.parse(localStorage.getItem('GameList'));

            const dataObj = {
                WGUID: '',
                GameTypeNo: ''
            };
            dataObj.WGUID = Vue.prototype.$WGUID;
            dataObj.GameTypeNo = '0'; // 0.全部 1.真人视讯 2.体育 3.彩票 4.电子
            const retData = await HttpService.PostDynamic(0, 214, dataObj);

            if (retData.Ret === 0 && this.GameList !== retData.Data.GameAPIVerdorList) {
                // 判断本地缓存是否与之前的相同
                this.GameList = retData.Data.GameAPIVerdorList;
                localStorage.setItem('GameList', JSON.stringify(retData.Data.GameAPIVerdorList));
            }

            for (let i = 0; i < this.GameList.length; i++) {
                // 2019.01.12 筛选游戏 并 判定状态 SeyoChen 启动中：702 维护中：703 关闭：704
                if (this.GameList[i].GameTypeNo === 2 && this.GameList[i].GameTypeState !== 704) {
                    this.allSportList.push(this.GameList[i]);
                }
            }
            // 调试时使用，数据不够时随机插入1-20倍数据
            // let pauseList = JSON.parse(JSON.stringify(this.allSportList));
            // let randomNum = 50 || Math.floor((Math.random() * 20) + 1);
            // console.log('randomNum', randomNum)
            // for (let i = 0; i < randomNum; i++) {
            //     this.allSportList.push(...pauseList)
            // }

            // 判断总页数
            this.PageCount = Math.ceil(this.allSportList.length / this.pageSize);
            // 只有一页时不需要显示换页
            this.PageCount = this.PageCount === 1 ? 0 : this.PageCount;
            // 初始计算第一页游戏条数
            this.$refs['sportContent'].computeGameList(1, this.allSportList);
        },
        // 体育博彩特有
        PlaySportGame: function (list) {
            const dataObj = {
                GameAPIVendor: list.GameApiNo, // 游戏GameApiNo
                GameCode: list.GameCode,
                // GameCatlog: list.GameTypeNo, // 游戏类型
                GameCatlog: BLDef.GameCatlogType.Sport, // 2018.12.01 传递游戏类型
                PlayType: BLDef.IdentityType.FORMAL,
                PlatformType: BLDef.PlatformType.Web_PC
            };
            localStorage.setItem('PlayGamePostData', URLService.GetUrlParameterFromObj(dataObj));
            CommUtility.OpenPlayGameWindow('webPlayGame', 'PlayGamePopUpWindow');
            // EventBus.$emit('showTransferMessage', this.GameList);
        },
        // 获取游戏列表 2018.11.15 SeyoChen end
    },
};
