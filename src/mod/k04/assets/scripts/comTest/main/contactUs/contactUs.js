import ContactUsForm from '@/mod/k04/components/sub/contactUs/contactUsForm.vue';
import CommonService from '@/mod/k04/assets/scripts/businessLogic/commonService.js';
import informationService from '@/mod/k04/assets/scripts/businessLogic/informationService.js';
import BLDef from '@/assets/scripts/common/BLDef.js'; 
import EventBus from '@/assets/scripts/common/EventBus.js';

export const ContactUs = {

    data() {
        return {
            // 登入狀態
            LoginStatus: undefined,

            // 類型選單
            TypeList: {},

            Account: undefined,
        };
    },

    mounted: function () {
        this.LoadMainPage();
    },

    methods: {
        // 載入頁面
        LoadMainPage: async function () {
            const retData = await CommonService.Comm_CheckPermission();

            if ( retData.Ret != BLDef.ErrorRetType.SUCCESS) {
                const notifyData = {
                    NotifyMessage: retData.Message,
                    CloseFunction: window.history.go(-1),
                };
                EventBus.$emit('showNotifyMessage', notifyData);
                return;
            } // end if

            
            if ( (retData.Status == BLDef.SysAccountStatus.LOGINED_ENABLED) || (retData.Status == BLDef.SysAccountStatus.LOGINED_FROZEN)) {
                // 帳號已登入的狀態
                EventBus.$emit('GlobalLoadingTrigger', true);
                const loginData = await CommonService.Comm_GetMemberInfo();
                EventBus.$emit('GlobalLoadingTrigger', false);
                this.Account = loginData.Data.Member.Acct;
                this.LoginStatus = true;
                const data = await informationService.ContactUs_LoadMainPage_LoginUser();
                this.TypeList = data.Data.TypeList;
                // console.log(temp);
            } else {
                // 帳號未登入的狀態
                this.LoginStatus = false;
                const data = await informationService.ContactUs_LoadMainPage_Guest();
                this.TypeList = data.Data.TypeList;
            } // end if
        }, // end LoadMainPage
    },

    components: {
        // GuestForm,
        // MemberForm,
        ContactUsForm,
    },
};