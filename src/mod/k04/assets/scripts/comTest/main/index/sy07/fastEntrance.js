import BLDef from '@/assets/scripts/common/BLDef.js';
import EventBus from '@/assets/scripts/common/EventBus.js';
import CommonService from '@/mod/k04/assets/scripts/businessLogic/commonService.js';

export const indexFast7 = {
	data() {
		return {
			IsLogin: undefined,
			MemberInfo: {},
		};
	},
	created: function() {
		this.CheckIsLogin();
	},
	methods: {
		// 檢查登入狀態
		CheckIsLogin: async function() {
			const data = await CommonService.Comm_CheckPermission();
			switch(data.Status) {
				case BLDef.SysAccountStatus.NOT_LOGIN:
					this.IsLogin = false;
					break;
				case BLDef.SysAccountStatus.LOGINED_ENABLED:
				case BLDef.SysAccountStatus.LOGINED_FROZEN:
					this.IsLogin = true;
					this.GetMemberInfo();
					break;
				default:
					break;
			}
		},
		GetMemberInfo: async function() {
			// 取得會員資訊
			const data = await CommonService.Comm_GetMemberInfo();
			if(data.Ret == 0) {
				this.MemberInfo = data.Data.Member;
			}
		},
		// 檢查帳號狀態
		CheckLoginStatusBeforeRoute: async function(routeName) {
			const notifyData = {};
			const data = await CommonService.Comm_CheckPermission();

			switch(data.Status) {
				case BLDef.SysAccountStatus.LOGINED_ENABLED:
					this.$router.push({
						name: routeName
					});
					break;
				case BLDef.SysAccountStatus.LOGINED_FROZEN:
					notifyData.NotifyMessage = '账户被冻结, 请联系客服人员';
					EventBus.$emit('showNotifyMessage', notifyData);
					break;
				default:
					break;
			}
		}
	},
	watch: {
		fastEntranceObj: function() {
			this.DealUpdatedData();
		}
	}
};