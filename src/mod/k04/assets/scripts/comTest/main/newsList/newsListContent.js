import InformationService from '@/mod/k04/assets/scripts/businessLogic/informationService.js';
import BLDef from '@/assets/scripts/common/BLDef.js';
import EventBus from '@/assets/scripts/common/EventBus.js';
import HttpService from '@/assets/scripts/common/HttpService.js';
import CommUtility from '@/assets/scripts/common/CommUtility.js';
import URLService from '@/assets/scripts/common/URLService.js';
import Vue from 'vue';

export const NewsListContent = {
    props: {
        GetCurrentPageData: '',
    },
    data () {
        return {
            NewsList: {
                Title: '',
                TitlePic: '',
                MessageCotent: '',
                MessageTick: '',
            },

            MessageContentData: [],

            // 分頁資料
            PageInfo: {
                PageNo: 1,
                PageSize: 3,
                PageCount: 0,
                TotalCount: 0,
            },

            CommUtility: CommUtility,

            // 資源位置
            Resource: Vue.prototype.$ResourceURL,
            // 資源旗標
            ResourceFlag: false,

            // 錨點暫存物件
            AnchorDataObject: {
                Anchor: undefined,
                PageNo: undefined,
                FromDetail: false,
            },

            NewsListNull: false, // 2018.12.05 SeyoChen 当新闻条数为0时 隐藏
        };
    },

    created: function () {
        this.GetNews(1);
        this.GoToAnchor();
    },
    mounted: function () {
        this.GetNews(1);
    },
    methods: {

        // 獲取新聞清單
        GetNews: async function (pageNo) {
            this.NewsList = []
            this.PageInfo.PageNo = pageNo;
            const dataObj = {
                WGUID: Vue.prototype.$WGUID,
                PagingInfo: this.PageInfo,
            };
            const retData = await InformationService.GetNewsList(dataObj);

            // 失敗
            if (retData.Ret != 0) {
                const notifyData = {
                    NotifyMessage: retData.Message
                };
                EventBus.$emit('showNotifyMessage', notifyData);
                return;
            } // end if

            // 成功
            console.log('temp11111111')
            console.log(retData.Data.NewsList)
            console.log('temp11111111111')
            this.NewsList = retData.Data.NewsList;

            this.GetContentData()
            // for (let i = 0; i < this.NewsList.length; i++) {
            // } // end for

            // 更新Paging
            this.PageInfo.PageCount = Math.ceil(retData.Data.PagingInfo.TotalCount / retData.Data.PagingInfo.PageSize);
            this.$parent.PageInfo.PageCount = this.PageInfo.PageCount;
            EventBus.$emit('Paginate_ResetPageNo', pageNo);
            // 資源顯示
            this.ResourceFlag = true;

            // 2018.12.05 SeyoChen 当新闻条数为0时不执行加载
            if (retData.Data.PagingInfo.TotalCount !== 0) {
                this.NewsListNull = true;
            }

            this.$nextTick(function () {
                // 新闻列表滚动条
                $('.news-main-cont').perfectScrollbar();
            });
        }, // end GetNews

        // 獲得內容
        GetContentData: async function () {
            const self = this
            self.MessageContentData = [];
            let isValueList;
            let tempText;
            for (let i = 0; i < self.NewsList.length; i++) {
                const contentData = self.NewsList[i].MessageCotent;
                console.log('contentData111111111')
                console.log(contentData)
                console.log('contentData111111111')
                if (contentData === '' || contentData === 'null' || contentData === undefined || contentData === null) {
                    console.log('1212')
                    tempText = self.NewsList[i].MessageCotent
                } else {
                    tempText = await HttpService.GetContent(self.Resource + self.NewsList[i].MessageCotent);
                }
                isValueList = false;
                for (let z = 0; z < self.MessageContentData.length; z++) {
                    if (self.NewsList[i].ID === self.MessageContentData[z].ID) {
                        isValueList = true;
                    }
                }
                if (!isValueList) {
                    self.MessageContentData.push({
                        'ID': self.NewsList[i].ID,
                        'MessageCotent': tempText,
                        'MessageTick': self.NewsList[i].MessageTick,
                        'MessageTime': self.NewsList[i].MessageTime,
                        'Title': self.NewsList[i].Title,
                        'TitlePic': self.NewsList[i].TitlePic
                    })
                }
            }
            console.log('this.MessageContentData++++++++++++++++++++++')
            console.log(self.NewsList)
            console.log(self.MessageContentData)
            console.log('this.MessageContentData-----------------------')
            // for (let g = 0; g < this.NewsList.length; g++) {
            //     for (let h = 0; h < this.MessageContentData.length; h++) {
            //         if (this.NewsList[g].ID == this.MessageContentData[h].ID) {
            //             const temp = await HttpService.GetContent(this.Resource + this.NewsList[g].MessageCotent);
            //             console.log(temp)
            //             this.MessageContentData.push({
            //                 'ID': this.NewsList[g].ID,
            //                 'MessageCotent': temp,
            //                 'MessageTick': this.NewsList[g].MessageTick,
            //                 'MessageTime': this.NewsList[g].MessageTime,
            //                 'Title': this.NewsList[g].Title,
            //                 'TitlePic': this.NewsList[g].TitlePic
            //             });
            //         }
            //     }
            // }
            // const temp = await HttpService.GetContent(this.Resource + this.NewsList[i].MessageCotent);
            // console.log(temp)
            // this.MessageContentData.push({
            //     'ID': this.NewsList[i].ID,
            //     'MessageCotent': temp,
            //     'MessageTick': this.NewsList[i].MessageTick,
            //     'MessageTime': this.NewsList[i].MessageTime,
            //     'Title': this.NewsList[i].Title,
            //     'TitlePic': this.NewsList[i].TitlePic
            // });

            // for (let i = 0; i < this.NewsList.length; i++) {
            //     if (this.NewsList[i].MessageCotent != '') {
            //         const temp = await HttpService.GetContent(this.Resource + this.NewsList[i].MessageCotent);
            //         if (this.MessageContentData[i].ID != this.NewsList[i].ID) {
            //             this.MessageContentData.push({
            //                 'ID': this.NewsList[i].ID,
            //                 'MessageCotent': temp[i],
            //                 'MessageTick': this.NewsList[i].MessageTick,
            //                 'MessageTime': this.NewsList[i].MessageTime,
            //                 'Title': this.NewsList[i].Title,
            //                 'TitlePic': this.NewsList[i].TitlePic
            //             });
            //         }
            //     }
            // } // end for
            // const contentData = this.NewsList[count].MessageCotent

            // this.MessageContentData.splice(count, 1, temp);
            // if (MessageCotent === '') {
            //     return
            // }

            // for (var n in this.NewsList) {
            //     console.log(this.NewsList[n])
            //     if (this.NewsList[n] != undefined) {
            //         this.NewsList[n] = this.MessageContentData[n];
            //     }
            // }
            // console.log('this.MessageContentData==========================')
            // console.log(this.NewsList)
            // console.log(this.MessageContentData)
            // console.log('this.MessageContentData1111111111111111111111')
        }, // end GetContentData

        //  跳至新聞詳細
        UrlRedirectDetail: function (target) {
            this.AnchorDataObject.Anchor = $(window).scrollTop();
            this.AnchorDataObject.PageNo = this.GetCurrentPageData == '' ? 1 : this.GetCurrentPageData;
            localStorage.setItem('newsListAnchor', URLService.GetUrlParameterFromObj(this.AnchorDataObject));
            this.$router.push({
                name: 'NewsListDetails',
                params: {
                    ID: target.ID
                }
            });
        }, // end UrlRedirectDetail

        // 將畫面移到指定錨點
        GoToAnchor: function () {
            if (localStorage.getItem('newsListAnchor')) {
                const tempObj = URLService.GetObjFromUrlParameter(localStorage.getItem('newsListAnchor'));
                if (tempObj.FromDetail) {
                    const self = this;
                    setTimeout(function () {
                        self.GetNews(tempObj.PageNo);
                        $(window).scrollTop(tempObj.Anchor);
                        localStorage.removeItem('newsListAnchor');
                    }, 0);
                } // end if
            } // end if
        }, // end GoToAnchor
    },

    watch: {
        GetCurrentPageData: function () {
            this.GetNews(this.GetCurrentPageData);
        },
    },
};
