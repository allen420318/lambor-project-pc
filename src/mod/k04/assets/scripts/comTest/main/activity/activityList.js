import InformationService from '@/mod/k04/assets/scripts/businessLogic/informationService.js';
import CommonService from '@/mod/k04/assets/scripts/businessLogic/commonService.js';
import CommUtility from '@/assets/scripts/common/CommUtility.js';
import BLDef from '@/assets/scripts/common/BLDef.js';
import EventBus from '@/assets/scripts/common/EventBus.js';
import HttpService from '@/assets/scripts/common/HttpService.js';
import ActivityListBanner from '@/mod/k04/components/sub/activity/activityListBanner.vue';
import ActivityListContent from '@/mod/k04/components/sub/activity/activityListContent.vue';
import ActivityListPaging from '@/mod/k04/components/sub/paginate.vue';
import Vue from 'vue';

export const ActivityList = {

    data() {
        return {
            showID: undefined,
            ActivityList: {

            },

            PageInfo: {
                PageCount: 0,
            },
            CurrentPageData: '',
            PromotionDetail: {

            },

            // 圖片旗標
            ActLogoFlag: false,
            // 資源位置
            Resource: Vue.prototype.$ResourceURL,
            // 首存，续存
            OtherActivityData: false,
            // 传递活动详情到个人中心
            TransActivityData: {

            }
        };
    },

    mounted: function () {
        // 60版 点击按钮关闭详情弹窗
        $('.activity-close').click(function () {
            $('#a-p-main').hide();
        });
        // 23版隐藏详情显示列表
        $('.activity-popup-back').click(function () {
            $('#a-p-main').fadeOut();
            $('.activity-page').fadeIn();
        });
    },
    methods: {
        TriggerMemberUser: function (data) {
            this.$store.dispatch('setSwitchNameText', data);
            EventBus.$emit('personalInfo');
        },
        GetCurrentPageData: function (page) {
            this.CurrentPageData = page;
        },
        // 弹窗弹出详细
        showDetail: function (ID) {
            if (ID >= 0) {
                this.showID = 0;
                this.OtherActivityData = true;
                this.GetPromotionDetail(ID);
            } else if (ID == -1) {
                this.showID = 1;
            } else if (ID == -2) {
                this.showID = 2;
            }
            $('#a-p-main').show();
            $('#activity-scroll-box').perfectScrollbar();
        },
        // 本页内动画显示详细
        UrlRedirectDetailAnimation: function (ID) {
            if (ID >= 0) {
                this.showID = 0;
                this.OtherActivityData = true;
                this.GetPromotionDetail(ID);
            } else if (ID == -1) {
                this.showID = 1;
            } else if (ID == -2) {
                this.showID = 2;
            }
            $('.activity-page').fadeOut();
            $('#a-p-main').fadeIn();
        }, // end UrlRedirectDetail
        // 獲得頁面資料
        GetPromotionDetail: async function (ID) {
            const dataObj = {
                WGUID: Vue.prototype.$WGUID,
                ID: ID
            };
            const retData = await InformationService.GetActivityDetail(dataObj);
            this.TransActivityData = retData.Data.Detail; // 2018.09.21 SeyoChen

            // 失敗
            if (retData.Ret != 0) {
                const notifyData = {
                    NotifyMessage: retData.Message
                };
                EventBus.$emit('showNotifyMessage', notifyData);
                return;
            } // end if

            // 成功
            this.PromotionDetail = retData.Data.Detail;
            const contentData = retData.Data.Detail.ActContent

            this.PromotionDetail.ActContent = await HttpService.GetContent(this.Resource + retData.Data.Detail.ActContent);
            if (contentData === '') {
                return
            }
            // 修改进入显示资源路径问题 2019.1.25 SeyoChen
            $('#a-p-main-detail').html(this.PromotionDetail.ActContent);
            // 修改进入显示资源路径问题 2019.1.25 SeyoChen end

            if (!(retData.Data.Detail.ActLogo == null)) {
                this.ActLogoFlag = true;
            } // end if

            $('.activity-popup-detail').perfectScrollbar();
        }, // end GetPromotionDetail

        // 開啟視窗
        OpenWindow: function () {
            $('#a-p-main').hide();
            CommUtility.WebShowUniqueForm('informationForm');
        }, // end OpenWindow

        // 开启弹窗但不隐藏优惠活动详情
        OpenAndNoHide: function () {
            CommUtility.WebShowUniqueForm('informationForm');
        }, // end OpenAndNoHide

        // 關閉視窗
        CloseWindow: function () {
            CommUtility.WebCloseUniqueForm('informationForm');
        }, // end CloseWindow

        // 传值到支付页面 2018.09.21 SeyoChen
        ToRechargePage: async function (num) {
            const retDataCheck = await CommonService.Comm_CheckPermission();
            console.log(retDataCheck.Status);
            if (retDataCheck.Status != 0 && retDataCheck.Status != BLDef.SysAccountStatus.LOGINED_FROZEN) {
                console.log("12")
                this.CloseWindow();

                // this.$router.push( { name: 'webMemberDeposit', params: { ChargeType: num, ActivityData: this.TransActivityData } });
                this.$route.params.ActivityData = this.TransActivityData;
                this.$route.params.ChargeType = num;
                this.$store.dispatch('setSwitchNameText', 'webMemberDeposit');
                EventBus.$emit('personalInfo');
            } else {
                this.CloseWindow();
                this.$router.push({
                    name: 'Login'
                });
            }
        }
    },

    components: {
        ActivityListBanner,
        ActivityListContent,
        ActivityListPaging,
    },

};
