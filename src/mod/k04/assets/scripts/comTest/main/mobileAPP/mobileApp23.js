	import BLDef from '@/assets/scripts/common/BLDef.js';
	import CommUtility from '@/assets/scripts/common/CommUtility.js';

	export const mobileApp23 = {
	    mounted () {
	        var list = $('.all-nav li'),
	            mb_list = $('.m-b-nav ul li');
	        var len = list.length;
	        var i = 0,
	            s = 0;
	        var me = 0,
	            mb_me = 0,
	            a, b, x;

	        mb_list.click(function () { // 手机下注切换
	            s = $(this).index();
	            $(this).addClass('on').siblings().removeClass('on');
	            animate2(s);
	            if (s == mb_me) {
	                return false;
	            } else {
	                if (s > mb_me) {
	                    Left('.m-b-main > div', '.m-b-main', s);
	                } else {
	                    Right('.m-b-main > div', '.m-b-main', s);
	                }
	            }
	            mb_me = s;
	        });

	        function Left (a, b, x) {
	            $(a).eq(x).stop().css({
	                'left': '50%',
	                'z-index': '1'
	            });
	            $(b).stop().animate({
	                'left': '-100%'
	            }, 500, function () {
	                $(b).stop().css('left', '0');
	                $(a).eq(x).stop().css({
	                    'left': '0'
	                });
	                $(a).eq(x).siblings().stop().css({
	                    'left': '0',
	                    'z-index': '0'
	                });
	            });
	        }

	        function Right (a, b, x) {
	            $(a).eq(x).stop().css({
	                'left': '-50%',
	                'z-index': '1'
	            });
	            $(b).stop().animate({
	                'left': '100%'
	            }, 500, function () {
	                $(b).stop().css('left', '0');
	                $(a).eq(x).stop().css({
	                    'left': '0'
	                });
	                $(a).eq(x).siblings().stop().css({
	                    'left': '0',
	                    'z-index': '0'
	                });
	            });
	        }

	        function animate2 (text2) { // 文字淡出
	            $('.m-b-main > div').eq(text2).find('.index-content .l-b-cont').css({
	                'bottom': '50%',
	                'opacity': '1'
	            });
	            $('.m-b-main > div').eq(text2).siblings().find('.index-content .l-b-cont').css({
	                'bottom': '30%',
	                'opacity': '0'
	            });
	        }
	    },
	    methods: {
	        // 玩AG Html5 Game
	        PlayAGHtml5Game: function () {
	            const dataObj = {
	                GameAPIVendor: BLDef.GameApiType.AG,
	                GameCode: '',
	                GameCatlog: BLDef.GameCatlogType.LiveVideonHtml5,
	                PlayType: BLDef.IdentityType.FORMAL,
	                Platform: BLDef.PlatformType.Web_PC
	            };
	            CommUtility.OpenPlayGameWindow('PlayGame', 'PlayGamePopUpWindow', dataObj);
	        }, // end PlayAGHtml5Game

	        // 導頁到pt html5大廳
	        RedirectToPTHtml5Lobby: function () {
	            CommUtility.OpenPlayGameWindow('Html5Lobby', 'PlayGamePopUpWindow');
	        }, // end RedirectToPTHtml5Lobby

	        // 玩SB Html5 Game
	        PlaySBHtml5Game: function () {
	            const dataObj = {
	                GameAPIVendor: BLDef.GameApiType.SABA,
	                GameCode: '',
	                GameCatlog: BLDef.GameCatlogType.ElectronicGameHtml5,
	                PlayType: BLDef.IdentityType.FORMAL,
	                Platform: BLDef.PlatformType.Web_PC
	            };
	            CommUtility.OpenPlayGameWindow('PlayGame', 'PlayGamePopUpWindow', dataObj);
	        }, // end PlaySBHtml5Game
	    }
	}
