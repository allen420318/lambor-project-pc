import BLDef from '@/assets/scripts/common/BLDef.js';
import CommUtility from '@/assets/scripts/common/CommUtility.js';

export const mobileApp02 = {
    created: function () {
        this.load();
    },
    methods: {
        load: function () {
            this.$nextTick(function () {
                // 计算scrollTop的高度切换导航栏
                function cst (top, height, dis) {
                    var all = $(window).scrollTop();
                    var distance = 200; // 显示的内容距离顶部显示框的高度
                    var order = parseInt((all - top + distance) / height);
                    var clientHeight = $('body').height();

                    if (all >= clientHeight - dis) {
                        order++;
                    }

                    $('.order' + order + '').addClass('hasSelect').siblings('li').removeClass('hasSelect');
                    $('.order_right span').css({
                        'display': 'none'
                    });
                    $('.order_right .point').removeClass('hasSelect');
                    $('.order' + order + ' span').css({
                        'display': 'block'
                    });
                    $('.order' + order + ' .point').addClass('hasSelect');
                    $('.order_top .order' + order + '').addClass('hasSelect');

                    // 手机投注，第一个选项的特别设置
                    if (order == 0) {
                        $('.mobile_am .order0 .hasSelect').css({
                            'border-color': '#ffffff',
                            'background': 'none'
                        });
                        $('.mobile_am .order0').siblings('li').children('.point').css({
                            'background': '#ffffff',
                            'border-color': '#ffffff'
                        });

                        // 手机投注 AG卡片动画重置
                        $('.card').removeClass('slider_at');
                        $('.card_1').removeClass('card_f1');
                        $('.card_2').removeClass('card_f2');
                        $('.mobile_img2').removeClass('mobile_img2_at');

                        $('.mobile_girl').css({
                            'opacity': '0'
                        });
                    } else {
                        $('.mobile_am .point').css({
                            'background': '#8e8d91',
                            'border-color': '#8e8d91'
                        });
                        $('.mobile_am li .hasSelect').css({
                            'background': '#ffffff'
                        });
                    }
                    // 滚动到第一部分
                    if (order == 1 && !$('.card_1').hasClass('slider_at')) {
                        // AG 卡牌飞入效果
                        $('.card_1').addClass('slider_at').addClass('card_f1');
                        $('.card_2').addClass('slider_at').addClass('card_f2');
                        $('.mobile_img2').addClass('mobile_img2_at');

                        $('.card_1').css({
                            'left': '-500px',
                            'top': '575px'
                        }).animate({
                            'left': '-400px',
                            'top': '650px'
                        }, 300);

                        $('.card_2').css({
                            'right': '-400px',
                            'top': '550px'
                        }).animate({
                            'right': '-300px',
                            'top': '600px'
                        }, 300);
                        // AG 说话圈
                        //		$(".prompt").css({'top':'500px','opacity':'0'}).animate({'top':'450px','opacity':'1'},750);
                        $('.mobile_phone').css({
                            'top': '480px',
                            'opacity': '0'
                        }).animate({
                            'top': '430px',
                            'opacity': '1'
                        }, 500);
                        $('.mobile_girl').css({
                            'opacity': '0'
                        }).delay(500).animate({
                            'opacity': '1'
                        }, 750);
                        $('.card_box1').css({
                            'opacity': '0'
                        }).delay(750).animate({
                            'opacity': '1'
                        });
                        $('.card_box2').css({
                            'opacity': '0'
                        }).delay(750).animate({
                            'opacity': '1'
                        });
                    }
                    // 滚动到第二部分
                    if (order == 2 && !$('.eagle').hasClass('eagle_at')) {
                        $('.mobile_PT_man').css({
                            'left': '-200px',
                            'opacity': '0'
                        }).animate({
                            'left': '0',
                            'opacity': '1'
                        }, 500);
                        $('.eagle').addClass('eagle_at');
                    } else if (order == 0 || order == 1) {
                        $('.eagle').removeClass('eagle_at');
                    }
                }
                // 执行滚动函数调用
                var height = $('.Introduction_list').height(); // 内容框的高度
                var top = $('.Introduction_list').offset().top; // 第一个内容模块到顶部的距离
                var num; // 记录点击第几个

                $('.mobile_am li').siblings('li').children('.point').css({
                    'background': '#ffffff'
                });

                // 右侧浮动导航按钮
                $('.order_right li').click(function () {
                    num = $(this).attr('data-num');
                    st(num, height, top, 0);
                });

                // 手机下注 down加载
                $('.m_down').click(function () {
                    st2(1440);
                });

                // 屏幕滚动
                $(window).scroll(function () {
                    cst(top, height, 400);
                });
                // 点击切换
                $('.mobile_select_list').mouseenter(function () {
                    $('.change').stop();
                    var num = $(this).attr('data-num');
                    $(this).addClass('hasSelect').siblings('.mobile_select_list').removeClass('hasSelect');

                    $('.change_' + num + '').css({
                        'display': 'block',
                        'opacity': '0'
                    }).siblings('.change').animate({
                        'opacity': '0'
                    }, 500);
                    $('.change_' + num + '').animate({
                        'opacity': '1'
                    }, 500).siblings('.change').animate({
                        'opacity': '0'
                    }, 500);

                    $('.change_' + num + '').siblings('.change').children('a').css({
                        'display': 'none'
                    });
                    $('.change_' + num + ' a').css({
                        'display': 'block'
                    });
                });
                // banner 动效
                $('.circle_box').css({
                    'opacity': '1'
                });
                $('.mobile_ipad_1').css({
                    'margin-top': '320px',
                    'opacity': '0'
                });
                $('.mobile_ipad_1').animate({
                    'margin-top': '284px',
                    'opacity': '1'
                }, 500);

                // 手机投注 banner动画
                $('.mobile_ipad_1').addClass('m_ipad_at');
                $('.m_ipad_bg').css({
                    'opacity': '0'
                });
                $('.m_ipad_bg').animate({
                    'opacity': '1'
                }, 500); // ipad背景
            })
        },
        // 玩AG Html5 Game
        PlayAGHtml5Game: function () {
            const dataObj = {
                GameAPIVendor: BLDef.GameApiType.AG,
                GameCode: '',
                GameCatlog: BLDef.GameCatlogType.LiveVideonHtml5,
                PlayType: BLDef.IdentityType.FORMAL,
                Platform: BLDef.PlatformType.Web_PC
            };
            CommUtility.OpenPlayGameWindow('webPlayGame', 'PlayGamePopUpWindow', dataObj);
        }, // end PlayAGHtml5Game

        // 導頁到pt html5大廳
        RedirectToPTHtml5Lobby: function () {
            CommUtility.OpenPlayGameWindow('webHtml5Lobby', 'PlayGamePopUpWindow');
        }, // end RedirectToPTHtml5Lobby

        // 玩SB Html5 Game
        PlaySBHtml5Game: function () {
            const dataObj = {
                GameAPIVendor: BLDef.GameApiType.SABA,
                GameCode: '',
                GameCatlog: BLDef.GameCatlogType.ElectronicGameHtml5,
                PlayType: BLDef.IdentityType.FORMAL,
                Platform: BLDef.PlatformType.Web_PC
            };
            CommUtility.OpenPlayGameWindow('webPlayGame', 'PlayGamePopUpWindow', dataObj);
        }, // end PlaySBHtml5Game
    },
};
