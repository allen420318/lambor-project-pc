/**
 * @name 捕鱼内容组件
 * @author dingruijie
 * @time 2019-09-12
 * @augments PageCount 计算后的分页总数 类型：Number 默认0
 * @augments pageSize 分页每页条数 类型: Number 默认每页10条
 * @event 父组件请求完数据 初始化分页数据 this.$refs[refname].computeGameList(1，allList) 初始传1表示渲染第一页
 */
// 分页组件
import catchfishPaginate from '@/mod/k04/components/sub/paginate.vue';
export default {
    name: 'catchfish-content',
    props: {
        PageCount: {
            type: Number,
            default: 0
        },
        pageSize: {
            type: Number,
            default: 10
        }
    },
    components: {
        catchfishPaginate
    },
    data () {
        return {
            pageNum: 1,
            CatchfishList: [],
            allCatchfishList: []
        }
    },
    methods: {
        // 打开游戏
        PlayGame () {
            this.$message({
                message: this.$t('message.noGame_data_yet'),
                type: 'warning',
                duration: 1500,
                center: true
            })
        },
        // 获取当前页游戏列表
        GetCurrentPageData: async function (pageNo) {
            this.computeGameList(pageNo);
        },
        // 计算每一页展示数据
        computeGameList (num, initList) {
            if (initList) this.allCatchfishList = initList;
            this.pageNum = num;
            this.lotteryList = [];
            let gameList = [];
            let pageSize = this.pageSize; // 每页最大9条
            let lowerLimit = pageSize * (num - 1); // 从下限开始
            let upperLimit = pageSize * num; // 从下限结束
            let allList = this.allCatchfishList; // 数据总列表
            if (allList.length > lowerLimit && allList.length > upperLimit) {
                // 游戏总数大于当前页最大序号
                for (let i = lowerLimit; i < upperLimit; i++) {
                    gameList.push(allList[i]);
                }
            } else {
                for (let i = lowerLimit; i < allList.length; i++) {
                    gameList.push(allList[i]);
                }
            }
            this.CatchfishList = gameList;
            // 插入box之前清除原先已有box避免重复
            $('.catchfish-expect').remove();
            setTimeout(() => {
                this.AjectAddBox(this.CatchfishList.length);
            }, 50);
        },
        // 2018.11.15 SeyoChen 动态识别添加游戏模块
        AjectAddBox: function (num) {
            const boxSize = parseInt(3); // 页面中一行的box个数
            const boxHtml = `<li class="catchfish-logo-li catchfish-expect catchfishAbnormal">
            <i class="staytuned abnormalDefault"></i></li>`;
            const box = $('.catchfish-logo-lists');
            if (num % boxSize === 0 && num !== 0) {} else {
                for (let i = 0; i < Math.abs(boxSize - (num % boxSize)); i++) {
                    box.append(boxHtml);
                }
            }
        },
    }
}
