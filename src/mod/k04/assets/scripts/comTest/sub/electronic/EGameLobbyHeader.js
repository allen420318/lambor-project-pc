import BLDef from '@/assets/scripts/common/BLDef.js';
import Vue from 'vue';
import CommUtility from '@/assets/scripts/common/CommUtility.js';
import GameService from '@/mod/k04/assets/scripts/businessLogic/gameService.js';
import HttpService from '@/assets/scripts/common/HttpService.js';
import Regex from '@/mod/k04/assets/scripts/common/CommDef.js';
import EventBus from '@/assets/scripts/common/EventBus.js';

export const EGameLobbyHeader = {
    props: {
        GetCurrentPageData: '',
        // GetGameApiNo: '',
    },
    data () {
        return {
            // 頁面DataModel
            LoadMainPageDataModel: {
                GameTypeFilterList: {

                },
            },

            // EGameList 2018.11.15 SeyoChen 遍历游戏
            GameList: [],
            EgameList: [],

            // 查詢 Post DataModel
            PostDataModel: {
                GameAPIVendor: {
                    Value: '',
                },
                GameTypeFilter: {
                    Value: '-1',
                },
                SlotLineFilter: {
                    Value: '-1',
                },
                Keyword: '',
            },

            // 分頁資料
            PageInfo: {
                PageNo: 1,
                PageSize: 12,
                PageCount: 0,
            },

            Reg: Regex.Regex,

            GameNum: 0,

            IfIndexGame: 0
        };
    },

    created: function () {
        this.GatGame(); // 2018.11.15 SeyoChen 遍历游戏
        // this.GetGameApiType();
        // this.BindGameTypeEvt(1);  // 载入第一页
        // this.LoadMainPage()
    },
    mounted: function () {
        CommUtility.BindEnterTrigger('ElectronicGameForm', this.Search);

        // 电子游戏 游戏图标的切换
        // const that = this; // 2018.12.04
        $('.ele-vendor-lists').on('click', '.ele-vendor-li', function () {
            $(this).addClass('on').siblings('.ele-vendor-li').removeClass('on');
            // const logoUrl = $(this).children().children('img').attr('src');
            // const index = ($(this).index()) + 1; // 2018.12.04
            // that.GameNum = index; // 2018.12.04
            // $('.logo-show').children('img').attr('src', logoUrl);
        });
        // this.$emit('getGameNum', this.GameNum);
        EventBus.$on('GameApiNo', flag => {
            console.log(flag)
            setTimeout(() => {
                $('.gameNo' + flag)
                    .addClass('on')
                    .siblings('.ele-vendor-li')
                    .removeClass('on');
            }, 200);
            this.BindGameTypeEvt(flag);
        })
    },
    methods: {
        // 遍历获取游戏列表
        GatGame: async function () {
            this.GameList = JSON.parse(localStorage.getItem('GameList'));

            const dataObj = {
                WGUID: '',
                GameTypeNo: '',
                PlatformType: BLDef.PlatformType.Web_PC
            };
            dataObj.WGUID = Vue.prototype.$WGUID;
            dataObj.GameTypeNo = '0'; // 0.全部 1.真人视讯 2.体育 3.彩票 4.电子
            const retData = await HttpService.PostDynamic(0, 214, dataObj);

            if (retData.Ret === 0 && this.GameList !== retData.Data.GameAPIVerdorList) {
                // 判断本地缓存是否与之前的相同
                this.GameList = retData.Data.GameAPIVerdorList;
                localStorage.setItem('GameList', JSON.stringify(retData.Data.GameAPIVerdorList));
            }

            for (let i = 0; i < this.GameList.length; i++) {
                // 2为 欧博游戏 2019.01.12 筛选游戏 并 判定状态 SeyoChen
                if (this.GameList[i].GameApiNo !== '2' && this.GameList[i].GameTypeNo === 4 && this.GameList[i].GameTypeState !== '704') {
                    this.EgameList.push(this.GameList[i]);
                }
            }

            if (this.$route.params.GameNo != undefined) {
                this.BindGameTypeEvt(this.$route.params.GameNo);
            } else {
                this.BindGameTypeEvt(this.EgameList[0].GameApiNo); // 载入第一页 this.EgameList[0].ApiStatus
            }
            // this.GetGameApiNo = this.EgameList[0].GameApiNo;
        },

        // 电子游戏点击事件
        // GetGameApiType: function (list) {
        //     this.PostDataModel.GameAPIVendor.Value = list;
        // },
        // 获取游戏列表 2018.11.15 SeyoChen end

        clearLoading () {
            EventBus.$off('GlobalLoadingTrigger');
        },
        BindGameTypeEvt: function (gameApiType) {
            this.PostDataModel.GameAPIVendor.Value = gameApiType;
            this.PostDataModel.GameTypeFilter.Value = -1; // 將過濾器設置回全部
            this.clearLoading();
            // this.GetElectronicGameList(1);

            // 2019.01.12 电子游戏详细游戏显示维护 SeyoChen
            // if (ApiStatus === 703) {
            //     $('.electronic-maintain').show();
            // } else {
            //     $('.electronic-maintain').hide();
            // }
            // 2019.01.12 电子游戏详细游戏显示维护 SeyoChen end

            // this.$emit('getGameNum', gameApiType);
            this.GameNum = gameApiType; // 2018.12.04 SeyoChen 图片类型对比，传递对应API编号
            this.LoadMainPage();
            // this.GetElectronicGameList(1);
            // this.GetElectronicGameList();
        },

        // 載入頁面
        LoadMainPage: async function (pageNo) {
            this.LoadMainPageDataModel = {
                GameTypeFilterList: {},
            };

            const dataObj = {
                GameAPIVendor: this.PostDataModel.GameAPIVendor,
            };

            const retData = await GameService.LoadMainPage(dataObj);

            if (retData.Ret != 0) {
                return;
            }

            this.LoadMainPageDataModel = retData.Data;
            if (pageNo == undefined) {
                this.GetElectronicGameList(1);
            } else {
                this.GetElectronicGameList(pageNo);
            }

            // 选中的厂商添加选中色块 20190724 SeyoChen
            if (this.$route.params.GameNo != undefined) {
                $('#game-list' + this.$route.params.GameNo).addClass('on').siblings('.ele-vendor-li').removeClass('on');
            }
            // 选中的厂商添加选中色块 20190724 SeyoChen end
        }, // end LoadMainPage

        // 取得電子遊戲List
        GetElectronicGameList: async function (pageNo) {
            // 更新Paging
            this.PageInfo.PageNo = pageNo;
            this.$parent.SearchPage = pageNo;
            EventBus.$emit('Paginate_ResetPageNo', pageNo);

            // 獲得頁面資料
            const loadDataObj = {
                GameAPIVendor: this.PostDataModel.GameAPIVendor,
            };

            const tempRetData = await GameService.LoadMainPage(loadDataObj);

            if (tempRetData.Ret != 0) {
                return;
            }
            this.LoadMainPageDataModel = tempRetData.Data;

            const dataObj = {
                WGUID: Vue.prototype.$WGUID,
                GameAPIVendor: this.PostDataModel.GameAPIVendor, // 遊戲供應商
                GameTypeFilter: this.PostDataModel.GameTypeFilter, // 遊戲類型過濾條件
                Keyword: '', // 關鍵字
                PagingInfo: this.PageInfo,
                PlatformType: 1 // 2019.01.28 SeyoChen 添加识别PC端
            };
            EventBus.$emit('GlobalLoadingTrigger', true);
            const retData = await GameService.GameQuery(dataObj);
            EventBus.$emit('GlobalLoadingTrigger', false);
            // 成功
            this.LoadMainPageDataModel.GameList = retData.Data.GameList;
            this.$parent.LoadMainPageDataModel = this.LoadMainPageDataModel;
            this.PostDataModel.Keyword = '';
            // 更新Paging
            if (retData.Data.PagingInfo) {
                this.PageInfo.PageCount = Math.ceil(retData.Data.PagingInfo.TotalCount / retData.Data.PagingInfo.PageSize);
                this.$parent.PageInfo.PageCount = this.PageInfo.PageCount;
            } else {
                this.PageInfo.PageCount = 0;
            } // end if
        }, // end GetElectronicGameList

        // 搜尋功能
        Search: async function () {
            const flag = await this.validate();

            if (flag == false) {
                return;
            } // end if

            // 初始在第一頁
            this.PageInfo.PageNo = 1;
            this.$parent.SearchPage = 1;

            // 獲得頁面資料
            const loadDataObj = {
                GameAPIVendor: this.PostDataModel.GameAPIVendor,
            };

            const tempRetData = await GameService.LoadMainPage(loadDataObj);

            if (tempRetData.Ret != 0) {
                return;
            }
            this.LoadMainPageDataModel = tempRetData.Data;

            // 將過濾器設置回全部
            this.PostDataModel.GameTypeFilter.Value = -1;

            const dataObj = {
                WGUID: Vue.prototype.$WGUID,
                GameAPIVendor: this.PostDataModel.GameAPIVendor, // 遊戲供應商
                GameTypeFilter: this.PostDataModel.GameTypeFilter, // 遊戲類型過濾條件
                Keyword: this.PostDataModel.Keyword == undefined ? '' : this.PostDataModel.Keyword, // 關鍵字
                PagingInfo: this.PageInfo
            };
            EventBus.$emit('GlobalLoadingTrigger', true);
            const retData = await GameService.GameQuery(dataObj);
            EventBus.$emit('GlobalLoadingTrigger', false);
            // 成功
            this.LoadMainPageDataModel.GameList = retData.Data.GameList;
            this.$parent.LoadMainPageDataModel = this.LoadMainPageDataModel;

            // 更新Paging
            if (retData.Data.PagingInfo) {
                this.PageInfo.PageCount = Math.ceil(retData.Data.PagingInfo.TotalCount / retData.Data.PagingInfo.PageSize);
                this.$parent.PageInfo.PageCount = this.PageInfo.PageCount;
            } else {
                this.PageInfo.PageCount = 0;
            } // end if
        }, // end Search

        // 所有欄位驗證
        validate: function () {
            Object.keys(this.fields).forEach(key => {
                this.fields[key].touched = true;
            });
            return this.$validator.validateAll().then(result => {
                return result;
            });
        }, // end validate
    },

    watch: {
        GetCurrentPageData: function () {
            this.GetElectronicGameList(this.GetCurrentPageData);
        },
        GameNum: function (numb) {
            this.$emit('getGameNum', numb);
        }
    },
};
