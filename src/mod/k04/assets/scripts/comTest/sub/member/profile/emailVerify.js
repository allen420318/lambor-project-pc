import CommUtility from '@/assets/scripts/common/CommUtility.js';
import EventBus from '@/assets/scripts/common/EventBus.js';
import PersonalCenterService from '@/mod/k04/assets/scripts/businessLogic/personalCenterService.js';
import BLDef from '@/assets/scripts/common/BLDef.js';

export const initEmVe = {
    props: {
        LoadMainPageDataModel: {},
        LoadEmailVerifyData: {},
    },
    data () {
        return {
            VCountTime: undefined,
            CountTime: undefined,
            ProcessingFlag: false,
            CountingFlag: false,
        };
    },

    created: function () {
        const self = this;
        EventBus.$on('verifyEmailWindowTrigger', () => {
            self.OpenWindow();
        });
    },

    methods: {
        // 弹出客服窗口
        OpenNewBox: function (serveLink) {
            window.open(serveLink, '客服', 'scrollbars=1,width=365,height=565,left=10,top=150');
            this.CloseWindow();
        },

        // 開啟視窗
        OpenWindow: async function () {
            CommUtility.WebShowUniqueForm('emailVerifyForm');
            if (this.CountingFlag) {
                return;
            } // end if
            this.Reciprocal(this.LoadEmailVerifyData.ResendSeconds);
        }, // end OpenWindow

        // 關閉視窗
        CloseWindow: function () {
            CommUtility.WebCloseUniqueForm('emailVerifyForm');
        }, // end CloseWindow

        // 客服彈窗
        CustomerWindow: function () {
            this.CloseWindow();
            // CommUtility.WebShowUniqueForm('CustomerServiceWindow');
        }, // end  CustomerWindow

        // 計時器
        Reciprocal: function (tempTime) {
            this.CountingFlag = true;
            document.getElementById('ResendButton').style.cursor = 'default';
            const self = this;
            self.CountTime = tempTime;
            this.$parent.CountTime = tempTime;
            this.VCountTime = '(' + self.CountTime + ')';
            setTimeout(function () {
                if (self.CountTime > 0) {
                    self.CountTime--;
                    self.Reciprocal(self.CountTime);
                } else {
                    document.getElementById('ResendButton').style.cursor = 'pointer';
                    self.VCountTime = '';
                    self.CountingFlag = false;
                }
            }, 1000);
        }, // end CountTime

        // 重新發送驗證信
        ReSendVerifyEmail: async function () {
            // 是否已經倒數完
            if (this.CountTime > 0) {
                return;
            } // end if

            // 處理中則直接返回
            if (this.ProcessingFlag === true) {
                return;
            } // end if
            this.ProcessingFlag = true;

            // 資料物件
            const dataObj = {
                Email: this.LoadMainPageDataModel.Member.Email,
                Domain: document.location.protocol + '//' + location.host,
                OrgEmail: this.LoadMainPageDataModel.OrgMember.Email,
            };
            const retData = await PersonalCenterService.EmailVerify_SendValidEmail(dataObj);

            if (retData.Ret == 0) {
                // 成功
                this.IsEmailSendable = retData.Data.IsEmailSendable;
                this.LoadMainPageDataModel.OrgMember.Email = this.LoadMainPageDataModel.Member.Email;
                this.Reciprocal(this.LoadEmailVerifyData.ResendSeconds);
            } else if (retData.Ret == BLDef.ErrorRetType.COMM_SEND_VERIFICATION_EMAIL_COUNT_LIMIT) {
                // 寄信額度超過
                this.CloseWindow();
                this.$parent.EmailVerifyLimitUp();
            } else {
                // 操作失敗
                const notifyData = {
                    NotifyMessage: retData.Message,
                };
                EventBus.$emit('showNotifyMessage', notifyData);
            } // end if
            this.ProcessingFlag = false;
        }, // end ReSendEmail
    },
    beforeDestroy () {
        EventBus.$off('verifyEmailWindowTrigger');
    }
};
