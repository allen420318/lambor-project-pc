import PersonalCenterService from '@/mod/k04/assets/scripts/businessLogic/personalCenterService.js';
import memberDepositRemittanceIncome from '@/mod/k04/components/main/member/memberDepositRemittanceIncome.vue';
//  import MemberDepositRemittanceAcct from '@/components/web/sub/member/deposit/memberDepositRemittanceAcct.vue';
//  import MemberDepositRemittanceForm from '@/components/web/sub/member/deposit/memberDepositRemittanceForm.vue';
import MemberDepositPayQRcode from '@/components/sub/member/deposit/memberDepositPayQRcode.vue';

export const initDeRt = {
    props: {
        CurSelectedActivity: {
            BonusSerialNo: undefined, // 红利对应的资料序号
            ActivityType: undefined, // 红利活动方式 ==》 -1：未定义; 1：存款(红利设定); 2：红利活动
            ActType: undefined, // 红利方式细项分类 ==》 1: 首存 2: 续存
        }
    },
    data () {
        return {
            CurrentPageData: {
                BankInfo: {}
            },

            DepositTypeList: [],
            BankTrans: false, // 银行转账
            QRcode: false, // 二维码
            miantainBank: true, // 银行转账维护
            miantainCode: true, // 扫码支付维护

            DefaultData: [], // 2019.02.27 SeyoChen
            judgeComplete: false, // 判断是否开通
            selectON: ''
        };
    },
    created: function () {
        this.GetDefaultData();
    },
    methods: {
        // 获取默认Data
        GetDefaultData: async function () {
            const dataObj = {
                ActType: this.CurSelectedActivity.ActType,
                BonusSerialNo: this.CurSelectedActivity.BonusSerialNo,
                ActivityType: this.CurSelectedActivity.ActivityType,
            };
            this.DefaultData = await PersonalCenterService.CompanyDeposit_LoadMainPage(dataObj);
            this.CurrentPageData = this.DefaultData.Data;
            this.$emit('CurrentPageData', this.CurrentPageData); // 将此后台返回的活动详情传到父级 deposit
        },

        // 2019.02.12 SeyoChen
        BackLevel: function () {
            this.BankTrans = false;
            this.QRcode = false;
            this.DepositTypeList = [];
            this.$refs.remittance_form.ResetTip(); // 2019.02.18 SeyoChen 重置提示语
        },
        //          SelectTransFun: function (mode) {
        //              this.GetPageData(mode);
        //              switch (mode) {
        //                  case 1:
        //                      this.BankTrans = true;
        //                      this.QRcode = false;
        //                      break;
        //                  case 2:
        //                      this.BankTrans = false;
        //                      this.QRcode = true;
        //                      break;
        //                  default:
        //                      this.BankTrans = false;
        //                      this.QRcode = false;
        //                      break;
        //              }
        //          },
        SelectTransFun: function (mode) {
            this.selectON = mode;
            this.GetPageData(mode);
            switch (mode) {
                case 1:
                    memberDepositRemittanceIncome.methods.ShowIncome();
                    //                      this.BankTrans = true;
                    //                      this.QRcode = false;
                    break;
                case 2:
                    MemberDepositPayQRcode.methods.ShowQRcode();
                    break;
                default:
                    //                      this.BankTrans = false;
                    //                      this.QRcode = false;
                    break;
            }
        },
        // 2019.02.12 SeyoChen end
        GetPageData: async function (mode) {
            const dataObj = {
                ActType: this.CurSelectedActivity.ActType,
                BonusSerialNo: this.CurSelectedActivity.BonusSerialNo,
                ActivityType: this.CurSelectedActivity.ActivityType,
            };
            const DefaultData = await PersonalCenterService.CompanyDeposit_LoadMainPage(dataObj);

            if (DefaultData.Ret == 0) {
                // 判断是否开通 1：银行转账 2：扫码支付
                for (let i = 0; i < this.CurrentPageData.BankInfo.length; i++) {
                    if (this.CurrentPageData.BankInfo[i].PaymentWayType == 1 && this.miantainBank != false) {
                        this.miantainBank = false;
                    } else if (this.CurrentPageData.BankInfo[i].PaymentWayType == 2 && this.miantainCode != false) {
                        this.miantainCode = false;
                    }
                }
                // 判断是否开通 1：银行转账 2：扫码支付 end

                // 2019.03.12 SeyoChen 弹出维护中图片
                if (this.miantainCode == true || this.miantainBank == true) {
                    this.judgeComplete = true;
                }
                // 2019.03.12 SeyoChen 弹出维护中图片 end

                if (mode === 2) {
                    // 2019.03.01 SeyoChen 判断二维码数量及显示的类型
                    let QRcodeNumApli = false;
                    let QRcodeNumWeChat = false;
                    for (let i = 0; i < DefaultData.Data.BankInfo.length; i++) {
                        if (DefaultData.Data.BankInfo[i].PaymentWayType == 2 && DefaultData.Data.BankInfo[i].QRCodePaymentWayType == 1) {
                            QRcodeNumApli = true;
                        } else if (DefaultData.Data.BankInfo[i].PaymentWayType == 2 && DefaultData.Data.BankInfo[i].QRCodePaymentWayType == 2) {
                            QRcodeNumWeChat = true;
                        }
                    }
                    // 2019.03.01 SeyoChen 判断二维码数量及显示的类型 end QRCodePaymentWayType 1:支付宝 2：微信

                    for (let i = 0; i < DefaultData.Data.DepositTypeList.length; i++) {
                        if ((DefaultData.Data.DepositTypeList[i].SettingCode === 8 && QRcodeNumApli == true) || (DefaultData.Data.DepositTypeList[i].SettingCode === 9 && QRcodeNumWeChat == true)) {
                            this.DepositTypeList.push(DefaultData.Data.DepositTypeList[i]);
                        }
                    }
                } else {
                    for (let i = 0; i < DefaultData.Data.DepositTypeList.length; i++) {
                        if (DefaultData.Data.DepositTypeList[i].SettingCode !== 8 && DefaultData.Data.DepositTypeList[i].SettingCode !== 9) {
                            this.DepositTypeList.push(DefaultData.Data.DepositTypeList[i]);
                        }
                    }
                }

                // 2019.03.01 SeyoChen
                this.CurrentPageData.DepositTypeList = this.DepositTypeList;
            }
        }
    },
    components: {
        memberDepositRemittanceIncome,
        //          MemberDepositRemittanceAcct,
        //          MemberDepositRemittanceForm,
        MemberDepositPayQRcode
    },
    // beforeDestroy() {
    //     EventBus.$off('remittanceDepositSuccess');
    // }
};
