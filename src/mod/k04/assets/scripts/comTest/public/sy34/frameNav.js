export const initNav34 = {
    name: 'HelloWorld',
    components: {},
    props: {
        barIndex: {
            type: String
        },
        phoneIndex: {
            type: String
        }
    },
    data () {
        return {
            loginFlag: true,
            // navBars: [{
            //     url1: this.$ResourceCDN + '/EditionImg/LamborFormal/images/'+this.$MessageLang+'/web34/nav_w_28.png',
            //     url2: this.$ResourceCDN + '/EditionImg/LamborFormal/images/'+this.$MessageLang+'/web34/nav_w_28a.png',
            //     link: 'LiveVideo'
            // }, {
            //     url1: this.$ResourceCDN + '/EditionImg/LamborFormal/images/'+this.$MessageLang+'/web34/nav_w_29.png',
            //     url2: this.$ResourceCDN + '/EditionImg/LamborFormal/images/'+this.$MessageLang+'/web34/nav_w_29a.png',
            //     link: 'Electronic'
            // }, {
            //     url1: this.$ResourceCDN + '/EditionImg/LamborFormal/images/'+this.$MessageLang+'/web34/nav_w_30.png',
            //     url2: this.$ResourceCDN + '/EditionImg/LamborFormal/images/'+this.$MessageLang+'/web34/nav_w_30a.png',
            //     link: 'Sport'
            // }, {
            //     url1: this.$ResourceCDN + '/EditionImg/LamborFormal/images/'+this.$MessageLang+'/web34/nav_w_31.png',
            //     url2: this.$ResourceCDN + '/EditionImg/LamborFormal/images/'+this.$MessageLang+'/web34/nav_w_31a.png',
            //     link: 'Lottery'
            // }, {
            //     url1: this.$ResourceCDN + '/EditionImg/LamborFormal/images/'+this.$MessageLang+'/web34/nav_s_32.png',
            //     url2: this.$ResourceCDN + '/EditionImg/LamborFormal/images/'+this.$MessageLang+'/web34/nav_s_32a.png',
            //     link: 'Poker'
            // }, {
            //     url1: this.$ResourceCDN + '/EditionImg/LamborFormal/images/'+this.$MessageLang+'/web34/nav_w_95.png',
            //     url2: this.$ResourceCDN + '/EditionImg/LamborFormal/images/'+this.$MessageLang+'/web34/nav_w_95a.png',
            //     link: 'Athletics'
            // }],
            navBars: [{
                url1: this.$ResourceCDN + '/EditionImg/LamborFormal/images/web34/lang/' + this.$MessageLang + '/nav_w_28b.png',
                url2: this.$ResourceCDN + '/EditionImg/LamborFormal/images/web34/lang/' + this.$MessageLang + '/nav_w_28c.png',
                link: 'Lottery'
            }, {
                url1: this.$ResourceCDN + '/EditionImg/LamborFormal/images/web34/lang/' + this.$MessageLang + '/nav_w_29b.png',
                url2: this.$ResourceCDN + '/EditionImg/LamborFormal/images/web34/lang/' + this.$MessageLang + '/nav_w_29c.png',
                link: 'Electronic'
            }, {
                url1: this.$ResourceCDN + '/EditionImg/LamborFormal/images/web34/lang/' + this.$MessageLang + '/nav_w_30b.png',
                url2: this.$ResourceCDN + '/EditionImg/LamborFormal/images/web34/lang/' + this.$MessageLang + '/nav_w_30c.png',
                link: 'Sport'
            }, {
                url1: this.$ResourceCDN + '/EditionImg/LamborFormal/images/web34/lang/' + this.$MessageLang + '/nav_w_31b.png',
                url2: this.$ResourceCDN + '/EditionImg/LamborFormal/images/web34/lang/' + this.$MessageLang + '/nav_w_31c.png',
                link: 'LiveVideo'
            }, {
                url1: this.$ResourceCDN + '/EditionImg/LamborFormal/images/web34/lang/' + this.$MessageLang + '/nav_w_32b.png',
                url2: this.$ResourceCDN + '/EditionImg/LamborFormal/images/web34/lang/' + this.$MessageLang + '/nav_w_32c.png',
                link: 'Poker'
            }, {
                url1: this.$ResourceCDN + '/EditionImg/LamborFormal/images/web34/lang/' + this.$MessageLang + '/nav_w_33b.png',
                url2: this.$ResourceCDN + '/EditionImg/LamborFormal/images/web34/lang/' + this.$MessageLang + '/nav_w_33c.png',
                link: 'Athletics'
            }],
            phoneFlag: true,
            favourableFlag: true,
            minFlag: true,
            newsFlag: true
            //              barIndex:0
        };
    },
    mounted () {

    },
    created: function () {
        this.load()
    },
    methods: {
        load: function () {
            this.$nextTick(function () {})
        },
        barTab (index) {
            let self = this
            self.navBars.forEach((item, key) => {
                if (index == key) {
                    self.$router.push({
                        name: item.link
                    })
                }
            })
        }
    }

};
