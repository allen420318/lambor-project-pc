import { replaceRoutes } from "@/helpers/router.js"
import restRoutes from "@/router/modules/other.js"
import mainRoutes from "@/router/modules/main.js"

const changes = {
  main: [
    {
      path: "",
      name: "Index",
      component: () => import("@/mod/k04/components/main/index/index.vue")
    },
    {
      // 真人视讯
      path: 'LiveVideo',
      name: 'LiveVideo',
      component: () => import('@/mod/k04/components/main/liveVideo/liveVideo.vue')
    },
    {
      // 电子游戏
      path: 'Electronic',
      name: 'Electronic',
      component: () => import('@/mod/k04/components/main/electronic/electronic.vue')
    },
    {
      // 电子竞技
      path: 'Athletics',
      name: 'Athletics',
      component: () => import('@/mod/k04/components/main/athletics/athletics.vue')
      
    },
    {
        // 体育博彩
      path: 'Sport',
      name: 'Sport',
      component: () => import('@/mod/k04/components/main/sport/sport.vue')
    },
    {
        // 彩票投注
        path: 'Lottery',
        name: 'Lottery',
        component: () => import('@/mod/k04/components/main/lottery/lottery.vue')
    },
    {
        // 棋牌游戏
        name: 'Poker',
        path: 'Poker',
        component: () => import('@/mod/k04/components/main/poker/poker.vue')
    },
    {
        // 优惠活动
        path: 'ActivityList',
        name: 'ActivityList',
        component: () => import('@/mod/k04/components/main/activity/activityList.vue')
    },
    {
        // 优惠活动--详情
        path: 'ActivityListDetails/:ID',
        name: 'ActivityListDetails',
        component: () => import('@/mod/k04/components/main/activity/activityListDetails.vue')
    },
    {
        // 新手教程
        path: 'Tutorial',
        name: 'Tutorial',
        component: () => import('@/mod/k04/components/main/tutorial/tutorial')
    },
    {
        // 新闻中心
        path: 'NewsList',
        name: 'NewsList',
        component: () => import('@/mod/k04/components/main/newsList/newsList')
    },
    {
        // 新闻中心--详情
        path: 'NewsListDetails/:ID',
        name: 'NewsListDetails',
        component: () => import('@/mod/k04/components/main/newsList/newsListDetails')
    },
    {
        // 手机APP
        path: 'MobileAPP',
        name: 'MobileAPP',
        component: () => import('@/mod/k04/components/main/mobileAPP/mobileAPP')
    },
    {
        // 关于我们
        path: 'AboutUs',
        name: 'AboutUs',
        component: () => import('@/mod/k04/components/main/aboutUs/aboutUs')
    },
    {
        // 厂商介绍
        path: 'Partner',
        name: 'Partner',
        component: () => import('@/mod/k04/components/main/partner/partner')
    },
    {
        // 代理加盟
        path: 'JoinUs/:Data?',
        name: 'JoinUs',
        component: () => import('@/mod/k04/components/main/agent/joinUs')
    },
    {
        // 捕鱼
        path: 'Catchfish',
        name: 'Catchfish',
        component: () => import('@/mod/k04/components/main/catchfish/catchfish')
    },
    {
        // 联系我们
        path: 'ContactUs',
        name: 'ContactUs',
        component: () => import('@/mod/k04/components/main/contactUs/contactUs')
    }
  ],
  rest: [
    {
      // 驗證電子信箱
      name: 'webEmailVerifyInformation',
      path: 'EmailVerifyInformation/:VerifyEmailCode',
      component: () => import('@/mod/k04/components/main/emailVerify/emailVerifyInformation')
    },
    {
        // 登录
        path: '/Login',
        name: 'Login',
        component: () => import('@/mod/k04/components/main/login/login')
    },
    {
        // 注册
        path: '/Register/:Data?',
        name: 'Register',
        component: () => import('@/mod/k04/components/main/register/register')
    },
    {
        // pt-h5
        name: 'webHtml5Lobby',
        path: 'Html5Lobby',
        component: () => import('@/mod/k04/components/main/h5/html5Lobby')
    },
    {
        // 注册成功
        path: '/RegisterSuccess',
        name: 'RegisterSuccess',
        component: () => import('@/mod/k04/components/main/register/registerSuccess')
    },
    {
        // 所有遊戲畫面
        name: 'webPlayGame',
        path: 'PlayGame',
        component: () => import('@/mod/k04/components/main/linkToGame/playGame')
    },
    {
        // 所有遊戲畫面
        name: 'webPlayMyGame',
        path: 'PlayMyGame',
        component: () => import('@/mod/k04/components/main/linkToGame/playMyGame')
    },
    {
        // 所有遊戲畫面--PNG
        name: 'webPlayPngGame',
        path: 'webPlayPngGame',
        component: () => import('@/mod/k04/components/main/linkToGame/playPngGame')
    },
    {
        // 在線支付頁面
        name: 'webDepositPayment',
        path: 'DepositPayment/:Data',
        component: () => import('@/mod/k04/components/main/memberNew/depositPayment')
    }
  ]
}

const adds = {
  main: [],
  rest: []
}

const finalMainRoutes = [...replaceRoutes(changes.main, mainRoutes), ...adds.main]
const finalRestRoutes = [...replaceRoutes(changes.rest, restRoutes), ...adds.rest]

const result = [
  {
    path: "/NotFoundComponent",
    name: "NotFoundComponent",
    component: () => import("@/components/base/notFound")
  },
  {
    // 最外层框架（包含main，member）
    path: "/",
    component: () => import("@/mod/k04/components/base/base.vue"),
    children: [
      ...finalRestRoutes,
      {
        path: "/",
        component: () => import("@/mod/k04/components/base/baseWebLayout"),
        children: [
          {
            path: "",
            component: () => import("@/mod/k04/components/base/baseMainLayout"),
            children: finalMainRoutes
          }
        ]
      }
    ]
  }
]

export default result
