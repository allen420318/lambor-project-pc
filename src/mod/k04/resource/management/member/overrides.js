const list = [
  "webMemberSummary",
  "webMemberBankData",
  "webMemberDeposit",
  "webMemberFundManage",
  "webMemberModifyPassword",
  "webMemberPrivateMessage",
  "webMemberProfile",
  "webMemberTransactionRecord",
  "webMemberWithdrawal",
  "webBetRecord",
]

export default list
